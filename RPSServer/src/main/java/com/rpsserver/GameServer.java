/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2019-01-24 15:53:02 -0500 (Thu, 24 Jan 2019) $
 * $Revision: 377 $
 * $Author: mbartgis $
 */

package com.rpsserver;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeoutException;

import com.rpscore.C;
import com.rpscore.cfg.ConfigManager;
import com.rpscore.entities.Entity;
import com.rpscore.lib.GUIDGenerator;
import com.rpscore.lib.MathUtils;
import com.rpscore.lib.Pair;
import com.rpscore.server.Communicator;
import com.rpscore.server.MessageManager;
import com.rpscore.server.MessageManager.PROTOCOL;

public class GameServer {

	private ConfigManager				config;

	// Main thread used to accept clients. This spawns a self-contained child
	// thread which handles all of the initial handshakes.
	private ServerThread				st;

	// Thread that routes incoming messages to the game instance, and outbound
	// messages to the group or privileged party.
	private UpdaterThread				ut;

	// Thread that is responsible to triggering the game-state to change and is
	// also responsible for triggering server-side heartbeats to all connected.
	// clients.
	private HeartBeatThread				hbt;

	// Thread that is responsible for listening to the input console and
	// executing server commands.
	private ConsoleThread				ct;

	public static final GUIDGenerator	GUID_MANAGER	= new GUIDGenerator( );

	public static final int				FIVE_SECONDS	= 5000;

	public static final int				THIRTY_SECONDS	= 30000;

	/*
	 * Constant used to define the delivery address for privileged messages.
	 */
	public static final int				SERVER_ADDRESS	= 0x7F000001;

	/*
	 * Constant used to define a default address for unknown clients (i.e.
	 * Clients that have not been assigned an address by the server).
	 */
	public static final int				NULL_ADDRESS	= 0x00000000;

	private int							tps;

	private long						timeout;

	public static int					MAP_GEN;

	public static double				difficulty;

	public GameServer( ) {

		this.config = ConfigManager.getConfigManager( );

		System.out.println( "Reading server config" );

		this.tps = this.config.getSetting( "ServerTPS", 60 );
		this.timeout = this.config.getSetting( "PlayerTimeout", THIRTY_SECONDS );

		if ( this.config.getSetting( "DefineSeed", false ) ) {
			MAP_GEN = this.config.getSetting( "ServerSeed", 0 );
		} else {
			MAP_GEN = MathUtils.getRandom( 0, Integer.MAX_VALUE );
		}

		difficulty = this.config.getSetting( "ServerDifficulty", 1.0 );

		System.out.println( "Setting Server Difficulty to: " + difficulty );

		GUID_MANAGER.reserve( NULL_ADDRESS );
		GUID_MANAGER.reserve( SERVER_ADDRESS );

		System.out.println( "Generating new map with id: " + MAP_GEN );

	}

	public void start( ) {
		this.st = new ServerThread( );
		this.st.setName( "Incoming Connection Thread" );

		this.ut = new UpdaterThread( );
		this.ut.setName( "I/O Transmitter Thread" );

		this.hbt = new HeartBeatThread( );
		this.hbt.setName( "Server Update Thread" );

		this.ct = new ConsoleThread( );
		this.ct.setName( "Console Thread" );

		System.out.println( "Server Setup Complete." );
	}

	public void shutDown( ) {

		System.out.println( "Shutting down the server..." );

		this.st.closeIsNotRequested = false;

		if ( this.st.server == null ) {
			return;
		}

		synchronized ( this.st.server ) {
			try {
				this.st.server.close( );
			} catch ( IOException e ) {}
		}

	}

	private class ServerThread extends Thread {

		@SuppressWarnings( "unused" )
		private long						timeout;

		private ArrayList< Communicator >	clients;

		private Semaphore					clientMutex;

		private boolean						closeIsNotRequested;

		private ServerSocket				server;

		public ServerThread( ) {

			this.clientMutex = new Semaphore( 1 );

			this.clients = new ArrayList< >( );

			this.closeIsNotRequested = true;

			this.server = null;

			this.start( );

		}

		private void performSetupOnClient( final Socket incoming ) {

			Runnable setupTask = ( ) -> {
				InetAddress cl = incoming.getInetAddress( );

				Communicator c_com = null;

				try {
					c_com = new Communicator( incoming );
					if ( c_com.isClosed( ) ) {
						c_com.forceDisconnect( );
						return;
					}
				} catch ( IOException e1 ) {
					return;
				}

				c_com.assign( GUID_MANAGER.generate( ) );

				c_com.sendMessage( new byte[ ] { MessageManager.PROTOCOL.INFO } );

				byte[ ] clientJoinMessage;
				try {
					clientJoinMessage = c_com.getNext( 5000 );
					if ( MessageManager.isPingMessage( clientJoinMessage ) ) {
						System.out.println( "User " + cl.getHostAddress( ) + " ping message." );
						c_com.sendMessage( MessageManager.createDataCarryingPingMessage( C.BUILD_NUMBER ) );

						// FIXME Make this solution less of a hack and more
						// viable.
						try {
							Thread.sleep( 5000 );
						} catch ( InterruptedException e ) {
							Thread.currentThread( ).interrupt( );
						}
						c_com.forceDisconnect( );
						return;
					}

					if ( !MessageManager.isJoinMessage( clientJoinMessage ) ) {

						// TODO get this working so that a "403 FORBIDDEN"
						// message is shown to browsers
						System.out.println( "Web browser access." );

						try {
							c_com.getClient( ).getOutputStream( ).write( new byte[ ] { 72, 84, 84, 80, 47, 49, 46, 41,
									52, 48, 51, 32, 70, 79, 82, 66, 73, 68, 68, 69, 78 } );
						} catch ( IOException e ) {}

						// If there is a different protocol, kill this
						// connection to protect resources on the server.
						c_com.forceDisconnect( );
						return;
					}

				} catch ( TimeoutException e1 ) {
					c_com.forceDisconnect( );
					return;
				}

				int asgt = c_com.getAssignment( );
				c_com.sendMessage( MessageManager.createAssignment( asgt, asgt ) );

				try {
					c_com.getNext( 5000 );
				} catch ( TimeoutException e1 ) {
					c_com.forceDisconnect( );
					return;
				}

				c_com.sendMessage( MessageManager.createMessage( PROTOCOL.MAP, asgt,
						MessageManager.convertIntArrayToByteArray( MAP_GEN ) ) );

				try {
					c_com.getNext( 5000 );
				} catch ( TimeoutException e1 ) {
					c_com.forceDisconnect( );
					return;
				}

				c_com.sendMessage( MessageManager.createDifficultyTransmission( GameServer.difficulty ) );

				MessageManager.changeAddress( asgt, clientJoinMessage );

				System.out.println( "User " + cl.getHostAddress( ) + " connected successfully." );

				hbt.gameInstance.addClientMessage( clientJoinMessage );

				try {
					this.clientMutex.acquire( );
				} catch ( InterruptedException e ) {
					Thread.currentThread( ).interrupt( );
				}

				this.clients.add( c_com );

				this.clientMutex.release( );

				synchronized ( ut ) {
					ut.notify( );
				}
				synchronized ( hbt ) {
					hbt.notifyAll( );
				}
			};

			Thread asyncSetupTask = new Thread( setupTask );
			asyncSetupTask.setName( "Setup Connection Task" );
			asyncSetupTask.setDaemon( true );
			new Thread( setupTask ).start( );
		}

		@Override
		public void run( ) {
			super.run( );

			try {
				this.server = new ServerSocket( 22000 );

				InetAddress addr = InetAddress.getLocalHost( );
				@SuppressWarnings( "unused" )
				String hostname = addr.getHostName( );

				System.out.println( "Actively listening for clients..." );

				while ( this.closeIsNotRequested ) {

					Socket client = server.accept( );

					if ( !this.closeIsNotRequested ) {
						break;
					}

					performSetupOnClient( client );

				}

			} catch ( IOException e ) {} finally {
				if ( server != null ) {
					try {
						server.close( );
					} catch ( IOException e ) {}
				}

				try {
					this.clientMutex.acquire( );
				} catch ( InterruptedException e ) {
					Thread.currentThread( ).interrupt( );
				}

				for ( Communicator client : clients ) {
					if ( client != null ) {
						boolean disconnected = client.disconnect( );
						InetAddress cl = client.getClient( ).getInetAddress( );
						if ( disconnected ) {
							System.out.println( "User " + cl.getHostAddress( ) + " disconnected successfully." );
						}
					}
				}

				if ( ut != null ) {
					ut.terminate( );
				}

				if ( hbt != null ) {
					hbt.terminate( );
				}

				if ( ct != null ) {
					ct.terminate( );
				}

			}

		}

	}

	// Sends out a heartbeat to each client every so often.
	private class HeartBeatThread extends Thread {
		private volatile boolean	closeNotRequested;

		private int					iterations;

		private GameServerInstance	gameInstance;

		public HeartBeatThread( ) {
			this.closeNotRequested = true;

			this.iterations = 0;

			this.gameInstance = new GameServerInstance( );

			start( );

		}

		private void terminate( ) {
			this.closeNotRequested = false;
			synchronized ( this ) {
				this.notifyAll( );
			}
		}

		@Override
		public void run( ) {
			super.run( );

			// FIXME - Why is this band-aid here?
			while ( st.clients == null ) {}

			while ( this.closeNotRequested ) {

				while ( st.clients.isEmpty( ) ) {
					if ( this.gameInstance != null ) {
						this.gameInstance.reset( );
					}
					synchronized ( this ) {
						try {
							this.wait( );
						} catch ( InterruptedException e ) {
							Thread.currentThread( ).interrupt( );
						}
					}
					if ( !this.closeNotRequested ) {
						break;
					}
				}

				if ( !this.closeNotRequested ) {
					break;
				}

				this.gameInstance.update( );

				// Every 5 seconds, trigger a heartbeat.
				if ( this.iterations >= 300 ) {
					this.iterations = 0;

					try {
						st.clientMutex.acquire( );
					} catch ( InterruptedException e ) {
						Thread.currentThread( ).interrupt( );
					}

					Iterator< Communicator > c_iter = st.clients.iterator( );

					while ( c_iter.hasNext( ) ) {
						c_iter.next( ).sendHeartbeat( );
					}
					st.clientMutex.release( );
				} else {
					this.iterations++;
				}

				try {
					Thread.sleep( 16 );
				} catch ( InterruptedException e ) {
					Thread.currentThread( ).interrupt( );
				}

			}

		}

	}

	private class UpdaterThread extends Thread {

		private volatile boolean closeNotRequested;

		public UpdaterThread( ) {
			this.closeNotRequested = true;

			start( );

		}

		private void terminate( ) {
			this.closeNotRequested = false;
			synchronized ( this ) {
				this.notify( );
			}
		}

		private void performServerActions( byte[ ] b ) {

			if ( b == null || b.length < 1 ) {
				return;
			}

			switch ( b[ 0 ] ) {
				case PROTOCOL.QUIT: {
					int adr = MessageManager.getAddressFromMessage( b );

					Iterator< Communicator > client_itr = st.clients.iterator( );
					while ( client_itr.hasNext( ) ) {
						Communicator client = client_itr.next( );
						if ( client.getAssignment( ) == adr ) {
							boolean disconnected = client.disconnect( );
							InetAddress cl = client.getClient( ).getInetAddress( );
							if ( disconnected ) {
								System.out.println( "User " + cl.getHostAddress( ) + " disconnected successfully." );
								client_itr.remove( );
							}
						}
					}
					break;
				}
			}

		}

		@Override
		public void run( ) {
			super.run( );

			// FIXME - Why is this band-aid here?
			while ( st.clients == null ) {}

			while ( this.closeNotRequested ) {

				while ( st.clients.isEmpty( ) ) {
					synchronized ( this ) {
						try {
							this.wait( );
						} catch ( InterruptedException e ) {
							Thread.currentThread( ).interrupt( );
						}

						if ( !this.closeNotRequested ) {
							break;
						}

					}

				}

				if ( !this.closeNotRequested ) {
					break;
				}

				synchronized ( st.clients ) {

					try {
						st.clientMutex.acquire( );
					} catch ( InterruptedException e ) {
						Thread.currentThread( ).interrupt( );
					}

					Iterator< Communicator > c_iter = st.clients.iterator( );
					while ( c_iter.hasNext( ) ) {
						Communicator initClient = null;

						initClient = c_iter.next( );

						byte[ ][ ] updatesFromClient = initClient.getMessages( );

						for ( int i = 0; i < updatesFromClient.length; i++ ) {

							// If any messages are null stamped from the client,
							// correct them.
							try {
								if ( !MessageManager.validateAddress( updatesFromClient[ i ] ) ) {
									MessageManager.changeAddress( initClient.getAssignment( ), updatesFromClient[ i ] );
								}
								hbt.gameInstance.addClientMessage( updatesFromClient[ i ] );
							} catch ( ArrayIndexOutOfBoundsException e ) {

							}

						}

						// Only enforce timeout policies when a timeout is
						// given.
						if ( timeout != -1 ) {
							if ( initClient.getHeartbeatTime( ) > timeout ) {
								hbt.gameInstance.addClientMessage(
										MessageManager.createQuitMessage( initClient.getAssignment( ) ) );
							}
						}
					}

					Iterator< byte[ ] > glob_messages = hbt.gameInstance.getGlobalOutboundMessages( ).iterator( );

					while ( glob_messages.hasNext( ) ) {

						byte[ ] msg = glob_messages.next( );

						Iterator< Communicator > client_sender_iter = st.clients.iterator( );
						while ( client_sender_iter.hasNext( ) ) {
							Communicator msgReceiver = client_sender_iter.next( );
							if ( MessageManager.getAddressFromMessage( msg ) != msgReceiver.getAssignment( ) ) {
								msgReceiver.sendMessage( msg );
							}
						}
					}

					Iterator< Pair< Integer, byte[ ] > > priv_messages = hbt.gameInstance.getPrivilegedMessages( )
							.iterator( );

					while ( priv_messages.hasNext( ) ) {
						Pair< Integer, byte[ ] > msg = priv_messages.next( );

						if ( msg.key == GameServer.SERVER_ADDRESS ) {
							this.performServerActions( msg.value );
						} else {
							Iterator< Communicator > client_sender_iter = st.clients.iterator( );
							while ( client_sender_iter.hasNext( ) ) {
								Communicator msgReceiver = client_sender_iter.next( );
								if ( msg.key == msgReceiver.getAssignment( ) ) {
									msgReceiver.sendMessage( msg.value );
								}

							}
						}
					}

					st.clientMutex.release( );
				}

				if ( tps != -1 && tps >= 0 ) {
					try {
						Thread.sleep( tps );
					} catch ( InterruptedException e ) {
						Thread.currentThread( ).interrupt( );
					}
				}

			}

		}

	}

	public class ConsoleThread extends Thread {

		private boolean	closeNotRequested;

		private long	adminPassword;

		private boolean	canIssueAdminCommand;

		public class commands {
			public static final String	SHUTDOWN		= "shutdown";
			public static final String	LOGIN			= "login";
			public static final String	LOGOUT			= "logout";
			public static final String	CREDENTIALS		= "credentials";
			public static final String	SAY				= "say";
			public static final String	HELP			= "help";
			public static final String	GET_ENTITIES	= "gent";
		}

		public ConsoleThread( ) {
			this.setDaemon( true );
			this.closeNotRequested = true;
			this.adminPassword = config.getSetting( "AdminPasswordHash", 0L );
			this.canIssueAdminCommand = false;
			this.start( );
		}

		@Override
		public void run( ) {
			super.run( );

			try ( Scanner cmdInputStream = new Scanner( System.in ); ) {

				while ( closeNotRequested ) {
	
					String command = cmdInputStream.nextLine( );
	
					if ( !closeNotRequested ) {
						return;
					}
	
					performCommand( command.split( " " ) );
	
				}
			} catch ( Exception e ) { }

		}

		public void performCommand( String[ ] command ) {

			for ( int i = 0; i < command.length; i++ ) {
				switch ( command[ i ] ) {
					case ( commands.SHUTDOWN ): {
						if ( !canIssueAdminCommand ) {
							System.out.println( "Failed to execute shutdown, admin privileges required." );
							return;
						}
						shutDown( );
						return;
					}
					case ( commands.LOGIN ): {
						try {
							String login = command[ i + 1 ];
							String password = command[ i + 2 ];

							if ( login.equals( config.getSetting( "AdminAccount", "" ) )
									&& ( MathUtils.hash64( password ) == this.adminPassword ) ) {

								this.canIssueAdminCommand = true;
								System.out.println( "Logged in successfully." );

							} else {
								System.out.println( "Username or password is incorrect." );
							}

						} catch ( ArrayIndexOutOfBoundsException e ) {
							System.out.println( "Correct usage for command \"login\": login <account> <password>" );
						}
						return;
					}
					case ( commands.LOGOUT ): {

						if ( this.canIssueAdminCommand ) {
							System.out.println( "Logged out successfully." );
							this.canIssueAdminCommand = false;
						} else {
							System.out.println( "Not currently logged in." );
						}

						return;
					}
					case ( commands.SAY ): {
						if ( hbt != null && hbt.gameInstance != null ) {
							try {
								byte[ ] message = MessageManager.createTextMessage( command[ i + 1 ], SERVER_ADDRESS );
								hbt.gameInstance.addClientMessage( message );
							} catch ( ArrayIndexOutOfBoundsException e ) {
								System.out.println( "Missing parameter on command \"say\"" );
								return;
							}

						}
						return;
					}
					case ( commands.CREDENTIALS ): {
						if ( !canIssueAdminCommand ) {
							System.out.println( "Failed to execute credentials change, admin privileges required." );
							return;
						}

						try {
							String login = command[ i + 1 ];
							String password = command[ i + 2 ];

							config.modifySetting( "AdminAccount", login );

							this.adminPassword = MathUtils.hash64( password );

							config.modifySetting( "AdminPasswordHash", ( ( Long ) this.adminPassword ).toString( ) );

							config.flushSettings( );

							System.out.println( "Credentials Saved Successfully" );

						} catch ( ArrayIndexOutOfBoundsException e ) {
							System.out.println( "Failed to parse command \"credentials\"" );
						}

						return;
					}
					case ( commands.GET_ENTITIES ): {

						try {
							// TODO implement entity filter.
							// String filter = command[ i + 1 ];
						} catch ( ArrayIndexOutOfBoundsException e ) {

							if ( hbt != null && hbt.gameInstance != null ) {
								List< ? extends Entity > ents = hbt.gameInstance.getCurrentEnemies( );

								System.out.println( ents.size( ) + " Enemies found." );

								Iterator< ? extends Entity > entItr = ents.iterator( );

								while ( entItr.hasNext( ) ) {
									System.out.println( "\t" + entItr.next( ) );
								}

							}

						}

						break;
					}
					default: {
						System.out.println("Unknown command: \"" + command[i] + "\".");
						return;
					}

				}
			}

		}

		public void terminate( ) {
			this.closeNotRequested = false;
			synchronized ( this ) {
				this.notifyAll( );
			}
		}

	}

}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2019-01-24 15:53:02 -0500 (Thu, 24 Jan 2019) $
 * $Revision: 377 $
 * $Author: mbartgis $
 */

package com.rpsserver;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

import com.rpscore.cfg.ConfigManager;
import com.rpscore.entities.Entity;
import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.entities.enemies.SpawnManager;
import com.rpscore.entities.spawned.SpawnedEntity;
import com.rpscore.entities.spawned.pickups.Pickup;
import com.rpscore.entities.spawned.weapons.Bomb;
import com.rpscore.entities.spawned.weapons.Bullet;
import com.rpscore.entities.spawned.weapons.FusionBomb;
import com.rpscore.lib.Pair;
import com.rpscore.lib.Updateable;
import com.rpscore.server.MessageManager;
import com.rpscore.terrain.Map;
import com.rpscore.terrain.TerrainGenerator;

public class GameServerInstance implements Updateable, EntityContainer {

    protected Map                                  map;

    private LinkedList< byte[ ] >                  inboundMessages;
    private Semaphore                              inboundMessagesMutex;

    private LinkedList< byte[ ] >                  outboundMessages;
    private Semaphore                              outboundMessagesMutex;

    private LinkedList< Pair< Integer, byte[ ] > > privilegedMessages;
    private Semaphore                              privilegedMessagesMutex;

    protected LinkedList< Bullet >                 bullets;
    protected LinkedList< Bomb >                   bombs;
    protected LinkedList< Pickup >                 pickups;

    protected SpawnManager                         spawner;

    public static int                              ENEMY_UPDATE_FREQUENCY = 60;
    protected int                                  enemyUpdateTimer;

    public GameServerInstance( ) {
        super( );

        setMap( TerrainGenerator.generateMap( 257, GameServer.MAP_GEN ) );

        this.inboundMessages = new LinkedList< >( );
        this.outboundMessages = new LinkedList< >( );

        this.outboundMessagesMutex = new Semaphore( 1 );
        this.inboundMessagesMutex = new Semaphore( 1 );

        this.privilegedMessages = new LinkedList< >( );
        this.privilegedMessagesMutex = new Semaphore( 1 );

        this.bullets = new LinkedList< >( );
        this.bombs = new LinkedList< >( );
        this.pickups = new LinkedList< >( );

        this.spawner = new SpawnManager( this, GameServer.GUID_MANAGER );
        this.spawner.setTelegramEnemies( true );
        this.spawner.setDifficulty( GameServer.difficulty );

        this.enemyUpdateTimer = 0;

        ENEMY_UPDATE_FREQUENCY = ConfigManager.getConfigManager( ).getSetting( "EnemyUpdateInterval", 60 );

        System.out.println( "Updating enemies every " + ENEMY_UPDATE_FREQUENCY + " ticks." );

        Entity.setEntityContainer( this );

    }

    public void addPlayer( Player p ) {
        // Test to see if a connection is already present.
        Iterator< Player > piter = this.spawner.getPlayers( ).iterator( );
        while ( piter.hasNext( ) ) {
            Player e = piter.next( );
            if ( e.equals( p ) ) { return; }
        }

        this.spawner.addPlayer( p );
    }

    public void removePlayer( Player p ) {
        this.spawner.removePlayer( p );
    }

    public void reset( ) {
        this.spawner.reset( );
    }

    @Override
    public boolean isFinished( ) {
        return false;
    }

    @Override
    public void update( ) {

        this.spawner.update( );

        // Update all spawned entities.
        Iterator< Bullet > bul_iter = bullets.iterator( );
        while ( bul_iter.hasNext( ) ) {
            Bullet b = bul_iter.next( );

            b.update( );
            if ( b.isFinished( ) ) {
                bul_iter.remove( );
            }
        }

        Iterator< Bomb > bomb_iter = bombs.iterator( );
        while ( bomb_iter.hasNext( ) ) {
            Bomb b = bomb_iter.next( );

            if ( map != null ) {

                // TODO get bombs effects on block working on the server side.
                /*
                 * @formatter:off // Diffuse submerged bombs if ( b.hasLanded( )
                 * && map.getBlockAtEntityCoords( b ).equals(
                 * BlockRegistry.BLOCK_TYPE.BLOCK_WATER ) ) { b.diffuse( ); }
                 * 
                 * // Detonate bombs in lava if ( b.hasLanded( ) &&
                 * map.getBlockAtEntityCoords( b ).equals(
                 * BlockRegistry.BLOCK_TYPE.BLOCK_LAVA ) ) { b.detonate( ); }
                 * 
                 * @formatter:on
                 */
            }

            b.update( );
            if ( b.isFinished( ) ) {
                bomb_iter.remove( );
            }
        }

        Iterator< Pickup > pick_iter = pickups.iterator( );
        while ( pick_iter.hasNext( ) ) {
            Pickup p = pick_iter.next( );

            p.update( );
            if ( p.isFinished( ) ) {
                pick_iter.remove( );
            }
        }

        byte[ ][ ] newEnemies = this.spawner.getEnemyTransmissions( );

        for ( byte[ ] enemy : newEnemies ) {
            this.createOutboundMessage( enemy );
        }

        // HANDLE MESSAGES
        while ( !this.inboundMessages.isEmpty( ) ) {

            try {
                this.inboundMessagesMutex.acquire( );
            } catch ( InterruptedException e ) {
                Thread.currentThread( ).interrupt( );
            }

            byte[ ] message = inboundMessages.pop( );

            this.inboundMessagesMutex.release( );

            // Leave this outside of the thread lock to avoid holding up new
            // incoming messages.
            this.decodeMessage( message );

        }

        if ( enemyUpdateTimer >= ENEMY_UPDATE_FREQUENCY ) {

            if ( this.spawner != null ) {
                Iterator< Enemy > enemies = this.spawner.getEnemies( ).iterator( );

                while ( enemies.hasNext( ) ) {
                    Enemy enemy = enemies.next( );
                    byte[ ] message = MessageManager.createEnemyUpdateMessage( enemy );
                    createOutboundMessage( message );
                }

            }

            enemyUpdateTimer = 0;
        } else {
            enemyUpdateTimer++;
        }

    }

    private void createOutboundMessage( byte[ ] b ) {
        try {
            this.outboundMessagesMutex.acquire( );
        } catch ( InterruptedException e ) {
            Thread.currentThread( ).interrupt( );
        }

        this.outboundMessages.add( b );

        this.outboundMessagesMutex.release( );
    }

    private void createPrivilegedMessage( int address, byte[ ] message ) {
        try {
            this.privilegedMessagesMutex.acquire( );
        } catch ( InterruptedException e ) {
            Thread.currentThread( ).interrupt( );
        }

        Pair< Integer, byte[ ] > p = new Pair< >( address, message );
        this.privilegedMessages.add( p );

        this.privilegedMessagesMutex.release( );
    }

    public void setMap( Map map ) {
        this.map = map;
    }

    public void addClientMessage( byte[ ] b ) {
        try {
            this.inboundMessagesMutex.acquire( );
        } catch ( InterruptedException e ) {
        }

        this.inboundMessages.push( b );

        this.inboundMessagesMutex.release( );
    }

    public List< byte[ ] > getGlobalOutboundMessages( ) {

        // Return a deep copy of the item to ensure thread safety.
        LinkedList< byte[ ] > nMessages = new LinkedList< >( );

        try {
            this.outboundMessagesMutex.acquire( );
        } catch ( InterruptedException e ) {
            Thread.currentThread( ).interrupt( );
        }

        while ( !this.outboundMessages.isEmpty( ) ) {
            nMessages.add( this.outboundMessages.removeFirst( ) );
        }
        this.outboundMessagesMutex.release( );

        return nMessages;
    }

    public void decodeMessage( byte[ ] b ) throws IllegalArgumentException, ArrayIndexOutOfBoundsException {
        byte protocol = b[ 0 ];

        switch ( protocol ) {

            case ( MessageManager.PROTOCOL.MOVE ): {
                int addr = MessageManager.getAddressFromMessage( b );
                Iterator< Player > p_iter = this.spawner.getPlayers( ).iterator( );

                while ( p_iter.hasNext( ) ) {
                    Player pl = p_iter.next( );
                    if ( pl.getID( ) == addr ) {
                        Point p = MessageManager.getCoordinatesFromByteArray( b );
                        pl.setX( p.x );
                        pl.setY( p.y );
                        break;
                    }
                }
                this.createOutboundMessage( b );
                break;
            }
            case ( MessageManager.PROTOCOL.FOCUS ): {
                int addr = MessageManager.getAddressFromMessage( b );
                Iterator< Player > p_iter = this.spawner.getPlayers( ).iterator( );

                while ( p_iter.hasNext( ) ) {
                    Player pl = p_iter.next( );
                    if ( pl.getID( ) == addr ) {
                        Point p = MessageManager.getCoordinatesFromByteArray( b );
                        pl.setMousePosition( p.x, p.y );
                        break;
                    }
                }
                this.createOutboundMessage( b );
                break;
            }
            case ( MessageManager.PROTOCOL.JOIN ): {

                int asgt = MessageManager.getAddressFromMessage( b );

                Player p = new Player( b );

                this.addPlayer( p );

                byte[ ] newPlayerJoin = MessageManager.createTextMessage( "[SERVER]: " + p.getName( ) + " has joined.",
                        asgt );

                // Give the player an assignment
                // FIXME The redundancy is real...

                // Give the player all of the other players' information.
                Iterator< Player > p_iter = this.spawner.getPlayers( ).iterator( );

                while ( p_iter.hasNext( ) ) {
                    Player pl = p_iter.next( );
                    if ( pl.getID( ) != asgt ) {
                        byte[ ] jm = MessageManager.createJoinMessage( pl.getName( ) );
                        MessageManager.changeAddress( pl.getID( ), jm );
                        this.createPrivilegedMessage( asgt, jm );
                    }
                }

                // Give the player all of the enemy information.
                Iterator< Enemy > e_iter = this.spawner.getEnemies( ).iterator( );

                while ( e_iter.hasNext( ) ) {
                    Enemy e = e_iter.next( );
                    this.createPrivilegedMessage( asgt, MessageManager.encodeEnemy( e ) );

                    byte[ ] waveMsg = MessageManager.createMessage( MessageManager.PROTOCOL.WAVE_START, asgt,
                            MessageManager.convertIntArrayToByteArray( this.spawner.getWave( ) ) );
                    this.createPrivilegedMessage( asgt, waveMsg );

                }

                // Create a join message which renders a player.
                this.createOutboundMessage( b );

                // Create a chat message indicating someone joined.
                this.createOutboundMessage( newPlayerJoin );

                break;
            }
            case ( MessageManager.PROTOCOL.QUIT ): {

                int asgt = MessageManager.getAddressFromMessage( b );

                Iterator< Player > p_iter = this.spawner.getPlayers( ).iterator( );

                while ( p_iter.hasNext( ) ) {
                    Player pl = p_iter.next( );
                    if ( pl.getID( ) == asgt ) {

                        this.createPrivilegedMessage( pl.getID( ), MessageManager.createAckMessage( ) );

                        this.removePlayer( pl );

                        byte[ ] playerQuit = MessageManager
                                .createTextMessage( "[SERVER]: " + pl.getName( ) + " has Left.", asgt );
                        this.createOutboundMessage( playerQuit );
                        break;
                    }
                }

                // Instruct the server to disconnect the player...
                this.createPrivilegedMessage( GameServer.SERVER_ADDRESS, b );

                this.createOutboundMessage( b );
                break;
            }

            case ( MessageManager.PROTOCOL.MSG ): {

                MessageManager.changeAddress( GameServer.SERVER_ADDRESS, b );
                this.createOutboundMessage( b );
                break;
            }
            case ( MessageManager.PROTOCOL.ACTION ): {

                int addr = MessageManager.getAddressFromMessage( b );
                Iterator< Player > p_iter = this.spawner.getPlayers( ).iterator( );

                while ( p_iter.hasNext( ) ) {
                    Player pl = p_iter.next( );
                    if ( pl.getID( ) == addr ) {
                        SpawnedEntity e = MessageManager.getPlayerAction( b );

                        // Fusion Bomb Detonation
                        if ( e == null ) {
                            for ( Bomb bomb : this.bombs ) {
                                if ( bomb instanceof FusionBomb && bomb.getSource( ).equals( pl ) ) {
                                    bomb.detonate( );
                                }
                            }
                        } else {

                            e.setSource( pl );

                            if ( e instanceof Bullet ) {
                                this.bullets.add( ( Bullet ) e );
                                createOutboundMessage( b );
                                createPrivilegedMessage( addr, b );
                            } else if ( e instanceof FusionBomb ) {
                                // Ensure that a player can only add 1 fusion
                                // bomb at a time.
                                boolean hasFusionBombAlready = false;
                                for ( Bomb bomb : this.bombs ) {
                                    if ( bomb instanceof FusionBomb && bomb.getSource( ).equals( pl ) ) {
                                        hasFusionBombAlready = true;
                                    }
                                }
                                if ( !hasFusionBombAlready ) {
                                    this.bombs.add( ( FusionBomb ) e );
                                    createOutboundMessage( b );
                                    createPrivilegedMessage( addr, b );
                                }
                            } else if ( e instanceof Bomb ) {
                                this.bombs.add( ( Bomb ) e );
                                createOutboundMessage( b );
                                createPrivilegedMessage( addr, b );
                            } else if ( e instanceof Pickup ) {
                                this.pickups.add( ( Pickup ) e );
                                createOutboundMessage( b );
                                createPrivilegedMessage( addr, b );
                            }
                        }
                        break;

                    }
                }


                break;
            }
            case ( MessageManager.PROTOCOL.STATEDEF ): {
                int asgt = MessageManager.getAddressFromMessage( b );

                Iterator< Player > p_iter = this.spawner.getPlayers( ).iterator( );

                while ( p_iter.hasNext( ) ) {
                    Player pl = p_iter.next( );
                    if ( pl.getID( ) == asgt ) {

                        this.createOutboundMessage( b );
                        break;
                    }
                }
                break;
            }
            case ( MessageManager.PROTOCOL.REQUEST_ENT ): {

                int address = MessageManager.getAddressFromMessage( b );
                int asgt = MessageManager.getIntFromByteArray( b, 5 );

                Iterator< Enemy > enemies = this.spawner.getEnemies( ).iterator( );
                while ( enemies.hasNext( ) ) {
                    Enemy e = enemies.next( );

                    if ( e.getID( ) == asgt ) {
                        byte[ ] obm = MessageManager.encodeEnemy( e );
                        createPrivilegedMessage( address, obm );
                        break;
                    }
                }
                break;
            }
            default: {
                createOutboundMessage( b );
                break;
            }

        }

        // If the message is valid, forward it to the other clients.

    }

    public List< Pair< Integer, byte[ ] > > getPrivilegedMessages( ) {
        List< Pair< Integer, byte[ ] > > temp = new LinkedList< >( );
        try {
            this.privilegedMessagesMutex.acquire( );
        } catch ( InterruptedException e ) {
            Thread.currentThread( ).interrupt( );
        }
        while ( !this.privilegedMessages.isEmpty( ) ) {
            temp.add( this.privilegedMessages.removeFirst( ) );
        }
        this.privilegedMessagesMutex.release( );

        return temp;
    }

    @Override
    public void addSpawnedEntity( SpawnedEntity e ) {

        if ( e instanceof Bomb ) {
            this.bombs.add( ( Bomb ) e );
        } else if ( e instanceof Bullet ) {
            this.bullets.add( ( Bullet ) e );
        } else if ( e instanceof Pickup ) {
            this.pickups.add( ( Pickup ) e );
        } else {
            throw new IllegalArgumentException( "Unsupported SpawnedEntity: " + e.getClass( ) );
        }

    }

    @Override
    public Player getPlayer( ) {
        return null;
    }

    @Override
    public List< Enemy > getCurrentEnemies( ) {
        return this.spawner.getEnemies( );
    }

    @Override
    public List< Player > getCurrentOtherPlayers( ) {
        if ( this.spawner != null ) {
            return this.spawner.getPlayers( );
        } else {
            return new ArrayList< >( 0 );
        }
    }

    @Override
    public List< Bomb > getCurrentBombs( ) {
        return this.bombs;
    }

    @Override
    public List< Bullet > getCurrentBullets( ) {
        return this.bullets;
    }

    @Override
    public List< Pickup > getCurrentPickups( ) {
        return this.pickups;
    }

    @Override
    public void addEventToKillfeed( Entity killer, Entity victim, Entity method ) {
        System.out.println( killer.getName( ) + " -> " + victim.getName( ) + " [" + method.getName( ) + "]" );
    }

    @Override
    public void addOtherPlayer( Player p ) {
        if ( this.spawner != null ) {
            this.spawner.addPlayer( p );
        }
    }

    @Override
    public void setPlayer( Player p ) {
        // Method intentionally left blank, no there is no "primary" player on
        // the server side.
    }

    @Override
    public void addEnemy( Enemy e ) {

        if ( this.spawner != null ) {
            this.spawner.getEnemies( ).add( e );
        }

    }

    @Override
    public void onWaveStart( ) {
        byte[ ] waveMsg = MessageManager.createMessage( MessageManager.PROTOCOL.WAVE_START, GameServer.NULL_ADDRESS,
                MessageManager.convertIntArrayToByteArray( this.spawner.getWave( ) ) );
        this.createOutboundMessage( waveMsg );
    }

    @Override
    public void onWaveEnd( ) {

        byte[ ] waveMsg = MessageManager.createMessage( MessageManager.PROTOCOL.WAVE_END, GameServer.NULL_ADDRESS,
                MessageManager.convertIntArrayToByteArray( ( int ) ( 1000 * spawner.getDifficulty( ) ) ) );
        this.createOutboundMessage( waveMsg );

    }

}

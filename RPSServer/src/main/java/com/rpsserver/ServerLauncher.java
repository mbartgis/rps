/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2019-01-24 15:53:02 -0500 (Thu, 24 Jan 2019) $
 * $Revision: 377 $
 * $Author: mbartgis $
 */

package com.rpsserver;

import com.rpscore.audio.SoundManager;

@SuppressWarnings("deprecation")
public class ServerLauncher {

	// private static long	TIMEOUT	= 30000;

	// private static int	TPS		= 60;

	public static void main( String[ ] args ) {

		SoundManager.disableAudio( );

		System.out.println( "Setting up server..." );
		GameServer gs = new GameServer( );

		System.out.println( "Starting server..." );
		gs.start( );
	}
}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-07-26 14:46:56 -0400 (Thu, 26 Jul 2018) $
 * $Revision: 362 $
 * $Author: mbartgis $
 */

package com.rps;

import java.awt.Color;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.rps.debug.DebugConsole;
import com.rps.gui.MainWindow;
import com.rpscore.C;
import com.rpscore.debug.Log;
import com.rpscore.entities.EntityRegistry;
import com.rpscore.entities.controlled.Player;
import com.rpscore.gui.game.WeaponSelectMenu;
import com.rpscore.terrain.BlockRegistry;

import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;

public class Launcher {

	/**
	 * Flag that tells the launcher that it was called from another game
	 * instance.
	 */
	public static boolean					relaunch	= true;

	/**
	 * Version ID to represent the current build.
	 */
	public static final double				VERSION		= 0.75;

	public static final String				STATE		= "Alpha";

	public static final String				BUILD_NAME	= "Argon";

	public static final Color				BUILD_THEME	= new Color( 0x4F0E95D3, true );

	public static final List< Controller >	GAMEPADS	= new LinkedList< Controller >( );

	public static String[ ]					argstore;

	public static final class GLOBAL_PARAMETERS {

		// Set the program to debug mode.
		public static boolean	IS_DEBUG				= false;

		// Skip all debug prompts (only works when in debug mode).
		public static boolean	NO_EXIT_PROMPT			= false;

		// Start out in the game menu instead of the main menu (saves time when
		// testing).
		public static boolean	GAME_START				= false;

		// Do not play any audio.
		public static boolean	NO_AUDIO				= false;

		// Do not write to a file (this helps when correcting write-out events
		// that could corrupt a data or config file).
		public static boolean	NO_FILE_OUTPUT			= false;

		// On startup, do not launch a window (useful for algorithmic
		// debugging).
		public static boolean	NO_GUI					= false;

		// Assign a developer to a runtime event.
		public static String	DEVELOPER				= "";

		public static int		START_WAVE				= 1;

		public static boolean	SPAWN_ENEMIES			= true;

		public static boolean	PRINT_LOG_AT_RUNTIME	= false;

		public static boolean	FAST_START				= false;

		private GLOBAL_PARAMETERS( ) {}

	}

	public static void setupArgs( String[ ] args ) {
		argstore = args;

		Log.info( "Checking Program Parameters..." );

		// Determine program arguments
		for ( int i = 0; i < args.length; i++ ) {
			String argument = args[ i ];
			switch ( argument ) {
				case "-d":
					Launcher.GLOBAL_PARAMETERS.IS_DEBUG = true;
					Log.info( C.Debug.MODIFIED_PARAMETER_DETECTED + argument );
					Log.info( "Debug mode activated..." );
					break;
				case "-gs":
					Launcher.GLOBAL_PARAMETERS.GAME_START = true;
					Log.info( C.Debug.MODIFIED_PARAMETER_DETECTED + argument );
					Log.warn( "Start In-Game mode activated... (This only works while in debug mode)" );
					break;
				case "-ne":
					Launcher.GLOBAL_PARAMETERS.NO_EXIT_PROMPT = true;
					Log.info( C.Debug.MODIFIED_PARAMETER_DETECTED + argument );
					Log.warn( "Remove Exit Prompt... (This only works while in debug mode)" );
					break;
				case "-naud":
					Launcher.GLOBAL_PARAMETERS.NO_AUDIO = true;
					Log.info( C.Debug.MODIFIED_PARAMETER_DETECTED + argument );
					Log.warn( "All Audio Disabled..." );
					break;
				case "-fs":
					Launcher.GLOBAL_PARAMETERS.FAST_START = true;
					Log.info( C.Debug.MODIFIED_PARAMETER_DETECTED + argument );
					Log.warn( "Enabling Fast Start..." );
					break;
				case "-nfo":
					Launcher.GLOBAL_PARAMETERS.NO_FILE_OUTPUT = true;
					Log.info( C.Debug.MODIFIED_PARAMETER_DETECTED + argument );
					Log.warn( "Disable All File Writing..." );
					break;
				case "-nwin":
					Launcher.GLOBAL_PARAMETERS.NO_GUI = true;
					Log.info( C.Debug.MODIFIED_PARAMETER_DETECTED + argument );
					Log.warn( "Disable Window Launch..." );
					break;
				case "-nen":
					Launcher.GLOBAL_PARAMETERS.SPAWN_ENEMIES = false;
					Log.info( C.Debug.MODIFIED_PARAMETER_DETECTED + argument );
					Log.warn( "Disable Enemy Spawning..." );
					break;
				case "-pl":
					Launcher.GLOBAL_PARAMETERS.PRINT_LOG_AT_RUNTIME = true;
					Log.setLoggerPrintOut( true );
					Log.info( C.Debug.MODIFIED_PARAMETER_DETECTED + argument );
					Log.warn( "Enable runtime log printing..." );
					break;
				case "-devlog":
					Log.info( C.Debug.MODIFIED_PARAMETER_DETECTED + argument );
					i++;
					try {
						argument = args[ i ];
					} catch ( ArrayIndexOutOfBoundsException e ) {
						Log.error( "The argument \"-devlog\" is missing a required parameter <user>" );
						continue;
					}
					if ( argument.startsWith( "-" ) ) {
						Log.error( "An argument parameter cannot begin with a \"-\": " + argument );
						i--;
					} else {
						Log.info( "Added user \"" + argument + "\" to session." );
						GLOBAL_PARAMETERS.DEVELOPER = argument;
					}
					break;
				case "-sw":
					Log.info( C.Debug.MODIFIED_PARAMETER_DETECTED + argument );
					i++;
					try {
						argument = args[ i ];
					} catch ( ArrayIndexOutOfBoundsException e ) {
						Log.error( "The argument \"-sw\" is missing a required parameter <number>" );
						continue;
					}
					if ( argument.startsWith( "-" ) ) {
						Log.error( "An argument parameter cannot begin with a \"-\": " + argument );
						i--;
					} else {
						try {
							GLOBAL_PARAMETERS.START_WAVE = Integer.parseInt( argument );
							Log.info( "Local game is set to start on wave " + argument + "." );
						} catch ( NumberFormatException e ) {
							Log.error( "The argument \"-sw\" parameter \"<number>\" must be of type int." );
						}
					}
					break;
				default:
					Log.warn( "Unknown parameter detected: " + argument );
					break;
			}
		}

		Log.setLoggerPrintOut( Launcher.GLOBAL_PARAMETERS.IS_DEBUG && Launcher.GLOBAL_PARAMETERS.PRINT_LOG_AT_RUNTIME );

		Log.info( "Finished checking program arguments." );
	}

	private static void setupGamepads( ) {

		try {
			addLibraryPath( C.File.LIBRARIES_FOLDER );
		} catch ( Exception e ) {}

		Log.info( "Checking for available gamepads..." );
		// Check for controllers
		Controller[ ] controllers = ControllerEnvironment.getDefaultEnvironment( ).getControllers( );

		for ( Controller c : controllers ) {
			c.getComponents( );
			if ( c.getType( ) == Controller.Type.GAMEPAD ) {
				Log.info( "Gamepad found: " + c.getName( ) );
				Launcher.GAMEPADS.add( c );
			}

		}

	}

	public static void addLibraryPath( String pathToAdd ) throws NoSuchFieldException, IllegalAccessException {
		Field usrPathsField = ClassLoader.class.getDeclaredField( "usr_paths" );
		usrPathsField.setAccessible( true );

		String[ ] paths = ( String[ ] ) usrPathsField.get( null );

		for ( String path : paths ) {
			if ( path.equals( pathToAdd ) ) {
				return;
			}
		}

		String[ ] newPaths = Arrays.copyOf( paths, paths.length + 1 );
		newPaths[ newPaths.length - 1 ] = pathToAdd;
		usrPathsField.set( null, newPaths );
	}

	public static void main( String[ ] args ) {

		// Setup program arguments
		// Note this should be called first.
		setupArgs( args );

		if ( GLOBAL_PARAMETERS.IS_DEBUG ) {
			DebugConsole.startConsole( );
		}

		if ( !GLOBAL_PARAMETERS.FAST_START ) {
			setupGamepads( );
		}

		// Fetch assets from the disk.
		BlockRegistry.buildBlockRegistry( );
		EntityRegistry.buildEntityRegistry( );
		Player.buildPlayer( );
		WeaponSelectMenu.loadWeaponIcons( );

		if ( !GLOBAL_PARAMETERS.NO_GUI ) {
			while ( relaunch ) {
				relaunch = false;

				MainWindow.getMainWindow( true );
			}
		}
	}

}

/**
 * // Static test long //
 * "  |       *       |       *       |       *       |       *        " long l
 * = 0b0000001110010010011011010110000000000000101000000000000010111111L;
 *
 * Random r = new Random(); long seed = r.nextLong( );
 *
 * // Force a dirt road (temporary requirement, method needs to change...) seed
 * |= 0b0000000000000000000000000000000000000000000000000000000000011111L;
 *
 * MAP_TEMP = TerrainGenerator.createChunk( seed, 10, 10 );
 */

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-05-14 02:34:20 -0400 (Mon, 14 May 2018) $
 * $Revision: 361 $
 * $Author: mbartgis $
 */

package com.rps.manualtests;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.Semaphore;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.Timer;

import com.rpscore.lib.MathUtils;
import com.rpscore.server.Communicator;

public class TestTextChat {

	public static void main( String[ ] args ) {

		TestTextChat ttc = new TestTextChat( );

		ttc.new WindowFrame( );

	}

	public class WindowFrame extends JFrame {

		Timer			t;

		Semaphore		chatMutex;

		Communicator	c;

		JTextArea		entryField;
		JTextArea		chat;

		public void terminate( ) {

			if ( t != null ) {
				t.stop( );
			}

			if ( this.c != null ) {
				this.c.disconnect( );
			}

			this.dispose( );

		}

		public WindowFrame( ) {

			chatMutex = new Semaphore( 1 );

			this.setLayout( null );
			this.setSize( 800, 600 );
			this.setResizable( false );
			this.setDefaultCloseOperation( JFrame.DO_NOTHING_ON_CLOSE );
			this.addWindowListener( new WindowListener( ) {

				@Override
				public void windowOpened( WindowEvent e ) {}

				@Override
				public void windowIconified( WindowEvent e ) {}

				@Override
				public void windowDeiconified( WindowEvent e ) {}

				@Override
				public void windowDeactivated( WindowEvent e ) {}

				@Override
				public void windowClosing( WindowEvent e ) {

					terminate( );

				}

				@Override
				public void windowClosed( WindowEvent e ) {}

				@Override
				public void windowActivated( WindowEvent e ) {}
			} );

			entryField = new JTextArea( );
			entryField.setSize( 600, 60 );
			entryField.setEditable( true );
			entryField.setLocation( 30, 500 );

			chat = new JTextArea( );

			chat.setSize( 700, 450 );
			chat.setEditable( false );
			chat.setLocation( 30, 30 );

			JButton sendMessage = new JButton( "Send" );

			sendMessage.setSize( 100, 30 );
			sendMessage.setLocation( 650, 500 );
			sendMessage.addActionListener( new ActionListener( ) {

				@Override
				public void actionPerformed( ActionEvent e ) {
					String text = WindowFrame.this.entryField.getText( );

					if ( !text.isEmpty( ) ) {
						byte[ ] transmissionText = MathUtils.toCompressedByteArray( text );
						WindowFrame.this.c.sendMessage( transmissionText );

						WindowFrame.this.entryField.setText( "" );

						try {
							chatMutex.acquire( );
						} catch ( InterruptedException e1 ) {}

						String s = WindowFrame.this.chat.getText( );
						s += "\n" + text;
						WindowFrame.this.chat.setText( s );

						chatMutex.release( );
					}

				}
			} );

			this.add( entryField );
			this.add( chat );
			this.add( sendMessage );

			this.setVisible( true );

			Socket s = null;

			try {
				String ip_addr = "74.99.74.214";
				s = new Socket( ip_addr, 22000 );

				this.c = new Communicator( s );
			} catch ( UnknownHostException e1 ) {
				// TODO Auto-generated catch block
				e1.printStackTrace( );
			} catch ( IOException e1 ) {
				// TODO Auto-generated catch block
				e1.printStackTrace( );
			}

			this.t = new Timer( 100, new TimerListener( ) );
			this.t.start( );

		}

		class TimerListener implements ActionListener {

			int cntr;

			public TimerListener( ) {
				super( );
				this.cntr = 0;
			}

			@Override
			public void actionPerformed( ActionEvent arg0 ) {

				byte[ ][ ] messages = c.getMessages( );

				if ( messages.length > 0 ) {

					System.out.println( "You've got mail!" );

					for ( int i = 0; i < messages.length; i++ ) {
						System.out.println( "New Message..." );
						for ( byte b : messages[ i ] ) {
							System.out.println( b );
						}
					}

					try {
						chatMutex.acquire( );
					} catch ( InterruptedException e1 ) {}

					String s = WindowFrame.this.chat.getText( );
					for ( int i = 0; i < messages.length; i++ ) {
						s += "\n" + MathUtils.convertCompressedToString( messages[ i ] );
						WindowFrame.this.chat.setText( s );
					}

					chatMutex.release( );
				}

				cntr++;

				if ( ( cntr % 50 ) == 0 ) {
					c.sendHeartbeat( );
				}

				if ( cntr == 100 ) {
					cntr = 0;
				}

			}

		}

	}

}

package com.rps.manualtests;

import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.rpscore.terrain.Block;
import com.rpscore.terrain.BlockRegistry;
import com.rpscore.terrain.CompositeBlock;

public class TestImages extends JFrame {

	JPanel jp = new JPanel() {
		protected void paintComponent(java.awt.Graphics g) {
			g.drawImage( i, 0, 0, null );
		};
	};
	Image i;
	
	public TestImages() {
		this.setSize( 800, 600 );
		BlockRegistry.buildBlockRegistry( );
		Block comp = new CompositeBlock( 401, BlockRegistry.BLOCK_TYPE.BLOCK_WATER, BlockRegistry.BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_RIGHT );
		i = comp.getLastBlockFrame( );
		
		this.setLayout( null );
		
		jp.setSize( 800, 600 );
		jp.setLocation(0, 0);
		jp.setLayout( null );
		jp.repaint( );
		
		this.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		
		
		this.add(jp);
		this.setVisible( true );
	
	}
	
	public static void main(String[] args) {
		new TestImages();
	}
	
	
	
}

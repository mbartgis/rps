/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date$
 * $Revision$
 * $Author$
 */

package com.rps.debug;

import java.util.LinkedList;
import java.util.Scanner;

import com.rps.Launcher;
import com.rpscore.C;
import com.rpscore.debug.Log;
import com.rpscore.entities.spawned.pickups.HealthPickup;
import com.rpscore.entities.spawned.pickups.MagicPickup;
import com.rpscore.entities.spawned.pickups.PickupSpawner;
import com.rpscore.entities.spawned.pickups.StaminaPickup;
import com.rpscore.game.PlayerManager;

public class DebugConsole extends Thread {

	public static DebugConsole		dc;

	private LinkedList< String >	keys;

	private LinkedList< String >	vars;

	public static void startConsole( ) {

		if ( dc == null ) {
			dc = new DebugConsole( );
		}

	}

	/*@formatter:off */
	public static final String[] LOOKUP_KEYS = new String[]{
			"HealthPickupDropRate",
			"MagicPickupDropRate",
			"StaminaPickupDropRate",
			"HealthPickupPotency",
			"MagicPickupPotency",
			"StaminaPickupPotency",
			"cwd"
	};

	public static final String[] LOOKUP_KEY_DESCRIPTIONS = new String[]{
			"The percent chance of dropping a health pickup.",
			"The percent chance of dropping a magic pickup.",
			"The percent chance of dropping a stamina pickup.",
			"Determines how powerful the health pickup is and how long it lasts on the field.",
			"Determines how powerful the magic pickup is and how long it lasts on the field.",
			"Determines how powerful the stamina pickup is and how long it lasts on the field.",
			"CONSTANT: Determines the Current Working Directory of the application."
	};

	public static final Object[] LOOKUP_REFERENCES = new Object[] {
		PickupSpawner.HEALTH_PICKUP_CHANCE,
		PickupSpawner.MAGIC_PICKUP_CHANCE,
		PickupSpawner.STAMINA_PICKUP_CHANCE,
		HealthPickup.HEALTH_PICKUP_POTENCY,
		MagicPickup.MAGIC_PICKUP_POTENCY,
		StaminaPickup.STAMINA_PICKUP_POTENCY
	};

	/*@formatter:on */

	private DebugConsole( ) {

		// Let the JRE kill this thread off when the program ends.
		this.setDaemon( true );

		this.keys = new LinkedList< String >( );
		this.vars = new LinkedList< String >( );

		this.start( );
	}

	@Override
	public void run( ) {
		super.run( );

		Scanner userInputStream = new Scanner( System.in );

		System.out.println( "Starting debug console:\n" + "-devlog " + Launcher.GLOBAL_PARAMETERS.DEVELOPER
				+ "\nVersion: " + Launcher.VERSION );

		Log.info( "Starting debug console..." );
		while ( true ) {
			String cmd = userInputStream.nextLine( );
			try {
				processCommand( cmd.split( " " ) );
				Log.info( "Command Successful: " + cmd );
			} catch ( Exception e ) {
				Log.error( "Command Failed: " + cmd );
				Log.error( e.getClass( ) + " :: " + e.getMessage( ) );

			}
		}

	}

	public void processCommand( String[ ] cmd ) {

		if ( cmd.length < 1 ) {
			throw new IllegalArgumentException( "Empty command..." );
		}

		if ( cmd[ 0 ].toLowerCase( ).equals( "for" ) ) {

			int iters = Integer.parseInt( cmd[ 1 ] );

			String[ ] cmd2 = new String[ cmd.length - 2 ];

			for ( int x = 0; x < cmd2.length; x++ ) {
				cmd2[ x ] = cmd[ x + 2 ];
			}

			for ( int i = 0; i < iters; i++ ) {
				processCommand( cmd2 );
			}

		}

		else if ( cmd[ 0 ].toLowerCase( ).equals( "print" ) ) {

			String[ ] prints = cmd[ 1 ].split( "\\+" );

			StringBuilder sb = new StringBuilder( );
			for ( int i = 0; i < prints.length; i++ ) {
				if ( prints[ i ].startsWith( "@" ) ) {
					prints[ i ] = lookup( prints[ i ] );
				} else {
					prints[ i ] = prints[ i ].replaceAll( "%20", " " );
				}
				sb.append( prints[ i ] );
			}

			System.out.println( sb.toString( ) );

		}

		else if ( cmd[ 0 ].toLowerCase( ).equals( "help" ) ) {

			/*@formatter:off */
			System.out.println(

					"Available commands:\n" +
					"for     <int iters> <array cmd> - Run a command a certain number of times.\n" +
					"help                            - Displays this page.\n" +
					"lookup  <lkp key>               - Lookup a runtime variable. NOTE: All keys are prepended with\"@\".\n" +
					"lookups <lkp key>               - Prints a table of keys and description of each.\n" +
					"modify  <lkp key> <str val>     - Modify a runtime variable. NOTE: All keys are prepended with\"@\".\n" +
					"print   <sen statement>         - Prints a sentence.\n" +
					"term    [int return_status]     - Exits the JRE with an optional exit status. NOTE: The default status is \"0\".\n" +
					"\n" +
					"NOTES:\n" +
					"\"<>\" Denotes a mandatory parameter.\n" +
					"\"[]\" Denotes an optional parameter.\n" +
					"int   - Any signed whole number from 0-((2^32)-1).\n" +
					"str   - Any literal string of characters. Spaces are represented by the regex \"%20\".\n" +
					"lkp   - A key to lookup that begins with an \"@\". (i.e. @HealthPickupDropRate)\n" +
					"array - Any set of types delimited by a space (0x20).\n" +
					"sen   - A sentence formed from concatenations of strings and lookups delimited by a \"+\"\n"


			);
			/*@formatter:on */

		}

		else if ( cmd[ 0 ].toLowerCase( ).equals( "lookups" ) ) {
			for ( int i = 0; i < LOOKUP_KEYS.length; i++ ) {
				System.out.println( "@" + LOOKUP_KEYS[ i ] + " - " + LOOKUP_KEY_DESCRIPTIONS[ i ] );
			}
		}

		else if ( cmd[ 0 ].toLowerCase( ).equals( "lookup" ) ) {
			if ( cmd[ 1 ].toLowerCase( ).equals( "all" ) ) {
				for ( int i = 0; i < LOOKUP_KEYS.length; i++ ) {
					System.out.println( LOOKUP_KEYS[ i ] + " : " + lookup( LOOKUP_KEYS[ i ] ) );
				}
			} else {
				System.out.println( lookup( cmd[ 1 ] ) );
			}
		}

		else if ( cmd[ 0 ].toLowerCase( ).equals( "modify" ) ) {
			modify( cmd[ 1 ], cmd[ 2 ] );
		}

		else if ( cmd[ 0 ].toLowerCase( ).equals( "term" ) ) {

			if ( cmd.length >= 2 ) {
				System.exit( Integer.parseInt( cmd[ 1 ] ) );
			} else {
				System.exit( 0 );
			}

		}

		else if ( cmd[ 0 ].toLowerCase( ).equals( "def" ) ) {
			for ( int i = 0; i < keys.size( ); i++ ) {
				if ( cmd[ 1 ].equals( keys.get( i ) ) ) {
					System.out.println( "Key already defined." );
					break;
				}
			}
			this.vars.add( cmd[ 2 ] );
			this.keys.add( cmd[ 1 ] );
		}

		else if ( cmd[ 0 ].toLowerCase( ).equals( "set" ) ) {
			boolean found = false;
			for ( int i = 0; i < keys.size( ); i++ ) {
				if ( cmd[ 1 ].equals( keys.get( i ) ) ) {
					vars.set( i, cmd[ 2 ] );
					found = true;
					break;
				}
			}
			if ( !found ) {
				System.out.println( "Key not found." );
			}
		}

		else if ( cmd[ 0 ].toLowerCase( ).equals( "get" ) ) {
			boolean found = false;
			for ( int i = 0; i < keys.size( ); i++ ) {
				if ( cmd[ 1 ].equals( keys.get( i ) ) ) {
					found = true;
					System.out.println( vars.get( i ) );
					break;
				}
			}
			if ( !found ) {
				System.out.println( "Key not found." );
			}
		} else if ( cmd[ 0 ].toLowerCase( ).equals( "ledit" ) ) {
			try {
				new LiveEditor( PlayerManager.getPlayerManagerInstance( ) );
			} catch ( NullPointerException e ) {

			}
		}

	}

	public String lookup( String key ) {

		key = key.replaceAll( "@", "" );

		switch ( key ) {
			case "HealthPickupDropRate":
				return "" + PickupSpawner.HEALTH_PICKUP_CHANCE;
			case "MagicPickupDropRate":
				return "" + PickupSpawner.MAGIC_PICKUP_CHANCE;
			case "StaminaPickupDropRate":
				return "" + PickupSpawner.STAMINA_PICKUP_CHANCE;
			case "HealthPickupPotency":
				return "" + HealthPickup.HEALTH_PICKUP_POTENCY;
			case "MagicPickupPotency":
				return "" + MagicPickup.MAGIC_PICKUP_POTENCY;
			case "StaminaPickupPotency":
				return "" + StaminaPickup.STAMINA_PICKUP_POTENCY;
			case "cwd":
				return C.File.CWD;
			default:
				break;
		}

		return "{not found}";

	}

	public void modify( String key, String val ) {

		key = key.replaceAll( "@", "" );

		switch ( key ) {
			case "HealthPickupDropRate":
				PickupSpawner.HEALTH_PICKUP_CHANCE = Double.parseDouble( val );
				return;
			case "MagicPickupDropRate":
				PickupSpawner.MAGIC_PICKUP_CHANCE = Double.parseDouble( val );
				return;
			case "StaminaPickupDropRate":
				PickupSpawner.STAMINA_PICKUP_CHANCE = Double.parseDouble( val );
				return;
			case "HealthPickupPotency":
				HealthPickup.HEALTH_PICKUP_POTENCY = Integer.parseInt( val );
				return;
			case "MagicPickupPotency":
				MagicPickup.MAGIC_PICKUP_POTENCY = Integer.parseInt( val );
				return;
			case "StaminaPickupPotency":
				StaminaPickup.STAMINA_PICKUP_POTENCY = Integer.parseInt( val );
				return;
			case "cwd":
				throw new IllegalArgumentException( "Cannot modify the constant \"CWD\"." );
			default:
				break;
		}
	}

}
/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date$
 * $Revision$
 * $Author$
 */

package com.rps.debug;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Semaphore;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.Timer;

import com.rpscore.cfg.ConfigManager;
import com.rpscore.game.PlayerManager;

public class LiveEditor extends JDialog {

	private PlayerManager		pmRef;

	private JTextArea			playerState;

	private JButton				sendState, clearState;

	private Timer				t;

	private Semaphore			stateLock;

	private JScrollPane			jsp;

	private boolean				lockout;

	private static final int	BINARY_SEMAPHORE	= 1;

	public LiveEditor( PlayerManager pm ) {
		this.pmRef = pm;

		this.lockout = false;

		this.setSize( 600, 800 );
		this.setLocation( 100, 100 );
		this.setLayout( null );
		this.setAlwaysOnTop( true );
		this.setDefaultCloseOperation( JDialog.DISPOSE_ON_CLOSE );

		this.stateLock = new Semaphore( BINARY_SEMAPHORE );

		this.playerState = new JTextArea( );
		this.playerState.setSize( 560, 700 );
		this.playerState.setLocation( 20, 20 );

		this.add( this.playerState );

		this.t = new Timer( 60, new ActionListener( ) {

			@Override
			public void actionPerformed( ActionEvent e ) {

				if ( !playerState.hasFocus( ) && !lockout ) {

					StringBuilder sb = new StringBuilder( );
					String[ ] pl = pmRef.getUnderlying( ).getFullConfigState( );

					for ( int i = 0; i < pl.length; i++ ) {
						sb.append( pl[ i ] );
						sb.append( "\n" );
					}

					try {
						stateLock.acquire( );
					} catch ( InterruptedException e1 ) {
						Thread.currentThread( ).interrupt( );
					}
					playerState.setText( sb.toString( ) );
					stateLock.release( );

				} else {
					lockout = true;
				}

				// Kill the editor window when a screen is no longer valid.
				if ( pmRef.isFinished( ) ) {
					dispose( );
				}

			}
		} );

		this.jsp = new JScrollPane( );
		this.jsp.setSize( 560, 700 );
		this.jsp.setLocation( 20, 20 );
		this.jsp.setViewportView( this.playerState );

		this.add( jsp );

		this.sendState = new JButton( "Send State" );
		this.sendState.setSize( 100, 30 );
		this.sendState.setLocation( 20, 720 );
		this.sendState.addActionListener( new ActionListener( ) {

			@Override
			public void actionPerformed( ActionEvent e ) {

				try {
					stateLock.acquire( );
				} catch ( InterruptedException e1 ) {
					Thread.currentThread( ).interrupt( );
				}
				String[ ] items = playerState.getText( ).split( "\n" );
				stateLock.release( );

				ConfigManager pmUnderlying = pmRef.getUnderlying( );

				for ( int i = 0; i < items.length; i++ ) {
					String[ ] term = items[ i ].split( "=" );
					try {
						pmUnderlying.modifySetting( term[ 0 ], term[ 1 ] );
					} catch ( ArrayIndexOutOfBoundsException ex ) {
						continue;
					}
				}

				pmRef.update( );

				lockout = false;

			}
		} );

		this.add( this.sendState );

		this.clearState = new JButton( "Clear State" );
		this.clearState.setSize( 100, 30 );
		this.clearState.setLocation( 140, 720 );
		this.clearState.addActionListener( new ActionListener( ) {

			@Override
			public void actionPerformed( ActionEvent e ) {

				lockout = false;

			}
		} );

		this.add( this.clearState );

		this.t.start( );

		this.setVisible( true );

	}

	@Override
	public void dispose( ) {

		this.pmRef = null;

		if ( this.t != null ) {
			this.t.stop( );
		}

		super.dispose( );
	}
}
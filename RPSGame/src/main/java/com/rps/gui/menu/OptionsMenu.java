/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-07-26 22:16:38 -0400 (Thu, 26 Jul 2018) $
 * $Revision: 363 $
 * $Author: mbartgis $
 */
package com.rps.gui.menu;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;

import com.rps.Launcher;
import com.rps.gui.MainWindow;
import com.rps.gui.ModularPanel;
import com.rps.gui.game.GamePanel;
import com.rpscore.C;
import com.rpscore.C.SettingKeys;
import com.rpscore.audio.SoundManager;
import com.rpscore.cfg.ConfigManager;
import com.rpscore.lib.FileUtils;

public class OptionsMenu extends ModularPanel {

	private SelectionLabel			returnLabel;
	private SelectionLabel			saveLabel;

	private SelectionLabel			defaultLabel;

	private MouseListener			mcl;

	private MouseMotionListener		mml;

	private static ConfigManager	cfg						= ConfigManager.getConfigManager( );

	private static final int		DEFAULT_BUTTON_WIDTH	= 100;
	private static final int		DEFAULT_BUTTON_HEIGHT	= 30;

	private static final String		SETTINGS_UP_TO_DATE		= "All settings are up to date.";
	private static final String		UNSAVED_SETTING			= "A setting must be saved.";

	public static Font				MENU_SECTION_FONT		= new Font( "Arial", Font.BOLD, 16 );

	public static final class DEFAULT_SETTINGS {
		public static final boolean	SETTING_SMOOTH_EDGES			= false;
		public static final int		SETTING_MASTER_VOLUME			= 0;
		public static final int		SETTING_BG_VOLUME				= 0;
		public static final int		SETTING_SFX_VOLUME				= -20;
		public static final boolean	SETTING_DISPLAY_FPS				= false;
		public static final int		SETTING_FPS_COLOR				= Launcher.BUILD_THEME.getRGB( );
		public static final int		SETTING_FPS_LOCATION			= 1;
		public static final int		SETTING_X_RESOLUTION			= 800;
		public static final int		SETTING_Y_RESOLUTION			= 600;
		public static final boolean	SETTING_DISPLAY_ROTATION_DATA	= false;
		public static final boolean	SETTING_DISPLAY_BOUNDING_BOXES	= false;
		public static final boolean	SETTING_CAP_FPS					= true;
		public static final int		SETTING_FPS_LIMIT				= 60;
		public static final boolean	SETTING_FULLSCREEN_WINDOW		= true;
		public static final int		SETTING_RENDER_DISTANCE			= 16;
		public static final boolean	SETTING_HUD						= true;
		public static final boolean	SETTING_MORTAR_LANDING_ASSIST	= true;
		public static final boolean	SETTING_BOMB_POWER_INDICATOR	= true;
		public static final boolean	SETTING_GENERATE_FOG			= false;

	}

	private boolean								allowRestart;

	// Variables captured on menu startup
	private boolean								initSmoothEdges;
	private int									initBGVolume;
	private int									initSFXVolume;
	private boolean								initDisplayFPS;
	private int									initFPSColor;
	private int									initFPSLocation;
	private boolean								initDisplayRotationData;
	private boolean								initDisplayBoundingBoxes;
	private boolean								initCapFPS;
	private int									initFPSLimit;
	private boolean								initFullScreenWindow;
	private int									initRenderDistance;
	private boolean								initHUD;
	private boolean								initMortarLandingAssist;
	private boolean								initBombPowerIndicator;
	private boolean								initGenerateFog;

	// GUI elements
	private JPanel								menuContainer;

	private JLabel								savedSettingsText;

	private JPanel								graphicsTab;
	private JPanel								audioTab;
	private JPanel								controlsTab;
	private JPanel								advancedTab;

	// Graphics UI Elements
	private JLabel								visualsLabel;

	private JCheckBox							smoothEdgesCB;

	private JCheckBox							capFPSCB;
	private JTextField							fpsLimitTF;
	private ValidationNumberBoxDocumentListener	fpsFieldValidator;

	private JCheckBox							fullscreenWindowCB;

	private JLabel								renderDistanceLabel;
	private JSlider								renderDistanceS;

	private JLabel								gameplayLabel;

	private JCheckBox							hudCB;
	private JCheckBox							mortarLandingAssistCB;
	private JCheckBox							bombPowerIndicatorCB;
	private JCheckBox							generateFogCB;

	// Audio UI Elements

	private JLabel								sfxVolumeLabel;
	private JSlider								sfxVolumeS;

	private JLabel								bgVolumeLabel;
	private JSlider								bgVolumeS;

	// Advanced UI Elements
	private JCheckBox							displayFPSCB;
	private JComboBox< String >					displayFPSLocationCB;
	private static final String[ ]				FPS_DISPLAY_LOCATIONS	= { "Top Left", "Top Right", "Bottom Left",
			"Bottom Right" };
	private JButton								fpsColorButton;

	private CHECK_SETTINGS_CHANGES				csc_listener;

	private JCheckBox							displayRotationDataCB;
	private JCheckBox							displayBoundingBoxesCB;

	private JLabel								graphicsTabLabel, audioTabLabel, controlsTabLabel, advancedTabLabel;

	private Image								settingIcon;

	private GamePanel							currentInstance;

	public OptionsMenu( ) {

		this.setLayout( null );
		this.setBackground( Color.BLACK );

		// Menu Buttons
		/* @formatter:off */
		this.returnLabel = new SelectionLabel( " Return" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				currentInstance = null;
				terminate( );
			}
		};
		this.returnLabel.setSize( 300, 100 );
		this.returnLabel.setLocation( 20, this.getHeight( ) - 150 );
		this.returnLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.returnLabel.setForeground( Color.WHITE );
		this.returnLabel.setFont( C.Fonts.MENU_FONT );

		this.add( this.returnLabel );

		// Menu Buttons
		/* @formatter:off */
		this.saveLabel = new SelectionLabel( " Save" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				boolean restartRequired = OptionsMenu.this.saveSettings( );

				if ( restartRequired ) {
					int option = JOptionPane.showConfirmDialog( OptionsMenu.this,
							"A restart is required to update some of the settings changed.\nRestart now?",
							"Restart Required", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE );

					Launcher.relaunch = option == JOptionPane.YES_OPTION;
				}

				if ( Launcher.relaunch ) {
					MainWindow.getMainWindow( ).terminate( );
				}

				OptionsMenu.this.savedSettingsText.setText( OptionsMenu.SETTINGS_UP_TO_DATE );
				OptionsMenu.this.savedSettingsText.setForeground( Color.WHITE );

				// If the options menu is spawned from an existing game
				// instance, update
				// that instance with all settings changes.
				OptionsMenu.this.allowRestart = false;

				if ( currentInstance != null ) {
					currentInstance.updateGraphicsSettings( false );
				}

				OptionsMenu.this.terminate( );
			}
		};
		this.saveLabel.setSize( 200, 100 );
		this.saveLabel.setLocation( 400, this.getHeight( ) - 150 );
		this.saveLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.saveLabel.setForeground( Color.WHITE );
		this.saveLabel.setFont( C.Fonts.MENU_FONT );

		this.add( this.saveLabel );

		// Menu Buttons
		/* @formatter:off */
		this.defaultLabel = new SelectionLabel( " Default Settings..." ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				OptionsMenu.this.defaultSettings( );
				OptionsMenu.this.csc_listener.actionPerformed( null );
			}
		};
		this.defaultLabel.setSize( 600, 100 );
		this.defaultLabel.setLocation( 700, this.getHeight( ) - 150 );
		this.defaultLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.defaultLabel.setForeground( Color.WHITE );
		this.defaultLabel.setFont( C.Fonts.MENU_FONT );

		this.add( this.defaultLabel );

		this.allowRestart = false;

		this.setSize( 800, 600 );

		try {
			this.settingIcon = FileUtils.importImage( C.File.SETTINGS_ICON ).getScaledInstance( 80, 80,
					Image.SCALE_SMOOTH );
		} catch ( MalformedURLException e1 ) {
			e1.printStackTrace( );
		} catch ( IOException e1 ) {
			e1.printStackTrace( );
		}

		initSettings( );

		this.csc_listener = new CHECK_SETTINGS_CHANGES( );

		// GUI Setup
		this.menuContainer = new JPanel( );
		this.menuContainer.setLayout( null );
		this.menuContainer.setSize( this.getWidth( ), this.getHeight( ) );
		this.menuContainer.setLocation( 0, 100 );

		this.savedSettingsText = new JLabel( SETTINGS_UP_TO_DATE );
		this.savedSettingsText.setForeground( Color.WHITE );
		this.savedSettingsText.setLocation( 720, 40 );
		this.savedSettingsText.setFont( C.Fonts.MENU_MOTD_FONT );
		this.savedSettingsText.setSize( 400, 30 );

		this.add( this.savedSettingsText );

		// Graphics Tab
		this.graphicsTabLabel = new JLabel( "Graphics" );
		this.graphicsTabLabel.setSize( 400, 30 );
		this.graphicsTabLabel.setLocation( 220, 20 );
		this.graphicsTabLabel.setForeground( Color.WHITE );
		this.graphicsTabLabel.setFont( C.Fonts.MENU_MOTD_FONT );

		menuContainer.add( this.graphicsTabLabel );

		this.graphicsTab = new JPanel( );
		this.graphicsTab.setLayout( null );
		this.graphicsTab.setLocation( 20, 0 );
		this.graphicsTab.setSize( 650, 400 );
		this.graphicsTab
				.setBorder( BorderFactory.createEtchedBorder( EtchedBorder.RAISED, Color.WHITE, Color.LIGHT_GRAY ) );

		this.visualsLabel = new JLabel( "Visuals" );
		this.visualsLabel.setSize( 200, 30 );
		this.visualsLabel.setLocation( 40, 20 );
		this.visualsLabel.setFont( MENU_SECTION_FONT );
		this.graphicsTab.add( this.visualsLabel );

		this.smoothEdgesCB = new JCheckBox( "Smooth Block Edges" );
		this.smoothEdgesCB.setSize( 200, 30 );
		this.smoothEdgesCB.setLocation( 20, 60 );
		this.smoothEdgesCB.setSelected( this.initSmoothEdges );
		this.smoothEdgesCB.addActionListener( this.csc_listener );
		this.graphicsTab.add( this.smoothEdgesCB );

		this.capFPSCB = new JCheckBox( "Limit Maximum FPS" );
		this.capFPSCB.setSize( 160, 30 );
		this.capFPSCB.setLocation( 20, 100 );
		this.capFPSCB.setSelected( this.initCapFPS );
		this.capFPSCB.addActionListener( this.csc_listener );
		this.capFPSCB.addActionListener( new ActionListener( ) {
			@Override
			public void actionPerformed( ActionEvent e ) {
				OptionsMenu.this.fpsLimitTF.setEnabled( OptionsMenu.this.capFPSCB.isSelected( ) );
			}
		} );
		this.graphicsTab.add( this.capFPSCB );

		this.fpsLimitTF = new JTextField( );
		this.fpsLimitTF.setSize( 40, 30 );
		this.fpsLimitTF.setLocation( 200, 100 );
		this.fpsLimitTF.setText( "" + this.initFPSLimit );
		this.fpsLimitTF.setEnabled( this.initCapFPS );
		this.fpsLimitTF.addActionListener( this.csc_listener );
		this.fpsLimitTF.setCaretColor( Color.WHITE );
		this.graphicsTab.add( this.fpsLimitTF );

		this.fpsFieldValidator = new ValidationNumberBoxDocumentListener( this.fpsLimitTF, 1, 10000, false, Color.BLACK,
				Color.RED );

		this.fpsLimitTF.getDocument( ).addDocumentListener( this.fpsFieldValidator );

		this.renderDistanceLabel = new JLabel( "Render Distance: " + this.initRenderDistance );
		this.renderDistanceLabel.setSize( 200, 30 );
		this.renderDistanceLabel.setLocation( 40, 140 );
		this.renderDistanceLabel.setFont( MENU_SECTION_FONT );
		this.graphicsTab.add( this.renderDistanceLabel );

		this.renderDistanceS = new JSlider( );
		this.renderDistanceS.setSize( 300, 60 );
		this.renderDistanceS.setLocation( 20, 160 );
		this.renderDistanceS.setValue( this.initRenderDistance );
		this.renderDistanceS.setMinimum( 10 );
		this.renderDistanceS.setMaximum( 40 );
		this.renderDistanceS.setPaintTicks( true );
		this.renderDistanceS.setMajorTickSpacing( 6 );
		this.renderDistanceS.setMinorTickSpacing( 3 );
		this.renderDistanceS.addChangeListener( changeEvent -> {

			renderDistanceLabel.setText( "Render Distance: " + renderDistanceS.getValue( ) );

			if ( OptionsMenu.this.settingChanged( ) ) {
				OptionsMenu.this.savedSettingsText.setText( OptionsMenu.UNSAVED_SETTING );
				OptionsMenu.this.savedSettingsText.setForeground( Color.RED );
			} else {
				OptionsMenu.this.savedSettingsText.setText( OptionsMenu.SETTINGS_UP_TO_DATE );
				OptionsMenu.this.savedSettingsText.setForeground( Color.WHITE );
			}

		} );
		this.graphicsTab.add( this.renderDistanceS );

		this.fullscreenWindowCB = new JCheckBox( "Fullscreen Window" );
		this.fullscreenWindowCB.setSize( 200, 30 );
		this.fullscreenWindowCB.setLocation( 20, 220 );
		this.fullscreenWindowCB.setSelected( this.initFullScreenWindow );
		this.fullscreenWindowCB.addActionListener( this.csc_listener );
		this.graphicsTab.add( this.fullscreenWindowCB );

		this.gameplayLabel = new JLabel( "Gameplay" );
		this.gameplayLabel.setSize( 200, 30 );
		this.gameplayLabel.setLocation( 420, 20 );
		this.gameplayLabel.setFont( MENU_SECTION_FONT );
		this.graphicsTab.add( this.gameplayLabel );

		this.hudCB = new JCheckBox( "Enable HUD" );
		this.hudCB.setSize( 160, 30 );
		this.hudCB.setLocation( 400, 60 );
		this.hudCB.setSelected( this.initHUD );
		this.hudCB.addActionListener( this.csc_listener );
		this.graphicsTab.add( this.hudCB );

		this.bombPowerIndicatorCB = new JCheckBox( "Bomb Yield Indicator" );
		this.bombPowerIndicatorCB.setSize( 160, 30 );
		this.bombPowerIndicatorCB.setLocation( 400, 100 );
		this.bombPowerIndicatorCB.setSelected( this.initBombPowerIndicator );
		this.bombPowerIndicatorCB.addActionListener( this.csc_listener );
		this.graphicsTab.add( this.bombPowerIndicatorCB );

		this.mortarLandingAssistCB = new JCheckBox( "Mortar Landing Assist" );
		this.mortarLandingAssistCB.setSize( 160, 30 );
		this.mortarLandingAssistCB.setLocation( 400, 140 );
		this.mortarLandingAssistCB.setSelected( this.initMortarLandingAssist );
		this.mortarLandingAssistCB.addActionListener( this.csc_listener );
		this.graphicsTab.add( this.mortarLandingAssistCB );

		this.generateFogCB = new JCheckBox( "Generate Fog" );
		this.generateFogCB.setSize( 160, 30 );
		this.generateFogCB.setLocation( 400, 180 );
		this.generateFogCB.setSelected( this.initGenerateFog );
		this.generateFogCB.addActionListener( this.csc_listener );
		this.graphicsTab.add( this.generateFogCB );

		// Audio Tab
		this.audioTabLabel = new JLabel( "Audio" );
		this.audioTabLabel.setSize( 400, 30 );
		this.audioTabLabel.setLocation( 950, 20 );
		this.audioTabLabel.setForeground( Color.WHITE );
		this.audioTabLabel.setFont( C.Fonts.MENU_MOTD_FONT );
		menuContainer.add( this.audioTabLabel );

		this.audioTab = new JPanel( );
		this.audioTab.setLayout( null );
		this.audioTab.setLocation( 700, 0 );
		this.audioTab.setSize( 650, 400 );
		this.audioTab
				.setBorder( BorderFactory.createEtchedBorder( EtchedBorder.RAISED, Color.WHITE, Color.LIGHT_GRAY ) );

		this.bgVolumeLabel = new JLabel( "Background Music Volume: " + this.initBGVolume );
		this.bgVolumeLabel.setSize( 300, 30 );
		this.bgVolumeLabel.setLocation( 40, 70 );
		this.bgVolumeLabel.setFont( MENU_SECTION_FONT );
		this.audioTab.add( this.bgVolumeLabel );

		this.bgVolumeS = new JSlider( );
		this.bgVolumeS.setSize( 600, 60 );
		this.bgVolumeS.setLocation( 20, 90 );
		this.bgVolumeS.setMinimum( -100 );
		this.bgVolumeS.setMaximum( 100 );
		this.bgVolumeS.setValue( this.initBGVolume );
		this.bgVolumeS.setPaintTicks( true );
		this.bgVolumeS.setMajorTickSpacing( 20 );
		this.bgVolumeS.setMinorTickSpacing( 10 );
		this.bgVolumeS.addChangeListener( changeEvent -> {

			bgVolumeLabel.setText( "Background Music Volume: " + bgVolumeS.getValue( ) );

			cfg.modifySetting( SettingKeys.BG_AUDIO_VOLUME, "" + bgVolumeS.getValue( ) );

			if ( OptionsMenu.this.settingChanged( ) ) {
				OptionsMenu.this.savedSettingsText.setText( OptionsMenu.UNSAVED_SETTING );
				OptionsMenu.this.savedSettingsText.setForeground( Color.RED );
			} else {
				OptionsMenu.this.savedSettingsText.setText( OptionsMenu.SETTINGS_UP_TO_DATE );
				OptionsMenu.this.savedSettingsText.setForeground( Color.WHITE );
			}

		} );
		this.audioTab.add( this.bgVolumeS );

		this.sfxVolumeLabel = new JLabel( "SFX Music Volume: " + this.initSFXVolume );
		this.sfxVolumeLabel.setSize( 300, 30 );
		this.sfxVolumeLabel.setLocation( 40, 160 );
		this.sfxVolumeLabel.setFont( MENU_SECTION_FONT );
		this.audioTab.add( this.sfxVolumeLabel );

		this.sfxVolumeS = new JSlider( );
		this.sfxVolumeS.setSize( 600, 60 );
		this.sfxVolumeS.setLocation( 20, 200 );
		this.sfxVolumeS.setMinimum( -100 );
		this.sfxVolumeS.setMaximum( 100 );
		this.sfxVolumeS.setValue( this.initSFXVolume );
		this.sfxVolumeS.setPaintTicks( true );
		this.sfxVolumeS.setMajorTickSpacing( 20 );
		this.sfxVolumeS.setMinorTickSpacing( 10 );
		this.sfxVolumeS.addChangeListener( changeEvent -> {

			sfxVolumeLabel.setText( "SFX Music Volume: " + sfxVolumeS.getValue( ) );

			cfg.modifySetting( SettingKeys.SFX_AUDIO_VOLUME, "" + sfxVolumeS.getValue( ) );

			if ( OptionsMenu.this.settingChanged( ) ) {
				OptionsMenu.this.savedSettingsText.setText( OptionsMenu.UNSAVED_SETTING );
				OptionsMenu.this.savedSettingsText.setForeground( Color.RED );
			} else {
				OptionsMenu.this.savedSettingsText.setText( OptionsMenu.SETTINGS_UP_TO_DATE );
				OptionsMenu.this.savedSettingsText.setForeground( Color.WHITE );
			}

			SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );

		} );
		this.audioTab.add( this.sfxVolumeS );

		// Controls Tab
		this.controlsTabLabel = new JLabel( "Controls" );
		this.controlsTabLabel.setSize( 600, 30 );
		this.controlsTabLabel.setLocation( 220, 450 );
		this.controlsTabLabel.setForeground( Color.WHITE );
		this.controlsTabLabel.setFont( C.Fonts.MENU_MOTD_FONT );
		menuContainer.add( this.controlsTabLabel );

		this.controlsTab = new JPanel( );
		this.controlsTab.setLayout( null );
		this.controlsTab.setLocation( 20, 430 );
		this.controlsTab.setSize( 650, 400 );
		this.controlsTab
				.setBorder( BorderFactory.createEtchedBorder( EtchedBorder.RAISED, Color.WHITE, Color.LIGHT_GRAY ) );

		// Advanced Tab
		this.advancedTabLabel = new JLabel( "Advanced/Developer" );
		this.advancedTabLabel.setSize( 600, 30 );
		this.advancedTabLabel.setLocation( 850, 450 );
		this.advancedTabLabel.setForeground( Color.WHITE );
		this.advancedTabLabel.setFont( C.Fonts.MENU_MOTD_FONT );
		menuContainer.add( this.advancedTabLabel );

		this.advancedTab = new JPanel( );
		this.advancedTab.setLayout( null );
		this.advancedTab.setLocation( 700, 430 );
		this.advancedTab.setSize( 650, 400 );
		this.advancedTab
				.setBorder( BorderFactory.createEtchedBorder( EtchedBorder.RAISED, Color.WHITE, Color.LIGHT_GRAY ) );

		this.displayFPSCB = new JCheckBox( "Display FPS Counter" );
		this.displayFPSCB.setSize( 200, 30 );
		this.displayFPSCB.setLocation( 20, 60 );
		this.displayFPSCB.setSelected( this.initDisplayFPS );
		this.displayFPSCB.addActionListener( this.csc_listener );
		this.displayFPSCB.addActionListener( new ActionListener( ) {

			@Override
			public void actionPerformed( ActionEvent e ) {
				OptionsMenu.this.displayFPSLocationCB.setEnabled( OptionsMenu.this.displayFPSCB.isSelected( ) );
				OptionsMenu.this.fpsColorButton.setEnabled( OptionsMenu.this.displayFPSCB.isSelected( ) );
			}
		} );
		this.advancedTab.add( this.displayFPSCB );

		this.displayFPSLocationCB = new JComboBox< >( FPS_DISPLAY_LOCATIONS );
		this.displayFPSLocationCB.setSize( 200, 30 );
		this.displayFPSLocationCB.setLocation( 40, 100 );
		this.displayFPSLocationCB.setSelectedIndex( this.initFPSLocation );
		this.displayFPSLocationCB.addActionListener( this.csc_listener );
		this.displayFPSLocationCB.setEnabled( this.initDisplayFPS );
		this.advancedTab.add( this.displayFPSLocationCB );

		this.fpsColorButton = new JButton( );
		this.fpsColorButton.setSize( DEFAULT_BUTTON_WIDTH, DEFAULT_BUTTON_HEIGHT );
		this.fpsColorButton.setLocation( 40, 140 );
		this.fpsColorButton.setBackground( new Color( this.initFPSColor ) );
		this.fpsColorButton.addActionListener( this.csc_listener );
		this.fpsColorButton.setEnabled( this.initDisplayFPS );
		this.fpsColorButton.addActionListener( new ActionListener( ) {

			@Override
			public void actionPerformed( ActionEvent e ) {

				Color c = JColorChooser.showDialog( OptionsMenu.this, "Select FPS Counter Color", Color.WHITE );
				if ( c != null ) {
					fpsColorButton.setBackground( c );
				}

			}
		} );
		this.advancedTab.add( this.fpsColorButton );

		this.displayRotationDataCB = new JCheckBox( "Display Debug Rotation Data" );
		this.displayRotationDataCB.setSize( 300, 30 );
		this.displayRotationDataCB.setLocation( 20, 180 );
		this.displayRotationDataCB.setSelected( this.initDisplayRotationData );
		this.displayRotationDataCB.addActionListener( this.csc_listener );
		this.advancedTab.add( this.displayRotationDataCB );

		this.displayBoundingBoxesCB = new JCheckBox( "Display Bounding Boxes" );
		this.displayBoundingBoxesCB.setSize( 300, 30 );
		this.displayBoundingBoxesCB.setLocation( 20, 220 );
		this.displayBoundingBoxesCB.setSelected( this.initDisplayBoundingBoxes );
		this.displayBoundingBoxesCB.addActionListener( this.csc_listener );
		this.advancedTab.add( this.displayBoundingBoxesCB );

		this.menuContainer.add( this.graphicsTab );
		this.menuContainer.add( this.audioTab );
		this.menuContainer.add( this.controlsTab );
		this.menuContainer.add( this.advancedTab );

		this.add( menuContainer );

		setAllElementsRecursively( menuContainer );

		this.audioTabLabel.setForeground( Color.YELLOW.darker( ) );
		this.graphicsTabLabel.setForeground( Color.YELLOW.darker( ) );
		this.controlsTabLabel.setForeground( Color.YELLOW.darker( ) );
		this.advancedTabLabel.setForeground( Color.YELLOW.darker( ) );

		this.fpsColorButton.setBackground( new Color( this.initFPSColor ) );

		this.setVisible( true );

		repaint( );

	}

	public void setCurrentGameInstance( GamePanel instance ) {
		this.currentInstance = instance;
	}

	private void setAllElementsRecursively( Container root ) {

		root.setBackground( Color.BLACK );
		root.setForeground( Color.WHITE );

		for ( Component child : root.getComponents( ) ) {

			child.setBackground( Color.BLACK );
			child.setForeground( Color.WHITE );

			if ( child instanceof Container ) {
				setAllElementsRecursively( ( Container ) child );
			}

		}

	}

	private boolean settingChanged( ) {

		if ( this.smoothEdgesCB.isSelected( ) != this.initSmoothEdges ) {
			return true;
		}

		if ( this.fullscreenWindowCB.isSelected( ) != this.initFullScreenWindow ) {
			return true;
		}

		if ( this.displayFPSCB.isSelected( ) != this.initDisplayFPS ) {
			return true;
		}

		if ( this.displayFPSLocationCB.getSelectedIndex( ) != this.initFPSLocation ) {
			return true;
		}

		if ( this.fpsColorButton.getBackground( ).getRGB( ) != this.initFPSColor ) {
			return true;
		}

		if ( this.displayRotationDataCB.isSelected( ) != this.initDisplayRotationData ) {
			return true;
		}

		if ( this.displayBoundingBoxesCB.isSelected( ) != this.initDisplayBoundingBoxes ) {
			return true;
		}

		if ( this.capFPSCB.isSelected( ) != this.initCapFPS ) {
			return true;
		}

		int fpsLimitTemp = 0;
		try {
			fpsLimitTemp = Integer.parseInt( this.fpsLimitTF.getText( ) );
		} catch ( NumberFormatException e ) {
			return true;
		}

		if ( fpsLimitTemp != this.initFPSLimit ) {
			return true;
		}

		if ( this.hudCB.isSelected( ) != this.initHUD ) {
			return true;
		}

		if ( this.mortarLandingAssistCB.isSelected( ) != this.initMortarLandingAssist ) {
			return true;
		}

		if ( this.bombPowerIndicatorCB.isSelected( ) != this.initBombPowerIndicator ) {
			return true;
		}

		if ( this.renderDistanceS.getValue( ) != this.initRenderDistance ) {
			return true;
		}

		if ( this.bgVolumeS.getValue( ) != this.initBGVolume ) {
			return true;
		}

		if ( this.sfxVolumeS.getValue( ) != this.initSFXVolume ) {
			return true;
		}

		if ( this.generateFogCB.isSelected( ) != this.initGenerateFog ) {
			return true;
		}

		return false;
	}

	private void initSettings( ) {

		this.initSmoothEdges = cfg.getSetting( SettingKeys.SMOOTH_BLOCK_EDGES, DEFAULT_SETTINGS.SETTING_SMOOTH_EDGES );

		this.initFullScreenWindow = cfg.getSetting( SettingKeys.FULLSCREEN,
				DEFAULT_SETTINGS.SETTING_FULLSCREEN_WINDOW );

		this.initDisplayFPS = cfg.getSetting( SettingKeys.DISPLAY_FPS, DEFAULT_SETTINGS.SETTING_DISPLAY_FPS );
		this.initFPSLocation = cfg.getSetting( SettingKeys.FPS_DISPLAY_LOCATION,
				DEFAULT_SETTINGS.SETTING_FPS_LOCATION );
		this.initFPSColor = ( int ) Long.parseLong( cfg.getSetting( SettingKeys.FPS_DISPLAY_COLOR, "FFFFFFFF" ), 16 );

		this.initDisplayRotationData = cfg.getSetting( SettingKeys.DISPLAY_ROTATION_DATA,
				DEFAULT_SETTINGS.SETTING_DISPLAY_ROTATION_DATA );

		this.initDisplayBoundingBoxes = cfg.getSetting( SettingKeys.DISPLAY_BOUNDING_BOXES,
				DEFAULT_SETTINGS.SETTING_DISPLAY_BOUNDING_BOXES );

		this.initCapFPS = cfg.getSetting( SettingKeys.CAP_FPS, DEFAULT_SETTINGS.SETTING_CAP_FPS );
		this.initFPSLimit = cfg.getSetting( SettingKeys.FPS_LIMIT, DEFAULT_SETTINGS.SETTING_FPS_LIMIT );

		this.initHUD = cfg.getSetting( SettingKeys.HUD, DEFAULT_SETTINGS.SETTING_HUD );

		this.initMortarLandingAssist = cfg.getSetting( SettingKeys.MORTAR_LANDING_ASSIST,
				DEFAULT_SETTINGS.SETTING_MORTAR_LANDING_ASSIST );
		this.initBombPowerIndicator = cfg.getSetting( SettingKeys.BOMB_POWER_INDICATOR,
				DEFAULT_SETTINGS.SETTING_BOMB_POWER_INDICATOR );

		this.initRenderDistance = cfg.getSetting( SettingKeys.RENDER_DISTANCE,
				DEFAULT_SETTINGS.SETTING_RENDER_DISTANCE );

		this.initBGVolume = cfg.getSetting( SettingKeys.BG_AUDIO_VOLUME, DEFAULT_SETTINGS.SETTING_BG_VOLUME );

		this.initSFXVolume = cfg.getSetting( SettingKeys.SFX_AUDIO_VOLUME, DEFAULT_SETTINGS.SETTING_SFX_VOLUME );

		this.initGenerateFog = cfg.getSetting( SettingKeys.GENERATE_FOG, DEFAULT_SETTINGS.SETTING_GENERATE_FOG );

	}

	// Saves all current settings and applies them to the current game session,
	// in addition this method writes all settings out to a local file.
	private boolean saveSettings( ) {

		boolean restartRequired = false;

		// Smooth Block Edges
		this.initSmoothEdges = this.smoothEdgesCB.isSelected( );
		cfg.modifySetting( SettingKeys.SMOOTH_BLOCK_EDGES, "" + this.initSmoothEdges );

		if ( this.fullscreenWindowCB.isSelected( ) != this.initFullScreenWindow ) {
			restartRequired = true;
		}

		this.initFullScreenWindow = this.fullscreenWindowCB.isSelected( );
		cfg.modifySetting( SettingKeys.FULLSCREEN, "" + this.initFullScreenWindow );

		// FPS Settings
		this.initCapFPS = this.capFPSCB.isSelected( );
		cfg.modifySetting( SettingKeys.CAP_FPS, "" + this.initCapFPS );

		try {
			this.initFPSLimit = Integer.parseInt( this.fpsLimitTF.getText( ) );
		} catch ( NumberFormatException e ) {
			this.initFPSLimit = DEFAULT_SETTINGS.SETTING_FPS_LIMIT;
		}

		if ( this.initFPSLimit == 0 ) {
			this.initFPSLimit++;
		}

		cfg.modifySetting( SettingKeys.FPS_LIMIT, "" + this.initFPSLimit );

		// FPS Counter Settings
		this.initDisplayFPS = this.displayFPSCB.isSelected( );
		cfg.modifySetting( SettingKeys.DISPLAY_FPS, "" + this.initDisplayFPS );
		this.initFPSLocation = displayFPSLocationCB.getSelectedIndex( );
		cfg.modifySetting( SettingKeys.FPS_DISPLAY_LOCATION, "" + this.initFPSLocation );
		this.initFPSColor = this.fpsColorButton.getBackground( ).getRGB( );
		cfg.modifySetting( SettingKeys.FPS_DISPLAY_COLOR, Integer.toHexString( this.initFPSColor ) );

		// Debug rotation data settings.
		this.initDisplayRotationData = this.displayRotationDataCB.isSelected( );
		cfg.modifySetting( SettingKeys.DISPLAY_ROTATION_DATA, "" + this.initDisplayRotationData );

		// Debug bounding boxes
		this.initDisplayBoundingBoxes = this.displayBoundingBoxesCB.isSelected( );
		cfg.modifySetting( SettingKeys.DISPLAY_BOUNDING_BOXES, "" + this.initDisplayBoundingBoxes );

		this.initHUD = this.hudCB.isSelected( );
		cfg.modifySetting( SettingKeys.HUD, "" + this.initHUD );

		this.initMortarLandingAssist = this.mortarLandingAssistCB.isSelected( );
		cfg.modifySetting( SettingKeys.MORTAR_LANDING_ASSIST, "" + this.initMortarLandingAssist );

		this.initBombPowerIndicator = this.bombPowerIndicatorCB.isSelected( );
		cfg.modifySetting( SettingKeys.BOMB_POWER_INDICATOR, "" + this.initBombPowerIndicator );

		this.initRenderDistance = this.renderDistanceS.getValue( );
		cfg.modifySetting( SettingKeys.RENDER_DISTANCE, "" + this.initRenderDistance );

		this.initBGVolume = this.bgVolumeS.getValue( );
		cfg.modifySetting( SettingKeys.BG_AUDIO_VOLUME, "" + this.initBGVolume );

		this.initSFXVolume = this.sfxVolumeS.getValue( );
		cfg.modifySetting( SettingKeys.SFX_AUDIO_VOLUME, "" + this.initSFXVolume );

		this.initGenerateFog = this.generateFogCB.isSelected( );
		cfg.modifySetting( SettingKeys.GENERATE_FOG, "" + this.initGenerateFog );

		cfg.flushSettings( );

		return restartRequired;
	}

	private void defaultSettings( ) {
		this.smoothEdgesCB.setSelected( DEFAULT_SETTINGS.SETTING_SMOOTH_EDGES );
		this.displayFPSCB.setSelected( DEFAULT_SETTINGS.SETTING_DISPLAY_FPS );
		this.displayFPSLocationCB.setSelectedIndex( DEFAULT_SETTINGS.SETTING_FPS_LOCATION );
		this.fpsColorButton.setBackground( new Color( DEFAULT_SETTINGS.SETTING_FPS_COLOR ) );
		this.generateFogCB.setSelected( DEFAULT_SETTINGS.SETTING_GENERATE_FOG );

		this.displayRotationDataCB.setSelected( DEFAULT_SETTINGS.SETTING_DISPLAY_ROTATION_DATA );
		this.displayBoundingBoxesCB.setSelected( DEFAULT_SETTINGS.SETTING_DISPLAY_BOUNDING_BOXES );

		this.displayFPSLocationCB.setEnabled( displayFPSCB.isSelected( ) );
		this.fpsColorButton.setEnabled( displayFPSCB.isSelected( ) );

		this.capFPSCB.setSelected( DEFAULT_SETTINGS.SETTING_CAP_FPS );
		this.fpsLimitTF.setText( "" + DEFAULT_SETTINGS.SETTING_FPS_LIMIT );

		this.fullscreenWindowCB.setSelected( DEFAULT_SETTINGS.SETTING_FULLSCREEN_WINDOW );

		this.fpsLimitTF.setEnabled( this.capFPSCB.isSelected( ) );

		this.hudCB.setSelected( DEFAULT_SETTINGS.SETTING_HUD );

		this.bombPowerIndicatorCB.setSelected( DEFAULT_SETTINGS.SETTING_BOMB_POWER_INDICATOR );
		this.mortarLandingAssistCB.setSelected( DEFAULT_SETTINGS.SETTING_MORTAR_LANDING_ASSIST );

		if ( settingChanged( ) ) {
			OptionsMenu.this.savedSettingsText.setText( OptionsMenu.UNSAVED_SETTING );
			OptionsMenu.this.savedSettingsText.setForeground( Color.RED );
		} else {
			OptionsMenu.this.savedSettingsText.setText( OptionsMenu.SETTINGS_UP_TO_DATE );
			OptionsMenu.this.savedSettingsText.setForeground( Color.BLACK );
		}

	}

	@Override
	public void updateGraphicsSettings( boolean allowRestart ) {}

	@Override
	public void onResize( int newWidth, int newHeight ) {
		this.setSize( newWidth, newHeight );
		this.returnLabel.setLocation( 20, this.getHeight( ) - 150 );
		this.saveLabel.setLocation( 400, this.getHeight( ) - 150 );
		this.defaultLabel.setLocation( 700, this.getHeight( ) - 150 );
		this.menuContainer.setSize( this.getWidth( ), this.getHeight( ) );
	}

	@Override
	public void onDestroy( ) {
		System.gc( );
	}

	@Override
	public void addInputControls( ) {
		this.mcl = new MouseListener( ) {

			@Override
			public void mouseReleased( MouseEvent e ) {}

			@Override
			public void mousePressed( MouseEvent e ) {

				OptionsMenu.this.repaint( );

				int mousex = e.getX( );
				int mousey = e.getY( );

				if ( OptionsMenu.this.returnLabel.isMouseInBounds( mousex, mousey ) ) {
					OptionsMenu.this.returnLabel.onClick( );
				} else if ( OptionsMenu.this.saveLabel.isMouseInBounds( mousex, mousey ) ) {
					OptionsMenu.this.saveLabel.onClick( );
				} else if ( OptionsMenu.this.defaultLabel.isMouseInBounds( mousex, mousey ) ) {
					OptionsMenu.this.defaultLabel.onClick( );
				}

			}

			@Override
			public void mouseExited( MouseEvent e ) {}

			@Override
			public void mouseEntered( MouseEvent e ) {}

			@Override
			public void mouseClicked( MouseEvent e ) {}
		};

		this.addMouseListener( this.mcl );

		this.mml = new MouseMotionListener( ) {

			@Override
			public void mouseMoved( MouseEvent e ) {

				int mousex = e.getX( );
				int mousey = e.getY( );

				if ( OptionsMenu.this.returnLabel.isMouseInBounds( mousex, mousey ) ) {
					OptionsMenu.this.returnLabel.setHighlighted( true );
					OptionsMenu.this.returnLabel.repaint( );
				} else {
					OptionsMenu.this.returnLabel.setHighlighted( false );
					OptionsMenu.this.returnLabel.repaint( );
				}

				if ( OptionsMenu.this.saveLabel.isMouseInBounds( mousex, mousey ) ) {
					OptionsMenu.this.saveLabel.setHighlighted( true );
					OptionsMenu.this.saveLabel.repaint( );
				} else {
					OptionsMenu.this.saveLabel.setHighlighted( false );
					OptionsMenu.this.saveLabel.repaint( );
				}

				if ( OptionsMenu.this.defaultLabel.isMouseInBounds( mousex, mousey ) ) {
					OptionsMenu.this.defaultLabel.setHighlighted( true );
					OptionsMenu.this.defaultLabel.repaint( );
				} else {
					OptionsMenu.this.defaultLabel.setHighlighted( false );
					OptionsMenu.this.defaultLabel.repaint( );
				}

			}

			@Override
			public void mouseDragged( MouseEvent e ) {}
		};

		this.addMouseMotionListener( this.mml );

	}

	public void setUpgradeState( ) {

	}

	@Override
	public void removeInputControls( ) {

		this.removeMouseListener( this.mcl );
		this.removeMouseMotionListener( this.mml );

	}

	@Override
	public void terminate( ) {

		cfg.modifySetting( SettingKeys.BG_AUDIO_VOLUME, "" + this.initBGVolume );

		super.terminate( );

	}

	private class CHECK_SETTINGS_CHANGES implements ActionListener {

		@Override
		public void actionPerformed( ActionEvent arg0 ) {

			if ( OptionsMenu.this.settingChanged( ) ) {
				OptionsMenu.this.savedSettingsText.setText( OptionsMenu.UNSAVED_SETTING );
				OptionsMenu.this.savedSettingsText.setForeground( Color.RED );
			} else {
				OptionsMenu.this.savedSettingsText.setText( OptionsMenu.SETTINGS_UP_TO_DATE );
				OptionsMenu.this.savedSettingsText.setForeground( Color.WHITE );
			}

		}

	}

	@Override
	protected void paintComponent( Graphics g ) {
		super.paintComponent( g );

		if ( this.settingIcon != null ) {
			g.drawImage( this.settingIcon, 5, 5, null );
		}

		g.setFont( C.Fonts.MENU_FONT );
		g.setColor( Color.YELLOW.darker( ) );
		g.drawString( "Settings", 110, 65 );

	}

	@Override
	public void onPause( ) {}

	@Override
	public void onResume( ) {}

	@Override
	public void onCreate( ) {}

}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * VERSION INFORMATION:
 * $Date: 2018-01-27 09:50:40 -0500 (Sat, 27 Jan 2018) $
 * $Revision: 305 $
 * $Author: mbartgis $
 */

package com.rps.gui.menu;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class DocumentInputLimiter extends PlainDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8846906999839040621L;

	public enum TEXT_ALLOWANCE_POLICY {
		ALL_CHARACTERS, LETTERS_ONLY, NUMBERS_ONLY, LOWER_CASE_LETTERS_ONLY, UPPER_CASE_LETTERS_ONLY
	}

	private int						lowerBound, upperBound;

	private int						limit;

	private TEXT_ALLOWANCE_POLICY	tap;

	public DocumentInputLimiter( int text_limit ) {
		super( );
		this.limit = text_limit;
		this.tap = TEXT_ALLOWANCE_POLICY.ALL_CHARACTERS;
		this.lowerBound = Integer.MIN_VALUE;
		this.upperBound = Integer.MAX_VALUE;
	}

	public int getLowerBound( ) {
		return lowerBound;
	}

	public void setLowerBound( int lower ) {
		this.lowerBound = lower;
	}

	public int getUpperBound( ) {
		return upperBound;
	}

	public void setUpperBound( int upper ) {
		this.upperBound = upper;
	}

	public int getLimit( ) {
		return limit;
	}

	public void setLimit( int text_limit ) {
		this.limit = text_limit;
	}

	public TEXT_ALLOWANCE_POLICY getTextAllowancePolicy( ) {
		return tap;
	}

	public void setTextAllowancePolicy( TEXT_ALLOWANCE_POLICY tapolicy ) {
		this.tap = tapolicy;
	}

	public void insertString( int offset, String str, AttributeSet attr ) throws BadLocationException {
		if ( str == null ) {
			return;
		}

		if ( !insertedFollowsTextAllowancePolicy( this.getText( 0, this.getLength( ) ) + str ) ) {
			return;
		}

		// Check to see if a number is in the predefined bounds
		if ( this.tap == TEXT_ALLOWANCE_POLICY.NUMBERS_ONLY ) {
			try {
				int num = Integer.parseInt( this.getText( 0, this.getLength( ) ) + str );
				if ( num < this.lowerBound || num > this.upperBound ) {
					return;
				}
			} catch ( NumberFormatException e ) {
				// A document was added to a field with an existing dataset.
			}
		}

		if ( ( getLength( ) + str.length( ) ) <= limit ) {
			super.insertString( offset, str, attr );
		}
	}

	private boolean insertedFollowsTextAllowancePolicy( String str ) {
		switch ( this.tap ) {
			case ALL_CHARACTERS:
				return true;
			case LETTERS_ONLY:
				for ( int i = 0; i < str.length( ); i++ ) {
					char c = str.charAt( i );
					if ( c < 'A' || ( c > 'Z' && c < 'a' ) || c > 'z' ) {
						return false;
					}
				}
				return true;
			case LOWER_CASE_LETTERS_ONLY:
				for ( int i = 0; i < str.length( ); i++ ) {
					char c = str.charAt( i );
					if ( c < 'a' || c > 'z' ) {
						return false;
					}
				}
				return true;
			case NUMBERS_ONLY:
				for ( int i = 0; i < str.length( ); i++ ) {
					char c = str.charAt( i );
					if ( c < '0' || c > '9' ) {
						return false;
					}
				}
				return true;
			case UPPER_CASE_LETTERS_ONLY:
				for ( int i = 0; i < str.length( ); i++ ) {
					char c = str.charAt( i );
					if ( c < 'A' || c > 'Z' ) {
						return false;
					}
				}
				return true;
			default:
				return false;
		}
	}

}
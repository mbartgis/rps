/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-28 22:44:18 -0500 (Sun, 28 Jan 2018) $
 * $Revision: 318 $
 * $Author: mbartgis $
 */

package com.rps.gui.menu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JLabel;

@SuppressWarnings( "serial" )
public abstract class SelectionLabel extends JLabel {

	private boolean	isHighlighted;

	private Color	outline;

	public SelectionLabel( String text ) {
		super( text );
		this.isHighlighted = false;
		this.outline = null;
	}

	public boolean isMouseInBounds( int x, int y ) {

		int labelx = SelectionLabel.this.getX( );
		int labely = SelectionLabel.this.getY( );

		int labelw = SelectionLabel.this.getWidth( );
		int labelh = SelectionLabel.this.getHeight( );

		if ( x >= labelx && x <= ( labelx + labelw ) ) {
			if ( y >= labely && y <= ( labely + labelh ) ) {
				return true;
			}
		}
		return false;
	}

	public boolean isHighlighted( ) {
		return isHighlighted;
	}

	public void setHighlighted( boolean isHighlighted ) {
		this.isHighlighted = isHighlighted;
	}

	@Override
	protected void paintComponent( Graphics g ) {

		Graphics2D g2d = ( Graphics2D ) g.create( );

		if ( this.outline != null ) {
			g2d.setColor( outline );
			if ( this.getParent( ) == null ) {
				g2d.drawRect( this.getX( ), this.getY( ), this.getWidth( ), this.getHeight( ) );
			} else {
				g2d.drawRect( 0, 0, this.getWidth( ), this.getHeight( ) );
			}

		}
		if ( isHighlighted ) {
			g2d.setColor( getBackground( ) );
			if ( this.getParent( ) == null ) {
				g.drawString( this.getText( ), this.getX( ),
						this.getY( ) + ( this.getHeight( ) / 2 ) + ( this.getFont( ).getSize( ) / 2 ) );
				g2d.fillRect( this.getX( ), this.getY( ), getWidth( ), getHeight( ) );
			} else {
				g2d.fillRect( 0, 0, getWidth( ), getHeight( ) );
				super.paintComponent( g2d );
			}
		} else {
			if ( this.getParent( ) == null ) {
				g.drawString( this.getText( ), this.getX( ),
						this.getY( ) + ( this.getHeight( ) / 2 ) + ( this.getFont( ).getSize( ) / 2 ) );
			} else {
				super.paintComponent( g2d );
			}
		}

		g2d.dispose( );
	}

	public Color getOutline( ) {
		return outline;
	}

	public void setOutline( Color outline ) {
		this.outline = outline;
	}

	public abstract void onClick( );

}

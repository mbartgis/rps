/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-04-01 05:16:57 -0400 (Sun, 01 Apr 2018) $
 * $Revision: 339 $
 * $Author: mbartgis $
 */

package com.rps.gui.menu;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import com.rps.gui.ModularPanel;
import com.rpscore.C;
import com.rpscore.audio.SoundManager;
import com.rpscore.game.PlayerManager;
import com.rpscore.lib.FileUtils;

public class CharacterPerkMenu extends ModularPanel {

	private static final String	UPGRADE				= "Upgrade";

	private static final String	UNLOCK				= "Unlock";

	private PlayerManager		pmRef;

	private JLabel				nameLabel;

	private JLabel				remainingPerkTokensLabel;

	private SelectionLabel		returnLabel;

	private MouseListener		mcl;

	private MouseMotionListener	mml;

	private JLabel				healthStat, magicStat, staminaStat;

	private JButton				healthUp, magicUp, staminaUp;

	private JLabel				bulletSpeedStat, bulletDamageStat, bulletDurationStat, bulletCDStat;

	private JButton				bulletSpeedUp, bulletDamageUp, bulletDurationUp, bulletCDDown;

	private JLabel				bombPowerStat, bombThrowStat;

	private JButton				bombPowerUp, bombThrowUp;

	private JLabel				unlockMortarStat, unlockLandminesStat;

	private JButton				unlockMortar, unlockLandmines;

	private JLabel				armorStat, speedStat;
	private JButton				incArmor, decArmor;

	public static final int		BOMB_POWER_MAX		= 1000;
	public static final int		BOMB_THROW_MAX		= 1000;
	public static final int		BULLET_DAMAGE_MAX	= 100;
	public static final int		BULLET_DURATION_MAX	= 360;
	public static final int		BULLET_COOLDOWN_MIN	= 0;

	public CharacterPerkMenu( PlayerManager pm ) {
		super( );

		if ( pm == null ) {
			throw new IllegalArgumentException( "Null player data object!" );
		}

		this.pmRef = pm;

		this.setLayout( null );
		this.setBackground( Color.BLACK );

		this.remainingPerkTokensLabel = new JLabel( "Remaining Perk Tokens: " + pmRef.getRemainingTokens( ) );
		this.remainingPerkTokensLabel.setSize( 600, 100 );
		this.remainingPerkTokensLabel.setLocation( this.getWidth( ) - 450, this.getHeight( ) - 130 );
		this.remainingPerkTokensLabel.setForeground( Color.WHITE );
		this.remainingPerkTokensLabel.setFont( C.Fonts.MENU_MOTD_FONT );

		this.add( this.remainingPerkTokensLabel );

		// Menu Buttons
		/* @formatter:off */
		this.returnLabel = new SelectionLabel( " Return" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				terminate( );
			}
		};
		this.returnLabel.setSize( 300, 100 );
		this.returnLabel.setLocation( 20, this.getHeight( ) - 150 );
		this.returnLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.returnLabel.setForeground( Color.WHITE );
		this.returnLabel.setFont( C.Fonts.MENU_FONT );

		this.add( this.returnLabel );

		this.nameLabel = new JLabel( pmRef.getName( ) );
		this.nameLabel.setFont( C.Fonts.MENU_FONT );
		this.nameLabel.setSize( 2000, 60 );
		this.nameLabel.setLocation( 20, 20 );
		this.nameLabel.setForeground( Color.WHITE );
		this.add( this.nameLabel );

		this.healthStat = new JLabel( "Health: " + this.pmRef.getHealth( ) );
		this.healthStat.setFont( C.Fonts.MENU_MOTD_FONT );
		this.healthStat.setSize( 200, 40 );
		this.healthStat.setLocation( 20, 120 );
		this.healthStat.setForeground( Color.YELLOW.darker( ) );
		this.add( this.healthStat );

		// TODO make this look better.
		Image i = null;
		try {
			i = FileUtils.importImage( C.File.ICONS_FOLDER + "buttons" + C.File.FS + "up_20.png" );
		} catch ( MalformedURLException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace( );
		} catch ( IOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace( );
		}

		this.healthUp = new JButton( new ImageIcon( i ) );
		this.healthUp.setBackground( Color.BLACK );
		this.healthUp.setBorder( null );
		this.healthUp.setSize( 100, 40 );
		this.healthUp.setLocation( 250, 120 );
		this.healthUp.addActionListener( new ButtonListener( this.healthUp ) );
		this.add( this.healthUp );

		this.magicStat = new JLabel( "Magic: " + this.pmRef.getMagic( ) );
		this.magicStat.setFont( C.Fonts.MENU_MOTD_FONT );
		this.magicStat.setSize( 200, 40 );
		this.magicStat.setLocation( 20, 180 );
		this.magicStat.setForeground( Color.YELLOW.darker( ) );
		this.add( this.magicStat );

		this.magicUp = new JButton( UPGRADE );
		this.magicUp.setSize( 100, 40 );
		this.magicUp.setLocation( 250, 180 );
		this.magicUp.addActionListener( new ButtonListener( this.magicUp ) );
		this.add( this.magicUp );

		this.staminaStat = new JLabel( "Stamina: " + this.pmRef.getStamina( ) );
		this.staminaStat.setFont( C.Fonts.MENU_MOTD_FONT );
		this.staminaStat.setSize( 200, 40 );
		this.staminaStat.setLocation( 20, 240 );
		this.staminaStat.setForeground( Color.YELLOW.darker( ) );
		this.add( this.staminaStat );

		this.staminaUp = new JButton( UPGRADE );
		this.staminaUp.setSize( 100, 40 );
		this.staminaUp.setLocation( 250, 240 );
		this.staminaUp.addActionListener( new ButtonListener( this.staminaUp ) );
		this.add( this.staminaUp );

		this.bulletDamageStat = new JLabel( "Bullet Damage: " + this.pmRef.getBulletDamage( ) );
		this.bulletDamageStat.setFont( C.Fonts.MENU_MOTD_FONT );
		this.bulletDamageStat.setSize( 320, 40 );
		this.bulletDamageStat.setLocation( 20, 360 );
		this.bulletDamageStat.setForeground( Color.YELLOW.darker( ) );
		this.add( this.bulletDamageStat );

		this.bulletDamageUp = new JButton( UPGRADE );
		this.bulletDamageUp.setSize( 100, 40 );
		this.bulletDamageUp.setLocation( 370, 360 );
		this.bulletDamageUp.addActionListener( new ButtonListener( this.bulletDamageUp ) );
		this.add( this.bulletDamageUp );

		this.bulletDurationStat = new JLabel( "Bullet Duration: " + this.pmRef.getBulletDuration( ) );
		this.bulletDurationStat.setFont( C.Fonts.MENU_MOTD_FONT );
		this.bulletDurationStat.setSize( 320, 40 );
		this.bulletDurationStat.setLocation( 20, 420 );
		this.bulletDurationStat.setForeground( Color.YELLOW.darker( ) );
		this.add( this.bulletDurationStat );

		this.bulletDurationUp = new JButton( UPGRADE );
		this.bulletDurationUp.setSize( 100, 40 );
		this.bulletDurationUp.setLocation( 370, 420 );
		this.bulletDurationUp.addActionListener( new ButtonListener( this.bulletDurationUp ) );
		this.add( this.bulletDurationUp );

		this.bulletCDStat = new JLabel( "Bullet Cooldown: " + this.pmRef.getBulletCooldown( ) );
		this.bulletCDStat.setFont( C.Fonts.MENU_MOTD_FONT );
		this.bulletCDStat.setSize( 320, 40 );
		this.bulletCDStat.setLocation( 20, 480 );
		this.bulletCDStat.setForeground( Color.YELLOW.darker( ) );
		this.add( this.bulletCDStat );

		this.bulletCDDown = new JButton( UPGRADE );
		this.bulletCDDown.setSize( 100, 40 );
		this.bulletCDDown.setLocation( 370, 480 );
		this.bulletCDDown.addActionListener( new ButtonListener( this.bulletCDDown ) );
		this.add( this.bulletCDDown );

		this.bombPowerStat = new JLabel( "Bomb Power: " + this.pmRef.getBombPower( ) );
		this.bombPowerStat.setFont( C.Fonts.MENU_MOTD_FONT );
		this.bombPowerStat.setSize( 320, 40 );
		this.bombPowerStat.setLocation( 20, 600 );
		this.bombPowerStat.setForeground( Color.YELLOW.darker( ) );
		this.add( this.bombPowerStat );

		this.bombPowerUp = new JButton( UPGRADE );
		this.bombPowerUp.setSize( 100, 40 );
		this.bombPowerUp.setLocation( 370, 600 );
		this.bombPowerUp.addActionListener( new ButtonListener( this.bombPowerUp ) );
		this.add( this.bombPowerUp );

		this.bombThrowStat = new JLabel( "Bomb Throw: " + this.pmRef.getBombThrow( ) );
		this.bombThrowStat.setFont( C.Fonts.MENU_MOTD_FONT );
		this.bombThrowStat.setSize( 320, 40 );
		this.bombThrowStat.setLocation( 20, 660 );
		this.bombThrowStat.setForeground( Color.YELLOW.darker( ) );
		this.add( this.bombThrowStat );

		this.bombThrowUp = new JButton( UPGRADE );
		this.bombThrowUp.setSize( 100, 40 );
		this.bombThrowUp.setLocation( 370, 660 );
		this.bombThrowUp.addActionListener( new ButtonListener( this.bombThrowUp ) );
		this.add( this.bombThrowUp );

		this.unlockMortarStat = new JLabel(
				this.pmRef.hasMortars( ) ? "Mortars Unlocked" : "Unlock Mortars (3 Tokens)" );
		this.unlockMortarStat.setFont( C.Fonts.MENU_MOTD_FONT );
		this.unlockMortarStat.setSize( 500, 40 );
		this.unlockMortarStat.setLocation( 700, 120 );

		if ( this.pmRef.hasMortars( ) ) {
			this.unlockMortarStat.setForeground( Color.GREEN.darker( ) );
		} else {
			this.unlockMortarStat.setForeground( Color.YELLOW.darker( ) );
		}

		this.add( this.unlockMortarStat );

		this.unlockMortar = new JButton( UNLOCK );
		this.unlockMortar.setSize( 100, 40 );
		this.unlockMortar.setLocation( 1200, 120 );
		this.unlockMortar.addActionListener( new ButtonListener( this.unlockMortar ) );
		this.add( this.unlockMortar );

		//
		this.unlockLandminesStat = new JLabel(
				this.pmRef.hasLandmines( ) ? "Landmines Unlocked" : "Unlock Landmines (4 Tokens)" );
		this.unlockLandminesStat.setFont( C.Fonts.MENU_MOTD_FONT );
		this.unlockLandminesStat.setSize( 500, 40 );
		this.unlockLandminesStat.setLocation( 700, 180 );

		if ( this.pmRef.hasLandmines( ) ) {
			this.unlockLandminesStat.setForeground( Color.GREEN.darker( ) );
		} else {
			this.unlockLandminesStat.setForeground( Color.YELLOW.darker( ) );
		}

		this.add( this.unlockLandminesStat );

		this.unlockLandmines = new JButton( UNLOCK );
		this.unlockLandmines.setSize( 100, 40 );
		this.unlockLandmines.setLocation( 1200, 180 );
		this.unlockLandmines.addActionListener( new ButtonListener( this.unlockLandmines ) );
		this.add( this.unlockLandmines );

		this.armorStat = new JLabel( "Armor: " + pmRef.getDiscreteArmorRating( ) + "%" );
		this.armorStat.setFont( C.Fonts.MENU_MOTD_FONT );
		this.armorStat.setSize( 320, 40 );
		this.armorStat.setLocation( 700, 600 );
		this.armorStat.setForeground( Color.CYAN.darker( ) );
		this.add( this.armorStat );

		this.speedStat = new JLabel( "Movement Speed: " + String.format( "%1$.1f", pmRef.getMovementSpeed( ) ) );
		this.speedStat.setFont( C.Fonts.MENU_MOTD_FONT );
		this.speedStat.setSize( 400, 40 );
		this.speedStat.setLocation( 700, 660 );
		this.speedStat.setForeground( Color.CYAN.darker( ) );
		this.add( this.speedStat );

		this.incArmor = new JButton( "+" );
		this.incArmor.setSize( 100, 40 );
		this.incArmor.setLocation( 1200, 600 );
		this.incArmor.addActionListener( new ActionListener( ) {

			@Override
			public void actionPerformed( ActionEvent e ) {

				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );

				pmRef.setArmorRating( pmRef.getDiscreteArmorRating( ) + 5 );
				armorStat.setText( "Armor: " + pmRef.getDiscreteArmorRating( ) + "%" );
				speedStat.setText( "Movement Speed: " + String.format( "%1$.1f", pmRef.getMovementSpeed( ) ) );
				pmRef.save( );

				updateButtonAvailablity( );

			}
		} );
		this.add( this.incArmor );

		this.decArmor = new JButton( "-" );
		this.decArmor.setSize( 100, 40 );
		this.decArmor.setLocation( 1200, 660 );
		this.decArmor.addActionListener( new ActionListener( ) {

			@Override
			public void actionPerformed( ActionEvent arg0 ) {

				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );

				pmRef.setArmorRating( pmRef.getDiscreteArmorRating( ) - 5 );
				armorStat.setText( "Armor: " + pmRef.getDiscreteArmorRating( ) + "%" );
				speedStat.setText( "Movement Speed: " + String.format( "%1$.1f", pmRef.getMovementSpeed( ) ) );
				pmRef.save( );

				updateButtonAvailablity( );

			}
		} );
		this.add( this.decArmor );

		// Update all buttons
		updateButtonAvailablity( );
	}

	@Override
	public void updateGraphicsSettings( boolean allowRestart ) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResize( int newWidth, int newHeight ) {
		this.setSize( newWidth, newHeight );
		this.returnLabel.setLocation( 20, this.getHeight( ) - 150 );
		this.remainingPerkTokensLabel.setLocation( this.getWidth( ) - 450, this.getHeight( ) - 130 );
	}

	@Override
	public void onDestroy( ) {
		if ( this.pmRef != null ) {
			this.pmRef.save( );
			this.pmRef = null;
		}
		System.gc( );
	}

	@Override
	public void addInputControls( ) {
		this.mcl = new MouseListener( ) {

			@Override
			public void mouseReleased( MouseEvent e ) {}

			@Override
			public void mousePressed( MouseEvent e ) {

				CharacterPerkMenu.this.repaint( );

				int mousex = e.getX( );
				int mousey = e.getY( );

				if ( CharacterPerkMenu.this.returnLabel.isMouseInBounds( mousex, mousey ) ) {
					CharacterPerkMenu.this.returnLabel.onClick( );
				}

			}

			@Override
			public void mouseExited( MouseEvent e ) {}

			@Override
			public void mouseEntered( MouseEvent e ) {}

			@Override
			public void mouseClicked( MouseEvent e ) {}
		};

		this.addMouseListener( this.mcl );

		this.mml = new MouseMotionListener( ) {

			@Override
			public void mouseMoved( MouseEvent e ) {

				int mousex = e.getX( );
				int mousey = e.getY( );

				if ( CharacterPerkMenu.this.returnLabel.isMouseInBounds( mousex, mousey ) ) {
					CharacterPerkMenu.this.returnLabel.setHighlighted( true );
					CharacterPerkMenu.this.returnLabel.repaint( );
				} else {
					CharacterPerkMenu.this.returnLabel.setHighlighted( false );
					CharacterPerkMenu.this.returnLabel.repaint( );
				}

			}

			@Override
			public void mouseDragged( MouseEvent e ) {
				// TODO Auto-generated method stub

			}
		};

		this.addMouseMotionListener( this.mml );

	}

	public void setUpgradeState( ) {

	}

	@Override
	public void removeInputControls( ) {

		this.removeMouseListener( this.mcl );
		this.removeMouseMotionListener( this.mml );

	}

	private void updateButtonAvailablity( ) {

		if ( CharacterPerkMenu.this.pmRef.getRemainingTokens( ) <= 2 ) {
			CharacterPerkMenu.this.unlockMortar.setEnabled( false );
			CharacterPerkMenu.this.unlockMortar.setVisible( false );
		}

		if ( CharacterPerkMenu.this.pmRef.getRemainingTokens( ) <= 3 ) {
			CharacterPerkMenu.this.unlockLandmines.setEnabled( false );
			CharacterPerkMenu.this.unlockLandmines.setVisible( false );
		}

		if ( CharacterPerkMenu.this.pmRef.getRemainingTokens( ) <= 0 ) {

			// Disable all upgrade buttons if no tokens are available.

			CharacterPerkMenu.this.healthUp.setEnabled( false );
			CharacterPerkMenu.this.healthUp.setVisible( false );
			CharacterPerkMenu.this.magicUp.setEnabled( false );
			CharacterPerkMenu.this.magicUp.setVisible( false );
			CharacterPerkMenu.this.staminaUp.setEnabled( false );
			CharacterPerkMenu.this.staminaUp.setVisible( false );

			CharacterPerkMenu.this.bulletDamageUp.setEnabled( false );
			CharacterPerkMenu.this.bulletDamageUp.setVisible( false );
			CharacterPerkMenu.this.bulletDurationUp.setEnabled( false );
			CharacterPerkMenu.this.bulletDurationUp.setVisible( false );
			CharacterPerkMenu.this.bulletCDDown.setEnabled( false );
			CharacterPerkMenu.this.bulletCDDown.setVisible( false );

			CharacterPerkMenu.this.bombPowerUp.setEnabled( false );
			CharacterPerkMenu.this.bombPowerUp.setVisible( false );
			CharacterPerkMenu.this.bombThrowUp.setEnabled( false );
			CharacterPerkMenu.this.bombThrowUp.setVisible( false );
		} else {

			if ( this.pmRef.getBulletDamage( ) >= BULLET_DAMAGE_MAX ) {
				CharacterPerkMenu.this.bulletDamageUp.setEnabled( false );
				CharacterPerkMenu.this.bulletDamageUp.setVisible( false );
			}

			if ( this.pmRef.getBulletDuration( ) >= BULLET_DURATION_MAX ) {
				CharacterPerkMenu.this.bulletDurationUp.setEnabled( false );
				CharacterPerkMenu.this.bulletDurationUp.setVisible( false );
			}

			if ( this.pmRef.getBulletCooldown( ) <= BULLET_COOLDOWN_MIN ) {
				CharacterPerkMenu.this.bulletCDDown.setEnabled( false );
				CharacterPerkMenu.this.bulletCDDown.setVisible( false );
			}

			if ( this.pmRef.getBombPower( ) >= BOMB_POWER_MAX ) {
				CharacterPerkMenu.this.bombPowerUp.setEnabled( false );
				CharacterPerkMenu.this.bombPowerUp.setVisible( false );
			}

			if ( this.pmRef.getBombThrow( ) >= BOMB_THROW_MAX ) {
				CharacterPerkMenu.this.bombThrowUp.setEnabled( false );
				CharacterPerkMenu.this.bombThrowUp.setVisible( false );
			}

			if ( this.pmRef.hasMortars( ) ) {
				CharacterPerkMenu.this.unlockMortar.setEnabled( false );
				CharacterPerkMenu.this.unlockMortar.setVisible( false );
			}

			if ( this.pmRef.hasLandmines( ) ) {
				CharacterPerkMenu.this.unlockLandmines.setEnabled( false );
				CharacterPerkMenu.this.unlockLandmines.setVisible( false );
			}

		}

		if ( this.pmRef.getArmorRating( ) >= 0.73 ) {
			CharacterPerkMenu.this.incArmor.setEnabled( false );
			CharacterPerkMenu.this.incArmor.setVisible( false );
		} else {
			CharacterPerkMenu.this.incArmor.setEnabled( true );
			CharacterPerkMenu.this.incArmor.setVisible( true );
		}

		if ( this.pmRef.getArmorRating( ) <= 0.02 ) {
			CharacterPerkMenu.this.decArmor.setEnabled( false );
			CharacterPerkMenu.this.decArmor.setVisible( false );
		} else {
			CharacterPerkMenu.this.decArmor.setEnabled( true );
			CharacterPerkMenu.this.decArmor.setVisible( true );
		}

	}

	private class ButtonListener implements ActionListener {

		private JButton ref;

		private ButtonListener( JButton r ) {
			this.ref = r;
		}

		@Override
		public void actionPerformed( ActionEvent e ) {

			SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );

			if ( this.ref == CharacterPerkMenu.this.healthUp ) {
				CharacterPerkMenu.this.pmRef.setHealth( CharacterPerkMenu.this.pmRef.getHealth( ) + 20 );
				CharacterPerkMenu.this.healthStat.setText( "Health: " + CharacterPerkMenu.this.pmRef.getHealth( ) );
				CharacterPerkMenu.this.pmRef.save( );
			}

			if ( this.ref == CharacterPerkMenu.this.magicUp ) {
				CharacterPerkMenu.this.pmRef.setMagic( CharacterPerkMenu.this.pmRef.getMagic( ) + 20 );
				CharacterPerkMenu.this.magicStat.setText( "Magic: " + CharacterPerkMenu.this.pmRef.getMagic( ) );
				CharacterPerkMenu.this.pmRef.save( );
			}

			if ( this.ref == CharacterPerkMenu.this.staminaUp ) {
				CharacterPerkMenu.this.pmRef.setStamina( CharacterPerkMenu.this.pmRef.getStamina( ) + 20 );
				CharacterPerkMenu.this.staminaStat.setText( "Stamina: " + CharacterPerkMenu.this.pmRef.getStamina( ) );
				CharacterPerkMenu.this.pmRef.save( );
			}

			if ( this.ref == CharacterPerkMenu.this.bulletDamageUp ) {
				CharacterPerkMenu.this.pmRef.setBulletDamage( CharacterPerkMenu.this.pmRef.getBulletDamage( ) + 5 );
				CharacterPerkMenu.this.bulletDamageStat
						.setText( "Bullet Damage: " + CharacterPerkMenu.this.pmRef.getBulletDamage( ) );
				CharacterPerkMenu.this.pmRef.save( );
			}

			if ( this.ref == CharacterPerkMenu.this.bulletDurationUp ) {
				CharacterPerkMenu.this.pmRef
						.setBulletDuration( CharacterPerkMenu.this.pmRef.getBulletDuration( ) + 10 );
				CharacterPerkMenu.this.bulletDurationStat
						.setText( "Bullet Duration: " + CharacterPerkMenu.this.pmRef.getBulletDuration( ) );
				CharacterPerkMenu.this.pmRef.save( );
			}

			if ( this.ref == CharacterPerkMenu.this.bulletCDDown ) {
				CharacterPerkMenu.this.pmRef.setBulletCooldown( CharacterPerkMenu.this.pmRef.getBulletCooldown( ) - 3 );
				CharacterPerkMenu.this.bulletCDStat
						.setText( "Bullet Cooldown: " + CharacterPerkMenu.this.pmRef.getBulletCooldown( ) );
				CharacterPerkMenu.this.pmRef.save( );
			}

			if ( this.ref == CharacterPerkMenu.this.bombPowerUp ) {
				CharacterPerkMenu.this.pmRef.setBombPower( CharacterPerkMenu.this.pmRef.getBombPower( ) + 100 );
				CharacterPerkMenu.this.bombPowerStat
						.setText( "Bomb Power: " + CharacterPerkMenu.this.pmRef.getBombPower( ) );
				CharacterPerkMenu.this.pmRef.save( );
			}

			if ( this.ref == CharacterPerkMenu.this.bombThrowUp ) {
				CharacterPerkMenu.this.pmRef.setBombThrow( CharacterPerkMenu.this.pmRef.getBombThrow( ) + 100 );
				CharacterPerkMenu.this.bombThrowStat
						.setText( "Bomb Throw: " + CharacterPerkMenu.this.pmRef.getBombThrow( ) );
				CharacterPerkMenu.this.pmRef.save( );
			}

			if ( this.ref == CharacterPerkMenu.this.unlockMortar ) {
				CharacterPerkMenu.this.pmRef.unlockMortars( true );
				CharacterPerkMenu.this.unlockMortarStat.setForeground( Color.GREEN.darker( ) );
				CharacterPerkMenu.this.unlockMortarStat.setText( "Mortars Unlocked" );
				CharacterPerkMenu.this.pmRef.save( );
			}

			if ( this.ref == CharacterPerkMenu.this.unlockLandmines ) {
				CharacterPerkMenu.this.pmRef.unlockLandmines( true );
				CharacterPerkMenu.this.unlockLandminesStat.setForeground( Color.GREEN.darker( ) );
				CharacterPerkMenu.this.unlockLandminesStat.setText( "Landmines Unlocked" );
				CharacterPerkMenu.this.pmRef.save( );
			}

			CharacterPerkMenu.this.remainingPerkTokensLabel
					.setText( "Remaining Perk Tokens: " + pmRef.getRemainingTokens( ) );

			CharacterPerkMenu.this.updateButtonAvailablity( );

		}

	}

	@Override
	public void onPause( ) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume( ) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCreate( ) {}

}

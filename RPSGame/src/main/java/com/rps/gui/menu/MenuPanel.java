/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-07-26 22:16:38 -0400 (Thu, 26 Jul 2018) $
 * $Revision: 363 $
 * $Author: mbartgis $
 */

package com.rps.gui.menu;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.AbstractList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.Timer;

import com.rps.Launcher;
import com.rps.gui.MainWindow;
import com.rps.gui.ModularPanel;
import com.rps.gui.SwingGraphicsAdapter;
import com.rpscore.C;
import com.rpscore.audio.SoundManager;
import com.rpscore.cfg.ConfigManager;
import com.rpscore.entities.Entity;
import com.rpscore.entities.enemies.Bomber;
import com.rpscore.entities.enemies.Dub;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.entities.enemies.RogueCannon;
import com.rpscore.entities.spawned.weapons.Bomb;
import com.rpscore.entities.spawned.weapons.Mortar;
import com.rpscore.game.GameInstance;
import com.rpscore.game.PlayerManager;
import com.rpscore.gui.game.GraphicsAdapter;

@SuppressWarnings( "serial" )
public class MenuPanel extends ModularPanel {

	private static GameInstance		DUMMY_GI;

	private SelectionLabel			gameLabel, onlineGameLabel, editorLabel, optionsLabel, customizeLabel, exitLabel,
			loadProfileLabel, campaignLabel, helpLabel, statsLabel;

	/* @formatter:off */
	public static final String[ ]	MOTD		= new String[ ] {
			"Dubs are by far the weakest enemy, but they can spawn in numbers and can be a big distraction.",
			"The power level of a bomb is expressed by its color and a number on its side, if you see a red bomb, RUN!",
			"Ghosts are difficult to spot, move faster, and can even teleport while they are hollowed, make sure you keep track of them at all times!",
			"Dashers have limited stamina just like you do, take advantage of any exhausted ones.",
			"Bombers can only throw a bomb equal to their current AP, this may be either very good or bad!",
			"Do not let an injured Bomber go, once they reach a certain point of desperation, they may use more powerful explosives!",
			"Bombers do not care about friendly fire, but any kills they earn will not benefit you.",
			"When hollowed, a ghost can be nearly invisible and is immune to both bullets and bombs.",
			"The further you are away from the epicenter of a bomb explosion, the less damage you will take from it.",
			"The power level of a bomb expresses both its blast radius and damage potential.",
			"Pace yourself when running; the more exhausted you are, the longer it will take for your stamina to start regenerating.",
			"Shoot a bomb to set it off prematurely, just make sure it's far enough away!",
			"Shoot a Bomber's bomb to detonate it, any collateral damage caused by the bomb will benefit you!",
			"If a bomb is caught in an explosion it will immediately detonate.",
			"In the arena, a bonus is awarded after each round based on performance."
			/*
			"While the Overdraw ability allows you to deal incredible damage over a long range, it has many risks including the destruction of your bow.",
			"Those who invest into the art of earth magic will be granted great defensive abilities.",
			"Fire magic can be devastating to an opponent's health, but it can backfire if you manage to catch yourself on fire.",
			"Lightning magic is by far the most destructive art, but is also the most ravenous on your magic supply.",
			"Not all \"hot\" surfaces are created equal. For instance the center of a pool of lava is far more deadly than its shore.",
			"Mastering a skill takes time. It will take you longer to transition from an expert level to a master level than from a beginner to an intermediate.",
			"Not all bosses are created equal. The number of stars near the boss' health bar describes the rank. While higher ranks are more rare and far more difficult, the rewards are usually well worth it.",
			"While defeating enemies is a quick, sure-fire way to level up, it is not the only way. Everything you do contributes to your both your skills and level.",
			"Bows are fantastic weapons for taking out enemies afar, but they don't make for good weapons up close.",
			"The Skillshot ability allows your arrows to deal more damage based on enemy distance. The further the hit, the more damage is given.",
			"Ghost weapons can pass through walls and enemies, but do not offer the same damage as an equivalent regular weapon.",
			"There are 5 levels for each skill: Beginner, Intermediate, Advanced, Expert, Master.",
			"For those who dedicate enough time to a skill, mastery may not adequate to define his or her skill.",
			"Dungeons are home to great loot, but are also home to many unusually powerful enemies and occasionally a boss.",
			"Ghastly enemies can only be damaged by conventional means when they attack you. This is why unconventional means are often a good way to take them down."
			*/
	};
	/* @formatter:on */

	private static ConfigManager	cfgm	= ConfigManager.getConfigManager( );

	private int						motdIndex;

	private SoloPlayMenu			spm;
	private OnlineSessionMenu		osm;
	private LoadProfileMenu			lpm;

	private MouseListener			mcl;
	private MouseMotionListener		mml;

	private AbstractList< Enemy >	logo;

	private Timer					clock;

	private Bomb					bomb;

	private RogueCannon				rc;

	private boolean					isFullscreen;

	public MenuPanel( ) {
		super( );

		// Set up the profile management first and foremost.
		this.lpm = new LoadProfileMenu( );
		this.lpm.setSize( this.getWidth( ) / 3, ( int ) ( this.getHeight( ) * 0.75 ) );
		this.lpm.setLocation( this.getWidth( ) / 2, 100 );
		this.add( lpm );

		DUMMY_GI = new GameInstance( null, 0L, 33 );

		this.logo = new LinkedList< Enemy >( );

		this.isFullscreen = cfgm.getSetting( "Fullscreen", OptionsMenu.DEFAULT_SETTINGS.SETTING_FULLSCREEN_WINDOW );

		this.setLocation( 0, 0 );
		this.setLayout( null );

		this.spm = new SoloPlayMenu( this );
		this.spm.setSize( this.getWidth( ) / 3, ( int ) ( this.getHeight( ) * 0.75 ) );
		this.spm.setLocation( this.getWidth( ) / 2, 100 );
		this.add( spm );

		/* @formatter:off */
		this.campaignLabel = new SelectionLabel( " Campaign" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				osm.setVisible( false );
				lpm.setVisible( false );
				spm.setVisible( !spm.isVisible( ) );

			}
		};
		this.campaignLabel.setSize( 400, 100 );
		this.campaignLabel.setLocation( 100, 40 );
		this.campaignLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.campaignLabel.setForeground( Color.DARK_GRAY );
		this.campaignLabel.setFont( C.Fonts.MENU_FONT );

		/* @formatter:off */
		this.gameLabel = new SelectionLabel( " Arena" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				osm.setVisible( false );
				lpm.setVisible( false );
				spm.setVisible( !spm.isVisible( ) );
			}
		};
		this.gameLabel.setSize( 400, 100 );
		this.gameLabel.setLocation( 100, 40 );
		this.gameLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.gameLabel.setForeground( Color.WHITE );
		this.gameLabel.setFont( C.Fonts.MENU_FONT );

		this.osm = new OnlineSessionMenu( this );
		this.osm.setSize( this.getWidth( ) / 3, ( int ) ( this.getHeight( ) * 0.75 ) );
		this.osm.setLocation( this.getWidth( ) / 2, 100 );
		this.add( osm );

		/* @formatter:off */
		this.onlineGameLabel = new SelectionLabel( " Play Online" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				spm.setVisible( false );
				lpm.setVisible( false );
				osm.setVisible( !osm.isVisible( ) );

				// Container parentWindow = this.getParent( );
				// while ( ! ( parentWindow instanceof MainWindow ) ) {
				// parentWindow = parentWindow.getParent( );
				// }

				// ( ( MainWindow ) parentWindow ).startOnlineGame( );

			}
		};
		this.onlineGameLabel.setSize( 400, 100 );
		this.onlineGameLabel.setLocation( 100, 140 );
		this.onlineGameLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.onlineGameLabel.setForeground( Color.WHITE );
		this.onlineGameLabel.setFont( C.Fonts.MENU_FONT );

		/* @formatter:off */
		this.editorLabel = new SelectionLabel( " Editor" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				// TODO Auto-generated method stub

			}
		};
		this.editorLabel.setSize( 400, 100 );
		this.editorLabel.setLocation( 100, 240 );
		this.editorLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.editorLabel.setForeground( Color.DARK_GRAY );
		this.editorLabel.setFont( C.Fonts.MENU_FONT );

		/* @formatter:off */
		this.optionsLabel = new SelectionLabel( " Options" ) {
		/* @formatter:on */

			@Override
			@SuppressWarnings( "unused" )
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				OptionsMenu om = new OptionsMenu( );
				MainWindow.getMainWindow( ).pushPanel( om );
				// new OptionsMenu_Dep( MenuPanel.this );
			}
		};
		this.optionsLabel.setSize( 400, 100 );
		this.optionsLabel.setLocation( 100, 540 );
		this.optionsLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.optionsLabel.setForeground( Color.WHITE );
		this.optionsLabel.setFont( C.Fonts.MENU_FONT );

		/* @formatter:off */
		this.customizeLabel = new SelectionLabel( " Skills" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				String saveFolder = C.File.PLAYER_SAVEDATA_FOLDER + cfgm.getSetting( "LastProfile", "dummy" )
						+ C.File.FS;
				PlayerManager pm = PlayerManager.getPlayerManager( saveFolder, true, 0L );
				CharacterPerkMenu cpm = new CharacterPerkMenu( pm );
				MainWindow.getMainWindow( ).pushPanel( cpm );

			}

		};
		this.customizeLabel.setSize( 400, 100 );
		this.customizeLabel.setLocation( 100, 340 );
		this.customizeLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.customizeLabel.setForeground( Color.WHITE );
		this.customizeLabel.setFont( C.Fonts.MENU_FONT );

		/* @formatter:off */
		this.helpLabel = new SelectionLabel( " Help" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				try {
					URL url = new URL( "file:" + C.File.FS + C.File.FS + C.File.FS + C.File.MANUAL_IMDEX_PAGE );
					URI uri = url.toURI( );
					Desktop.getDesktop( ).browse( uri );
				} catch ( IOException e ) {
					e.printStackTrace( );
				} catch ( URISyntaxException e ) {
					e.printStackTrace( );
				}
			}
		};
		this.helpLabel.setSize( 400, 100 );
		this.helpLabel.setLocation( 100, 640 );
		this.helpLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.helpLabel.setForeground( Color.WHITE );
		this.helpLabel.setFont( C.Fonts.MENU_FONT );

		/* @formatter:off */
		this.exitLabel = new SelectionLabel( " Quit Game" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				MainWindow.getMainWindow( ).terminate( );
			}

		};
		this.exitLabel.setSize( 400, 100 );
		this.exitLabel.setLocation( 100, 740 );
		this.exitLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.exitLabel.setForeground( Color.WHITE );
		this.exitLabel.setFont( C.Fonts.MENU_FONT );

		/* @formatter:off */
		this.loadProfileLabel = new SelectionLabel( " Load Profile" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				spm.setVisible( false );
				osm.setVisible( false );
				lpm.setVisible( !lpm.isVisible( ) );
			}

		};
		this.loadProfileLabel.setSize( 400, 100 );
		this.loadProfileLabel.setLocation( 100, 240 );
		this.loadProfileLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.loadProfileLabel.setForeground( Color.WHITE );
		this.loadProfileLabel.setFont( C.Fonts.MENU_FONT );

		this.setBackground( new Color( 0xFF050505 ) );

		/* @formatter:off */
		this.statsLabel = new SelectionLabel( " Stats" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				String saveFolder = C.File.PLAYER_SAVEDATA_FOLDER + cfgm.getSetting( "LastProfile", "dummy" )
						+ C.File.FS;
				PlayerManager pm = PlayerManager.getPlayerManager( saveFolder, true, 0L );
				StatsMenu sm = new StatsMenu( pm );
				MainWindow.getMainWindow( ).pushPanel( sm );
			}

		};
		this.statsLabel.setSize( 400, 100 );
		this.statsLabel.setLocation( 100, 440 );
		this.statsLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.statsLabel.setForeground( Color.WHITE );
		this.statsLabel.setFont( C.Fonts.MENU_FONT );

		this.setBackground( new Color( 0xFF050505 ) );

		// this.add( this.campaignLabel );
		this.add( this.gameLabel );
		this.add( this.onlineGameLabel );
		// this.add( this.editorLabel );
		this.add( this.optionsLabel );
		this.add( this.customizeLabel );
		this.add( this.helpLabel );
		this.add( this.exitLabel );
		this.add( this.loadProfileLabel );
		this.add( this.statsLabel );

		Random r = new Random( );
		motdIndex = r.nextInt( MOTD.length );

		setupRPSLogo( );

		this.clock = new Timer( 16, new BGActionLoop( ) );

		this.clock.start( );

		this.repaint( );

		if ( Launcher.GLOBAL_PARAMETERS.IS_DEBUG && Launcher.GLOBAL_PARAMETERS.GAME_START ) {
			this.spm.quickStart( );
		}

	}

	@Override
	protected void paintComponent( Graphics g ) {
		super.paintComponent( g );

		// TODO turn this into a static reference adapter.
		GraphicsAdapter gfxa = new SwingGraphicsAdapter( g );

		Iterator< Enemy > dubs = logo.iterator( );

		while ( dubs.hasNext( ) ) {
			dubs.next( ).draw( gfxa );
		}

		if ( this.bomb != null ) {
			this.bomb.draw( gfxa );
		}

		g.setColor( Launcher.BUILD_THEME );
		g.setFont( C.Fonts.MENU_FONT );
		g.drawString( Launcher.STATE + " " + Launcher.VERSION + " \"" + Launcher.BUILD_NAME + "\"", 560, 700 );

		g.setColor( Color.WHITE );
		g.setFont( C.Fonts.MENU_MOTD_FONT );

		if ( this.isFullscreen ) {
			g.drawString( MOTD[ this.motdIndex ], 20, this.getHeight( ) - 40 );
		} else {
			g.drawString( MOTD[ this.motdIndex ], 20, this.getHeight( ) - 80 );
		}

	}

	@Override
	public void onDestroy( ) {
		SoundManager.getSoundManager( ).fadeCurrentBGTrack( );
		this.clock.stop( );
		this.removeAll( );
	}

	@Override
	public void onResize( int newWidth, int newHeight ) {
		this.setSize( newWidth, newHeight );

		this.osm.onResize( this.getWidth( ) / 3, ( int ) ( this.getHeight( ) * 0.75 ) );
		this.osm.setLocation( this.getWidth( ) / 2, 100 );

		this.spm.onResize( this.getWidth( ) / 3, ( int ) ( this.getHeight( ) * 0.75 ) );
		this.spm.setLocation( this.getWidth( ) / 2, 100 );

		this.lpm.onResize( this.getWidth( ) / 3, ( int ) ( this.getHeight( ) * 0.75 ) );
		this.lpm.setLocation( this.getWidth( ) / 2, 100 );

	}

	@Override
	public void updateGraphicsSettings( boolean allowRestart ) {

		if ( allowRestart ) {
			MainWindow.getMainWindow( ).setFullscreen(
					cfgm.getSetting( "Fullscreen", OptionsMenu.DEFAULT_SETTINGS.SETTING_FULLSCREEN_WINDOW ) );
		}

	}

	private void interact( int mx, int my ) {

		if ( this.bomb == null ) {

			Random r = new Random( );

			if ( r.nextBoolean( ) ) {

				Dub deb = new Dub( ( double ) mx, ( double ) my );

				this.bomb = new Bomb( mx, my, r.nextInt( 1000 ), 150, 0, deb );
				this.bomb.drop( );
			} else {
				this.bomb = new Mortar( mx, my, r.nextInt( 200 ), this.rc );
			}

		}

	}

	@Override
	public void addInputControls( ) {

		this.mcl = new MouseListener( ) {

			@Override
			public void mouseReleased( MouseEvent e ) {}

			@Override
			public void mousePressed( MouseEvent e ) {

				int src = e.getButton( );

				if ( src == MouseEvent.BUTTON1 ) {
					MenuPanel.this.repaint( );

					int mousex = e.getX( );
					int mousey = e.getY( );

					if ( MenuPanel.this.gameLabel.isMouseInBounds( mousex, mousey ) ) {
						MenuPanel.this.gameLabel.onClick( );
					}

					if ( MenuPanel.this.helpLabel.isMouseInBounds( mousex, mousey ) ) {
						MenuPanel.this.helpLabel.onClick( );
					}

					/*
					 * else if ( MenuPanel.this.editorLabel.isMouseInBounds(
					 * mousex, mousey ) ) { MenuPanel.this.editorLabel.onClick(
					 * ); }
					 */
					else if ( MenuPanel.this.customizeLabel.isMouseInBounds( mousex, mousey ) ) {
						MenuPanel.this.customizeLabel.onClick( );
					}

					else if ( MenuPanel.this.optionsLabel.isMouseInBounds( mousex, mousey ) ) {
						MenuPanel.this.optionsLabel.onClick( );
					}

					else if ( MenuPanel.this.exitLabel.isMouseInBounds( mousex, mousey ) ) {
						MenuPanel.this.exitLabel.onClick( );
					}

					else if ( MenuPanel.this.onlineGameLabel.isMouseInBounds( mousex, mousey ) ) {
						MenuPanel.this.onlineGameLabel.onClick( );
					}

					else if ( MenuPanel.this.statsLabel.isMouseInBounds( mousex, mousey ) ) {
						MenuPanel.this.statsLabel.onClick( );
					}

					else if ( MenuPanel.this.loadProfileLabel.isMouseInBounds( mousex, mousey ) ) {
						MenuPanel.this.loadProfileLabel.onClick( );
					}
				} else if ( src == MouseEvent.BUTTON3 ) {
					interact( e.getX( ), e.getY( ) );
				}

			}

			@Override
			public void mouseExited( MouseEvent e ) {}

			@Override
			public void mouseEntered( MouseEvent e ) {}

			@Override
			public void mouseClicked( MouseEvent e ) {}
		};

		this.addMouseListener( this.mcl );

		this.mml = new MouseMotionListener( ) {

			@Override
			public void mouseMoved( MouseEvent e ) {

				int mousex = e.getX( );
				int mousey = e.getY( );

				if ( MenuPanel.this.gameLabel.isMouseInBounds( mousex, mousey ) || spm.isVisible( ) ) {
					MenuPanel.this.gameLabel.setHighlighted( true );
					MenuPanel.this.gameLabel.repaint( );
				} else {
					MenuPanel.this.gameLabel.setHighlighted( false );
					MenuPanel.this.gameLabel.repaint( );
				}

				if ( MenuPanel.this.statsLabel.isMouseInBounds( mousex, mousey ) ) {
					MenuPanel.this.statsLabel.setHighlighted( true );
					MenuPanel.this.statsLabel.repaint( );
				} else {
					MenuPanel.this.statsLabel.setHighlighted( false );
					MenuPanel.this.statsLabel.repaint( );
				}

				/*
				 * if ( MenuPanel.this.editorLabel.isMouseInBounds( mousex,
				 * mousey ) ) { MenuPanel.this.editorLabel.setHighlighted( true
				 * ); MenuPanel.this.editorLabel.repaint( ); } else {
				 * MenuPanel.this.editorLabel.setHighlighted( false );
				 * MenuPanel.this.editorLabel.repaint( ); }
				 */

				if ( MenuPanel.this.optionsLabel.isMouseInBounds( mousex, mousey ) ) {
					MenuPanel.this.optionsLabel.setHighlighted( true );
					MenuPanel.this.optionsLabel.repaint( );
				} else {
					MenuPanel.this.optionsLabel.setHighlighted( false );
					MenuPanel.this.optionsLabel.repaint( );
				}

				if ( MenuPanel.this.loadProfileLabel.isMouseInBounds( mousex, mousey ) || lpm.isVisible( ) ) {
					MenuPanel.this.loadProfileLabel.setHighlighted( true );
					MenuPanel.this.loadProfileLabel.repaint( );
				} else {
					MenuPanel.this.loadProfileLabel.setHighlighted( false );
					MenuPanel.this.loadProfileLabel.repaint( );
				}

				if ( MenuPanel.this.customizeLabel.isMouseInBounds( mousex, mousey ) ) {
					MenuPanel.this.customizeLabel.setHighlighted( true );
					MenuPanel.this.customizeLabel.repaint( );
				} else {
					MenuPanel.this.customizeLabel.setHighlighted( false );
					MenuPanel.this.customizeLabel.repaint( );
				}

				if ( MenuPanel.this.onlineGameLabel.isMouseInBounds( mousex, mousey ) || osm.isVisible( ) ) {
					MenuPanel.this.onlineGameLabel.setHighlighted( true );
					MenuPanel.this.onlineGameLabel.repaint( );
				} else {
					MenuPanel.this.onlineGameLabel.setHighlighted( false );
					MenuPanel.this.onlineGameLabel.repaint( );
				}

				if ( MenuPanel.this.exitLabel.isMouseInBounds( mousex, mousey ) ) {
					MenuPanel.this.exitLabel.setHighlighted( true );
					MenuPanel.this.exitLabel.repaint( );
				} else {
					MenuPanel.this.exitLabel.setHighlighted( false );
					MenuPanel.this.exitLabel.repaint( );
				}

				if ( MenuPanel.this.helpLabel.isMouseInBounds( mousex, mousey ) ) {
					MenuPanel.this.helpLabel.setHighlighted( true );
					MenuPanel.this.helpLabel.repaint( );
				} else {
					MenuPanel.this.helpLabel.setHighlighted( false );
					MenuPanel.this.helpLabel.repaint( );
				}

			}

			@Override
			public void mouseDragged( MouseEvent e ) {
				// TODO Auto-generated method stub

			}
		};

		this.addMouseMotionListener( this.mml );

	}

	@Override
	public void removeInputControls( ) {

		this.removeMouseListener( this.mcl );
		this.removeMouseMotionListener( this.mml );

	}

	private void setupRPSLogo( ) {

		int OFFSET = 120;

		// R
		logo.add( new Dub( 600.0, 100.0 ) );
		logo.add( new Dub( 600.0, 180.0 ) );
		logo.add( new Dub( 600.0, 260.0 ) );
		logo.add( new Dub( 600.0, 340.0 ) );
		logo.add( new Dub( 600.0, 420.0 ) );
		logo.add( new Dub( 600.0, 500.0 ) );
		logo.add( new Dub( 600.0, 580.0 ) );

		logo.add( new Dub( 680.0, 100.0 ) );
		logo.add( new Dub( 760.0, 100.0 ) );

		logo.add( new Dub( 840.0, 180.0 ) );
		logo.add( new Dub( 840.0, 260.0 ) );
		logo.add( new Dub( 760.0, 340.0 ) );
		logo.add( new Dub( 680.0, 340.0 ) );

		logo.add( new Dub( 680.0, 340.0 ) );
		logo.add( new Dub( 760.0, 420.0 ) );
		logo.add( new Dub( 840.0, 500.0 ) );
		logo.add( new Dub( 840.0, 580.0 ) );

		// P
		logo.add( new Dub( 920.0 + OFFSET, 100.0 ) );
		logo.add( new Dub( 920.0 + OFFSET, 180.0 ) );
		logo.add( new Dub( 920.0 + OFFSET, 260.0 ) );
		logo.add( new Dub( 920.0 + OFFSET, 340.0 ) );
		logo.add( new Dub( 920.0 + OFFSET, 420.0 ) );
		logo.add( new Dub( 920.0 + OFFSET, 500.0 ) );
		logo.add( new Dub( 920.0 + OFFSET, 580.0 ) );

		logo.add( new Dub( 1000.0 + OFFSET, 100.0 ) );
		logo.add( new Dub( 1080.0 + OFFSET, 100.0 ) );

		logo.add( new Dub( 1160.0 + OFFSET, 180.0 ) );
		logo.add( new Dub( 1160.0 + OFFSET, 260.0 ) );
		logo.add( new Dub( 1080.0 + OFFSET, 340.0 ) );
		logo.add( new Dub( 1000.0 + OFFSET, 340.0 ) );

		// S
		logo.add( new Dub( 1240.0 + ( 2 * OFFSET ), 180.0 ) );
		logo.add( new Dub( 1320.0 + ( 2 * OFFSET ), 100.0 ) );
		logo.add( new Dub( 1400.0 + ( 2 * OFFSET ), 100.0 ) );
		logo.add( new Dub( 1480.0 + ( 2 * OFFSET ), 180.0 ) );

		logo.add( new Dub( 1240.0 + ( 2 * OFFSET ), 260.0 ) );
		logo.add( new Dub( 1320.0 + ( 2 * OFFSET ), 340.0 ) );
		logo.add( new Dub( 1400.0 + ( 2 * OFFSET ), 340.0 ) );
		logo.add( new Dub( 1480.0 + ( 2 * OFFSET ), 420.0 ) );

		logo.add( new Dub( 1480.0 + ( 2 * OFFSET ), 500.0 ) );
		logo.add( new Dub( 1400.0 + ( 2 * OFFSET ), 580.0 ) );
		logo.add( new Dub( 1320.0 + ( 2 * OFFSET ), 580.0 ) );
		logo.add( new Dub( 1240.0 + ( 2 * OFFSET ), 500.0 ) );

		logo.add( new Bomber( ( double ) 940, ( double ) 580 ) );
		logo.add( new Bomber( ( double ) 1240 + OFFSET, ( double ) 580 ) );
		logo.add( new Bomber( ( double ) 1560 + ( OFFSET * 2 ), ( double ) 580 ) );

		this.rc = new RogueCannon( 2000.0, 1200.0 );
		rc.setDirection( 1 );
		logo.add( this.rc );

		Iterator< Enemy > dubs = logo.iterator( );

		while ( dubs.hasNext( ) ) {
			Enemy d = dubs.next( );
			d.setDisplayBars( false );
			d.setDisplayIdentifiers( false );
		}

	}

	private class BGActionLoop implements ActionListener {

		@SuppressWarnings( "deprecation" )
		@Override
		public void actionPerformed( ActionEvent arg0 ) {
			Iterator< Enemy > dubs = logo.iterator( );

			while ( dubs.hasNext( ) ) {
				Enemy d = dubs.next( );
				d.update( );

				if ( d.getHealth( ) <= 0 ) {
					dubs.remove( );
				}
			}

			if ( bomb != null ) {
				bomb.updateActions( logo, null, null );

				if ( bomb.isFinished( ) ) {
					bomb = null;
				}
			}

			repaint( );

		}

	}

	@Override
	public void onPause( ) {
		this.clock.stop( );

	}

	@Override
	public void onResume( ) {
		Entity.setEntityContainer( MenuPanel.DUMMY_GI );
		this.clock.start( );

	}

	@Override
	public void onCreate( ) {
		SoundManager.getSoundManager( ).playSongFromGroup( C.File.MUSIC_MENU );
		SoundManager.getSoundManager( ).setSongLoop( false );
		SoundManager.setTrackSkipMode( true );
	}

}

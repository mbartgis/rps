/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-27 10:27:12 -0500 (Sat, 27 Jan 2018) $
 * $Revision: 309 $
 * $Author: mbartgis $
 */
package com.rps.gui.menu;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import com.rpscore.C;

public class ProfileCell extends JPanel implements ListCellRenderer< ProfileCell > {

	private String	name;
	private Integer	level;
	private double	skillLevel;

	private long	passwordHash;

	private String	url;

	private boolean	isHighlighted;

	private boolean	isLocked;

	public ProfileCell( String profName, int profLevel, String profURL, double skill, long password ) {

		this.name = profName;
		this.level = profLevel;
		this.url = profURL;
		this.skillLevel = skill;

		this.passwordHash = password;

		this.isLocked = false;

	}

	@Override
	protected void paintComponent( Graphics g ) {
		super.paintComponent( g );

		if ( this.isHighlighted ) {
			g.setColor( Color.YELLOW );
		} else {
			if ( isLocked ) {
				g.setColor( Color.RED );
			} else {
				g.setColor( Color.WHITE );
			}
		}

		g.fillRect( 0, 0, this.getWidth( ), this.getHeight( ) );

		g.setColor( Color.BLACK );
		g.setFont( C.Fonts.MENU_MOTD_FONT );

		g.drawRect( 0, 0, this.getWidth( ), this.getHeight( ) - 1 );

		g.drawString( this.level.toString( ) + "/" + ( ( int ) this.skillLevel ), 20, ( this.getHeight( ) / 2 ) + 10 );
		g.drawString( this.name, 100, ( this.getHeight( ) / 2 ) + 10 );

	}

	public void setHighlighted( boolean highlight ) {
		this.isHighlighted = highlight;
	}

	@Override
	public ProfileCell getListCellRendererComponent( JList< ? extends ProfileCell > arg0, ProfileCell arg1, int arg2,
			boolean arg3, boolean arg4 ) {
		return arg1;
	}

	@Override
	public String getName( ) {
		return name;
	}

	@Override
	public void setName( String name ) {
		this.name = name;
	}

	public Integer getLevel( ) {
		return level;
	}

	public void setLevel( Integer level ) {
		this.level = level;
	}

	public String getURL( ) {
		return url;
	}

	public void setURL( String url ) {
		this.url = url;
	}

	public boolean isHighlighted( ) {
		return isHighlighted;
	}

	public long getPasswordHash( ) {
		return passwordHash;
	}

	public void setPasswordHash( long passwordHash ) {
		this.passwordHash = passwordHash;
	}

	public boolean isLocked( ) {
		return isLocked;
	}

	public void setLocked( boolean isLocked ) {
		this.isLocked = isLocked;
	}

}
/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-07-27 04:03:54 -0400 (Fri, 27 Jul 2018) $
 * $Revision: 364 $
 * $Author: mbartgis $
 */

package com.rps.gui.menu;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextField;

import com.rps.gui.MainWindow;
import com.rps.gui.ModularPanel;
import com.rps.gui.game.GamePanel;
import com.rpscore.C;
import com.rpscore.audio.SoundManager;
import com.rpscore.cfg.ConfigManager;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.lib.MathUtils;

public class SoloPlayMenu extends JComponent {

	private ConfigManager			cfg				= ConfigManager.getConfigManager( );

	private JComboBox< String >		difficultyCB;
	private static final String[ ]	DIFFICULTIES	= { "Peaceful (" + Enemy.DIFFICULTY_PEACEFUL + ")",
			"Easy (" + Enemy.DIFFICULTY_EASY + ")", "Medium (" + Enemy.DIFFICULTY_NORMAL + ")",
			"Hard (" + Enemy.DIFFICULTY_HARD + ")", "Insane (" + Enemy.DIFFICULTY_INSANE + ")",
			"Impossible (" + Enemy.DIFFICULTY_IMPOSSIBLE + ")", "Custom..." };

	private JButton					joinButton;

	private JTextField				seedTF;

	private ModularPanel			parent;

	private JComboBox< String >		mapSizeCB;

	private static final String[ ]	mapSizeDescs	= { "Very Small - 33", "Small - 65", "129 - Normal", "Large - 257",
			"Very Large - 513", "Massive (Very Laggy!) - 1025" };

	private static final int[ ]		sizes			= { 33, 65, 129, 257, 1025, 2049, 1073741825 };

	public SoloPlayMenu( ModularPanel nParent ) {

		this.parent = nParent;

		this.setLayout( null );

		this.difficultyCB = new JComboBox< >( DIFFICULTIES );
		this.difficultyCB.setSize( 400, 40 );
		this.difficultyCB.setLocation( 40, 100 );
		this.difficultyCB.setFont( C.Fonts.MENU_MOTD_FONT );
		this.difficultyCB.setSelectedIndex( cfg.getSetting( "LastDifficultySelected", 2 ) );
		this.difficultyCB.addItemListener( new ItemListener( ) {
			@Override
			public void itemStateChanged( ItemEvent e ) {
				cfg.modifySetting( "LastDifficultySelected", "" + difficultyCB.getSelectedIndex( ) );
			}
		} );
		this.add( this.difficultyCB );

		this.joinButton = new JButton( "Start Battle" );
		this.joinButton.setSize( 200, 60 );
		this.joinButton.setLocation( ( this.getWidth( ) / 2 ) - ( this.joinButton.getWidth( ) / 2 ),
				this.getHeight( ) - 82 );
		this.joinButton.setFont( C.Fonts.MENU_MOTD_FONT );
		this.joinButton.addActionListener( new ActionListener( ) {

			@Override
			public void actionPerformed( ActionEvent e ) {

				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );

				Container parentWindow = SoloPlayMenu.this.getParent( );
				while ( ! ( parentWindow instanceof MainWindow ) ) {
					parentWindow = parentWindow.getParent( );
				}

				double difficulty = 0.0;

				switch ( SoloPlayMenu.this.difficultyCB.getSelectedIndex( ) ) {
					case 0:
						difficulty = Enemy.DIFFICULTY_PEACEFUL;
						break;
					case 1:
						difficulty = Enemy.DIFFICULTY_EASY;
						break;
					case 2:
						difficulty = Enemy.DIFFICULTY_NORMAL;
						break;
					case 3:
						difficulty = Enemy.DIFFICULTY_HARD;
						break;
					case 4:
						difficulty = Enemy.DIFFICULTY_INSANE;
						break;
					case 5:
						difficulty = Enemy.DIFFICULTY_IMPOSSIBLE;
						break;
					case 6:
						difficulty = 10.00;
						break;
				}

				long seed = 0L;

				try {
					seed = Long.parseLong( seedTF.getText( ) );
				} catch ( NumberFormatException ex ) {
					seed = MathUtils.hash64( seedTF.getText( ) );
				}

				parent.reinstantiateOnResume( );

				GamePanel gp = new GamePanel( seed, sizes[ mapSizeCB.getSelectedIndex( ) ], true );
				gp.getGameInstance( ).setDifficulty( difficulty );

				( ( MainWindow ) parentWindow ).pushPanel( gp );

			}
		} );
		this.add( joinButton );

		Random r = new Random( );
		Long initSeed = r.nextLong( );
		this.seedTF = new JTextField( initSeed.toString( ) );
		this.seedTF.setSize( 400, 40 );
		this.seedTF.setLocation( 40, 300 );
		this.seedTF.setFont( C.Fonts.MENU_MOTD_FONT );

		this.add( this.seedTF );

		this.mapSizeCB = new JComboBox< >( mapSizeDescs );
		this.mapSizeCB.setSize( 400, 40 );
		this.mapSizeCB.setLocation( 40, 500 );
		this.mapSizeCB.setFont( C.Fonts.MENU_MOTD_FONT );

		this.add( this.mapSizeCB );

		this.setVisible( false );
		this.repaint( );

	}

	public void onResize( int newWidth, int newHeight ) {
		this.setSize( newWidth, newHeight );

		this.joinButton.setLocation( ( this.getWidth( ) / 2 ) - ( this.joinButton.getWidth( ) / 2 ),
				this.getHeight( ) - 82 );
	}

	public void quickStart( ) {
		new Thread( ( ) -> {
			while ( this.getParent( ) == null ) {}
			this.joinButton.doClick( );
		} ).start( );

	}

	@Override
	protected void paintComponent( Graphics g ) {
		super.paintComponent( g );

		g.setColor( Color.BLACK );
		g.fillRect( 4, 4, this.getWidth( ) - 9, this.getHeight( ) - 9 );

		g.setColor( Color.RED );
		g.drawRect( 0, 0, this.getWidth( ) - 1, this.getHeight( ) - 1 );
		g.drawRect( 2, 2, this.getWidth( ) - 5, this.getHeight( ) - 5 );

		g.setFont( C.Fonts.MENU_MOTD_FONT );
		g.drawString( "Arena Difficulty", 40, 60 );
		g.drawString( "Arena Seed", 40, 260 );
		g.drawString( "Map Size", 40, 460 );

	}

}

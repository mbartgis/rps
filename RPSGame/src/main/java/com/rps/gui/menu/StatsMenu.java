/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-04-01 05:16:57 -0400 (Sun, 01 Apr 2018) $
 * $Revision: 339 $
 * $Author: mbartgis $
 */
package com.rps.gui.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.JLabel;

import com.rps.gui.ModularPanel;
import com.rpscore.C;
import com.rpscore.audio.SoundManager;
import com.rpscore.game.PlayerManager;

public class StatsMenu extends ModularPanel {

	private SelectionLabel			returnLabel;

	private MouseListener			mcl;

	private MouseMotionListener		mml;

	private PlayerManager			pmRef;

	private LinkedList< JLabel >	statLabels;

	private JLabel					nameLabel;

	public StatsMenu( PlayerManager pm ) {

		this.pmRef = pm;

		this.setLayout( null );
		this.setBackground( Color.BLACK );

		// Menu Buttons
		/* @formatter:off */
		this.returnLabel = new SelectionLabel( " Return" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				terminate( );
			}
		};
		this.returnLabel.setSize( 300, 100 );
		this.returnLabel.setLocation( 20, this.getHeight( ) - 150 );
		this.returnLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.returnLabel.setForeground( Color.WHITE );
		this.returnLabel.setFont( C.Fonts.MENU_FONT );

		this.add( this.returnLabel );

		this.nameLabel = new JLabel( pmRef.getName( ) );
		this.nameLabel.setFont( new Font( "Consolas", Font.BOLD, 48 ) );
		this.nameLabel.setForeground( Color.YELLOW );
		this.nameLabel.setSize( 300, 60 );
		this.nameLabel.setLocation( 20, 20 );

		this.add( this.nameLabel );

		this.statLabels = new LinkedList< >( );

		String[ ] stats = pmRef.getUnderlying( ).getFullConfigState( );

		this.statLabels.add( decorateText( "Rank:              " + pmRef.getRank( ) ) );
		this.statLabels
				.add( decorateText( "Skill Level:       " + String.format( "%1$.2f", pmRef.getSkillLevel( ) ) ) );
		this.statLabels.add( decorateText( "K/D Ratio:         " + String.format( "%1$.2f", pmRef.getKDR( ) ) ) );
		this.statLabels.add( decorateText( "Level:             " + pmRef.getLevel( ) ) );
		this.statLabels.add( decorateText( "XP:                " + pmRef.getXP( ) ) );

		long timePlayed = pmRef.getTImePlayed( );

		long days = timePlayed / ( 1000L * 3600L * 24L );
		timePlayed %= ( 1000L * 3600L * 24L );

		long hours = timePlayed / ( 1000L * 3600L );
		timePlayed %= ( 1000L * 3600L );

		long minutes = timePlayed / 60000L;
		timePlayed %= 60000L;

		long seconds = timePlayed / 1000L;

		StringBuilder time = new StringBuilder( );

		if ( days > 0 ) {
			time.append( days );
			time.append( " d, " );
		}

		time.append( String.format( "%1$02d", hours ) );
		time.append( ":" );
		time.append( String.format( "%1$02d", minutes ) );
		time.append( ":" );
		time.append( String.format( "%1$02d", seconds ) );

		this.statLabels.add( decorateText( "Time Played:       " + time.toString( ) ) );
		this.statLabels.add(
				decorateText( "Avg. Difficulty:   " + String.format( "%1$.2f", pmRef.getDifficultyPlayedAt( ) ) ) );
		this.statLabels.add( decorateText( "Rounds Started:    " + pmRef.getRoundsStarted( ) ) );
		this.statLabels.add( decorateText( "Rounds Won:        " + pmRef.getRoundsPlayed( ) ) );
		this.statLabels.add( decorateText( "Rounds Lost:       " + pmRef.getRoundsLost( ) ) );
		this.statLabels.add( decorateText( "Kills:             " + pmRef.getTotalKills( ) ) );
		this.statLabels.add( decorateText( "Deaths:            " + pmRef.getTotalDeaths( ) ) );
		this.statLabels.add( decorateText( "" ) );
		this.statLabels.add( decorateText( "Bullet Kills:      " + "TODO" ) );
		this.statLabels.add( decorateText( "Bullet Deaths:     " + "TODO" ) );
		this.statLabels.add( decorateText( "" ) );
		this.statLabels.add( decorateText( "Bomb Kills:        " + "TODO" ) );
		this.statLabels.add( decorateText( "Bomb Deaths:       " + pmRef.getBombDeaths( ) ) );
		this.statLabels.add( decorateText( "Bomb Suicides:     " + pmRef.getBombSuicides( ) ) );

		organizeJLabelGrid( statLabels );

		Iterator< JLabel > labelItr = this.statLabels.iterator( );

		while ( labelItr.hasNext( ) ) {
			this.add( labelItr.next( ) );
		}

	}

	private JLabel decorateText( String text ) {
		JLabel statLabel = new JLabel( text );
		statLabel.setSize( 600, 40 );
		statLabel.setForeground( Color.WHITE );
		statLabel.setFont( new Font( "Consolas", Font.BOLD, 30 ) );

		return statLabel;
	}

	private void organizeJLabelGrid( LinkedList< JLabel > stats ) {

		for ( int i = 0; i < stats.size( ); i++ ) {
			// TODO replace these magic numbers with ones that dynamically
			// change per the screen resolution.
			stats.get( i ).setLocation( 20 + ( ( i / 20 ) * 800 ), ( i % 20 * 40 ) + 100 );
		}
	}

	@Override
	public void updateGraphicsSettings( boolean allowRestart ) {}

	@Override
	public void onResize( int newWidth, int newHeight ) {
		this.setSize( newWidth, newHeight );
		this.returnLabel.setLocation( 20, this.getHeight( ) - 150 );
	}

	@Override
	public void onDestroy( ) {
		if ( this.pmRef != null ) {
			this.pmRef.save( );
			this.pmRef = null;
		}
		System.gc( );
	}

	@Override
	public void addInputControls( ) {
		this.mcl = new MouseListener( ) {

			@Override
			public void mouseReleased( MouseEvent e ) {}

			@Override
			public void mousePressed( MouseEvent e ) {

				StatsMenu.this.repaint( );

				int mousex = e.getX( );
				int mousey = e.getY( );

				if ( StatsMenu.this.returnLabel.isMouseInBounds( mousex, mousey ) ) {
					StatsMenu.this.returnLabel.onClick( );
				}

			}

			@Override
			public void mouseExited( MouseEvent e ) {}

			@Override
			public void mouseEntered( MouseEvent e ) {}

			@Override
			public void mouseClicked( MouseEvent e ) {}
		};

		this.addMouseListener( this.mcl );

		this.mml = new MouseMotionListener( ) {

			@Override
			public void mouseMoved( MouseEvent e ) {

				int mousex = e.getX( );
				int mousey = e.getY( );

				if ( StatsMenu.this.returnLabel.isMouseInBounds( mousex, mousey ) ) {
					StatsMenu.this.returnLabel.setHighlighted( true );
					StatsMenu.this.returnLabel.repaint( );
				} else {
					StatsMenu.this.returnLabel.setHighlighted( false );
					StatsMenu.this.returnLabel.repaint( );
				}

			}

			@Override
			public void mouseDragged( MouseEvent e ) {}
		};

		this.addMouseMotionListener( this.mml );

	}

	public void setUpgradeState( ) {

	}

	@Override
	public void removeInputControls( ) {

		this.removeMouseListener( this.mcl );
		this.removeMouseMotionListener( this.mml );

	}

	@Override
	public void onPause( ) {}

	@Override
	public void onResume( ) {}

	@Override
	public void onCreate( ) {}

}

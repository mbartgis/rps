/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-28 22:44:18 -0500 (Sun, 28 Jan 2018) $
 * $Revision: 318 $
 * $Author: mbartgis $
 */
package com.rps.gui.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.rpscore.C;
import com.rpscore.audio.SoundManager;
import com.rpscore.cfg.ConfigManager;
import com.rpscore.debug.Log;
import com.rpscore.game.PlayerManager;
import com.rpscore.lib.FileUtils;
import com.rpscore.lib.MathUtils;

public class LoadProfileMenu extends JPanel {

	private ProfileCell[ ]	profileModel;
	private ProfilesList	profiles;

	private JScrollPane		jsp;

	private JTextField		newUserName;

	private JPasswordField	newUserPw;

	private JButton			createNewUser;
	private JButton			deleteUser;

	public LoadProfileMenu( ) {

		this.profiles = new ProfilesList( );

		setProfilesList( );

		this.setLayout( null );

		this.jsp = new JScrollPane( );
		this.jsp.setSize( 600, 300 );
		this.jsp.setLocation( 40, 100 );

		this.profiles.setSize( 600, 300 );
		this.profiles.setLocation( 40, 100 );
		// this.profiles.setBackground( Color.BLACK );
		this.profiles.setFixedCellHeight( 60 );

		this.add( this.profiles );

		this.profiles.addMouseMotionListener( new MouseMotionListener( ) {

			@Override
			public void mouseMoved( MouseEvent e ) {

				for ( int i = 0; i < profiles.getModel( ).getSize( ); i++ ) {
					profiles.getModel( ).getElementAt( i ).setHighlighted( false );
				}

				Point location = e.getPoint( );

				int index = profiles.locationToIndex( location );

				try {
					ProfileCell pc = profiles.getModel( ).getElementAt( index );
					pc.setHighlighted( true );
				} catch ( ArrayIndexOutOfBoundsException ex ) {
					// There are no list items, do nothing because one should
					// automatically be created on a separate thread.
				}

			}

			@Override
			public void mouseDragged( MouseEvent e ) {
				// TODO Auto-generated method stub

			}
		} );

		this.profiles.addMouseListener( new MouseListener( ) {

			@Override
			public void mouseReleased( MouseEvent e ) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed( MouseEvent e ) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited( MouseEvent e ) {
				for ( int i = 0; i < profiles.getModel( ).getSize( ); i++ ) {
					profiles.getModel( ).getElementAt( i ).setHighlighted( false );
				}
			}

			@Override
			public void mouseEntered( MouseEvent e ) {

			}

			@Override
			public void mouseClicked( MouseEvent e ) {

				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );

				Point location = e.getPoint( );

				int index = profiles.locationToIndex( location );

				ConfigManager.getConfigManager( ).modifySetting( "LastProfile", profileModel[ index ].getURL( ) );

				LoadProfileMenu.this.setVisible( false );

			}
		} );

		this.setVisible( false );

		this.newUserName = new JTextField( );
		this.newUserName.setSize( 400, 40 );
		this.newUserName.setLocation( 80, 520 );
		this.newUserName.setFont( C.Fonts.MENU_MOTD_FONT );
		this.newUserName.getDocument( ).addDocumentListener( new DocumentListener( ) {

			@Override
			public void removeUpdate( DocumentEvent e ) {

				if ( newUserName.getText( ).isEmpty( ) ) {
					createNewUser.setEnabled( false );
					createNewUser.setVisible( false );
				} else {
					createNewUser.setEnabled( true );
					createNewUser.setVisible( true );
				}

				if ( nameIsInList( newUserName.getText( ) ) ) {
					createNewUser.setEnabled( false );
					deleteUser.setVisible( true );
					if ( credentialsMatch( newUserName.getText( ), newUserPw.getText( ) ) ) {
						deleteUser.setEnabled( true );
					} else {
						deleteUser.setEnabled( false );
					}
				} else {
					createNewUser.setEnabled( true );
					deleteUser.setEnabled( false );
					deleteUser.setVisible( false );
				}

			}

			@Override
			public void insertUpdate( DocumentEvent e ) {

				if ( newUserName.getText( ).isEmpty( ) ) {
					createNewUser.setEnabled( false );
					createNewUser.setVisible( false );
				} else {
					createNewUser.setEnabled( true );
					createNewUser.setVisible( true );
				}

				if ( nameIsInList( newUserName.getText( ) ) ) {
					createNewUser.setEnabled( false );
					deleteUser.setVisible( true );
					if ( credentialsMatch( newUserName.getText( ), newUserPw.getText( ) ) ) {
						deleteUser.setEnabled( true );
					} else {
						deleteUser.setEnabled( false );
					}
				} else {
					createNewUser.setEnabled( true );
					deleteUser.setEnabled( false );
					deleteUser.setVisible( false );
				}

			}

			@Override
			public void changedUpdate( DocumentEvent e ) {

				if ( newUserName.getText( ).isEmpty( ) ) {
					createNewUser.setEnabled( false );
					createNewUser.setVisible( false );
				} else {
					createNewUser.setEnabled( true );
					createNewUser.setVisible( true );
				}

				if ( nameIsInList( newUserName.getText( ) ) ) {
					createNewUser.setEnabled( false );
					deleteUser.setVisible( true );
					if ( credentialsMatch( newUserName.getText( ), newUserPw.getText( ) ) ) {
						deleteUser.setEnabled( true );
					} else {
						deleteUser.setEnabled( false );
					}
				} else {
					createNewUser.setEnabled( true );
					deleteUser.setEnabled( false );
					deleteUser.setVisible( false );
				}

			}
		} );

		this.add( this.newUserName );

		this.newUserPw = new JPasswordField( );
		this.newUserPw.setSize( 400, 40 );
		this.newUserPw.setLocation( 80, 620 );
		this.newUserPw.setFont( C.Fonts.MENU_MOTD_FONT );
		this.newUserPw.getDocument( ).addDocumentListener( new DocumentListener( ) {

			@Override
			public void removeUpdate( DocumentEvent e ) {

				if ( newUserName.getText( ).isEmpty( ) ) {
					createNewUser.setEnabled( false );
					createNewUser.setVisible( false );
				} else {
					createNewUser.setEnabled( true );
					createNewUser.setVisible( true );
				}

				if ( nameIsInList( newUserName.getText( ) ) ) {
					createNewUser.setEnabled( false );
					deleteUser.setVisible( true );
					if ( credentialsMatch( newUserName.getText( ), newUserPw.getText( ) ) ) {
						deleteUser.setEnabled( true );
					} else {
						deleteUser.setEnabled( false );
					}
				} else {
					createNewUser.setEnabled( true );
					deleteUser.setEnabled( false );
					deleteUser.setVisible( false );
				}

			}

			@Override
			public void insertUpdate( DocumentEvent e ) {

				if ( newUserName.getText( ).isEmpty( ) ) {
					createNewUser.setEnabled( false );
					createNewUser.setVisible( false );
				} else {
					createNewUser.setEnabled( true );
					createNewUser.setVisible( true );
				}

				if ( nameIsInList( newUserName.getText( ) ) ) {
					createNewUser.setEnabled( false );
					deleteUser.setVisible( true );
					if ( credentialsMatch( newUserName.getText( ), newUserPw.getText( ) ) ) {
						deleteUser.setEnabled( true );
					} else {
						deleteUser.setEnabled( false );
					}
				} else {
					createNewUser.setEnabled( true );
					deleteUser.setEnabled( false );
					deleteUser.setVisible( false );
				}

			}

			@Override
			public void changedUpdate( DocumentEvent e ) {

				if ( newUserName.getText( ).isEmpty( ) ) {
					createNewUser.setEnabled( false );
					createNewUser.setVisible( false );
				} else {
					createNewUser.setEnabled( true );
					createNewUser.setVisible( true );
				}

				if ( nameIsInList( newUserName.getText( ) ) ) {
					createNewUser.setEnabled( false );
					deleteUser.setVisible( true );
					if ( credentialsMatch( newUserName.getText( ), newUserPw.getText( ) ) ) {
						deleteUser.setEnabled( true );
					} else {
						deleteUser.setEnabled( false );
					}
				} else {
					createNewUser.setEnabled( true );
					deleteUser.setEnabled( false );
					deleteUser.setVisible( false );
				}

			}
		} );

		this.add( this.newUserPw );

		this.createNewUser = new JButton( "Create Profile" );
		this.createNewUser.setSize( 200, 60 );
		this.createNewUser.setLocation( 40, 720 );
		this.createNewUser.setEnabled( false );
		this.createNewUser.setVisible( false );
		this.createNewUser.setFont( new Font( "Arial", Font.BOLD, 20 ) );
		this.createNewUser.addActionListener( new ActionListener( ) {

			@Override
			public void actionPerformed( ActionEvent e ) {

				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );

				PlayerManager.generateNewProfile( newUserName.getText( ), newUserPw.getText( ) );

				// Update the profiles to the list
				setProfilesList( );

				newUserName.setText( "" );
				newUserPw.setText( "" );

			}
		} );

		this.add( this.createNewUser );

		this.deleteUser = new JButton( "Delete Profile" );
		this.deleteUser.setSize( 200, 60 );
		this.deleteUser.setLocation( 250, 720 );
		this.deleteUser.setEnabled( false );
		this.deleteUser.setVisible( false );
		this.deleteUser.setFont( new Font( "Arial", Font.BOLD, 20 ) );
		this.deleteUser.addActionListener( new ActionListener( ) {

			@Override
			public void actionPerformed( ActionEvent e ) {

				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );

				String name = newUserName.getText( );

				newUserName.setText( "" );
				newUserPw.setText( "" );

				String folderName = PlayerManager.deriveFolderName( name );

				Log.info( "Deleting save folder:  ../" + folderName );

				String gameSaveFolderURL = C.File.PLAYER_SAVEDATA_FOLDER;

				gameSaveFolderURL += ( folderName + C.File.FS );

				File gameFolder = new File( gameSaveFolderURL );

				FileUtils.recursiveDeletion( gameFolder );

				// Update the profiles to the list
				setProfilesList( );

				// Determine if the player's active profile was deleted, if it
				// appears in the list, no further action is needed.
				for ( int i = 0; i < profiles.getModel( ).getSize( ); i++ ) {

					String listFolderName = PlayerManager
							.deriveFolderName( profiles.getModel( ).getElementAt( i ).getName( ) );
					String lastProfile = ConfigManager.getConfigManager( ).getSetting( "LastProfile", "" );

					if ( listFolderName.equals( lastProfile ) ) {
						return;
					}
				}

				// If the player just deleted his/her active profile, we need to
				// set a new on up for them. If the list is empty, set a new
				// account called "Player" for them. If there is an account,
				// switch it over to the first one.
				String profile = profiles.getModel( ).getElementAt( 0 ).getName( );

				ConfigManager.getConfigManager( ).modifySetting( "LastProfile",
						PlayerManager.deriveFolderName( profile ) );

			}
		} );

		this.add( this.deleteUser );

	}

	private boolean nameIsInList( String name ) {
		for ( int i = 0; i < this.profiles.getModel( ).getSize( ); i++ ) {
			if ( this.profiles.getModel( ).getElementAt( i ).getName( ).equals( name ) ) {
				return true;
			}
		}

		return false;
	}

	private boolean credentialsMatch( String name, String password ) {

		for ( int i = 0; i < this.profiles.getModel( ).getSize( ); i++ ) {
			if ( this.profiles.getModel( ).getElementAt( i ).getName( ).equals( name ) ) {

				if ( this.profiles.getModel( ).getElementAt( i ).getPasswordHash( ) == 0L ) {
					return true;
				}

				if ( this.profiles.getModel( ).getElementAt( i ).getPasswordHash( ) == MathUtils.hash64( password ) ) {
					return true;
				}

				break;

			}
		}

		return false;

	}

	private void setProfilesList( ) {

		String gameSaveFolderURL = C.File.PLAYER_SAVEDATA_FOLDER;

		File gameSaveFolder = new File( gameSaveFolderURL );

		File[ ] saves = gameSaveFolder.listFiles( );

		ArrayList< ProfileCell > profileStore = new ArrayList< >( saves.length );

		for ( File f : saves ) {
			try {

				File testEncryption = new File( f.getAbsolutePath( ) + C.File.FS + "player.dat" );

				boolean unprotected = MathUtils.validateKey( FileUtils.getRawFileContent( testEncryption ), 0L,
						"Name=" );

				if ( unprotected ) {

					PlayerManager pm = PlayerManager.getPlayerManager( f.getAbsolutePath( ), true, 0L );

					int level = pm.getLevel( );
					String name = pm.getName( );

					profileStore.add(
							new ProfileCell( name, level, f.getName( ), pm.getSkillLevel( ), pm.getPasswordHash( ) ) );
				} else {
					ProfileCell pc = new ProfileCell( f.getName( ), 0, f.getName( ), 1.00,
							MathUtils.getRandom( 0, Integer.MAX_VALUE ) );
					pc.setLocked( true );

					// TODO Reintroduce this once entering passwords is working.
					// profileStore.add( pc );

				}
			} catch ( Exception e ) {
				continue;
			}

		}

		if ( profileStore.size( ) == 0 ) {

			// If there is a corrupt player folder, delete it.
			File pFolder = new File( gameSaveFolder + C.File.FS + C.GameplayConstants.DEFAULT_PLAYER_NAME );
			if ( pFolder.exists( ) ) {
				FileUtils.recursiveDeletion( pFolder );
			}

			PlayerManager.generateNewProfile( "Player", "" );
			ConfigManager.getConfigManager( ).modifySetting( "LastProfile", C.GameplayConstants.DEFAULT_PLAYER_NAME );

			// Try running this method again now that a new profile is
			// generated. Since this is run recursively, return immediately
			// afterwards to avoid duplicating work done.
			setProfilesList( );
			return;
		}

		this.profileModel = new ProfileCell[ profileStore.size( ) ];

		for ( int i = 0; i < profileModel.length; i++ ) {
			this.profileModel[ i ] = profileStore.get( i );
		}

		this.profiles.setListData( this.profileModel );

	}

	public void onResize( int newWidth, int newHeight ) {
		this.setSize( newWidth, newHeight );

		this.jsp.setSize( this.getWidth( ) - 80, 300 );
		this.profiles.setSize( this.getWidth( ) - 80, 300 );

	}

	@Override
	protected void paintComponent( Graphics g ) {
		super.paintComponent( g );

		if ( this.isVisible( ) ) {
			g.setColor( Color.BLACK );
			g.fillRect( 4, 4, this.getWidth( ) - 9, this.getHeight( ) - 9 );

			g.setColor( Color.WHITE );
			g.drawRect( 0, 0, this.getWidth( ) - 1, this.getHeight( ) - 1 );
			g.drawRect( 2, 2, this.getWidth( ) - 5, this.getHeight( ) - 5 );

			g.setFont( C.Fonts.MENU_FONT );
			g.drawString( "Load Profile", 40, 60 );

			g.setFont( C.Fonts.MENU_MOTD_FONT );
			g.drawString( "Profile Manager", 40, 460 );
			g.drawString( "Username", 80, 500 );
			g.drawString( "Password", 80, 600 );

			g.setFont( new Font( "Arial", Font.ITALIC, 16 ) );

			g.drawString( "*A Password is optional, but it can protect your account.", 80, 690 );

		}

		this.add( this.jsp );
		this.jsp.setViewportView( this.profiles );

		this.profiles.repaint( );

	}

	private class ProfilesList extends JList< ProfileCell > {

		@Override
		protected void paintComponent( Graphics g ) {

			super.paintComponent( g );

			for ( int i = 0; i < getModel( ).getSize( ); i++ ) {
				// getModel( ).getElementAt( i ).setRender( true );
				setCellRenderer( getModel( ).getElementAt( i ) );
				// getModel( ).getElementAt( i ).setRender( false );
			}

		}
	}

}

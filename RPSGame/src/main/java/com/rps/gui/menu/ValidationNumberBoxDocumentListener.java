/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * VERSION INFORMATION:
 * $Date: 2018-01-27 09:50:40 -0500 (Sat, 27 Jan 2018) $
 * $Revision: 305 $
 * $Author: mbartgis $
 */

package com.rps.gui.menu;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class ValidationNumberBoxDocumentListener implements DocumentListener {

	private int			min, max;

	private JTextField	ref;

	private boolean		allowEmpty;

	private Color		valid, invalid;

	private boolean		isValid;

	private int			getInitNumber;

	public ValidationNumberBoxDocumentListener( JTextField ref, int min, int max, boolean allowEmpty, Color valid,
			Color invalid ) {

		this.ref = ref;
		this.min = min;
		this.max = max;
		this.allowEmpty = allowEmpty;
		this.valid = valid;
		this.invalid = invalid;

		try {
			if ( ref.getText( ).isEmpty( ) && allowEmpty ) {
				this.isValid = true;
				this.getInitNumber = 0;
				return;
			}
			int i = Integer.parseInt( ref.getText( ) );
			if ( i > max || i < min ) {
				this.getInitNumber = 0;
				this.isValid = false;
				return;
			}
			this.isValid = true;
			getInitNumber = i;
			return;

		} catch ( NumberFormatException ex ) {
			ref.setBorder( BorderFactory.createLineBorder( invalid, 1 ) );
			this.isValid = false;
			actionIfInvalid( );
			this.getInitNumber = 0;
			this.isValid = false;
			return;
		}

	}

	public void actionIfValid( ) {

	}

	public void actionIfInvalid( ) {

	}

	public int getCurrentValue( ) throws IllegalStateException {

		if ( !isValid )
			throw new IllegalStateException( "The state of the box is not currently valid." );

		return this.getInitNumber;
	}

	public boolean isValid( ) {

		return this.isValid;

	}

	@Override
	public void insertUpdate( DocumentEvent e ) {

		try {
			if ( ref.getText( ).isEmpty( ) && allowEmpty ) {
				ref.setBorder( BorderFactory.createLineBorder( valid, 1 ) );
				this.isValid = true;
				actionIfValid( );
				this.getInitNumber = 0;
				return;
			}
			int i = Integer.parseInt( ref.getText( ) );
			if ( i > max || i < min ) {
				ref.setBorder( BorderFactory.createLineBorder( invalid, 1 ) );
				this.isValid = false;
				actionIfInvalid( );
				return;
			}
			ref.setBorder( BorderFactory.createLineBorder( valid, 1 ) );
			this.isValid = true;
			this.getInitNumber = i;
			actionIfValid( );

		} catch ( NumberFormatException ex ) {
			ref.setBorder( BorderFactory.createLineBorder( invalid, 1 ) );
			this.isValid = false;
			actionIfInvalid( );
		}

	}

	@Override
	public void removeUpdate( DocumentEvent e ) {

		try {
			if ( ref.getText( ).isEmpty( ) && allowEmpty ) {
				ref.setBorder( BorderFactory.createLineBorder( valid, 1 ) );
				this.isValid = true;
				actionIfValid( );
				this.getInitNumber = 0;
				return;
			}
			int i = Integer.parseInt( ref.getText( ) );
			if ( i > max || i < min ) {
				ref.setBorder( BorderFactory.createLineBorder( invalid, 1 ) );
				this.isValid = false;
				actionIfInvalid( );
				return;
			}
			ref.setBorder( BorderFactory.createLineBorder( valid, 1 ) );
			this.isValid = true;
			this.getInitNumber = i;
			actionIfValid( );

		} catch ( NumberFormatException ex ) {
			ref.setBorder( BorderFactory.createLineBorder( invalid, 1 ) );
			this.isValid = false;
			actionIfInvalid( );
		}

	}

	@Override
	public void changedUpdate( DocumentEvent e ) {

		try {
			if ( ref.getText( ).isEmpty( ) && allowEmpty ) {
				ref.setBorder( BorderFactory.createLineBorder( valid, 1 ) );
				this.isValid = true;
				actionIfValid( );
				this.getInitNumber = 0;
				return;
			}
			int i = Integer.parseInt( ref.getText( ) );
			if ( i > max || i < min ) {
				ref.setBorder( BorderFactory.createLineBorder( invalid, 1 ) );
				this.isValid = false;
				actionIfInvalid( );
				return;
			}
			ref.setBorder( BorderFactory.createLineBorder( valid, 1 ) );
			this.isValid = true;
			this.getInitNumber = i;
			actionIfValid( );

		} catch ( NumberFormatException ex ) {
			ref.setBorder( BorderFactory.createLineBorder( invalid, 1 ) );
			this.isValid = false;
			actionIfInvalid( );
		}

	}

}
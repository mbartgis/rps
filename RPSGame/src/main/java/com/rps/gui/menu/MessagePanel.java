/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-04-01 05:16:57 -0400 (Sun, 01 Apr 2018) $
 * $Revision: 339 $
 * $Author: mbartgis $
 */

package com.rps.gui.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import com.rps.gui.ModularPanel;
import com.rpscore.C;
import com.rpscore.audio.SoundManager;

public class MessagePanel extends ModularPanel {

	private String				message, subtext;

	private SelectionLabel		returnLabel;

	private MouseListener		mcl;

	private MouseMotionListener	mml;

	public MessagePanel( String message, String subtext ) {

		this.message = message;
		this.subtext = subtext;

		this.setLayout( null );

		this.setBackground( Color.BLACK );

		/* @formatter:off */
		this.returnLabel = new SelectionLabel( " Return" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				terminate( );
			}
		};
		this.returnLabel.setSize( 300, 100 );
		this.returnLabel.setLocation( 20, this.getHeight( ) - 150 );
		this.returnLabel.setBackground( new Color( 0x80FFFF80, true ) );
		this.returnLabel.setForeground( Color.WHITE );
		this.returnLabel.setFont( C.Fonts.MENU_FONT );

		this.add( this.returnLabel );

		repaint( );

	}

	@Override
	protected void paintComponent( Graphics g ) {
		super.paintComponent( g );

		if ( this.message == null || this.message.isEmpty( ) ) {
			return;
		}

		String[ ] print = this.message.split( "\n" );

		g.setFont( new Font( "Arial", Font.BOLD, 36 ) );
		g.setColor( Color.WHITE );

		if ( this.subtext == null || this.subtext.isEmpty( ) ) {
			for ( int i = 0; i < print.length; i++ ) {
				g.drawString( print[ i ], ( int ) ( this.getWidth( ) / 2.5 ), this.getHeight( ) / 2 + ( i * 30 ) );
			}
		} else {

			String[ ] printst = this.subtext.split( "\n" );

			for ( int i = 0; i < print.length; i++ ) {
				g.drawString( print[ i ], ( int ) ( this.getWidth( ) / 2.5 ), this.getHeight( ) / 10 + ( i * 30 ) );
			}

			g.setFont( new Font( "Arial", Font.BOLD, 20 ) );

			for ( int i = 0; i < printst.length; i++ ) {
				g.drawString( printst[ i ], ( int ) ( this.getWidth( ) / 2.5 ),
						this.getHeight( ) / 10 + ( print.length * 30 ) + ( i * 20 ) );
			}

		}

	}

	@Override
	public void updateGraphicsSettings( boolean allowRestart ) {}

	@Override
	public void onResize( int newWidth, int newHeight ) {
		this.setSize( newWidth, newHeight );
		this.returnLabel.setLocation( 20, this.getHeight( ) - 150 );
	}

	@Override
	public void onDestroy( ) {}

	@Override
	public void addInputControls( ) {
		this.mcl = new MouseListener( ) {

			@Override
			public void mouseReleased( MouseEvent e ) {}

			@Override
			public void mousePressed( MouseEvent e ) {

				MessagePanel.this.repaint( );

				int mousex = e.getX( );
				int mousey = e.getY( );

				if ( MessagePanel.this.returnLabel.isMouseInBounds( mousex, mousey ) ) {
					MessagePanel.this.returnLabel.onClick( );
				}

			}

			@Override
			public void mouseExited( MouseEvent e ) {}

			@Override
			public void mouseEntered( MouseEvent e ) {}

			@Override
			public void mouseClicked( MouseEvent e ) {}
		};

		this.addMouseListener( this.mcl );

		this.mml = new MouseMotionListener( ) {

			@Override
			public void mouseMoved( MouseEvent e ) {

				int mousex = e.getX( );
				int mousey = e.getY( );

				if ( MessagePanel.this.returnLabel.isMouseInBounds( mousex, mousey ) ) {
					MessagePanel.this.returnLabel.setHighlighted( true );
					MessagePanel.this.returnLabel.repaint( );
				} else {
					MessagePanel.this.returnLabel.setHighlighted( false );
					MessagePanel.this.returnLabel.repaint( );
				}

			}

			@Override
			public void mouseDragged( MouseEvent e ) {}
		};

		this.addMouseMotionListener( this.mml );
	}

	@Override
	public void removeInputControls( ) {
		this.removeMouseListener( this.mcl );
		this.removeMouseMotionListener( this.mml );
	}

	@Override
	public void onPause( ) {}

	@Override
	public void onResume( ) {}

	@Override
	public void onCreate( ) {}

}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-08-30 21:23:28 -0400 (Thu, 30 Aug 2018) $
 * $Revision: 367 $
 * $Author: mbartgis $
 */

package com.rps.gui.menu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.TimeoutException;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.rps.Launcher;
import com.rps.gui.MainWindow;
import com.rps.gui.ModularPanel;
import com.rps.gui.game.online.OnlineGamePanel;
import com.rps.gui.menu.DocumentInputLimiter.TEXT_ALLOWANCE_POLICY;
import com.rpscore.C;
import com.rpscore.audio.SoundManager;
import com.rpscore.cfg.ConfigManager;
import com.rpscore.debug.Log;
import com.rpscore.server.Communicator;
import com.rpscore.server.MessageManager;

public class OnlineSessionMenu extends JComponent {

	private static ConfigManager	cfg	= ConfigManager.getConfigManager( );

	/*
	 * private JTextField[ ] ipOctetFields;
	 */

	private JTextField				host;

	private JTextField				portField, sessionNameField;

	private JLabel					serverIP, serverPort, sessionName, serverStatus;

	private JButton					joinButton;

	private ModularPanel			parent;

	public OnlineSessionMenu( ModularPanel nParent ) {
		super( );

		this.parent = nParent;

		this.setLayout( null );

		this.serverIP = new JLabel( "Server IP Address" );
		this.serverIP.setSize( 300, 60 );
		this.serverIP.setLocation( 20, 20 );
		this.serverIP.setForeground( Color.BLUE );
		this.serverIP.setFont( C.Fonts.MENU_MOTD_FONT );
		this.add( this.serverIP );

		String ipStr = cfg.getSetting( "LastIPAddress", "127.0.0.1" );
		// String[ ] ipAddress = ipStr.split( "\\." );

		// if ( !isIPAddressValid( ipAddress ) ) {
		// ipAddress = new String[ ] { "127", "0", "0", "1" };
		// }

		this.host = new JTextField( );
		this.host.setSize( 400, 60 );
		this.host.setLocation( 20, 100 );
		this.host.setFont( C.Fonts.MENU_MOTD_FONT );
		this.host.setText( ipStr );
		this.add( this.host );

		/* @formatter:off
		this.ipOctetFields = new JTextField[ 4 ];

		for ( int i = 0; i < this.ipOctetFields.length; i++ ) {

			DocumentInputLimiter ipOctetDocument = new DocumentInputLimiter( 3 );
			ipOctetDocument.setTextAllowancePolicy( TEXT_ALLOWANCE_POLICY.NUMBERS_ONLY );
			ipOctetDocument.setLowerBound( 0 );
			ipOctetDocument.setUpperBound( 255 );

			this.ipOctetFields[ i ] = new JTextField( );
			this.ipOctetFields[ i ].setDocument( ipOctetDocument );
			this.ipOctetFields[ i ].setText( ipAddress[ i ] );
			this.ipOctetFields[ i ].setSize( 90, 60 );
			this.ipOctetFields[ i ].setLocation( i * 100 + 20, 100 );
			this.ipOctetFields[ i ].setFont( MenuPanel.MOTD_FONT );
			this.ipOctetFields[ i ].getDocument( ).addDocumentListener( new DocumentListener( ) {

				@Override
				public void removeUpdate( DocumentEvent e ) {
					if ( aFieldIsEmpty( ) ) {
						joinButton.setEnabled( false );
					} else {
						joinButton.setEnabled( true );
					}
				}

				@Override
				public void insertUpdate( DocumentEvent e ) {
					if ( aFieldIsEmpty( ) ) {
						joinButton.setEnabled( false );
					} else {
						joinButton.setEnabled( true );
					}
				}

				@Override
				public void changedUpdate( DocumentEvent e ) {
					if ( aFieldIsEmpty( ) ) {
						joinButton.setEnabled( false );
					} else {
						joinButton.setEnabled( true );
					}
				}
			} );

			this.add( this.ipOctetFields[ i ] );


		}
		 * @formatter:on
		 */

		this.serverPort = new JLabel( "Server Port" );
		this.serverPort.setSize( 200, 60 );
		this.serverPort.setLocation( 20, 180 );
		this.serverPort.setForeground( Color.BLUE );
		this.serverPort.setFont( C.Fonts.MENU_MOTD_FONT );
		this.add( this.serverPort );

		this.serverStatus = new JLabel( );
		this.serverStatus.setSize( 500, 60 );
		this.serverStatus.setLocation( 20, 500 );
		this.serverStatus.setForeground( Color.BLUE );
		this.serverStatus.setFont( C.Fonts.MENU_MOTD_FONT );
		this.add( this.serverStatus );

		DocumentInputLimiter portDocument = new DocumentInputLimiter( 5 );
		portDocument.setTextAllowancePolicy( TEXT_ALLOWANCE_POLICY.NUMBERS_ONLY );
		portDocument.setLowerBound( 0 );
		portDocument.setUpperBound( 65535 );

		String port = cfg.getSetting( "LastPort", "22000" );

		this.portField = new JTextField( );
		this.portField.setDocument( portDocument );
		this.portField.setText( port );
		this.portField.setSize( 200, 60 );
		this.portField.setLocation( 20, 260 );
		this.portField.setFont( C.Fonts.MENU_MOTD_FONT );
		this.portField.getDocument( ).addDocumentListener( new DocumentListener( ) {

			@Override
			public void removeUpdate( DocumentEvent e ) {
				if ( aFieldIsEmpty( ) ) {
					joinButton.setEnabled( false );
				} else {
					joinButton.setEnabled( true );
				}
			}

			@Override
			public void insertUpdate( DocumentEvent e ) {
				if ( aFieldIsEmpty( ) ) {
					joinButton.setEnabled( false );
				} else {
					joinButton.setEnabled( true );
				}
			}

			@Override
			public void changedUpdate( DocumentEvent e ) {
				if ( aFieldIsEmpty( ) ) {
					joinButton.setEnabled( false );
				} else {
					joinButton.setEnabled( true );
				}
			}
		} );

		this.add( this.portField );

		this.sessionName = new JLabel( "Session Name" );
		this.sessionName.setSize( 300, 60 );
		this.sessionName.setLocation( 20, 340 );
		this.sessionName.setForeground( Color.BLUE );
		this.sessionName.setFont( C.Fonts.MENU_MOTD_FONT );
		this.add( this.sessionName );

		String userName = cfg.getSetting( "SessionName", "Player" );

		DocumentInputLimiter sessionNameDocument = new DocumentInputLimiter( 16 );
		sessionNameDocument.setTextAllowancePolicy( TEXT_ALLOWANCE_POLICY.ALL_CHARACTERS );

		this.sessionNameField = new JTextField( );
		this.sessionNameField.setDocument( sessionNameDocument );
		this.sessionNameField.setText( userName );
		this.sessionNameField.setSize( 400, 60 );
		this.sessionNameField.setLocation( 20, 420 );
		this.sessionNameField.setFont( C.Fonts.MENU_MOTD_FONT );
		this.sessionNameField.getDocument( ).addDocumentListener( new DocumentListener( ) {

			@Override
			public void removeUpdate( DocumentEvent e ) {
				if ( aFieldIsEmpty( ) ) {
					joinButton.setEnabled( false );
				} else {
					joinButton.setEnabled( true );
				}
			}

			@Override
			public void insertUpdate( DocumentEvent e ) {
				if ( aFieldIsEmpty( ) ) {
					joinButton.setEnabled( false );
				} else {
					joinButton.setEnabled( true );
				}
			}

			@Override
			public void changedUpdate( DocumentEvent e ) {
				if ( aFieldIsEmpty( ) ) {
					joinButton.setEnabled( false );
				} else {
					joinButton.setEnabled( true );
				}
			}
		} );

		this.add( this.sessionNameField );

		this.joinButton = new JButton( "Join Game" );
		this.joinButton.setSize( 200, 60 );
		this.joinButton.setLocation( ( this.getWidth( ) / 2 ) - ( this.joinButton.getWidth( ) / 2 ),
				this.getHeight( ) - 82 );
		this.joinButton.setFont( C.Fonts.MENU_MOTD_FONT );
		this.joinButton.addActionListener( new ActionListener( ) {

			@Override
			public void actionPerformed( ActionEvent e ) {

				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );

				if ( aFieldIsEmpty( ) ) {
					return;
				}

				MainWindow parentWindow = MainWindow.getMainWindow( );

				/*
				 * String ip = ipOctetFields[ 0 ].getText( ) + "." +
				 * ipOctetFields[ 1 ].getText( ) + "." + ipOctetFields[ 2
				 * ].getText( ) + "." + ipOctetFields[ 3 ].getText( );
				 */

				String ip = host.getText( );

				cfg.modifySetting( "LastIPAddress", ip );
				cfg.modifySetting( "LastPort", portField.getText( ) );

				int absport = Integer.parseInt( portField.getText( ) );

				OnlineGamePanel ogp = null;

				Communicator session = null;

				try {

					Socket connection = new Socket( ip, Integer.parseInt( port ) );

					session = new Communicator( connection );

					if ( session == null || session.isClosed( ) ) {
						// TODO popup error
						return;
					}

					session.getNext( 5000 );

					byte[ ] nameMsg = MessageManager.createJoinMessage( sessionNameField.getText( ) );
					session.sendMessage( nameMsg );

					byte[ ] assignment = session.getNext( 5000 );

					session.sendMessage( MessageManager.createAckMessage( ) );
					byte[ ] map = session.getNext( 5000 );

					session.sendMessage( MessageManager.createAckMessage( ) );
					byte[ ] difficulty = session.getNext( 5000 );

					int mapID = MessageManager.getAssignmentFromMessage( map );

					int assgt = MessageManager.getAssignmentFromMessage( assignment );

					ogp = new OnlineGamePanel( sessionNameField.getText( ), session );

					ogp.setSessionID( assgt );
					ogp.setMap( mapID );
					double diff = MessageManager.decodeDifficulty( difficulty );
					ogp.getGameInstance( ).setDifficulty( diff );

					Log.info( "Set server difficulty at: " + diff );

				} catch ( IOException e1 ) {

					parentWindow.popPanel( );

					String errorMsg = "Could not connect to host...\n\n\n" + e1.getMessage( );
					String subtext = "";

					if ( Launcher.GLOBAL_PARAMETERS.IS_DEBUG ) {
						subtext += "\n\nStack Trace:\n\n";

						StackTraceElement[ ] st = e1.getStackTrace( );

						for ( int i = 0; i < st.length; i++ ) {
							subtext += st[ i ].getClassName( ) + "." + st[ i ].getMethodName( ) + ", Line: "
									+ st[ i ].getLineNumber( ) + "\n";
						}

					}

					SoundManager.getSoundManager( ).fadeCurrentBGTrack( );
					parent.reinstantiateOnResume( );
					parentWindow.pushPanel( new MessagePanel( errorMsg, subtext ) );
					return;
				} catch ( TimeoutException e1 ) {

					if ( session != null ) {
						session.disconnect( );
					}

					parentWindow.popPanel( );

					String errorMsg = "Server stopped communicating...\n\n\n" + e1.getMessage( );
					String subtext = "";

					if ( Launcher.GLOBAL_PARAMETERS.IS_DEBUG ) {
						subtext += "\n\nStack Trace:\n\n";

						StackTraceElement[ ] st = e1.getStackTrace( );

						for ( int i = 0; i < st.length; i++ ) {
							subtext += st[ i ].getClassName( ) + "." + st[ i ].getMethodName( ) + ", Line: "
									+ st[ i ].getLineNumber( ) + "\n";
						}

					}

					SoundManager.getSoundManager( ).fadeCurrentBGTrack( );
					parent.reinstantiateOnResume( );
					parentWindow.pushPanel( new MessagePanel( errorMsg, subtext ) );
					return;
				}

				SoundManager.getSoundManager( ).fadeCurrentBGTrack( );
				parent.reinstantiateOnResume( );
				parentWindow.pushPanel( ogp );

			}
		} );
		this.add( joinButton );

		this.setVisible( false );

		this.repaint( );

	}

	private boolean isIPAddressValid( String[ ] ipAddress ) {
		// Test to ensure that the IP address is correct, if it is not, set a
		// default IP.
		if ( ipAddress.length != 4 ) {
			return false;
		} else {
			for ( int i = 0; i < ipAddress.length; i++ ) {
				try {
					int test = Integer.parseInt( ipAddress[ i ] );
					if ( test > 255 || test < 0 ) {
						return false;
					}
				} catch ( NumberFormatException e ) {
					return false;
				}
			}
		}
		return true;
	}

	private boolean aFieldIsEmpty( ) {
		return /*
				 * this.ipOctetFields[ 0 ].getText( ).isEmpty( ) ||
				 * this.ipOctetFields[ 1 ].getText( ).isEmpty( ) ||
				 * this.ipOctetFields[ 2 ].getText( ).isEmpty( ) ||
				 * this.ipOctetFields[ 3 ].getText( ).isEmpty( )
				 */
		this.host.getText( ).isEmpty( ) || this.portField.getText( ).isEmpty( )
				|| this.sessionNameField.getText( ).isEmpty( );
	}

	public void onResize( int newWidth, int newHeight ) {
		this.setSize( newWidth, newHeight );
		this.joinButton.setLocation( ( this.getWidth( ) / 2 ) - ( this.joinButton.getWidth( ) / 2 ),
				this.getHeight( ) - 82 );
	}

	@Override
	protected void paintComponent( Graphics g ) {
		super.paintComponent( g );

		if ( this.isVisible( ) ) {
			g.setColor( Color.BLACK );
			g.fillRect( 4, 4, this.getWidth( ) - 9, this.getHeight( ) - 9 );

			g.setColor( Color.BLUE );
			g.drawRect( 0, 0, this.getWidth( ) - 1, this.getHeight( ) - 1 );
			g.drawRect( 2, 2, this.getWidth( ) - 5, this.getHeight( ) - 5 );

		}

	}

	@Override
	public void setVisible( boolean aFlag ) {

		Thread asyncPingTask = null;
		// Fetch the server ping when this menu is popped open.
		if ( aFlag ) {
			Runnable pingTask = ( ) -> {

				Log.info( "Pinging: " + this.host.getText( ) + ":" + this.portField.getText( ) );

				Socket connection = null;
				Communicator session = null;
				try {
					connection = new Socket( this.host.getText( ), Integer.parseInt( this.portField.getText( ) ) );
					session = new Communicator( connection );
				} catch ( IOException | NumberFormatException e ) {

					if ( session != null ) {
						session.forceDisconnect( );
					}

					serverStatus.setText( "*Server Unavailable" );
					serverStatus.setForeground( Color.RED );

					return;
				}

				if ( session == null || session.isClosed( ) ) {
					// TODO popup error
					if ( session != null ) {
						session.forceDisconnect( );
					}
					return;
				}

				try {
					session.getNext( 5000 );
				} catch ( TimeoutException e ) {

					session.disconnect( );

					serverStatus.setText( "*Server Unavailable" );
					serverStatus.setForeground( Color.RED );
					Thread.currentThread( ).interrupt( );
				}

				Log.info( "Connected for ping: " + this.host.getText( ) + ":" + this.portField.getText( ) );

				long time = System.currentTimeMillis( );
				session.sendMessage( MessageManager.createPingMessage( ) );

				try {
					byte[ ] msg = session.getNext( 5000 );
					time = System.currentTimeMillis( ) - time;

					Log.info( "Received PONG from: " + this.host.getText( ) + ":" + this.portField.getText( ) );

					int serverVersion = -1;

					try {
						serverVersion = MessageManager.getIntFromByteArray( msg, 1 );
					} catch ( ArrayIndexOutOfBoundsException | NullPointerException ex ) {
						Log.error( "Server did not give a build version." );
					}

					if ( serverVersion == C.BUILD_NUMBER ) {
						serverStatus.setText( "*Server Available: " + time + "ms" );
						if ( time < 100 ) {
							serverStatus.setForeground( Color.GREEN );
						} else if ( time < 200 ) {
							serverStatus.setForeground( Color.YELLOW );
						} else {
							serverStatus.setForeground( Color.ORANGE );
						}
					} else if ( serverVersion < C.BUILD_NUMBER ) {
						serverStatus.setText( "*OUTDATED SERVER" );

						serverStatus.setForeground( Color.RED );

					} else {
						serverStatus.setText( "*OUTDATED CLIENT" );
						serverStatus.setForeground( Color.RED );

					}

				} catch ( TimeoutException e ) {
					session.forceDisconnect( );

					serverStatus.setText( "*Server Unavailable" );
					serverStatus.setForeground( Color.RED );

					Thread.currentThread( ).interrupt( );
				}

				session.forceDisconnect( );

			};

			asyncPingTask = new Thread( pingTask );
			asyncPingTask.setName( "Async-Ping Task" );
			asyncPingTask.setDaemon( true );
			asyncPingTask.start( );

			repaint( );

		}

		// Allow the frame to render while the async task is running, both are
		// long processes and this will reduce the time waited by the user.
		super.setVisible( aFlag );

	}

}

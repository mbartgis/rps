/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-04-01 05:16:57 -0400 (Sun, 01 Apr 2018) $
 * $Revision: 339 $
 * $Author: mbartgis $
 */

package com.rps.gui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import com.rps.Launcher;
import com.rpscore.cfg.ConfigManager;

@SuppressWarnings( "serial" )
public abstract class ModularPanel extends JPanel {

	private boolean reinstantiate;

	public ModularPanel( ) {
		super( );
		this.reinstantiate = false;
	}

	@Override
	protected void paintComponent( Graphics g ) {
		super.paintComponent( g );

		if ( Launcher.GLOBAL_PARAMETERS.IS_DEBUG
				&& ConfigManager.getConfigManager( ).getSetting( "DisplayBoundingBoxes", false ) ) {
			g.setColor( Color.WHITE );
			g.drawRect( 0, 0, 1366, 768 );
			g.drawRect( 0, 0, 1920, 1080 );
			g.drawRect( 0, 0, 2560, 1440 );
			g.drawRect( 0, 0, 3840, 2160 );
		}
	}

	public abstract void updateGraphicsSettings( boolean allowRestart );

	public void terminate( ) {
		MainWindow.getMainWindow( ).popPanel( );
	}

	public abstract void onResize( int newWidth, int newHeight );

	public abstract void onCreate( );

	public abstract void onDestroy( );

	public abstract void addInputControls( );

	public abstract void removeInputControls( );

	public abstract void onPause( );

	protected abstract void onResume( );

	public final void resume( ) {
		if ( this.reinstantiate ) {
			this.reinstantiate = false;
			onCreate( );
		}
		onResume( );
	}

	public final void reinstantiateOnResume( ) {
		this.reinstantiate = true;
	}

}

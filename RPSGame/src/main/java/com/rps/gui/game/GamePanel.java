/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-07-26 22:16:38 -0400 (Thu, 26 Jul 2018) $
 * $Revision: 363 $
 * $Author: mbartgis $
 */

package com.rps.gui.game;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

import javax.swing.Timer;

import com.rps.gui.MainWindow;
import com.rps.gui.ModularPanel;
import com.rps.gui.SwingGraphicsAdapter;
import com.rps.gui.game.online.OnlineGamePanel;
import com.rps.gui.menu.OptionsMenu;
import com.rps.gui.menu.SelectionLabel;
import com.rpscore.C;
import com.rpscore.C.SettingKeys;
import com.rpscore.audio.SoundManager;
import com.rpscore.cfg.ConfigManager;
import com.rpscore.debug.Log;
import com.rpscore.entities.Entity;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.controlled.PlayerCursor;
import com.rpscore.game.GameInstance;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.gui.game.Popup;
import com.rpscore.gui.game.Popup.ANIMATION;
import com.rpscore.gui.game.WaveDisplay;
import com.rpscore.gui.game.WeaponSelectMenu;
import com.rpscore.lib.FileUtils;
import com.rpscore.lib.GUIUtils;
import com.rpscore.lib.TimedMessage;
import com.rpscore.terrain.FogGenerator;

public class GamePanel extends ModularPanel {

	protected static transient Log				LOGGER				= Log.getLogger( );

	protected transient SoundManager			SM					= SoundManager.getSoundManager( );

	/**
	 *
	 */
	protected static final transient long		serialVersionUID	= 1105794473303234154L;

	public static final transient int			ONE_SECOND			= 60;

	protected transient ConfigManager			cfgm				= ConfigManager.getConfigManager( );

	public static transient int					FPS					= 1000 / 60;

	protected static transient Font				FPS_FONT			= new Font( "Consolas", Font.BOLD, 20 );

	protected transient Timer					clock;

	public static final transient int			GFX_THREAD_COUNT	= 1;

	// FPS variables
	protected transient Timer					fpsclock;
	protected transient int						fpsCnt;
	protected transient boolean					displayFPS;
	protected transient int						displayFPSLocation;
	protected transient Color					displayFPSColor;
	protected transient int						lastFPS;

	protected transient InGamePauseMenu			igpm;

	protected transient Semaphore				fpsMutex;

	protected transient volatile Popup			songTitlePopup;

	protected transient WaveDisplay				waveDisplay;

	protected transient MouseListener			mcl;
	protected transient MouseMotionListener		mml;
	protected transient KeyListener				kl;
	protected transient MouseWheelListener		mwl;

	protected transient GameInstance			game;

	protected transient boolean					chatActive;

	protected transient LinkedList< String >	chatHistory;

	protected transient StringBuilder			messageDraft;

	protected transient List< Popup >			killfeed;

	// Graphics options
	protected transient boolean					capFPS;
	protected transient int						maxFPS;

	private static final int					SECOND_TO_NANO		= 1000000000;
	private static final int					MILLI_TO_NANO		= 1000000;

	// Debug options

	protected transient RenderThread[ ]			gfxLoop;

	public GamePanel( long seed, int mapSize, boolean createLoops ) {
		super( );

		this.setBackground( Color.BLACK );
		this.game = new GameInstance( null, seed, mapSize );

		this.game.setOnline( !createLoops );

		this.setLocation( 0, 0 );
		this.setLayout( null );

		this.fpsMutex = new Semaphore( 1 );

		this.gfxLoop = new RenderThread[ GamePanel.GFX_THREAD_COUNT ];

		this.displayFPS = Boolean.parseBoolean( cfgm.getSetting( C.SettingKeys.DISPLAY_FPS, "false" ) );

		if ( this.displayFPS ) {
			this.displayFPSLocation = Integer.parseInt( cfgm.getSetting( C.SettingKeys.FPS_DISPLAY_LOCATION, "1" ) );
			int color = ( int ) Long.parseLong( cfgm.getSetting( C.SettingKeys.FPS_DISPLAY_COLOR, "FFFFFFFF" ), 16 );
			this.displayFPSColor = new Color( color );
			this.lastFPS = 0;
		}

		this.capFPS = cfgm.getSetting( C.SettingKeys.CAP_FPS, true );
		this.maxFPS = cfgm.getSetting( C.SettingKeys.FPS_LIMIT, 60 );

		if ( createLoops ) {
			this.clock = new Timer( FPS, new GameLoop( ) );
			if ( this.displayFPS ) {
				this.fpsclock = new Timer( 1000, new FPSLoop( ) );
			} else {
				this.fpsclock = null;
			}
			startClock( );
		}
		this.fpsCnt = 0;

		this.igpm = new InGamePauseMenu( GamePanel.this, 0, 0, 0, 0 );

		this.igpm.setWidth( this.getWidth( ) / 3 );
		this.igpm.setHeight( this.getHeight( ) / 2 );
		this.igpm.setX( ( this.getWidth( ) / 2 ) - ( this.igpm.getWidth( ) / 2 ) );
		this.igpm.setY( ( this.getHeight( ) / 2 ) - ( this.igpm.getHeight( ) / 2 ) );

		this.setFocusable( true );
		this.requestFocus( );

		Toolkit toolkit = Toolkit.getDefaultToolkit( );
		Image image = null;
		try {
			image = FileUtils.importImage( C.File.ICON_PLAYER_CURSOR );
		} catch ( IOException e1 ) {}
		if ( image != null ) {
			Cursor c = toolkit.createCustomCursor( image, new Point( this.getX( ), this.getY( ) ), "img" );
			this.setCursor( c );
		}

		this.waveDisplay = new WaveDisplay( this.game.getSpawner( ), this.getWidth( ) );

		this.waveDisplay.setWidth( this.getWidth( ) / 6 );
		this.waveDisplay.setHeight( this.getHeight( ) / 8 );

		this.waveDisplay.setX( this.getWidth( ) - ( this.waveDisplay.getWidth( ) + 20 ) );
		this.waveDisplay.setY( this.getHeight( ) - ( this.waveDisplay.getHeight( ) + 20 ) );

		this.chatActive = false;
		this.chatHistory = new LinkedList< >( );

		this.messageDraft = new StringBuilder( );

		if ( cfgm.getSetting( "GenerateFog", false ) ) {
			FogGenerator.update( GUIUtils.getSessionResolution( )[ 0 ], GUIUtils.getSessionResolution( )[ 1 ] );
		}

		this.killfeed = new ArrayList< >( 6 );

		Entity.setEntityContainer( this.game );

	}

	@Override
	public void onResize( int nWidth, int nHeight ) {

		boolean fullScreen = cfgm.getSetting( SettingKeys.FULLSCREEN,
				OptionsMenu.DEFAULT_SETTINGS.SETTING_FULLSCREEN_WINDOW );

		game.onResize( nWidth, nHeight, fullScreen );

		this.waveDisplay.setParentWidth( nWidth );

		if ( !fullScreen ) {
			this.setSize( nWidth, nHeight );

			int igpmw = this.getWidth( ) / 3;
			int igpmh = this.getHeight( ) / 2;
			int igpmx = ( this.getWidth( ) / 2 ) - ( igpmw / 2 );
			int igpmy = this.getHeight( ) / 3;

			this.igpm.setWidth( ( igpmw >= 400 ) ? igpmw : 400 );
			this.igpm.setHeight( ( igpmh >= 500 ) ? igpmh : 500 );
			this.igpm.setX( igpmx );
			this.igpm.setY( igpmy );

			this.waveDisplay.setWidth( nWidth / 6 );
			this.waveDisplay.setHeight( nHeight / 8 );

			this.waveDisplay.setX( nWidth - ( this.waveDisplay.getWidth( ) + 20 ) );
			this.waveDisplay.setY( nHeight - ( this.waveDisplay.getHeight( ) + 60 ) );
		} else {
			this.setSize( nWidth, nHeight );

			int igpmw = this.getWidth( ) / 3;
			int igpmh = this.getHeight( ) / 2;
			int igpmx = ( this.getWidth( ) / 2 ) - ( igpmw / 2 );
			int igpmy = this.getHeight( ) / 3;

			this.igpm.setWidth( ( igpmw >= 400 ) ? igpmw : 400 );
			this.igpm.setHeight( ( igpmh >= 500 ) ? igpmh : 500 );
			this.igpm.setX( igpmx );
			this.igpm.setY( igpmy );

			this.waveDisplay.setWidth( nWidth / 6 );
			this.waveDisplay.setHeight( nHeight / 8 );

			this.waveDisplay.setX( nWidth - ( this.waveDisplay.getWidth( ) + 20 ) );
			this.waveDisplay.setY( nHeight - ( this.waveDisplay.getHeight( ) + 20 ) );
		}

	}

	@Override
	public void updateGraphicsSettings( boolean allowRestart ) {

		this.game.updateGraphicsSettings( allowRestart );

		this.displayFPS = cfgm.getSetting( SettingKeys.DISPLAY_FPS, this.displayFPS );
		this.displayFPSLocation = cfgm.getSetting( SettingKeys.FPS_DISPLAY_LOCATION, 1 );
		int color = ( int ) Long.parseLong( cfgm.getSetting( SettingKeys.FPS_DISPLAY_COLOR, "FFFFFFFF" ), 16 );
		this.displayFPSColor = new Color( color );

		this.capFPS = cfgm.getSetting( SettingKeys.CAP_FPS, true );
		this.maxFPS = cfgm.getSetting( SettingKeys.FPS_LIMIT, 60 );

		if ( this.gfxLoop != null ) {
			for ( RenderThread gfx : this.gfxLoop ) {
				gfx.setMaxFPS( ( this.capFPS ) ? this.maxFPS : -1 );
			}
		}

		if ( allowRestart ) {
			MainWindow.getMainWindow( ).setFullscreen(
					cfgm.getSetting( SettingKeys.FULLSCREEN, OptionsMenu.DEFAULT_SETTINGS.SETTING_FULLSCREEN_WINDOW ) );
		}

	}

	public void setSongTitlePopup( Popup p ) {
		this.songTitlePopup = p;
	}

	public void startClock( ) {

		if ( this.clock != null ) {
			this.clock.start( );
		}
		if ( this.fpsclock != null ) {
			this.fpsclock.start( );
		}

		if ( this.igpm != null && this.igpm.skillsMenuAccessed( ) ) {
			this.game.loadWeaponSelectMenu( );
			igpm.unpause( );
		}
	}

	public void stopClock( ) {

		if ( this.getClass( ) == OnlineGamePanel.class ) {
			return;
		}

		if ( this.clock != null ) {
			this.clock.stop( );
		}
		if ( this.fpsclock != null ) {
			this.fpsclock.stop( );
		}
	}

	public void updateFPS( int fps ) {
		this.lastFPS = fps;
	}

	// Render Loop
	@Override
	protected void paintComponent( Graphics g ) {
		super.paintComponent( g );

		// TODO turn this into a static reference adapter.
		GraphicsAdapter gfxa = new SwingGraphicsAdapter( g );

		this.game.draw( gfxa );

		g.setColor( Color.WHITE );

		g.setFont( FPS_FONT );

		if ( this.displayFPS ) {

			g.setColor( this.displayFPSColor );

			int x = 20;
			int y = 20;
			// FPS Location is on the Right
			if ( ( this.displayFPSLocation % 2 ) != 0 ) {
				x = this.getWidth( ) - 40;
			}
			// FPS Location is on the Bottom
			if ( this.displayFPSLocation > 1 ) {
				y = this.getHeight( ) - 60;
			}
			g.drawString( "" + this.lastFPS, x, y );
		}

		ArrayList< String > debugText = new ArrayList< >( );

		if ( this.game.canDisplayDebugRotationData( ) ) {

			Player player = this.game.getPlayer( );

			debugText.add( "Hypotenuse: " + player.getPlayerMouseHypotenuse( ) + "\n" );
			debugText.add( "Angle:      " + ( player.getPlayerMouseAngleAbsoluteAsDegree( ) ) + "�\n" );

			g.drawLine( ( ( int ) player.getX( ) ), ( ( int ) player.getY( ) ), ( ( int ) player.getMX( ) ),
					( ( int ) player.getMY( ) ) );
			g.drawLine( ( ( int ) player.getX( ) ), ( ( int ) player.getY( ) ), ( ( int ) player.getMX( ) ),
					( ( int ) player.getY( ) ) );
			g.drawLine( ( ( int ) player.getMX( ) ), ( ( int ) player.getMY( ) ), ( ( int ) player.getMX( ) ),
					( ( int ) player.getY( ) ) );

		}

		// Draw all debug text
		for ( int i = 0; i < debugText.size( ); i++ ) {
			g.drawString( debugText.get( i ), 0, ( 20 * i ) + 20 + ( this.songTitlePopup != null ? 50 : 0 ) );
		}

		if ( !game.clearHUDForChat( )
				&& cfgm.getSetting( SettingKeys.HUD, OptionsMenu.DEFAULT_SETTINGS.SETTING_HUD ) ) {

			Iterator< Popup > pops = this.killfeed.iterator( );

			while ( pops.hasNext( ) ) {
				Popup p = pops.next( );
				p.draw( gfxa );
			}

			try {
				if ( this.songTitlePopup != null ) {
					songTitlePopup.draw( gfxa );
				}
			} catch ( NullPointerException e ) {
				/*
				 * This object is not synchronized with the game loop and render
				 * loop, and can be changed to null once it has entered the
				 * protective conditional. A mutex is not used to synchronize
				 * this object due to the time it takes to render and perform a
				 * game loop iteration. Attempting to draw and failing due to an
				 * NPE will be faster in the average case and prevent an FPS
				 * drop.
				 */
			}

			this.waveDisplay.draw( gfxa );
		}

		if ( this.game.isGamePaused( ) ) {
			g.setColor( new Color( 0x80000000, true ) );
			g.fillRect( GamePanel.this.getX( ), GamePanel.this.getY( ), GamePanel.this.getWidth( ),
					GamePanel.this.getHeight( ) );

			this.igpm.draw( gfxa );
		}

		if ( this.chatActive ) {

			int starty = this.getHeight( ) - ( this.getHeight( ) / 3 );

			int gamma = 150;
			g.setColor( new Color( gamma * ( 256 * 256 * 256 ), true ) );
			g.fillRect( 0, starty, this.getWidth( ), this.getHeight( ) );

			g.setColor( Color.WHITE );
			g.setFont( new Font( "Arial", Font.BOLD, 20 ) );

			Iterator< String > chat_itr = this.chatHistory.iterator( );
			g.drawString( messageDraft.toString( ), 25, this.getHeight( ) - 50 );
			g.drawRect( 20, this.getHeight( ) - 72, this.getWidth( ) - 60, 30 );

			int space = 0;
			while ( chat_itr.hasNext( ) ) {
				String s = chat_itr.next( );
				g.drawString( s, 20, ( starty + 20 ) + ( space * 20 ) );
				space++;
			}

		}

		if ( cfgm.getSetting( "GenerateFog", false ) ) {
			FogGenerator.draw( g );
		}

		try {
			this.fpsMutex.acquire( );
		} catch ( InterruptedException e ) {
			Thread.currentThread( ).interrupt( );
		}
		this.fpsCnt++;
		this.fpsMutex.release( );

	}

	@Override
	public void onDestroy( ) {
		this.stopClock( );
		if ( this.clock != null ) {
			this.clock.stop( );
		}
		this.game.onDestroy( );
		Entity.setEntityContainer( null );

		SoundManager.getSoundManager( ).fadeCurrentBGTrack( );

		dispose( );
	}

	public void dispose( ) {
		if ( this.gfxLoop != null ) {
			for ( RenderThread gfx : this.gfxLoop ) {
				if ( gfx != null ) {
					gfx.terminate( );
				}
			}
		}
	}

	public GameInstance getGameInstance( ) {
		return this.game;
	}

	protected class FPSLoop implements ActionListener {

		private long lastTimeFired;

		protected FPSLoop( ) {
			FPSLoop.this.lastTimeFired = System.nanoTime( );
		}

		@Override
		public void actionPerformed( ActionEvent arg0 ) {

			long temp = System.nanoTime( );
			long diff = temp - FPSLoop.this.lastTimeFired;

			double seconds = ( ( double ) diff ) / SECOND_TO_NANO;

			int calculatedFPS = ( int ) ( GamePanel.this.fpsCnt / seconds );

			GamePanel.this.updateFPS( calculatedFPS );

			FPSLoop.this.lastTimeFired = temp;

			synchronized ( GamePanel.this ) {
				GamePanel.this.fpsCnt = 0;
			}

		}

	}

	protected class GameLoop implements ActionListener {

		// Game Loop
		@Override
		public void actionPerformed( ActionEvent arg0 ) {

			game.update( );

			if ( SM.hasNewSong( ) ) {
				Popup p = new Popup( SM.getSongInfo( ), 300 );
				p.setAnimation( ANIMATION.FADE );
				p.setBackground( new Color( 0x3FFFFFFF, true ) );
				p.setForeground( new Color( 0xAA000000, true ) );
				p.setWidth( 300 );
				setSongTitlePopup( p );
			}

			if ( songTitlePopup != null ) {
				songTitlePopup.update( );
				if ( songTitlePopup.isFinished( ) ) {
					songTitlePopup = null;
				}
			}

			List< TimedMessage > kf = game.getKillfeed( );
			if ( kf != null ) {
				for ( int i = 0; i < kf.size( ); i++ ) {
					Popup p = new Popup( kf.remove( 0 ) );
					p.setY( ( i * 40 ) + 80 );
					p.setBackground( new Color( 0x8F000000, true ) );
					p.setForeground( new Color( 0xAAFFFFFF, true ) );
					p.setAnimation( Popup.ANIMATION.HORIZONTAL_SLIDE_POSITIVE );

					killfeed.add( p );

					/*
					 * g.drawString( killfeed.get( i ).getMessage( ), 0, + (
					 * this.songTitlePopup != null ? 50 : 0 ) + (
					 * debugText.size( ) * 30 ) );
					 */
				}
			}

			int cnt = 0;
			Iterator< Popup > pops = killfeed.iterator( );

			while ( pops.hasNext( ) ) {
				Popup p = pops.next( );
				if ( p.isFinished( ) ) {
					pops.remove( );
				} else {
					p.setY( ( ( 42 * cnt ) + 80 ) );
					p.update( );

					cnt++;
				}
			}

			for ( int i = 0; i < GamePanel.this.gfxLoop.length; i++ ) {
				if ( ( GamePanel.this.gfxLoop[ i ] == null ) ) {
					if ( GamePanel.this.capFPS ) {
						GamePanel.this.gfxLoop[ i ] = new RenderThread( GamePanel.this.maxFPS );
					} else {
						GamePanel.this.gfxLoop[ i ] = new RenderThread( -1 );
					}
					GamePanel.this.gfxLoop[ i ].start( );
				}
			}

			for ( int i = 0; i < GamePanel.this.gfxLoop.length; i++ ) {

				if ( GamePanel.this.gfxLoop[ i ] != null ) {

					if ( ( GamePanel.this.gfxLoop[ i ].waitRequested( ) ) ) {
						try {
							GamePanel.this.gfxLoop.wait( );
						} catch ( InterruptedException e ) {
							Thread.currentThread( ).interrupt( );
						}
					}

					if ( GamePanel.this.gfxLoop[ i ].isCloseRequested( ) ) {
						GamePanel.this.stopClock( );
					}
				}
			}

		}

	}

	private class RenderThread extends Thread {

		private volatile boolean	closeNotRequested;
		private volatile boolean	waitFlag;
		private int					fpslimit;

		public RenderThread( int fpslim ) {
			this.closeNotRequested = true;
			this.waitFlag = false;
			this.fpslimit = SECOND_TO_NANO / fpslim;
		}

		@SuppressWarnings( "unused" )
		public void setWaitFlag( ) {
			this.waitFlag = true;
		}

		public boolean waitRequested( ) {
			return this.waitFlag;
		}

		public void terminate( ) {
			this.closeNotRequested = false;
		}

		public void setMaxFPS( int fpslim ) {
			this.fpslimit = SECOND_TO_NANO / fpslim;
		}

		public boolean isCloseRequested( ) {
			return !this.closeNotRequested;
		}

		@Override
		public void run( ) {
			while ( closeNotRequested ) {

				long time = System.nanoTime( );
				GamePanel.this.repaint( );
				long timeAdj = System.nanoTime( ) - time;

				if ( fpslimit == -1 ) {
					continue;
				}

				long deltaTime = ( fpslimit - timeAdj );
				if ( deltaTime > 0 ) {
					try {
						long t = deltaTime / MILLI_TO_NANO;
						int n = ( int ) deltaTime % MILLI_TO_NANO;
						RenderThread.sleep( t, n );
					} catch ( InterruptedException e ) {
						Thread.currentThread( ).interrupt( );
					}
				}
			}

			Log.info( "GFX Loop Ended" );
		}

	}

	public void setNumberOfGFXThreads( int nThreads ) {
		if ( nThreads < 1 ) {
			throw new IllegalArgumentException( "At least one graphics Thread must be present to run." );
		}
	}

	@Override
	public void addInputControls( ) {

		this.kl = new KeyListener( ) {

			@Override
			public void keyTyped( KeyEvent e ) {}

			@Override
			public void keyReleased( KeyEvent e ) {
				Player player = game.getPlayer( );
				switch ( e.getKeyCode( ) ) {
					case KeyEvent.VK_W:
						player.setMoveUp( false );
						break;
					case KeyEvent.VK_S:
						player.setMoveDown( false );
						break;
					case KeyEvent.VK_A:
						player.setMoveLeft( false );
						break;
					case KeyEvent.VK_D:
						player.setMoveRight( false );
						break;
					case KeyEvent.VK_SPACE:
						player.setRunning( false );
						break;
					default:
						break;
				}
			}

			@Override
			public void keyPressed( KeyEvent e ) {

				Player player = game.getPlayer( );

				if ( chatActive && e.getKeyCode( ) != KeyEvent.VK_ESCAPE && e.getKeyCode( ) != KeyEvent.VK_ENTER ) {

					if ( ( e.getKeyCode( ) == KeyEvent.VK_DELETE || e.getKeyCode( ) == KeyEvent.VK_BACK_SPACE ) ) {
						if ( messageDraft.length( ) > 0 ) {
							messageDraft.deleteCharAt( messageDraft.length( ) - 1 );
						}
					} else {

						int x = e.getKeyChar( );

						if ( x < 32 || x > 126 ) {
							return;
						}

						messageDraft.append( e.getKeyChar( ) );
					}
				}

				switch ( e.getKeyCode( ) ) {

					case KeyEvent.VK_W:
						if ( !chatActive ) {
							player.setMoveUp( true );
							player.setMoveDown( false );
						}
						break;
					case KeyEvent.VK_S:

						if ( !chatActive ) {
							player.setMoveDown( true );
							player.setMoveUp( false );
						}
						break;
					case KeyEvent.VK_A:
						if ( !chatActive ) {
							player.setMoveLeft( true );
							player.setMoveRight( false );
						}
						break;
					case KeyEvent.VK_D:
						if ( !chatActive ) {
							player.setMoveRight( true );
							player.setMoveLeft( false );
						}
						break;
					case KeyEvent.VK_SPACE:
						if ( !chatActive ) {
							player.setRunning( true );
						}
						break;
					case KeyEvent.VK_T:
						if ( !game.isGamePaused( ) ) {
							chatActive = true;
							game.clearHUDForChat( chatActive );
						}

						break;
					case KeyEvent.VK_ENTER:
						sendMessage( messageDraft.toString( ) );
						// Clear the old message.
						messageDraft = new StringBuilder( );
						chatActive = false;
						game.clearHUDForChat( chatActive );
						break;
					case KeyEvent.VK_ESCAPE:
						if ( chatActive ) {
							chatActive = false;
							game.clearHUDForChat( chatActive );
						} else {
							if ( !game.isGamePaused( ) ) {
								stopClock( );
								game.setGamePaused( true );
							} else {
								startClock( );
								game.setGamePaused( false );
							}
						}
						break;
					default:
						break;
				}
			}
		};

		this.addKeyListener( this.kl );

		this.mcl = new MouseListener( ) {

			@Override
			public void mouseReleased( MouseEvent e ) {}

			@Override
			public void mousePressed( MouseEvent e ) {
				if ( game.isGamePaused( ) ) {
					SelectionLabel active = GamePanel.this.igpm.getActiveSelectionLabel( e.getX( ), e.getY( ) );
					if ( active != null ) {
						active.onClick( );
					}
				} else {
					Player player = game.getPlayer( );
					if ( e.getButton( ) == MouseEvent.BUTTON1 && player.canShoot( ) ) {
						player.shootPrimary( );
					} else if ( e.getButton( ) == MouseEvent.BUTTON3 ) {
						player.shootSecondary( );
					}
				}
			}

			@Override
			public void mouseExited( MouseEvent e ) {}

			@Override
			public void mouseEntered( MouseEvent e ) {}

			@Override
			public void mouseClicked( MouseEvent e ) {

			}
		};

		this.addMouseListener( this.mcl );

		this.mml = new MouseMotionListener( ) {

			@Override
			public void mouseMoved( MouseEvent e ) {

				if ( game.isGamePaused( ) ) {
					GamePanel.this.igpm.updateMenuHighlight( e.getX( ), e.getY( ) );
				} else {

				}

				Player player = game.getPlayer( );
				PlayerCursor playerCursor = game.getPlayerCursor( );

				player.setMousePosition( e.getX( ), e.getY( ) );
				playerCursor.setX( e.getX( ) );
				playerCursor.setY( e.getY( ) );

			}

			@Override
			public void mouseDragged( MouseEvent e ) {
				if ( game.isGamePaused( ) ) {
					GamePanel.this.igpm.updateMenuHighlight( e.getX( ), e.getY( ) );
				} else {

				}

				Player player = game.getPlayer( );
				PlayerCursor playerCursor = game.getPlayerCursor( );

				player.setMousePosition( e.getX( ), e.getY( ) );
				playerCursor.setX( e.getX( ) );
				playerCursor.setY( e.getY( ) );

			}
		};

		this.addMouseMotionListener( this.mml );

		this.mwl = new MouseWheelListener( ) {

			@Override
			public void mouseWheelMoved( MouseWheelEvent e ) {

				int dir = e.getWheelRotation( );

				Player player = game.getPlayer( );

				WeaponSelectMenu weaponSelect = game.getPlayerWeaponSelect( );

				if ( dir > 0 ) {
					weaponSelect.scroll( true );
				} else if ( dir < 0 ) {
					weaponSelect.scroll( false );
				} else {
					// Mouse has not moved, ignore this event.
				}

				player.setSecondaryWeapon( weaponSelect.getCurrentWeapon( ) );

			}
		};

		this.addMouseWheelListener( this.mwl );

		this.game.getPlayer( ).getPlayerManager( ).startPlayerTimeClock( );
		this.game.onWaveStart( );

	}

	@Override
	public void removeInputControls( ) {

		this.removeKeyListener( this.kl );
		this.removeMouseListener( this.mcl );
		this.removeMouseMotionListener( this.mml );
		this.removeMouseWheelListener( this.mwl );

		this.game.getPlayer( ).getPlayerManager( ).stopPlayerTimeClock( );

	}

	@Override
	public void onPause( ) {
		stopClock( );
	}

	@Override
	public void onResume( ) {

	}

	// Template for sending messages...
	public void sendMessage( String message ) {
		return;
	}

	@Override
	public void onCreate( ) {}

}

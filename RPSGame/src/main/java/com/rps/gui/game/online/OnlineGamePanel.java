/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-11-30 18:15:44 -0500 (Fri, 30 Nov 2018) $
 * $Revision: 373 $
 * $Author: mbartgis $
 */

package com.rps.gui.game.online;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.concurrent.TimeoutException;

import javax.swing.Timer;

import com.rps.Launcher;
import com.rps.gui.SwingGraphicsAdapter;
import com.rps.gui.game.GamePanel;
import com.rpscore.debug.Log;
import com.rpscore.entities.Entity;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.entities.spawned.SpawnedEntity;
import com.rpscore.entities.spawned.weapons.Bomb;
import com.rpscore.entities.spawned.weapons.FusionBomb;
import com.rpscore.game.GameInstance;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.lib.GUIUtils;
import com.rpscore.server.Communicator;
import com.rpscore.server.MessageManager;

public class OnlineGamePanel extends GamePanel {

	protected transient Communicator	session;

	protected String					sessionName;

	protected int						sessionID;

	protected int						heartbeatTimer;

	private double						lastx, lasty, lastmx, lastmy;

	private boolean						hasLoggedIn, hasSentInfo;

	private int							tickCounter;

	private long						dataSentInSecond;

	private long						dataReceivedInSecond;

	public OnlineGamePanel( String sessionName, Communicator session ) {
		// TODO get map sizing working correctly.
		super( 0L, 257, false );

		Log.info( "Sending Session Name: " + sessionName );

		this.sessionName = sessionName;

		this.lastx = 0;
		this.lasty = 0;
		this.lastmx = 0;
		this.lasty = 0;

		this.sessionID = 0;

		this.hasLoggedIn = false;
		this.hasSentInfo = false;

		this.tickCounter = 0;

		this.dataReceivedInSecond = 0;
		this.dataSentInSecond = 0;

		// Static setup

		this.session = session;

		this.game.getPlayer( ).setSession( this.session );

		if ( this.session == null ) {
			// The communicator failed to connect.
			return;
		}

		this.getGameInstance( ).getSpawner( ).setSlaveToServer( true );

		this.clock = new Timer( GamePanel.FPS, new OnlineGameLoop( ) );

		if ( this.displayFPS ) {
			// this.fpsclock = new Timer( 1000, new GamePanel.FPSLoop( ) );
		} else {
			this.fpsclock = null;
		}

		startClock( );

		// TODO implement this on the server side.

	}

	public void setSessionID( int id ) {
		this.sessionID = id;
		this.game.getPlayer( ).setID( id );
	}

	public int getSessionID( ) {
		return this.sessionID;
	}

	public void setMap( int id ) {
		if ( this.game != null ) {
			this.game.setMap( id );
		}
	}

	@Override
	public void onResize( int nWidth, int nHeight ) {
		super.onResize( nWidth, nHeight );
	}

	@Override
	public void onDestroy( ) {

		if ( this.session != null ) {

			byte[ ] leaveMessage = MessageManager.createQuitMessage( this.sessionID );

			this.session.sendMessage( leaveMessage );

			boolean serverAckQuit = false;

			while ( !serverAckQuit && this.session.getHeartbeatTime( ) < 30000 ) {
				super.onDestroy( );
				try {
					Thread.sleep( 1 );
				} catch ( InterruptedException e ) {
					Thread.currentThread( ).interrupt( );
				}

				byte[ ] msg = null;
				try {
					msg = session.getNext( 5000 );
				} catch ( TimeoutException e ) {
					break;
				}

				if ( MessageManager.isAckMessage( msg ) ) {
					serverAckQuit = true;
				}

			}

			this.session.disconnect( );
		}

	}

	@Override
	protected void paintComponent( Graphics g ) {
		super.paintComponent( g );

		// TODO turn this into a static reference adapter
		GraphicsAdapter gfxa = new SwingGraphicsAdapter( g );

		for ( Player p : this.game.getCurrentOtherPlayers( ) ) {
			if ( p != null ) {
				p.drawRemote( gfxa );
			}
		}

		g.setFont( new Font( "Consolas", Font.BOLD, 20 ) );
		if ( !game.isGamePaused( ) ) {
			if ( this.session != null ) {
				long latency = this.session.getHeartbeatTime( );
				if ( latency >= 3000 || Launcher.GLOBAL_PARAMETERS.IS_DEBUG ) {
					if ( ( latency < 5000 ) ) {
						g.setColor( Color.GREEN );
					} else if ( ( latency >= 5000 ) && ( latency < 10000 ) ) {
						g.setColor( Color.YELLOW );
					} else {
						g.setColor( Color.RED );
					}
					g.drawString( latency + "ms", this.getWidth( ) / 2, 80 );
				}

				g.setColor( Color.WHITE );
				long dataSent = this.session.getDataTransmitted( );
				g.drawString( dataSent + " Bytes Sent @" + this.dataSentInSecond + " Bytes/s", this.getWidth( ) / 2,
						140 );

				long dataReceived = this.session.getDataReceived( );
				g.drawString( dataReceived + " Bytes Received @" + this.dataReceivedInSecond + " Bytes/s",
						this.getWidth( ) / 2, 110 );

			}
		}

		if ( this.game != null && this.game.getMiniMap( ) != null ) {
			g.setColor( Color.WHITE );
			g.drawString( "My Session ID: " + this.sessionID, this.game.getMiniMap( ).getX( ), 300 );
			Iterator< Entity > otherEnts = game.getAllEntities( ).iterator( );

			int offset = 30;
			while ( otherEnts.hasNext( ) ) {
				Entity e = otherEnts.next( );
				g.drawString( e.getName( ) + ": " + e.getID( ), this.game.getMiniMap( ).getX( ), 300 + offset );
				offset += 30;
			}
		}

	}

	protected void handleMessages( ) {

		if ( this.session != null && !this.session.isClosed( ) ) {

			byte[ ][ ] messages = this.session.getMessages( );

			for ( byte[ ] message : messages ) {

				if ( message.length > 0 ) {

					// Determine the protocol
					switch ( message[ 0 ] ) {

						case ( MessageManager.PROTOCOL.MOVE ): {
							int addr = MessageManager.getAddressFromMessage( message );
							Iterator< Player > p_iter = this.game.getCurrentOtherPlayers( ).iterator( );

							while ( p_iter.hasNext( ) ) {
								Player pl = p_iter.next( );
								if ( pl.getID( ) == addr ) {
									Point p = MessageManager.getCoordinatesFromByteArray( message );
									pl.setRemoteLocation( p.x, p.y );
									break;
								}
							}
							break;
						}
						case ( MessageManager.PROTOCOL.FOCUS ): {
							int addr = MessageManager.getAddressFromMessage( message );
							Iterator< Player > p_iter = this.game.getCurrentOtherPlayers( ).iterator( );

							while ( p_iter.hasNext( ) ) {
								Player pl = p_iter.next( );
								if ( pl.getID( ) == addr ) {
									Point p = MessageManager.getCoordinatesFromByteArray( message );
									pl.setMousePosition( p.x, p.y );
									break;
								}
							}
							break;
						}
						case ( MessageManager.PROTOCOL.ACTION ): {

							int addr = MessageManager.getAddressFromMessage( message );

							if ( addr == this.sessionID ) {
								SpawnedEntity e = MessageManager.getPlayerAction( message );
								// Fusion Bomb Detonation
								if ( e == null ) {
									for ( Bomb bomb : this.game.getCurrentBombs( ) ) {
										if ( bomb instanceof FusionBomb && bomb.getSource( ).getID( ) == addr ) {

											bomb.detonate( );
											if (addr == this.game.getPlayer( ).getID( )) {
												this.game.getPlayer( ).removeActiveFusionBomb( );
											}
										}
									}
								} else {
									e.setSource( game.getPlayer( ) );

									this.game.addSpawnedEntity( e );
								}
								break;
							}

							Iterator< Player > p_iter = this.game.getCurrentOtherPlayers( ).iterator( );

							while ( p_iter.hasNext( ) ) {
								Player pl = p_iter.next( );
								if ( pl.getID( ) == addr ) {
									SpawnedEntity e = MessageManager.getPlayerAction( message );

									e.setSource( pl );

									this.game.addSpawnedEntity( e );
									break;
								}
							}

							break;
						}
						case MessageManager.PROTOCOL.MSG: {
							String msg_s = MessageManager.getTextMessageFromByteArray( message );
							this.chatHistory.add( msg_s );
							if ( chatHistory.size( ) > ( GUIUtils.getSessionResolution( )[ 1 ] / 77 ) ) {
								chatHistory.removeFirst( );
							}
							break;
						}
						case MessageManager.PROTOCOL.JOIN: {
							Player nPlayer = new Player( message );
							this.game.addOtherPlayer( nPlayer );
							this.game.getMiniMap( ).addEntity( nPlayer );
							break;
						}
						case MessageManager.PROTOCOL.QUIT: {
							int asgt = MessageManager.getAddressFromMessage( message );
							Iterator< Player > p_iter = this.game.getCurrentOtherPlayers( ).iterator( );

							while ( p_iter.hasNext( ) ) {
								Player pl = p_iter.next( );
								if ( pl.getID( ) == asgt ) {
									this.game.getMiniMap( ).removeEntity( pl );
									this.game.removeOtherPlayer( pl );
								}
							}
							break;
						}
						case ( MessageManager.PROTOCOL.STATEDEF ): {

							int asgt = MessageManager.getAddressFromMessage( message );

							Iterator< Player > p_iter = this.game.getCurrentOtherPlayers( ).iterator( );

							while ( p_iter.hasNext( ) ) {
								Player pl = p_iter.next( );
								if ( pl.getID( ) == asgt ) {
									MessageManager.updatePlayerStateDef( message, pl );
									break;
								}
							}
							break;
						}
						case ( MessageManager.PROTOCOL.SPAWN_ENEMY ): {

							try {
								Enemy e = MessageManager.decodeEnemy( message, this.game );

								// TODO make this duct tape a bit easier on the
								// eyes.
								this.game.getSpawner( ).getEnemies( ).add( e );
								this.game.getMiniMap( ).addEntity( e );
							} catch ( IllegalArgumentException e ) {}
							break;
						}
						case ( MessageManager.PROTOCOL.ENT_UPDATE ): {

							Iterator< Enemy > enItr = this.game.getSpawner( ).getEnemies( ).iterator( );

							boolean enemyFound = false;

							while ( !enemyFound && enItr.hasNext( ) ) {
								Enemy e = enItr.next( );
								if ( MessageManager.updateEnemy( message, e, this.game ) ) {
									enemyFound = true;
								}
							}

							if ( !enemyFound ) {
								int address = MessageManager.getIntFromByteArray( message, 2 );
								if ( this.session != null && !this.session.isClosed( ) ) {
									this.session.sendMessage(
											MessageManager.createEntityRequestMessage( this.sessionID, address ) );
								}
							}

							break;
						}
						case ( MessageManager.PROTOCOL.WAVE_START ): {
							int wave = MessageManager.decodeWaveMessage( message );
							this.game.getSpawner( ).triggerWaveStart( wave );
							break;
						}
						case ( MessageManager.PROTOCOL.WAVE_END ): {
							int xpReward = MessageManager.decodeWaveMessage( message );
							this.game.getPlayer( ).increaseScore( xpReward );
							this.game.getSpawner( ).triggerWaveEnd( );
							break;
						}

					}

				}
			}

		}

		if ( this.tickCounter % 60 == 0 && this.session != null ) {
			this.dataReceivedInSecond = this.session.getDataReceivedSincePoll( );
			this.dataSentInSecond = this.session.getDataTransmittedSincePoll( );
		}

		this.tickCounter++;

	}

	protected void sendUpdates( ) {

		if ( this.session != null && !this.session.isClosed( ) ) {

			if ( this.hasPlayerMovedX( ) || this.hasPlayerMovedY( ) ) {
				byte[ ] move = MessageManager.encodeCoordinates( MessageManager.PROTOCOL.MOVE, this.sessionID,
						( ( int ) game.getPlayer( ).getX( ) ), ( ( int ) game.getPlayer( ).getY( ) ) );

				this.session.sendMessage( move );
			}

			if ( this.hasPlayerMovedMX( ) || this.hasPlayerMovedMY( ) ) {
				byte[ ] move = MessageManager.encodeCoordinates( MessageManager.PROTOCOL.FOCUS, this.sessionID,
						( ( int ) game.getPlayer( ).getMX( ) ), ( ( int ) game.getPlayer( ).getMY( ) ) );

				this.session.sendMessage( move );
			}

		}

	}

	public void removePlayer( String playerName ) {
		for ( Player rp : this.game.getCurrentOtherPlayers( ) ) {
			if ( rp.getName( ).equals( playerName ) ) {
				this.game.getCurrentOtherPlayers( ).remove( rp );
			}
		}
	}

	protected boolean hasPlayerMovedY( ) {
		return this.game.getPlayer( ).getY( ) != this.lasty;
	}

	protected boolean hasPlayerMovedX( ) {
		return this.game.getPlayer( ).getX( ) != this.lastx;
	}

	protected boolean hasPlayerMovedMY( ) {
		return this.game.getPlayer( ).getMY( ) != this.lastmy;
	}

	protected boolean hasPlayerMovedMX( ) {
		return this.game.getPlayer( ).getMX( ) != this.lastmx;
	}

	protected class OnlineGameLoop extends GamePanel.GameLoop {
		@Override
		public void actionPerformed( ActionEvent arg0 ) {
			super.actionPerformed( arg0 );

			Iterator< Player > p_iter = game.getCurrentOtherPlayers( ).iterator( );

			while ( p_iter.hasNext( ) ) {
				Player pl = p_iter.next( );

				pl.setXOffset( GameInstance.PXO );
				pl.setYOffset( GameInstance.PYO );

				pl.updateRemote( );

			}

			handleMessages( );
			sendUpdates( );

			lastx = game.getPlayer( ).getX( );
			lasty = game.getPlayer( ).getY( );
			lastmx = game.getPlayer( ).getMX( );
			lastmy = game.getPlayer( ).getMY( );

			heartbeatTimer++;

			if ( ( heartbeatTimer % 50 ) == 0 ) {
				session.sendHeartbeat( );
			}

			if ( heartbeatTimer == 100 ) {
				heartbeatTimer = 0;
			}

		}
	}

	public String lookupPlayerName( int pid ) {
		for ( Player rp : this.game.getCurrentOtherPlayers( ) ) {
			if ( rp.getID( ) == pid ) {
				return rp.getName( );
			}
		}
		return null;
	}

	@Override
	public void sendMessage( String message ) {
		super.sendMessage( message );
		this.session
				.sendMessage( MessageManager.createTextMessage( this.sessionName + ": " + message, this.sessionID ) );
	}

}

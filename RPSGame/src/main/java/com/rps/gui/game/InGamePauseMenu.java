/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-07-26 22:16:38 -0400 (Thu, 26 Jul 2018) $
 * $Revision: 363 $
 * $Author: mbartgis $
 */

package com.rps.gui.game;

import java.awt.Color;

import com.rps.Launcher;
import com.rps.gui.MainWindow;
import com.rps.gui.SwingGraphicsAdapter;
import com.rps.gui.menu.CharacterPerkMenu;
import com.rps.gui.menu.OptionsMenu;
import com.rps.gui.menu.SelectionLabel;
import com.rpscore.C;
import com.rpscore.audio.SoundManager;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.lib.Drawable;

@SuppressWarnings( "serial" )
public class InGamePauseMenu implements Drawable {

	private int				x, y, width, height;

	private SelectionLabel	optionsLabel, characterLabel, exitLabel;

	private GamePanel		gpRef;

	private boolean			skillsMenuAccessed;

	public boolean skillsMenuAccessed( ) {
		return this.skillsMenuAccessed;
	}

	public void unpause( ) {
		this.skillsMenuAccessed = false;
	}

	public InGamePauseMenu( GamePanel reference, int locx, int locy, int nwidth, int nheight ) {

		this.gpRef = reference;
		this.x = locx;
		this.y = locy;
		this.width = nwidth;
		this.height = nheight;

		this.skillsMenuAccessed = false;

		/* @formatter:off */
		this.optionsLabel = new SelectionLabel( " Options" ) {
		/* @formatter:on */

			@SuppressWarnings( "unused" )
			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				OptionsMenu o = new OptionsMenu( );
				o.setCurrentGameInstance( reference );
				MainWindow.getMainWindow( ).pushPanel( o );

			}
		};
		this.optionsLabel.setSize( this.width - 6, ( this.height / 3 ) - 66 );
		this.optionsLabel.setLocation( this.x + 3, this.y + 43 );
		this.optionsLabel.setBackground( new Color( 0x80FFFFFF, true ) );
		this.optionsLabel.setForeground( Color.WHITE );
		this.optionsLabel.setFont( C.Fonts.MENU_FONT );

		/* @formatter:off */
		this.characterLabel = new SelectionLabel( " Skills" ) {
		/* @formatter:on */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				InGamePauseMenu.this.skillsMenuAccessed = true;

				MainWindow.getMainWindow( )
						.pushPanel( new CharacterPerkMenu( gpRef.getGameInstance( ).getPlayerManager( ) ) );
			}
		};
		this.characterLabel.setSize( this.width - 6, ( this.height / 3 ) - 66 );
		this.characterLabel.setLocation( this.x + 3, this.characterLabel.getHeight( ) + this.y + 43 );
		this.characterLabel.setBackground( new Color( 0x80FFFFFF, true ) );
		this.characterLabel.setForeground( Color.WHITE );
		this.characterLabel.setFont( C.Fonts.MENU_FONT );

		/* @formatter:off */
		this.exitLabel = new SelectionLabel( " Quit Game" ) {
		/* @formatter:off */

			@Override
			public void onClick( ) {
				SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
				MainWindow.getMainWindow( ).popPanel( );
				if ( Launcher.GLOBAL_PARAMETERS.IS_DEBUG && Launcher.GLOBAL_PARAMETERS.GAME_START ) {
					MainWindow.getMainWindow( ).terminate( );
				}
			}
		};
		this.exitLabel.setSize( this.width - 6, ( this.height / 3 ) - 66 );
		this.exitLabel.setLocation( this.x + 3, ( 2 * this.characterLabel.getHeight( ) ) + this.y + 43 );
		this.exitLabel.setBackground( new Color( 0x80FFFFFF, true ) );
		this.exitLabel.setForeground( Color.WHITE );
		this.exitLabel.setFont( C.Fonts.MENU_FONT  );

	}

	@Override
	public void draw( GraphicsAdapter g ) {

		g.setColor( Color.WHITE );
		g.setFont( C.Fonts.MENU_FONT  );
		String text = "Pause";
		g.drawString( text, this.x + ( this.width / 2 ) - ( ( g.getFont( ).getSize( ) ) ), this.y + 10 );

		g.setColor( Color.RED );
		g.drawRect( this.x, this.y + 40, this.width, this.height );
		g.drawRect( this.x + 2, this.y + 42, this.width - 4, this.height - 4 );

		this.characterLabel.paint( ( ( SwingGraphicsAdapter ) g ).getUnderlying( ) );
		this.optionsLabel.paint( ( ( SwingGraphicsAdapter ) g ).getUnderlying( ) );
		this.exitLabel.paint( ( ( SwingGraphicsAdapter ) g ).getUnderlying( ) );

	}

	/**
	 * @param mx
	 *            Current x coordinate of the mouse.
	 * @param my
	 *            current y coordinate of the mouse.
	 * @return Returns an active selection label if one is in bounds, if no
	 *         label is selected, this function returns null;
	 */
	public SelectionLabel getActiveSelectionLabel( int mx, int my ) {
		if ( this.characterLabel.isMouseInBounds( mx, my ) ) {
			return this.characterLabel;
		}
		if ( this.optionsLabel.isMouseInBounds( mx, my ) ) {
			return this.optionsLabel;
		}
		if ( this.exitLabel.isMouseInBounds( mx, my ) ) {
			return this.exitLabel;
		}

		return null;
	}

	public void updateMenuHighlight( int mx, int my ) {

		if ( this.optionsLabel.isMouseInBounds( mx, my ) ) {
			this.optionsLabel.setHighlighted( true );
		} else {
			this.optionsLabel.setHighlighted( false );
		}

		if ( this.characterLabel.isMouseInBounds( mx, my ) ) {
			this.characterLabel.setHighlighted( true );
		} else {
			this.characterLabel.setHighlighted( false );
		}

		if ( this.exitLabel.isMouseInBounds( mx, my ) ) {
			this.exitLabel.setHighlighted( true );
		} else {
			this.exitLabel.setHighlighted( false );
		}

	}

	public int getX( ) {
		return x;
	}

	public void setX( int x ) {
		this.x = x;
		this.optionsLabel.setLocation( this.x + 2, this.y + 2 );

		this.characterLabel.setLocation( this.x + 3, this.characterLabel.getHeight( ) + this.y + 43 );

		this.exitLabel.setLocation( this.x + 3, ( 2 * this.characterLabel.getHeight( ) ) + this.y + 43 );
	}

	public int getY( ) {
		return y;
	}

	public void setY( int y ) {
		this.y = y;
		this.optionsLabel.setLocation( this.x + 3, this.y + 43 );

		this.characterLabel.setLocation( this.x + 3, this.characterLabel.getHeight( ) + this.y + 43 );

		this.exitLabel.setLocation( this.x + 3, ( 2 * this.characterLabel.getHeight( ) ) + this.y + 43 );
	}

	public int getWidth( ) {
		return width;
	}

	public void setWidth( int width ) {
		this.width = width;
		this.optionsLabel.setSize( this.width - 6, ( this.height / 3 ) - 66 );

		this.characterLabel.setSize( this.width - 6, ( this.height / 3 ) - 66 );

		this.exitLabel.setSize( this.width - 6, ( this.height / 3 ) - 66 );
	}

	public int getHeight( ) {
		return height;
	}

	public void setHeight( int height ) {
		this.height = height;
		this.optionsLabel.setSize( this.width - 6, ( this.height / 3 ) - 66 );
		this.optionsLabel.setLocation( this.x + 3, this.y + 43 );

		this.characterLabel.setSize( this.width - 6, ( this.height / 3 ) - 66 );
		this.characterLabel.setLocation( this.x + 3, ( this.height / 3 ) + this.y + 43 );

		this.exitLabel.setSize( this.width - 6, ( this.height / 3 ) - 66 );
		this.exitLabel.setLocation( this.x + 3, ( 2 * ( this.height / 3 ) ) + this.y + 43 );
	}

}

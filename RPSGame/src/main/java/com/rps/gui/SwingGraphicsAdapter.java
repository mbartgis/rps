/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

package com.rps.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import com.rpscore.gui.game.GraphicsAdapter;

public class SwingGraphicsAdapter implements GraphicsAdapter {

	private Graphics g;

	public SwingGraphicsAdapter( Graphics ng ) {
		this.g = ng;
	}

	@Override
	public void setColor( Color color ) {
		g.setColor( color );
	}

	@Override
	public void setFont( Font font ) {
		g.setFont( font );
	}

	@Override
	public void drawImage( Image image, int x, int y ) {
		g.drawImage( image, x, y, null );
	}

	@Override
	public void drawString( String text, int x, int y ) {
		g.drawString( text, x, y );

	}

	@Override
	public void drawRect( int x, int y, int width, int height ) {
		g.drawRect( x, y, width, height );

	}

	@Override
	public void fillRect( int x, int y, int width, int height ) {
		g.fillRect( x, y, width, height );

	}

	@Override
	public void drawOval( int x, int y, int width, int height ) {
		g.drawOval( x, y, width, height );

	}

	@Override
	public void fillOval( int x, int y, int width, int height ) {
		g.fillOval( x, y, width, height );

	}

	@Override
	public void drawLine( int x1, int y1, int x2, int y2 ) {
		g.drawLine( x1, y1, x2, y2 );
	}

	@Override
	public Color getColor( ) {
		return g.getColor( );
	}

	@Override
	public Font getFont( ) {
		return g.getFont( );
	}

	public Graphics getUnderlying( ) {
		return this.g;
	}

}

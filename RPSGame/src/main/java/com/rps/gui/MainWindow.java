/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-04-01 05:16:57 -0400 (Sun, 01 Apr 2018) $
 * $Revision: 339 $
 * $Author: mbartgis $
 */

package com.rps.gui;

import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.Stack;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.rps.Launcher;
import com.rps.gui.menu.MenuPanel;
import com.rps.gui.menu.OptionsMenu;
import com.rpscore.C;
import com.rpscore.audio.SoundManager;
import com.rpscore.cfg.ConfigManager;
import com.rpscore.debug.Log;
import com.rpscore.lib.FileUtils;

public class MainWindow extends JFrame {

	/**
	 *
	 */
	private static final long				serialVersionUID	= 660022269964988726L;

	private static ModularPanel				contentPanel;

	private static Stack< ModularPanel >	panelStack;

	private SoundManager					sm					= SoundManager.getSoundManager( );

	private ConfigManager					cfg					= ConfigManager.getConfigManager( );

	private static MainWindow				MAIN_WINDOW;

	public static MainWindow getMainWindow( ) {
		return getMainWindow( false );
	}

	public static MainWindow getMainWindow( boolean newInstance ) {
		if ( MAIN_WINDOW == null || newInstance ) {
			MAIN_WINDOW = new MainWindow( );
		}

		return MAIN_WINDOW;
	}

	private MainWindow( ) {
		super( "RPG Pixel Survival" + " " + Launcher.STATE + "-" + Launcher.VERSION );

		try {
			Image i = FileUtils.importImage( C.File.RPS_LOGO );

			this.setIconImage( i );
		} catch ( IOException e ) {
			Log.getLogger( ).error( "Could not load RPS window decorator image." );
		}

		panelStack = new Stack< ModularPanel >( );

		boolean fullScreen = cfg.getSetting( "Fullscreen", OptionsMenu.DEFAULT_SETTINGS.SETTING_FULLSCREEN_WINDOW );

		if ( fullScreen ) {
			setExtendedState( JFrame.MAXIMIZED_BOTH );
			setUndecorated( true );
		} else {
			setExtendedState( JFrame.NORMAL );
			this.setSize( cfg.getSetting( "WindowWidth", 1920 ), cfg.getSetting( "WindowHeight", 1080 ) );
			setUndecorated( false );
		}

		this.setDefaultCloseOperation( JFrame.DO_NOTHING_ON_CLOSE );

		this.setLayout( null );

		this.addWindowListener( new WindowListener( ) {

			@Override
			public void windowOpened( WindowEvent e ) {}

			@Override
			public void windowIconified( WindowEvent e ) {}

			@Override
			public void windowDeiconified( WindowEvent e ) {}

			@Override
			public void windowDeactivated( WindowEvent e ) {}

			@Override
			public void windowClosing( WindowEvent e ) {
				terminate( );
			}

			@Override
			public void windowClosed( WindowEvent e ) {}

			@Override
			public void windowActivated( WindowEvent e ) {}

		} );

		this.addComponentListener( new ComponentListener( ) {

			@Override
			public void componentShown( ComponentEvent e ) {
				if ( contentPanel != null ) {
					contentPanel.onResize( MainWindow.this.getWidth( ), MainWindow.this.getHeight( ) );
				}
			}

			@Override
			public void componentResized( ComponentEvent e ) {
				if ( contentPanel != null ) {
					contentPanel.onResize( MainWindow.this.getWidth( ), MainWindow.this.getHeight( ) );
				}

				cfg.modifySetting( "WindowWidth", "" + MainWindow.this.getWidth( ) );
				cfg.modifySetting( "WindowHeight", "" + MainWindow.this.getHeight( ) );

			}

			@Override
			public void componentMoved( ComponentEvent e ) {
				if ( contentPanel != null ) {
					contentPanel.onResize( MainWindow.this.getWidth( ), MainWindow.this.getHeight( ) );

				}
			}

			@Override
			public void componentHidden( ComponentEvent e ) {}
		} );

		contentPanel = new MenuPanel( );
		contentPanel.reinstantiateOnResume( );

		contentPanel.addInputControls( );
		contentPanel.resume( );

		this.add( contentPanel );

		SwingUtilities.invokeLater( ( ) -> this.setVisible( true ) );

	}

	public void setFullscreen( boolean fullscreen ) {
		/*
		 * if ( this.getExtendedState( ) == ( ( fullscreen ) ?
		 * JFrame.MAXIMIZED_BOTH : JFrame.NORMAL ) ) { return; }
		 */

		Log.info( ( ( fullscreen ) ? "Enabling" : "Disabling" ) + " fullscreen window." );

		Launcher.relaunch = true;
		terminate( );

	}

	public void pushPanel( ModularPanel p ) {

		if ( p == null ) {
			Log.info( "Attempted to push a null panel ont the panel stack." );
			return;
		}

		Log.info( "Pushing new panel: " + p.getClass( ).getName( ) );
		contentPanel.onPause( );
		panelStack.push( contentPanel );
		this.remove( contentPanel );
		contentPanel.removeInputControls( );
		contentPanel = p;
		contentPanel.setSize( this.getWidth( ), this.getHeight( ) );
		contentPanel.addInputControls( );
		contentPanel.onResize( MainWindow.this.getWidth( ), MainWindow.this.getHeight( ) );
		contentPanel.onCreate( );
		contentPanel.setFocusable( true );
		add( contentPanel );
		contentPanel.repaint( );
		contentPanel.requestFocusInWindow( );
	}

	public void popPanel( ) {
		if ( !panelStack.isEmpty( ) ) {
			if ( contentPanel == null ) {
				Log.error( "Selected panel is null while the panel stack is not empty." );
				return;
			}
			Log.info( "Popping top panel: " + contentPanel.getClass( ).getName( ) );
			this.remove( contentPanel );
			contentPanel.removeInputControls( );
			contentPanel.onPause( );
			contentPanel.onDestroy( );
			contentPanel = panelStack.pop( );
			contentPanel.setSize( this.getWidth( ), this.getHeight( ) );
			contentPanel.addInputControls( );
			contentPanel.onResize( MainWindow.this.getWidth( ), MainWindow.this.getHeight( ) );
			contentPanel.resume( );
			contentPanel.setFocusable( true );
			this.add( contentPanel );
			contentPanel.repaint( );
			contentPanel.requestFocusInWindow( );
		}
	}

	@Deprecated
	public void setPanel( ModularPanel p ) {

		Log.info( "Setting panel to instance of: " + p.getClass( ).getName( ) );

		if ( contentPanel != null ) {
			this.remove( contentPanel );
			contentPanel.removeInputControls( );
			contentPanel.onPause( );
			contentPanel.onDestroy( );
		}
		contentPanel = p;
		contentPanel.setSize( this.getWidth( ), this.getHeight( ) );
		contentPanel.addInputControls( );
		contentPanel.onResize( MainWindow.this.getWidth( ), MainWindow.this.getHeight( ) );
		contentPanel.onResume( );
		contentPanel.setFocusable( true );
		add( contentPanel );
		contentPanel.repaint( );
		contentPanel.requestFocusInWindow( );
	}

	public void terminate( ) {

		Log.info( "Destroyed panel: " + contentPanel );

		if ( contentPanel != null ) {
			contentPanel.removeInputControls( );
			contentPanel.onDestroy( );
		}

		while ( !panelStack.isEmpty( ) ) {
			ModularPanel mp = panelStack.pop( );
			mp.removeInputControls( );
			mp.onDestroy( );
		}

		dispose( );

		Log.info( "Main window closed successfully." );

		Log.info( "Flushing local game settings..." );
		cfg.flushSettings( );

		if ( Launcher.relaunch ) {
			Log.info( "Relaunching instance of game..." );
			Launcher.main( Launcher.argstore );
		} else {
			// Create log dump for debugging.
			if ( Launcher.GLOBAL_PARAMETERS.IS_DEBUG ) {
				Log.getLogger( ).printLogToFile( );
			}
		}

	}

}

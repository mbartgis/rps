# RPS

## Code Coverage

### Pipeline
[![pipeline status](https://gitlab.com/mbartgis/rps/badges/master/pipeline.svg)](https://gitlab.com/mbartgis/rps/commits/master)

#### RPSCore
[![coverage report](https://gitlab.com/mbartgis/rps/badges/master/coverage.svg?job=test-core)](https://mbartgis.gitlab.io/rps/RPSCore/)

#### RPSGame - Java Swing
[![coverage report](https://gitlab.com/mbartgis/rps/badges/master/coverage.svg?job=test-game)](https://mbartgis.gitlab.io/rps/RPSGame/)

#### RPSServer
[![coverage report](https://gitlab.com/mbartgis/rps/badges/master/coverage.svg?job=test-server)](https://mbartgis.gitlab.io/rps/RPSServer/)

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date$
 * $Revision$
 * $Author$
 */

package com.rpscore.lib;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.imageio.ImageIO;

import com.rpscore.C;
import com.rpscore.debug.Log;

public class FileUtils {

	public static String[ ] getFileLines( File in_file ) {

		@SuppressWarnings( "resource" )
		Scanner in_stream = null;

		ArrayList< String > appendable_list = new ArrayList< String >( );

		String[ ] static_list;

		try {
			in_stream = new Scanner( in_file );
			while ( in_stream.hasNextLine( ) ) {
				appendable_list.add( in_stream.nextLine( ) );
			}
			static_list = new String[ appendable_list.size( ) ];
			for ( int i = 0; i < static_list.length; i++ ) {
				static_list[ i ] = appendable_list.get( i );
			}
		} catch ( IOException e ) {
			static_list = null;
		} finally {
			if ( in_stream != null ) {
				in_stream.close( );
			}
		}

		return static_list;
	}

	public static boolean writeLinesToFile( File fileToWrite, String[ ] linesToWrite ) {

		@SuppressWarnings( "resource" )
		PrintWriter out_stream = null;

		boolean operation_success = true;

		try {
			out_stream = new PrintWriter( fileToWrite );
			for ( String line : linesToWrite ) {
				out_stream.write( line + C.File.LS );
			}
		} catch ( IOException e ) {
			operation_success = false;
		} finally {
			if ( out_stream != null ) {
				out_stream.close( );
			}
		}

		return operation_success;

	}

	public static byte[ ] getRawFileContent( File in_file ) {

		byte[ ] ret = null;

		try {
			FileInputStream fin = new FileInputStream( in_file );
			int i = 0;
			ArrayList< Integer > bytes = new ArrayList< >( fin.available( ) );
			while ( ( i = fin.read( ) ) != -1 ) {
				bytes.add( i );
			}

			ret = new byte[ bytes.size( ) ];

			for ( int j = 0; j < ret.length; j++ ) {
				ret[ j ] = ( byte ) ( bytes.get( j ) & 0x000000FF );
			}

			fin.close( );
		} catch ( Exception e ) {
			e.printStackTrace( );
		}

		return ret;

	}

	public static boolean writeRawBytesToFile( File f, byte[ ] b ) {
		boolean result = false;
		FileOutputStream stream = null;
		try {
			stream = new FileOutputStream( f );
			stream.write( b );
			result = true;
		} catch ( FileNotFoundException e ) {

			e.printStackTrace( );
		} catch ( IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace( );
		} finally {
			if ( stream != null ) {
				try {
					stream.close( );
				} catch ( IOException e ) {
					result = false;
				}
			}
		}

		return result;
	}

	public static Image importImage( String image_file ) throws MalformedURLException, IOException {
		BufferedImage img = ImageIO.read( new File( image_file ).toURI( ).toURL( ) );
		return img;
	}

	public static void recursiveDeletion( File root ) {
		for ( File f : root.listFiles( ) ) {
			if ( f.isDirectory( ) ) {
				recursiveDeletion( f );
			}

			Log.info( "Attempting to delete: " + f.getAbsolutePath( ) );
			if ( !f.delete( ) ) {
				Log.error( "Could not delete: " + f.getAbsolutePath( ) );
			}
		}

		root.delete( );
	}

}
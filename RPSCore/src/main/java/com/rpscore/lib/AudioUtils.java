/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-02-04 15:57:36 -0500 (Sun, 04 Feb 2018) $
 * $Revision: 326 $
 * $Author: mbartgis $
 */

package com.rpscore.lib;

import static javax.sound.sampled.AudioFormat.Encoding.PCM_SIGNED;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.FloatControl;

import com.rpscore.cfg.ConfigManager;

public class AudioUtils {

	public static void audioDelay( int time ) {
		try {
			Thread.sleep( time );
		} catch ( InterruptedException e ) {
			Thread.currentThread( ).interrupt( );
		}
	}

	public static AudioFormat getOutFormat( AudioFormat inFormat ) {
		final int ch = inFormat.getChannels( );
		final float rate = inFormat.getSampleRate( );
		return new AudioFormat( PCM_SIGNED, rate, 16, ch, ch * 2, rate, false );
	}

	public static float getVolume( FloatControl fc, String key ) {
		float gain = ConfigManager.getConfigManager( ).getSetting( key, 0 );

		if ( gain >= 0.0 ) {
			gain = gain * ( fc.getMaximum( ) / 100 );
		} else {
			gain = gain * ( -1 * ( fc.getMinimum( ) / 100 ) );
		}

		return gain;
	}

}
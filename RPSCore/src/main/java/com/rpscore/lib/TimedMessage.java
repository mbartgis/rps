/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date$
 * $Revision$
 * $Author$
 */

package com.rpscore.lib;

public class TimedMessage implements Updateable {

	private String	message;

	private int		timer;

	private boolean	isPaused;

	public TimedMessage( String s, int duration ) {
		this.message = s;
		this.timer = duration;
		this.isPaused = false;
	}

	public void pause( ) {
		this.isPaused = true;
	}

	public void resume( ) {
		this.isPaused = false;
	}

	public boolean isPaused( ) {
		return this.isPaused;
	}

	public String getMessage( ) {
		return this.message;
	}

	public void decrementTimer( ) {
		this.timer--;
	}

	public void incrementTimer( ) {
		this.timer++;
	}

	public int getDuration( ) {
		return this.timer;
	}

	@Override
	public void update( ) {
		if ( !this.isPaused ) {
			decrementTimer( );
		}
	}

	@Override
	public boolean isFinished( ) {
		return this.timer <= 0;
	}

}
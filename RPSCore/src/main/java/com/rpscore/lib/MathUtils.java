/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date$
 * $Revision$
 * $Author$
 */

package com.rpscore.lib;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class MathUtils {

	private static Random generator = new Random( );

	/**
	 * Returns the distance between 2 objects in a Cartesian plane. Note: The
	 * accuracy of the distance is subject to the limitation of the IEEE 754
	 * standard applied to 64-bits.
	 *
	 * @param x1
	 *            Cartesian x coordinate of the first point.
	 * @param y1
	 *            Cartesian y coordinate of the first point.
	 * @param x2
	 *            Cartesian x coordinate of the second point.
	 * @param y2
	 *            Cartesian y coordinate of the second point.
	 * @return The distance between both points in 2-dimensional space.
	 */
	public static double getDistance( int x1, int y1, int x2, int y2 ) {

		double x = ( Math.abs( x2 - x1 ) );
		double y = ( Math.abs( y2 - y1 ) );

		return Math.sqrt( ( x * x ) + ( y * y ) );

	}

	/**
	 * Returns a whole integer approximation of the absolute distance between 2
	 * objects in a Cartesian plane.
	 *
	 * @param x1
	 *            Cartesian x coordinate of the first point.
	 * @param y1
	 *            Cartesian y coordinate of the first point.
	 * @param x2
	 *            Cartesian x coordinate of the second point.
	 * @param y2
	 *            Cartesian y coordinate of the second point.
	 * @return The distance between both points in 2-dimensional space.
	 */
	public static int getRoundedDistance( int x1, int y1, int x2, int y2 ) {
		double x = ( Math.abs( x2 - x1 ) );
		double y = ( Math.abs( y2 - y1 ) );

		return ( int ) Math.round( Math.sqrt( ( x * x ) + ( y * y ) ) );
	}

	/**
	 * Supplies a pseudorandom integer contained within the range of the 2
	 * provided numbers. This function relies on a Monte Carlo approach and the
	 * variety of results will be based on the difference between the lower
	 * bound and upper bound. (i.e. Using getRandom(10000, 10010), only 0 - 9
	 * will be rolled through instead of y = getRandom(0-10010), where y >=
	 * 10000.
	 *
	 * Note: Both numbers must be positive integers in the form lbound <=
	 * hbound.
	 *
	 * @param lbound
	 *            Lower bound of the number to determine.
	 * @param hbound
	 *            Upper bound of the number to determine.
	 * @return
	 * @throws IllegalArgumentException
	 *             Thrown if hbound < lbound or if lbound < 0 or if hbound < 0.
	 */
	public static int getRandom( int lbound, int hbound ) throws IllegalArgumentException {

		if ( lbound < 0 || hbound < 0 ) {
			throw new IllegalArgumentException( "Both bounds must be non-negative integers." );
		}

		if ( hbound < lbound ) {
			throw new IllegalArgumentException( "The upperbound must be larger or equal to the lower bound." );
		}

		// If the range is 1 number, return that number.
		if ( lbound == hbound ) {
			return lbound;
		}

		// While a given number bounded with 0 and hbound is greater than the
		// lower bound given, re-roll.
		return generator.nextInt( hbound - lbound ) + lbound;

	}

	/**
	 * Performs a dice roll with the percentage input being the success
	 * percentage. (i.e. if the function getDiscreteChance(0.10) is called 10
	 * times, it should, on average, return true 1 time. In theory all 10
	 * returns may be true or none of them may be.
	 *
	 * @param percent
	 *            Percent chance that this function will return true.
	 * @return Result of the roll made with the given input percentage.
	 */
	public static boolean getDiscreteChance( double percent ) {

		if ( percent >= 1.00 ) {
			return true;
		}

		int e = ( int ) ( Integer.MAX_VALUE * percent );

		int f = generator.nextInt( Integer.MAX_VALUE );

		return f < e;
	}

	public static byte[ ] convertIntsToByteArray( int... ns ) {
		byte[ ] b = new byte[ ns.length * 4 ];
		for ( int i = 0; i < b.length; i++ ) {
			b[ i ] = ( byte ) ( ( ns[ i / 4 ] >> 24 ) & 0x000000FF );
			i++;
			b[ i ] = ( byte ) ( ( ns[ i / 4 ] >> 16 ) & 0x000000FF );
			i++;
			b[ i ] = ( byte ) ( ( ns[ i / 4 ] >> 8 ) & 0x000000FF );
			i++;
			b[ i ] = ( byte ) ( ns[ i / 4 ] & 0x000000FF );
		}

		return b;
	}

	public static long hash64( String string ) {
		long h = 1125899906842597L; // prime
		int len = string.length( );

		for ( int i = 0; i < len; i++ ) {
			h = 31 * h + string.charAt( i );
		}
		return h;
	}

	public static String convertStringToHex( String str ) {

		char[ ] chars = str.toCharArray( );

		StringBuffer hex = new StringBuffer( );
		for ( int i = 0; i < chars.length; i++ ) {
			String hexOut = Integer.toHexString( chars[ i ] );
			if ( hexOut.length( ) == 1 ) {
				hexOut = "0" + hexOut;
			}
			hex.append( hexOut );
		}

		return hex.toString( );
	}

	public static String convertHexToString( String hex ) {

		StringBuilder sb = new StringBuilder( );
		StringBuilder temp = new StringBuilder( );

		// 49204c6f7665204a617661 split into two characters 49, 20, 4c...
		for ( int i = 0; i < hex.length( ) - 1; i += 2 ) {

			// grab the hex in pairs
			String output = hex.substring( i, ( i + 2 ) );
			// convert hex to decimal
			int decimal = Integer.parseInt( output, 16 );
			// convert the decimal to character
			sb.append( ( char ) decimal );

			temp.append( decimal );
		}

		return sb.toString( );
	}

	public static byte[ ] encryptFile( String[ ] lines, long hashKey ) {

		hashKey = Math.abs( hashKey );

		String keyHex = Long.toString( hashKey );

		while ( keyHex.length( ) != 32 ) {
			keyHex = "0" + keyHex;
		}

		StringBuilder b = new StringBuilder( );

		for ( int i = 0; i < lines.length - 1; i++ ) {
			b.append( lines[ i ] );
			b.append( "\n" );
		}

		b.append( lines[ lines.length - 1 ] );

		final String plaintextHex = convertStringToHex( b.toString( ) );

		SecretKey key = new SecretKeySpec( DatatypeConverter.parseHexBinary( keyHex ), "AES" );

		Cipher cipher;

		byte[ ] result = null;

		try {
			cipher = Cipher.getInstance( "AES/ECB/PKCS5Padding" );
			cipher.init( Cipher.ENCRYPT_MODE, key );

			result = cipher.doFinal( DatatypeConverter.parseHexBinary( plaintextHex ) );
		} catch ( NoSuchAlgorithmException e ) {} catch ( NoSuchPaddingException e ) {} catch ( InvalidKeyException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace( );
		} catch ( IllegalBlockSizeException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace( );
		} catch ( BadPaddingException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace( );
		}

		return result;
	}

	public static String[ ] decryptFile( byte[ ] bytes, long hashKey ) throws BadPaddingException {

		hashKey = Math.abs( hashKey );

		String keyHex = Long.toString( hashKey );

		while ( keyHex.length( ) != 32 ) {
			keyHex = "0" + keyHex;
		}

		StringBuilder b = new StringBuilder( );

		for ( int i = 0; i < bytes.length - 1; i++ ) {
			b.append( bytes[ i ] );
			b.append( "\n" );
		}

		b.append( bytes[ bytes.length - 1 ] );

		final String plaintextHex = b.toString( );

		SecretKey key = new SecretKeySpec( DatatypeConverter.parseHexBinary( keyHex ), "AES" );

		Cipher cipher;

		byte[ ] result = null;

		try {
			cipher = Cipher.getInstance( "AES/ECB/PKCS5Padding" );
			cipher.init( Cipher.DECRYPT_MODE, key );

			result = cipher.doFinal( bytes );
		} catch ( NoSuchAlgorithmException e ) {} catch ( NoSuchPaddingException e ) {} catch ( InvalidKeyException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace( );
		} catch ( IllegalBlockSizeException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace( );
		}

		return new String( result ).split( "\n" );
	}

	public static boolean validateKey( byte[ ] encryptedContents, long key, String contains ) {
		try {
			String[ ] lines = decryptFile( encryptedContents, key );

			for ( String line : lines ) {
				if ( line.contains( contains ) ) {
					return true;
				}
			}
		} catch ( BadPaddingException e ) {
			return false;
		}

		return false;
	}

	public static byte[ ] toByteArray( String s ) {
		byte[ ] b = new byte[ s.length( ) * 2 ];

		for ( int i = 0; i < ( ( s.length( ) * 2 ) - 1 ); i++ ) {
			char blen = s.charAt( i / 2 );

			b[ i ] = ( byte ) ( ( blen >> 8 ) & 0x00FF );
			i++;
			b[ i ] = ( byte ) ( blen & 0x00FF );

		}

		return b;

	}

	public static byte[ ] toCompressedByteArray( String s ) {
		byte[ ] b = new byte[ s.length( ) ];

		for ( int i = 0; i < s.length( ); i++ ) {
			char blen = s.charAt( i );

			b[ i ] = ( byte ) ( blen & 0x00FF );

		}

		return b;
	}

	public static String convertToString( byte[ ] in ) {
		StringBuilder ret = new StringBuilder( );

		for ( int i = 0; i < in.length - 1; i += 2 ) {
			char c = ( char ) ( ( in[ i ] * 256 ) + in[ i + 1 ] );
			ret.append( c );
		}

		return ret.toString( );
	}

	public static String convertCompressedToString( byte[ ] in ) {
		StringBuilder ret = new StringBuilder( );

		for ( int i = 0; i < in.length; i++ ) {
			char c = ( char ) in[ i ];
			ret.append( c );
		}

		return ret.toString( );
	}

	public static byte[ ] expandByteArray( byte[ ] in ) {

		byte[ ] out = new byte[ in.length * 2 ];

		for ( int i = 0; i < out.length; i++ ) {
			out[ i ] = 0;
			out[ i ] = in[ ( i + 1 ) / 2 ];
		}

		return out;

	}

	public static byte[ ] contractByteArray( byte[ ] in ) {
		boolean set = true;
		if ( set ) {
			return in;
		}

		byte[ ] out = new byte[ in.length / 2 ];

		for ( int i = 0; i < in.length; i++ ) {
			i++;
			out[ i / 2 ] = in[ i ];
		}

		return out;

	}

}
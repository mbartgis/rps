/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

package com.rpscore.lib;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Documented
@Retention( RetentionPolicy.SOURCE )
public @interface Unused {

}
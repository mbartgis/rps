/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date$
 * $Revision$
 * $Author$
 */

package com.rpscore.lib;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * This is a Globally Unique Identifier (GUID) generation tool. THis will
 * provide a pseudorandom identifier and ensure that it has not been created
 * previously.
 *
 * @author Matt
 *
 */
public class GUIDGenerator {

	private SortedSet< Integer > history;

	public GUIDGenerator( ) {
		history = new TreeSet< >( );

	}

	public int generate( ) {

		int i = 0;
		do {
			i = MathUtils.getRandom( 0x00000000, 0x7FFFFFFF );
		} while ( this.contains( i ) );

		history.add( i );

		return i;
	}

	private boolean contains( int i ) {
		return this.history.contains( i );
	}

	public SortedSet< Integer > getHistory( ) {
		return this.history;
	}

	public void reserve( int i ) {
		if ( !history.contains( i ) ) {
			history.add( i );
		} else {
			throw new IllegalArgumentException( i + " is already in the generator's history." );
		}
	}

	public void flush( ) {
		this.history.clear( );
	}

}
package com.rpscore.lib;

public class Statistics {

	private int	count;
	private int	total;

	public Statistics( ) {
		this.count = 0;
		this.total = 0;
	}

	public void put( int... ns ) {

		this.count += ns.length;

		for ( int n : ns ) {
			this.total += n;
		}

	}

	public int getCount( ) {
		return count;
	}

	public int getTotal( ) {
		return total;
	}

	public double getAverage( ) {
		return ( double ) total / ( double ) count;
	}

}
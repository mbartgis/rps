/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date$
 * $Revision$
 * $Author$
 */

package com.rpscore.lib;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

import com.rpscore.cfg.ConfigManager;

public class GUIUtils {

	public static int[ ] getSessionResolution( ) {
		ConfigManager cfg = ConfigManager.getConfigManager( );
		if ( cfg.getSetting( "Fullscreen", false ) ) {
			GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment( ).getDefaultScreenDevice( );
			int width = gd.getDisplayMode( ).getWidth( );
			int height = gd.getDisplayMode( ).getHeight( );
			return new int[ ] { width, height };
		}

		return new int[ ] { cfg.getSetting( "WindowWidth", 800 ), cfg.getSetting( "WindowHeight", 600 ) };

	}

}
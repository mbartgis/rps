/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date$
 * $Revision$
 * $Author$
 */

package com.rpscore.lib;

import com.rpscore.gui.game.GraphicsAdapter;

public interface Drawable {

	public abstract void draw( GraphicsAdapter g );

}
/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * VERSION INFORMATION:
 * $Date$
 * $Revision$
 * $Author$
 */

package com.rpscore.lib;

import java.util.Random;

public class TestUtils {
	public static int	SIZE_TRIVIAL	= -1;
	public static int	SIZE_MEDIUM		= -2;
	public static int	SIZE_LARGE		= -3;
	public static int	SIZE_MASSIVE	= -4;

	public static byte[ ] createRandomTransmissionArray( int length ) {
		Random r = new Random( );

		if ( length == SIZE_TRIVIAL ) {
			length = r.nextInt( 10 );
		} else if ( length == SIZE_MEDIUM ) {
			length = r.nextInt( 1000 );
		} else if ( length == SIZE_LARGE ) {
			length = r.nextInt( 1000000 );
		} else if ( length == SIZE_MASSIVE ) {
			length = r.nextInt( 1000000000 );
		}

		if ( length <= 0 ) {
			length = 1;
		}

		byte[ ] ret = new byte[ length ];

		for ( int i = 0; i < length; i++ ) {
			byte b = 0;
			while ( ( b = ( byte ) ( r.nextInt( 255 ) ) ) == 0 ) {}
			ret[ i ] = b;
		}
		return ret;

	}
}
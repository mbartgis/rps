/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-11-20 20:28:45 -0500 (Tue, 20 Nov 2018) $
 * $Revision: 370 $
 * $Author: mbartgis $
 */

package com.rpscore.server;

import java.io.Closeable;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

public class DataStream implements Closeable {

	private LinkedList< byte[ ] >	data;

	private int						pSize, iterator;

	// TODO Figure this out...
	private byte[ ]					cache;

	private byte[ ]					appenderLocation;

	public DataStream( int packetSize ) {
		this.data = new LinkedList< byte[ ] >( );

		this.pSize = packetSize;
		this.iterator = 0;

		this.appenderLocation = null;

	}

	public DataStream( ) {
		this( 256 );
	}

	public void append( byte b ) {

		this.cache = null;

		int index = this.iterator % this.pSize;

		if ( index == 0 ) {
			this.appenderLocation = new byte[ pSize ];
			this.data.add( this.appenderLocation );
		}

		this.appenderLocation[ index ] = b;

		this.iterator++;

	}

	public boolean isEmpty( ) {
		return this.data.isEmpty( );
	}

	public int size( ) {
		return this.iterator;
	}

	public byte getByteAtIndex( int n ) {

		if ( this.data.size( ) == 0 ) {
			throw new ArrayIndexOutOfBoundsException( "Cannot access a stream with no data." );
		}

		int group = n / this.pSize;
		int index = n % this.pSize;

		return this.data.get( group )[ index ];
	}

	public byte[ ] toByteArray( ) {

		if ( this.data.size( ) == 0 ) {
			throw new ArrayIndexOutOfBoundsException( "Cannot access a stream with no data." );
		}

		if ( this.cache != null ) {
			return this.cache;
		}

		byte[ ] ret = new byte[ iterator ];
		int group = this.iterator / this.pSize;
		int index = this.iterator % this.pSize;

		Iterator< byte[ ] > iter = this.data.iterator( );
		iter.next( );

		int cnt = 0;
		byte[ ] b;

		// if there is only 1 packet (edge case)
		if ( this.iterator <= this.pSize ) {
			b = data.getFirst( );
			for ( int i = 0; i < index; i++ ) {
				ret[ i ] = b[ i ];
			}
			this.cache = ret;

			return ret;
		}

		for ( b = data.getFirst( ); b != data.getLast( ); b = iter.next( ) ) {
			for ( int j = 0; j < this.pSize; j++ ) {
				ret[ ( cnt * this.pSize ) + j ] = b[ j ];
			}
			cnt++;
		}

		for ( int j = 0; j < ret.length - ( ( group - 1 ) * this.pSize ); j++ ) {
			// FIXME Get rid of this band-aid.
			if ( j < 256 ) {
				ret[ ( ( group - 1 ) * this.pSize ) + j ] = b[ j ];
			}
		}

		this.cache = ret;

		return ret;
	}

	@Override
	public void close( ) throws IOException {
		this.cache = null;
	}

}

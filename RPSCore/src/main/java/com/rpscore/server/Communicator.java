/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-05-05 01:10:23 -0400 (Sat, 05 May 2018) $
 * $Revision: 356 $
 * $Author: mbartgis $
 */

package com.rpscore.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeoutException;

public class Communicator {

	private ReaderThread		rt;
	private WriterThread		wt;

	private boolean				isClosed;

	private long				lastMessageReceived;

	// Using a mutex as opposed to a synchronized block because the scope of a
	// synchronized block would be detrimental to I/O performance.
	private Semaphore			heartbeatMutex;

	private Socket				client;

	private int					assignment;
	private boolean				isAssigned;

	private volatile boolean	interrupted;
	private volatile boolean	waitingForResponse;

	private Lock				lock;

	private long				connectionStartTime;

	private long				dataSentPoll;
	private long				dataReceivedPoll;

	private static final int	REASONABLE_PACKET_SIZE	= 100000;

	private class Lock {

		private volatile boolean isLocked;

		private void lock( ) {
			this.isLocked = true;
		}

		private void unlock( ) {
			this.isLocked = false;
		}

		private boolean isLocked( ) {
			return this.isLocked;
		}

	}

	private volatile boolean acceptNewMessages;

	public Communicator( Socket clientReference ) throws IOException {

		if ( clientReference == null ) {
			throw new IllegalArgumentException( "Cannot wrap a null socket." );
		}

		if ( clientReference.isClosed( ) ) {
			throw new IOException( "Cannot wrap a socket which is already closed" );
		}

		this.client = clientReference;

		this.isClosed = false;

		this.rt = new ReaderThread( );
		this.wt = new WriterThread( );

		this.heartbeatMutex = new Semaphore( 1 );

		this.lastMessageReceived = System.currentTimeMillis( );
		this.connectionStartTime = this.lastMessageReceived;

		this.assignment = 0;
		this.isAssigned = false;
		this.acceptNewMessages = true;
		this.lock = new Lock( );
		this.lock.unlock( );

		this.interrupted = false;
		this.waitingForResponse = false;

		this.dataReceivedPoll = 0;
		this.dataSentPoll = 0;

	}

	public long getDataTransmitted( ) {
		return this.wt.dataUpCounter;
	}

	public long getDataReceived( ) {
		return this.rt.dataDownCounter;
	}

	public long getDataTransmittedSincePoll( ) {
		long dataSent = this.wt.dataUpCounter - this.dataSentPoll;
		this.dataSentPoll = this.wt.dataUpCounter;
		return dataSent;
	}

	public long getDataReceivedSincePoll( ) {
		long dataReceived = this.rt.dataDownCounter - this.dataReceivedPoll;
		this.dataReceivedPoll = this.rt.dataDownCounter;
		return dataReceived;
	}

	public void assign( int n ) {
		this.assignment = n;
		this.isAssigned = true;
	}

	public int getAssignment( ) {
		return this.assignment;
	}

	public boolean isAssigned( ) {
		return this.isAssigned;
	}

	public long getConnectionLife( ) {
		return System.currentTimeMillis( ) - this.connectionStartTime;
	}

	public byte[ ][ ] getMessages( ) {

		// If an access point is currently waiting, do not allow any messages to
		// be consumed through here.
		if ( waitingForResponse ) {
			return new byte[ ][ ] { };
		}

		List< byte[ ] > messages = rt.getMessages( );
		byte[ ][ ] ret = new byte[ messages.size( ) ][ ];

		for ( int i = 0; i < ret.length; i++ ) {
			ret[ i ] = messages.get( i );
		}

		return ret;
	}

	public void sendHeartbeat( ) {
		this.sendMessage( new byte[ ] { } );
	}

	public long getHeartbeatTime( ) {

		try {
			this.heartbeatMutex.acquire( );
		} catch ( InterruptedException e ) {
			Thread.currentThread( ).interrupt( );
		}
		long time = System.currentTimeMillis( ) - this.lastMessageReceived;
		this.heartbeatMutex.release( );

		return time;

	}

	public void sendMessage( byte[ ] message ) {

		if ( !this.acceptNewMessages ) {
			return;
		}

		byte[ ] message_ptr = message;
		this.wt.queueMessage( message_ptr );
		synchronized ( this.wt ) {
			this.wt.notify( );
		}
	}

	public boolean isClosed( ) {
		return this.isClosed;
	}

	public boolean disconnect( ) {
		this.isClosed = true;
		// TODO Allow the shutdown request to return a state.
		this.wt.requestShutDown( );
		boolean rts = this.rt.shutDown( );

		boolean sc = false;

		try {
			this.client.close( );
			sc = true;
		} catch ( IOException e ) {}

		return rts && sc;

	}

	public boolean forceDisconnect( ) {
		this.isClosed = true;

		boolean wts = this.wt.shutDown( );
		boolean rts = this.rt.shutDown( );

		boolean sc = false;

		try {
			this.client.close( );
			sc = true;
		} catch ( IOException e ) {}

		return wts && rts && sc;
	}

	/**
	 * This method blocks until a message has been fully received. This method
	 * is not 100% thread safe in that it is possible for the consumer method
	 * getMessages() to take out messages before this has a chance to return
	 * them. This method should not be called asynchronously from getMessages().
	 *
	 * @return The next received message.
	 */
	public byte[ ] getNext( ) throws TimeoutException {

		this.interrupted = false;

		synchronized ( rt.inMessages ) {

			// TODO clean this up so that it doesen't have a nested object as a
			// reference.
			if ( !rt.inMessages.isEmpty( ) ) {
				Byte[ ] retB = rt.inMessages.removeFirst( );
				// rt.mutex.release( );
				this.waitingForResponse = false;

				byte[ ] ret = new byte[ retB.length ];

				for ( int i = 0; i < ret.length; i++ ) {
					ret[ i ] = retB[ i ];
				}

				return ret;
			}

			this.waitingForResponse = true;
		}

		synchronized ( this.lock ) {
			this.lock.lock( );
			while ( this.lock.isLocked( ) ) {
				try {
					this.lock.wait( );
					waitingForResponse = false;
				} catch ( InterruptedException e ) {
					return null;
				}
			}
		}

		if ( interrupted ) {
			waitingForResponse = false;
			throw new TimeoutException( );
		}

		// This code block ensures that at least 1 entry will be filled. Repeat
		// the method until it returns the desired result.
		return getNext( );

	}

	protected volatile boolean getCompleted;

	public byte[ ] getNext( int timeout ) throws TimeoutException {

		long start = System.currentTimeMillis( );

		getCompleted = false;

		Runnable timeoutClock = ( ) -> {

			while ( !getCompleted && ( ( System.currentTimeMillis( ) - start ) < timeout ) ) {

				try {
					Thread.sleep( 1 );
				} catch ( InterruptedException e ) {
					break;
				}

			}

			synchronized ( lock ) {
				interrupted = true;
				lock.unlock( );
				lock.notifyAll( );
			}

		};

		Thread asyncTimeout = new Thread( timeoutClock );
		asyncTimeout.setName( "Async-Timeout" );
		asyncTimeout.setDaemon( true );
		asyncTimeout.start( );

		byte[ ] ret = getNext( );
		getCompleted = true;

		try {
			asyncTimeout.join( );
		} catch ( InterruptedException e ) {
			Thread.currentThread( ).interrupt( );
		}

		return ret;

	}

	public Socket getClient( ) {
		return this.client;
	}

	private class ReaderThread extends Thread {

		private LinkedList< Byte[ ] >	inMessages;

		private InputStream				inStream;

		private long					dataDownCounter;

		private volatile boolean		isCloseRequested;

		private int						mLength;

		private int						counter;

		private ArrayList< Byte >		inStreamList;

		public ReaderThread( ) {

			this.setName( "Comm Reader Thread" );
			this.setDaemon( true );

			this.inMessages = new LinkedList< >( );

			this.isCloseRequested = false;

			this.inStream = null;

			this.inStreamList = null;

			this.mLength = -1;

			this.counter = 0;

			this.dataDownCounter = 0;

			try {
				this.inStream = Communicator.this.client.getInputStream( );
			} catch ( IOException e ) {}

			this.start( );

		}

		public List< byte[ ] > getMessages( ) {
			List< byte[ ] > ret = new LinkedList< >( );

			synchronized ( this.inMessages ) {
				while ( !this.inMessages.isEmpty( ) ) {

					Byte[ ] a = this.inMessages.removeFirst( );
					byte[ ] b = new byte[ a.length ];

					for ( int i = 0; i < a.length; i++ ) {
						b[ i ] = a[ i ];
					}
					ret.add( b );
				}
			}

			return ret;
		}

		public boolean shutDown( ) {
			this.isCloseRequested = true;

			boolean success = true;

			if ( this.inStream != null ) {
				try {
					this.inStream.close( );
				} catch ( IOException e ) {
					success = false;
				}
			}

			this.inStream = null;

			return success;
		}

		@Override
		public void run( ) {
			super.run( );
			while ( !this.isCloseRequested ) {

				try {

					// Get the message length
					if ( this.mLength == -1 ) {
						this.mLength = 0;

						this.mLength += this.inStream.read( ) * ( 256 * 256 * 256 );
						this.mLength += this.inStream.read( ) * ( 256 * 256 );
						this.mLength += this.inStream.read( ) * ( 256 );
						this.mLength += this.inStream.read( );

						this.dataDownCounter++;

						if ( this.mLength > 0 ) {
							// Protect against OutOfMemoryErrors (or at least be
							// able to control them).
							if ( this.mLength > REASONABLE_PACKET_SIZE ) {
								this.inStreamList = new ArrayList< >( REASONABLE_PACKET_SIZE );
							} else {
								this.inStreamList = new ArrayList< >( this.mLength );
							}
						} else {
							this.inStreamList = new ArrayList< >( );
						}

						// The message is a heartbeat, ignore it.
						if ( this.mLength == 0 ) {
							this.mLength = -1;
							try {
								Communicator.this.heartbeatMutex.acquire( );
							} catch ( InterruptedException e1 ) {
								Thread.currentThread( ).interrupt( );
							}
							Communicator.this.lastMessageReceived = System.currentTimeMillis( );
							Communicator.this.heartbeatMutex.release( );
						}
					}

					if ( this.counter < ( this.mLength ) ) {

						int read = this.inStream.read( );
						this.dataDownCounter++;

						this.inStreamList.add( ( byte ) read );

						this.counter++;

						continue;

					} else {

						if ( !this.inStreamList.isEmpty( ) ) {
							try {
								Communicator.this.heartbeatMutex.acquire( );
							} catch ( InterruptedException e1 ) {
								Thread.currentThread( ).interrupt( );
							}
							Communicator.this.lastMessageReceived = System.currentTimeMillis( );
							Communicator.this.heartbeatMutex.release( );

							Byte[ ] newMessage = inStreamList.toArray( new Byte[ ] { } );

							synchronized ( this.inMessages ) {

								this.inMessages.add( newMessage );

								lock.unlock( );
								synchronized ( lock ) {
									lock.notifyAll( );
								}

							}

							this.mLength = -1;
							this.counter = 0;
						}

					}

				} catch ( IOException | NullPointerException e ) {}
			}

		}

	}

	private class WriterThread extends Thread {

		private LinkedList< byte[ ] >	outMessages;

		private OutputStream			outStream;

		private volatile boolean		isCloseRequested;

		private long					dataUpCounter;

		public WriterThread( ) {

			this.setName( "Comm Writer Thread" );
			this.setDaemon( true );

			this.outMessages = new LinkedList< >( );

			this.isCloseRequested = false;

			this.outStream = null;

			this.dataUpCounter = 0;

			try {
				this.outStream = Communicator.this.client.getOutputStream( );
			} catch ( IOException e ) {}

			this.start( );
		}

		public void queueMessage( byte[ ] message ) {
			synchronized ( this.outMessages ) {
				this.outMessages.add( message );
			}
		}

		/**
		 * This method finishes all outbound communications, then disconnects
		 * the communicator. This method blocks until all outbound messages have
		 * been sent.
		 *
		 * @return
		 */
		public void requestShutDown( ) {

			acceptNewMessages = false;
			while ( !wt.outMessages.isEmpty( ) ) {}
			shutDown( );

		}

		public boolean shutDown( ) {
			this.isCloseRequested = true;

			boolean success = true;

			if ( this.outStream != null ) {
				try {
					this.outStream.close( );
				} catch ( IOException e ) {
					success = false;
				}
			}

			this.outStream = null;

			synchronized ( this ) {
				this.notify( );
			}

			return success;
		}

		private void writeMessage( byte[ ] text ) {

			try {

				// Write message header
				if ( text.length > ( 256 * 256 * 256 ) ) {
					this.outStream.write( ( text.length >> 24 ) & 0x000000FF );
					this.outStream.write( ( text.length >> 16 ) & 0x000000FF );
					this.outStream.write( ( text.length >> 8 ) & 0x000000FF );
					this.outStream.write( text.length & 0x000000FF );
				} else if ( text.length > ( 256 * 256 ) ) {
					this.outStream.write( 0 );
					this.outStream.write( ( text.length >> 16 ) & 0x000000FF );
					this.outStream.write( ( text.length >> 8 ) & 0x000000FF );
					this.outStream.write( text.length & 0x000000FF );
				} else if ( text.length > ( 256 ) ) {
					this.outStream.write( 0 );
					this.outStream.write( 0 );
					this.outStream.write( ( text.length >> 8 ) & 0x000000FF );
					this.outStream.write( text.length & 0x000000FF );
				} else {
					this.outStream.write( 0 );
					this.outStream.write( 0 );
					this.outStream.write( 0 );
					this.outStream.write( text.length );
				}

				this.dataUpCounter++;

				for ( int i = 0; i < text.length; i++ ) {
					this.outStream.write( text[ i ] );
					this.dataUpCounter++;
				}

			} catch ( IOException | NullPointerException e ) {

			}
		}

		@Override
		public void run( ) {
			super.run( );

			while ( !isCloseRequested ) {

				// If there are messages, grab the resource mutex early to avoid
				// blocking during the execution loop.
				if ( !this.outMessages.isEmpty( ) ) {

					synchronized ( this.outMessages ) {

						while ( !this.outMessages.isEmpty( ) ) {

							byte[ ] text = this.outMessages.removeFirst( );

							if ( text == null ) {
								continue;
							}

							writeMessage( text );

						}

					}

				}

				synchronized ( this ) {
					try {
						this.wait( );
					} catch ( InterruptedException e ) {
						Thread.currentThread( ).interrupt( );
					}
				}

			}

		}

	}

}

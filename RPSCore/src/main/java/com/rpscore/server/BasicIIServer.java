/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

package com.rpscore.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class BasicIIServer {

	public ServerThread st;

	public BasicIIServer( ) {
		this.st = new ServerThread( );
	}

	public void shutDown( ) {

		System.out.println( "Shutting down the server..." );

		this.st.closeIsNotRequested = false;

		if ( this.st.server == null ) {
			return;
		}

		synchronized ( this.st.server ) {
			try {
				this.st.server.close( );
			} catch ( IOException e ) {}
		}

	}

	private class ServerThread extends Thread {

		@SuppressWarnings( "unused" )
		private long						timeout;

		private ArrayList< Communicator >	clients;

		private UpdaterThread				ut;

		private Semaphore					clientMutex;

		private boolean						closeIsNotRequested;

		private ServerSocket				server;

		public ServerThread( ) {

			this.clientMutex = new Semaphore( 1 );

			this.clients = new ArrayList< Communicator >( );

			this.ut = new UpdaterThread( );

			this.closeIsNotRequested = true;

			this.server = null;

			this.start( );

		}

		@Override
		public void run( ) {
			super.run( );

			try {
				this.server = new ServerSocket( 22000 );

				InetAddress addr = InetAddress.getLocalHost( );
				@SuppressWarnings( "unused" )
				String hostname = addr.getHostName( );

				System.out.println( "Server Setup Complete." );

				M_LABEL: while ( this.closeIsNotRequested ) {

					@SuppressWarnings( "resource" )
					Socket client = server.accept( );

					if ( !this.closeIsNotRequested ) {
						continue M_LABEL;
					}

					InetAddress cl = client.getInetAddress( );
					System.out.println( "User " + cl.getHostAddress( ) + " connected successfully." );

					try {
						this.clientMutex.acquire( );
					} catch ( InterruptedException e ) {}

					clients.add( new Communicator( client ) );

					this.clientMutex.release( );

					synchronized ( this.ut ) {
						this.ut.notify( );
					}

				}

			} catch ( IOException e ) {} finally {
				if ( server != null ) {
					try {
						server.close( );
					} catch ( IOException e ) {}
				}

				try {
					this.clientMutex.acquire( );
				} catch ( InterruptedException e ) {}

				for ( Communicator client : clients ) {
					if ( client != null ) {
						boolean disconnected = client.disconnect( );
						InetAddress cl = client.getClient( ).getInetAddress( );
						if ( disconnected ) {
							System.out.println( "User " + cl.getHostAddress( ) + " disconnected successfully." );
						}
					}
				}

				this.ut.terminate( );
				this.clientMutex.release( );

			}

		}

	}

	private class UpdaterThread extends Thread {

		private volatile boolean closeNotRequested;

		public UpdaterThread( ) {
			this.closeNotRequested = true;

			start( );

		}

		private void terminate( ) {
			this.closeNotRequested = false;
			synchronized ( this ) {
				this.notify( );
			}
		}

		@Override
		public void run( ) {
			super.run( );

			while ( this.closeNotRequested ) {

				if ( st == null ) {
					continue;
				}

				if ( st.clients != null && st.clients.size( ) == 0 ) {
					synchronized ( this ) {
						try {
							this.wait( );
						} catch ( InterruptedException e ) {
							e.printStackTrace( );
						}
					}
				}

				synchronized ( st.clients ) {

					try {
						st.clientMutex.acquire( );
					} catch ( InterruptedException e ) {}

					for ( int i = 0; i < st.clients.size( ); i++ ) {
						byte[ ][ ] updatesFromClient = st.clients.get( i ).getMessages( );

						// Only enforce timeout policies when a timeout is
						// given.
						if ( TIMEOUT != -1 ) {
							if ( st.clients.get( i ).getHeartbeatTime( ) > TIMEOUT ) {
								boolean disconnected = st.clients.get( i ).disconnect( );
								InetAddress cl = st.clients.get( i ).getClient( ).getInetAddress( );
								if ( disconnected ) {
									System.out
											.println( "User " + cl.getHostAddress( ) + " disconnected successfully." );
									st.clients.remove( st.clients.get( i ) );
									i = 0;
								}
							}
						}

						for ( int k = 0; k < st.clients.size( ); k++ ) {
							if ( st.clients.get( i ) != st.clients.get( k ) ) {
								for ( int j = 0; j < updatesFromClient.length; j++ ) {
									// Ignore empty messages

									st.clients.get( k ).sendMessage( updatesFromClient[ j ] );

								}
							}
						}
					}

					st.clientMutex.release( );
				}

				if ( TPS > 0 ) {
					try {
						Thread.sleep( TPS );
					} catch ( InterruptedException e ) {}
				}

			}

		}

	}

	static long	TIMEOUT	= 30000000000000L;

	static int	TPS		= 0;

	@SuppressWarnings( "unused" )
	public static void main( String[ ] args ) {

		for ( int i = 0; i < args.length; i++ ) {
			if ( args[ i ].equals( "-tps" ) ) {
				if ( args.length >= i ) {
					try {
						int tps = Integer.parseInt( args[ i ] );

						if ( tps != -1 ) {
							TPS = tps / 1000;
						}
					} catch ( NumberFormatException e ) {
						TPS = 1;
					}
				} else {
					TPS = 1;
				}
			}
			if ( args[ i ].equals( "-to" ) ) {
				if ( args.length >= i ) {
					try {
						int to = Integer.parseInt( args[ i ] );

						if ( to != -1 ) {
							TIMEOUT = to;
						}
					} catch ( NumberFormatException e ) {
						TIMEOUT = 30000;
					}
				} else {
					TIMEOUT = 30000;
				}
			}

		}

		new BasicIIServer( );
	}

}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.

 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-11-30 18:15:44 -0500 (Fri, 30 Nov 2018) $
 * $Revision: 373 $
 * $Author: mbartgis $
 */
package com.rpscore.server;

import java.awt.Point;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;

import com.rpscore.entities.Entity;
import com.rpscore.entities.LivingEntity;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.enemies.Bomber;
import com.rpscore.entities.enemies.Dasher;
import com.rpscore.entities.enemies.Dub;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.entities.enemies.Ghost;
import com.rpscore.entities.enemies.MadBomber;
import com.rpscore.entities.enemies.RogueCannon;
import com.rpscore.entities.spawned.SpawnedEntity;
import com.rpscore.entities.spawned.weapons.Bomb;
import com.rpscore.entities.spawned.weapons.Bullet;
import com.rpscore.entities.spawned.weapons.FusionBomb;
import com.rpscore.entities.spawned.weapons.Mine;
import com.rpscore.entities.spawned.weapons.Mortar;
import com.rpscore.game.GameInstance;

public class MessageManager {

    public static final class PROTOCOL {
        public static final byte MOVE        = 0b00000000;
        public static final byte FOCUS       = 0b00000001;
        public static final byte JOIN        = 0b00000010;
        public static final byte QUIT        = 0b00000011;
        public static final byte STATEDEF    = 0b00000100;
        public static final byte ACTION      = 0b00000101;
        public static final byte MSG         = 0b00000111;
        public static final byte REQ         = 0b00001000;
        public static final byte INFO        = 0b00001001;
        public static final byte ASSIGN      = 0b00001010;
        public static final byte SPAWN_ENT   = 0b00001011;
        public static final byte ACK         = 0b00001100;
        public static final byte MAP         = 0b00001101;
        public static final byte PING        = 0b00001110;
        public static final byte SPAWN_ENEMY = 0b00001111;
        public static final byte ENT_UPDATE  = 0b00010000;
        public static final byte REQUEST_ENT = 0b00010001;
        public static final byte WAVE_START  = 0b00010010;
        public static final byte WAVE_END    = 0b00010011;
        public static final byte VERSION     = 0b00010100;
        public static final byte DIFFICULTY  = 0b00010101;

        private PROTOCOL( ) {
        }

    }

    public static final int NULL_ADDRESS = 0x00000000;

    public static byte[ ] createJoinMessage( String name ) {

        if ( name.length( ) > 255 ) { throw new IllegalArgumentException( "Name is too long!" ); }

        if ( name.isEmpty( ) ) { throw new IllegalArgumentException( "Name cannot be blank!" ); }

        byte[ ] b = new byte[ name.length( ) + 1 ];

        b[ 0 ] = ( byte ) name.length( );

        for ( int i = 1; i < b.length; i++ ) {
            b[ i ] = ( byte ) name.charAt( i - 1 );
        }

        return createMessage( PROTOCOL.JOIN, NULL_ADDRESS, b );

    }

    public static byte[ ] createMessage( byte protocol, int address, byte[ ] content ) {
        byte[ ] b = new byte[ content.length + 5 ];

        b[ 0 ] = protocol;

        // This is guaranteed to have 4 indices.
        byte[ ] c = convertIntArrayToByteArray( address );

        b[ 1 ] = c[ 0 ];
        b[ 2 ] = c[ 1 ];
        b[ 3 ] = c[ 2 ];
        b[ 4 ] = c[ 3 ];

        for ( int i = 5; i < b.length; i++ ) {
            b[ i ] = content[ i - 5 ];
        }

        return b;

    }

    public static String decodeNameFromJoinMessage( byte[ ] b ) {

        StringBuilder sb = new StringBuilder( );

        for ( int i = 0; i < b[ 5 ]; i++ ) {
            sb.append( ( char ) b[ i + 6 ] );
        }

        return sb.toString( );

    }

    public static byte[ ] encodeCoordinates( byte protocol, int address, int x, int y ) {
        byte[ ] c = convertIntArrayToByteArray( x, y );
        return createMessage( protocol, address, c );
    }

    public static Point getCoordinatesFromByteArray( byte[ ] b ) {
        Point p = new Point( );
        p.x += ( b[ 5 ] & 0x000000FF ) * 256 * 256 * 256;
        p.x += ( b[ 6 ] & 0x000000FF ) * 256 * 256;
        p.x += ( b[ 7 ] & 0x000000FF ) * 256;
        p.x += ( b[ 8 ] & 0x000000FF );
        p.y += ( b[ 9 ] & 0x000000FF ) * 256 * 256 * 256;
        p.y += ( b[ 10 ] & 0x000000FF ) * 256 * 256;
        p.y += ( b[ 11 ] & 0x000000FF ) * 256;
        p.y += ( b[ 12 ] & 0x000000FF );

        return p;
    }

    public static byte[ ] convertIntArrayToByteArray( int... msg ) {

        byte[ ] message = new byte[ msg.length * 4 ];

        for ( int i = 0; i < message.length; i += 4 ) {
            message[ i ] = ( byte ) ( ( msg[ i / 4 ] >> 24 ) & 0x000000FF );
            message[ i + 1 ] = ( byte ) ( ( msg[ i / 4 ] >> 16 ) & 0x000000FF );
            message[ i + 2 ] = ( byte ) ( ( msg[ i / 4 ] >> 8 ) & 0x000000FF );
            message[ i + 3 ] = ( byte ) ( msg[ i / 4 ] & 0x000000FF );
        }

        return message;

    }

    public static byte[ ] createInitialSpawn( ) {
        return null;
    }

    public static byte[ ] createTextMessage( String message, int address ) {

        if ( message.length( ) > 65535 ) { throw new IllegalArgumentException( "Message is too long!" ); }

        if ( message.isEmpty( ) ) { throw new IllegalArgumentException( "Message cannot be blank!" ); }

        byte[ ] b = new byte[ message.length( ) ];

        b[ 0 ] = PROTOCOL.MSG;

        for ( int i = 0; i < b.length; i++ ) {
            b[ i ] = ( byte ) message.charAt( i );
        }

        return createMessage( PROTOCOL.MSG, address, b );

    }

    public static String getTextMessageFromByteArray( byte[ ] b ) {
        StringBuilder sb = new StringBuilder( );

        for ( int i = 5; i < b.length; i++ ) {
            sb.append( ( char ) b[ i ] );
        }

        return sb.toString( );
    }

    /**
     * Ensures that the address is not null.
     *
     * @param msg
     * @return
     */
    public static boolean validateAddress( byte[ ] msg ) {
        return ( msg[ 1 ] + msg[ 2 ] + msg[ 3 ] + msg[ 4 ] ) > 0;
    }

    public static void changeAddress( int nAddress, byte[ ] msg ) {
        byte[ ] c = convertIntArrayToByteArray( nAddress );

        msg[ 1 ] = c[ 0 ];
        msg[ 2 ] = c[ 1 ];
        msg[ 3 ] = c[ 2 ];
        msg[ 4 ] = c[ 3 ];

    }

    public static int getAddressFromMessage( byte[ ] b ) {
        return ( ( b[ 1 ] & 0x000000FF ) * 256 * 256 * 256 ) + ( ( b[ 2 ] & 0x000000FF ) * 256 * 256 )
                + ( ( b[ 3 ] & 0x000000FF ) * 256 ) + ( b[ 4 ] & 0x000000FF );
    }

    public static byte[ ] createAssignment( int address, int assignment ) {

        return createMessage( PROTOCOL.ASSIGN, address, convertIntArrayToByteArray( assignment ) );

    }

    public static int getAssignmentFromMessage( byte[ ] b ) {
        return ( ( b[ 5 ] & 0x000000FF ) * 256 * 256 * 256 ) + ( ( b[ 6 ] & 0x000000FF ) * 256 * 256 )
                + ( ( b[ 7 ] & 0x000000FF ) * 256 ) + ( b[ 8 ] & 0x000000FF );
    }

    public static byte[ ] createQuitMessage( int address ) {
        return createMessage( PROTOCOL.QUIT, address, new byte[ ] { } );
    }

    public static int getIntFromByteArray( byte[ ] b, int position ) throws ArrayIndexOutOfBoundsException {
        return ( ( b[ position ] & 0x000000FF ) * 256 * 256 * 256 ) + ( ( b[ position + 1 ] & 0x000000FF ) * 256 * 256 )
                + ( ( b[ position + 2 ] & 0x000000FF ) * 256 ) + ( b[ position + 3 ] & 0x000000FF );
    }

    // FIXME These need to be reduced to bytes to reduce the data transfer
    // overhead.
    public static byte[ ] createActionMessage( int address, int... properties ) {

        return createMessage( PROTOCOL.ACTION, address, convertIntArrayToByteArray( properties ) );
    }

    public static SpawnedEntity getPlayerAction( byte[ ] b ) {
        SpawnedEntity e = null;

        switch ( getIntFromByteArray( b, 5 ) ) {
            case SpawnedEntity.TYPE.BULLET: {
                e = new Bullet( getIntFromByteArray( b, 9 ), getIntFromByteArray( b, 13 ), getIntFromByteArray( b, 17 ),
                        getIntFromByteArray( b, 21 ), getIntFromByteArray( b, 25 ), getIntFromByteArray( b, 29 ),
                        getIntFromByteArray( b, 33 ), null );
                break;
            }
            case SpawnedEntity.TYPE.BOMB: {
                e = new Bomb( getIntFromByteArray( b, 9 ), getIntFromByteArray( b, 13 ), getIntFromByteArray( b, 17 ),
                        getIntFromByteArray( b, 21 ), getIntFromByteArray( b, 25 ), getIntFromByteArray( b, 29 ),
                        getIntFromByteArray( b, 33 ), null );
                break;
            }
            case SpawnedEntity.TYPE.MORTAR: {
                e = new Mortar( getIntFromByteArray( b, 9 ), getIntFromByteArray( b, 13 ), getIntFromByteArray( b, 17 ),
                        getIntFromByteArray( b, 21 ), getIntFromByteArray( b, 25 ), null );
                break;
            }
            case SpawnedEntity.TYPE.LANDMINE: {
                e = new Mine( getIntFromByteArray( b, 9 ), getIntFromByteArray( b, 13 ), getIntFromByteArray( b, 17 ),
                        getIntFromByteArray( b, 21 ), getIntFromByteArray( b, 25 ), null );
                break;
            }
            case SpawnedEntity.TYPE.FUSION_BOMB: {
                e = new FusionBomb( getIntFromByteArray( b, 9 ), getIntFromByteArray( b, 13 ),
                        getIntFromByteArray( b, 17 ), getIntFromByteArray( b, 21 ), getIntFromByteArray( b, 25 ), getIntFromByteArray( b, 29 ),
                        null );
                break;
            }
            case SpawnedEntity.TYPE.FUSION_BOMB_DETONATION: {
                e = null;
                break;
            }
        }
        return e;
    }

    public static byte[ ] getPlayerStatedef( Player p ) {

        byte[ ] stats = convertIntArrayToByteArray( ( int ) p.getHealth( ) );

        return createMessage( PROTOCOL.STATEDEF, p.getID( ), stats );

    }

    public static void updatePlayerStateDef( byte[ ] stats, Player p ) {

        int e = getIntFromByteArray( stats, 5 );

        p.setHealth( e );
    }

    public static boolean isAckMessage( byte[ ] msg ) {
        return msg.length > 0 && msg[ 0 ] == PROTOCOL.ACK;
    }

    public static byte[ ] createAckMessage( ) {
        return new byte[ ] { PROTOCOL.ACK };
    }

    public static boolean isPingMessage( byte[ ] msg ) {
        return msg.length > 0 && msg[ 0 ] == PROTOCOL.PING;
    }

    public static byte[ ] createPingMessage( ) {
        return new byte[ ] { PROTOCOL.PING };
    }

    public static byte[ ] createDataCarryingPingMessage( int data ) {
        byte[ ] b = convertIntArrayToByteArray( data );
        return new byte[ ] { MessageManager.PROTOCOL.PING, b[ 0 ], b[ 1 ], b[ 2 ], b[ 3 ] };
    }

    public enum EnemyType {

        DUB ( ( byte ) 0, Dub.class ), DASHER ( ( byte ) 1, Dasher.class ), BOMBER ( ( byte ) 2,
                Bomber.class ), GHOST ( ( byte ) 3, Ghost.class ), ROGUE_CANNON ( ( byte ) 4,
                        RogueCannon.class ), MAD_BOMBER ( ( byte ) 5, MadBomber.class );

        private byte  value;
        private Class assoc;

        private EnemyType( byte type, Class c ) {
            this.value = type;
            this.assoc = c;
        }

        public byte getValue( ) {
            return this.value;
        }

        public Class getAssociatedClass( ) {
            return this.assoc;
        }
    }

    public static EnemyType getEnemyType( Enemy e ) {
        if ( e == null ) { throw new IllegalArgumentException( "Null Enemy" ); }

        for ( EnemyType et : EnemyType.values( ) ) {
            if ( et.getAssociatedClass( ) == e.getClass( ) ) { return et; }
        }

        throw new IllegalArgumentException( "Enemy type not Found!" );

    }

    public static EnemyType getEnemyType( byte val ) {
        for ( EnemyType et : EnemyType.values( ) ) {
            if ( et.getValue( ) == val ) { return et; }
        }
        throw new IllegalArgumentException( "Enemy type not found." );
    }

    public static byte[ ] encodeEnemy( Enemy e ) {

        byte[ ] data = convertIntArrayToByteArray( e.getID( ), e.getTarget( ).getID( ), ( int ) e.getX( ),
                ( int ) e.getY( ), ( int ) e.getHealth( ), ( int ) e.getHealthMax( ), ( int ) e.getMagic( ),
                ( int ) e.getMagicMax( ), ( int ) e.getStamina( ), ( int ) e.getStaminaMax( ) );

        byte[ ] ret = new byte[ data.length + 2 ];

        ret[ 0 ] = PROTOCOL.SPAWN_ENEMY;
        EnemyType type = getEnemyType( e );

        ret[ 1 ] = type.getValue( );

        for ( int i = 2; i < data.length; i++ ) {
            ret[ i ] = data[ i - 2 ];
        }

        return ret;

    }

    public static Enemy decodeEnemy( byte[ ] data, GameInstance reference ) throws NullPointerException {

        EnemyType et = getEnemyType( data[ 1 ] );

        Entity plTest = reference.getEntityByID( getIntFromByteArray( data, 6 ) );

        if ( plTest == null || !( plTest instanceof Player ) ) { throw new IllegalArgumentException(
                "Retrieved reference target is not a Player." ); }

        double x = getIntFromByteArray( data, 10 );
        double y = getIntFromByteArray( data, 14 );

        Player target = ( Player ) plTest;

        Enemy e = null;

        try {
            Constructor< Enemy > c = et.getAssociatedClass( )
                    .getConstructor( new Class[ ] { Double.class, Double.class } );

            try {
                e = c.newInstance( x, y );
            } catch ( InstantiationException | IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException e1 ) {

                e1.printStackTrace( );
            }

        } catch ( NoSuchMethodException | SecurityException ex ) {
            ex.printStackTrace( );
            throw new IllegalArgumentException( "Invalid Enemy Type: " + data[ 1 ] );
        }

        e.setDifficulty( reference.getSpawner( ).getDifficulty( ) );

        e.setID( getIntFromByteArray( data, 2 ) );

        e.setTarget( target );

        e.setHealth( getIntFromByteArray( data, 18 ) );
        e.setHealthMax( getIntFromByteArray( data, 22 ) );
        e.setMagic( getIntFromByteArray( data, 26 ) );
        e.setMagicMax( getIntFromByteArray( data, 30 ) );
        e.setStamina( getIntFromByteArray( data, 34 ) );
        e.setStaminaMax( getIntFromByteArray( data, 38 ) );

        return e;

    }

    public static boolean isJoinMessage( byte[ ] msg ) {
        return msg != null && msg.length > 0 && msg[ 0 ] == PROTOCOL.JOIN;
    }

    public static byte[ ] createEnemyUpdateMessage( Enemy e ) {
        byte[ ] ret = new byte[ 30 ];

        ret[ 0 ] = PROTOCOL.ENT_UPDATE;
        ret[ 1 ] = ( byte ) 0;

        byte[ ] remaining = convertIntArrayToByteArray( e.getID( ), e.getTarget( ).getID( ), ( int ) e.getX( ),
                ( int ) e.getY( ), ( int ) e.getHealth( ), ( int ) e.getMagic( ), ( int ) e.getStamina( ) );

        for ( int i = 2; i < ret.length; i++ ) {
            ret[ i ] = remaining[ i - 2 ];
        }

        return ret;
    }

    public static boolean updateEnemy( byte[ ] msg, Enemy e, GameInstance reference ) {

        int id = getIntFromByteArray( msg, 2 );

        if ( e.getID( ) == id ) {

            int targetID = getIntFromByteArray( msg, 6 );
            Entity target = reference.getEntityByID( targetID );

            if ( target instanceof LivingEntity ) {
                e.setTarget( ( LivingEntity ) target );
            }

            e.setX( getIntFromByteArray( msg, 10 ) );
            e.setY( getIntFromByteArray( msg, 14 ) );
            e.setHealth( getIntFromByteArray( msg, 18 ) );
            e.setMagic( getIntFromByteArray( msg, 22 ) );
            e.setStamina( getIntFromByteArray( msg, 26 ) );

            return true;

        }

        return false;

    }

    public static byte[ ] createEntityRequestMessage( int pid, int id ) {

        byte[ ] bID = convertIntArrayToByteArray( pid, id );

        return new byte[ ] { PROTOCOL.REQUEST_ENT, bID[ 0 ], bID[ 1 ], bID[ 2 ], bID[ 3 ], bID[ 4 ], bID[ 5 ], bID[ 6 ],
                bID[ 7 ] };
    }

    public static int decodeWaveMessage( byte[ ] msg ) {
        return getIntFromByteArray( msg, 5 );
    }

    public static byte[ ] toByteArray( double... values ) {
        ByteBuffer bb = ByteBuffer.allocate( values.length * 8 );
        for ( double d : values ) {
            bb.putDouble( d );
        }
        return bb.array( );
    }

    public static double[ ] toDoubleArray( byte[ ] bytearray ) {
        ByteBuffer bb = ByteBuffer.wrap( bytearray );
        double[ ] doubles = new double[ bytearray.length / 8 ];
        for ( int i = 0; i < doubles.length; i++ ) {
            doubles[ i ] = bb.getDouble( );
        }
        return doubles;
    }

    public static byte[ ] createDifficultyTransmission( double d ) {
        byte[ ] b = toByteArray( d );
        return new byte[ ] { PROTOCOL.DIFFICULTY, b[ 0 ], b[ 1 ], b[ 2 ], b[ 3 ], b[ 4 ], b[ 5 ], b[ 6 ], b[ 7 ] };
    }

    public static double decodeDifficulty( byte[ ] message ) {
        byte[ ] extracted = new byte[ 8 ];
        for ( int i = 1; i < message.length; i++ ) {
            extracted[ i - 1 ] = message[ i ];
        }
        return toDoubleArray( extracted )[ 0 ];
    }

}

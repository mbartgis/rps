/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-08-30 21:23:28 -0400 (Thu, 30 Aug 2018) $
 * $Revision: 367 $
 * $Author: mbartgis $
 */

package com.rpscore.entities;

import java.util.LinkedList;

import com.rpscore.entities.spawned.pickups.ItemPickup;
import com.rpscore.lib.MathUtils;
import com.rpscore.terrain.Block;

public abstract class LivingEntity extends Entity {

	protected double					magic, magicMax;
	protected double					stamina, staminaMax;
	protected double					health, healthMax;
	protected double					baseWalkSpeed, baseRunSpeed, walkSpeed, runSpeed, armorRating;

	protected LinkedList< ItemPickup >	pickups;

	protected void addItemPickup( ItemPickup p ) {
		this.pickups.add( p );
	}

	protected boolean hasItemPickup( ) {
		return !this.pickups.isEmpty( );
	}

	public LivingEntity( double x, double y, int width, int height ) {
		super( x, y, width, height );

		this.pickups = new LinkedList< ItemPickup >( );
	}

	@Override
	public void update( ) {

		updateMagic( );
		updateStamina( );
		updateHealth( );

	}

	public double getHealth( ) {
		return health;
	}

	public void setHealth( double h ) {
		this.health = h;
		if ( this.health < 0 ) {
			this.health = 0;
		}
	}

	public void addHealth( double h ) {
		this.health += h;

		if ( this.health > this.healthMax ) {
			this.health = this.healthMax;
		}
		if ( this.health < 0 ) {
			this.health = 0;
		}
	}

	public double getWalkSpeed( ) {
		return baseWalkSpeed;
	}

	public void setRunSpeed( double runSpeed ) {
		this.baseRunSpeed = runSpeed;
	}

	public void setWalkSpeed( double walkSpeed ) {
		this.baseWalkSpeed = walkSpeed;
	}

	public double getRunSpeed( ) {
		return baseRunSpeed;
	}

	public double getMagic( ) {
		return magic;
	}

	public void setMagic( double d ) {
		this.magic = d;
		if ( this.magic < 0 ) {
			this.magic = 0;
		}
	}

	public double getStamina( ) {
		return stamina;
	}

	public void setStamina( double d ) {
		this.stamina = d;
		if ( this.stamina < 0 ) {
			this.stamina = 0;
		}
	}

	public void addMagic( int magic ) {
		this.magic += magic;

		if ( this.magic > this.magicMax ) {
			this.magic = this.magicMax;
		}
		if ( this.magic < 0 ) {
			this.magic = 0;
		}
	}

	public void addStamina( int stamina ) {
		this.stamina += stamina;

		if ( this.stamina > this.staminaMax ) {
			this.stamina = this.staminaMax;
		}
		if ( this.stamina < 0 ) {
			this.stamina = 0;
		}
	}

	public double getMagicMax( ) {
		return magicMax;
	}

	public void setMagicMax( double magicMax ) {
		this.magicMax = magicMax;
	}

	public double getStaminaMax( ) {
		return staminaMax;
	}

	public void setStaminaMax( double staminaMax ) {
		this.staminaMax = staminaMax;
	}

	public double getHealthMax( ) {
		return healthMax;
	}

	public void setHealthMax( double healthMax ) {
		this.healthMax = healthMax;
	}

	@Override
	public boolean isFinished( ) {
		return this.health <= 0;
	}

	protected abstract void updateStamina( );

	protected abstract void updateMagic( );

	public void applyBlockEffects( Block b ) {

		// If the entity is dead, do not apply any effects.
		if ( this.isFinished( ) ) {
			return;
		}

		int tempDamage = b.getTemperature( );

		if ( tempDamage > 0 && MathUtils.getDiscreteChance( 0.10 * tempDamage ) ) {
			// Heat damage can inflict higher health damage.
			this.damage( 1, true );
		}
		// Cold environments can negatively impact stamina.
		else if ( ( tempDamage < 0 ) && MathUtils.getDiscreteChance( 0.10 * Math.abs( tempDamage ) ) ) {
			this.addStamina( -1 * tempDamage );
		}

		this.damage( tempDamage, false );

		int toxDamage = b.getpH( );

		this.damage( toxDamage, true );

		int radDamage = b.getRadiation( );

		// Radiation primarily affects magic, but can also impact health over
		// time.
		if ( radDamage > 0 ) {
			if ( MathUtils.getDiscreteChance( 0.01 ) ) {
				this.damage( 1, true );
			}
			if ( MathUtils.getDiscreteChance( 0.25 ) ) {
				this.addMagic( -1 * radDamage );
			}
		}

		this.walkSpeed = this.baseWalkSpeed * b.getMovementModifier( );
		this.runSpeed = this.baseRunSpeed * b.getMovementModifier( );

	}

	public void damage( double amt, boolean ignoreArmor ) {

		if ( ignoreArmor ) {
			setHealth( getHealth( ) - amt );
		} else {
			int healthDiff = ( int ) ( amt * ( 1.0 - armorRating ) );
			setHealth( getHealth( ) - healthDiff );
		}

	}

}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

package com.rpscore.entities;

import java.awt.Image;
import java.io.IOException;
import java.net.MalformedURLException;

import com.rpscore.C;
import com.rpscore.debug.Log;
import com.rpscore.lib.FileUtils;

public class EntityRegistry {

	public static final class DubRes {
		public static Image[ ]	FRAMES_NORMAL;
		public static Image[ ]	FRAMES_DAMAGED;

		public static boolean build( ) {
			// Dub
			Log.info( "Creating Image cache for Dubs..." );
			DubRes.FRAMES_NORMAL = new Image[ 8 ];
			DubRes.FRAMES_DAMAGED = new Image[ 8 ];
			try {
				DubRes.FRAMES_NORMAL[ 0 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_back.png" );
				DubRes.FRAMES_NORMAL[ 1 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_front.png" );
				DubRes.FRAMES_NORMAL[ 2 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_left.png" );
				DubRes.FRAMES_NORMAL[ 3 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_right.png" );
				DubRes.FRAMES_NORMAL[ 4 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_back_2.png" );
				DubRes.FRAMES_NORMAL[ 5 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_front_2.png" );
				DubRes.FRAMES_NORMAL[ 6 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_left_2.png" );
				DubRes.FRAMES_NORMAL[ 7 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_right_2.png" );
				DubRes.FRAMES_DAMAGED[ 0 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_damaged_back.png" );
				DubRes.FRAMES_DAMAGED[ 1 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_damaged_front.png" );
				DubRes.FRAMES_DAMAGED[ 2 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_damaged_left.png" );
				DubRes.FRAMES_DAMAGED[ 3 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_damaged_right.png" );
				DubRes.FRAMES_DAMAGED[ 4 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_damaged_back_2.png" );
				DubRes.FRAMES_DAMAGED[ 5 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_damaged_front_2.png" );
				DubRes.FRAMES_DAMAGED[ 6 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_damaged_left_2.png" );
				DubRes.FRAMES_DAMAGED[ 7 ] = FileUtils.importImage( C.File.DUB_GFX_FOLDER + "dub_damaged_right_2.png" );
			} catch ( MalformedURLException e ) {
				Log.error( "A MalformedURLException occurred while creating the image cache for Dubs." );
			} catch ( IOException e ) {
				Log.error( "An IOException occurred while creating the image cache for Dubs." );
			}
			Log.info( "Image cache creation for Dubs successful." );

			return true;
		}

	}

	public static final class BomberRes {
		public static Image[ ]	FRAMES_NORMAL;
		public static Image[ ]	FRAMES_DAMAGED;

		public static void build( ) {
			// Bomber
			Log.info( "Creating Image cache for Bombers..." );
			FRAMES_NORMAL = new Image[ 6 ];
			try {
				FRAMES_NORMAL[ 0 ] = FileUtils.importImage( C.File.BOMBER_GFX_FOLDER + "bomber_front.png" );
				FRAMES_NORMAL[ 1 ] = FileUtils.importImage( C.File.BOMBER_GFX_FOLDER + "bomber_front_2.png" );
				FRAMES_NORMAL[ 2 ] = FileUtils.importImage( C.File.BOMBER_GFX_FOLDER + "bomber_front_3.png" );
				FRAMES_NORMAL[ 3 ] = FileUtils.importImage( C.File.BOMBER_GFX_FOLDER + "bomber_front_4.png" );
				FRAMES_NORMAL[ 4 ] = FileUtils.importImage( C.File.BOMBER_GFX_FOLDER + "bomber_front_5.png" );
				FRAMES_NORMAL[ 5 ] = FileUtils.importImage( C.File.BOMBER_GFX_FOLDER + "bomber_front_6.png" );

			} catch ( MalformedURLException e ) {
				Log.error( "A MalformedURLException occurred while creating the image cache for Bombers." );
			} catch ( IOException e ) {
				Log.error( "An IOException occurred while creating the image cache for Bombers." );
			}
			Log.info( "Image cache creation for Bombers successful." );
		}
	}

	public static final class MadBomberRes {
		public static Image[ ]	FRAMES_NORMAL;
		public static Image[ ]	FRAMES_DAMAGED;

		public static void build( ) {
			// Bomber
			Log.info( "Creating Image cache for Mad Bombers..." );
			FRAMES_NORMAL = new Image[ 2 ];
			try {
				FRAMES_NORMAL[ 0 ] = FileUtils.importImage( C.File.MAD_BOMBER_GFX_FOLDER + "mad_bomber_0.png" );
				FRAMES_NORMAL[ 1 ] = FileUtils.importImage( C.File.MAD_BOMBER_GFX_FOLDER + "mad_bomber_1.png" );

			} catch ( MalformedURLException e ) {
				Log.error( "A MalformedURLException occurred while creating the image cache for Mad Bombers." );
			} catch ( IOException e ) {
				Log.error( "An IOException occurred while creating the image cache for Bombers." );
			}
			Log.info( "Image cache creation for Mad Bombers successful." );
		}
	}

	public static final class RogueCannonRes {
		public static Image[ ]	FRAMES_NORMAL;
		public static Image[ ]	FRAMES_DAMAGED;

		public static void build( ) {
			// Bomber
			Log.info( "Creating Image cache for Rogue Cannons..." );
			FRAMES_NORMAL = new Image[ 4 ];
			try {
				FRAMES_NORMAL[ 0 ] = FileUtils.importImage( C.File.ROGUE_CANNON_GFX_FOLDER + "rc_front.png" );
				FRAMES_NORMAL[ 1 ] = FileUtils.importImage( C.File.ROGUE_CANNON_GFX_FOLDER + "rc_back.png" );
				FRAMES_NORMAL[ 2 ] = FileUtils.importImage( C.File.ROGUE_CANNON_GFX_FOLDER + "rc_left.png" );
				FRAMES_NORMAL[ 3 ] = FileUtils.importImage( C.File.ROGUE_CANNON_GFX_FOLDER + "rc_right.png" );

			} catch ( MalformedURLException e ) {
				Log.error( "A MalformedURLException occurred while creating the image cache for Rogue Cannons." );
			} catch ( IOException e ) {
				Log.error( "An IOException occurred while creating the image cache for Rogue Cannons." );
			}
			Log.info( "Image cache creation for Rogue Cannons successful." );
		}
	}

	public static final class HealthPickupRes {

		public static Image[ ] FRAMES_NORMAL;

		public static void build( ) {
			// Bomber
			Log.info( "Creating Image cache for Health Pickups..." );
			FRAMES_NORMAL = new Image[ 16 ];
			try {
				for ( int i = 0; i < FRAMES_NORMAL.length; i++ ) {
					FRAMES_NORMAL[ i ] = FileUtils
							.importImage( C.File.HEALTH_PICKUP_GFX_FOLDER + "health_" + i + ".png" );
				}

			} catch ( MalformedURLException e ) {
				Log.error( "A MalformedURLException occurred while creating the image cache for Health Pickups." );
			} catch ( IOException e ) {
				Log.error( "An IOException occurred while creating the image cache for Health Pickups." );
			}
			Log.info( "Image cache creation for Health Pickups successful." );
		}
	}

	public static final class StaminaPickupRes {

		public static Image[ ] FRAMES_NORMAL;

		public static void build( ) {
			// Bomber
			Log.info( "Creating Image cache for Health Pickups..." );
			FRAMES_NORMAL = new Image[ 16 ];
			try {
				for ( int i = 0; i < FRAMES_NORMAL.length; i++ ) {
					FRAMES_NORMAL[ i ] = FileUtils
							.importImage( C.File.STAMINA_PICKUP_GFX_FOLDER + "stamina_" + i + ".png" );
				}

			} catch ( MalformedURLException e ) {
				Log.error( "A MalformedURLException occurred while creating the image cache for Stamina Pickups." );
			} catch ( IOException e ) {
				Log.error( "An IOException occurred while creating the image cache for Stamina Pickups." );
			}
			Log.info( "Image cache creation for Stamina Pickups successful." );
		}
	}

	public static final class MagicPickupRes {

		public static Image[ ] FRAMES_NORMAL;

		public static void build( ) {
			// Bomber
			Log.info( "Creating Image cache for Magic Pickups..." );
			FRAMES_NORMAL = new Image[ 16 ];
			try {
				for ( int i = 0; i < FRAMES_NORMAL.length; i++ ) {
					FRAMES_NORMAL[ i ] = FileUtils
							.importImage( C.File.MAGIC_PICKUP_GFX_FOLDER + "magic_" + i + ".png" );
				}

			} catch ( MalformedURLException e ) {
				Log.error( "A MalformedURLException occurred while creating the image cache for Magic Pickups." );
			} catch ( IOException e ) {
				Log.error( "An IOException occurred while creating the image cache for Magic Pickups." );
			}
			Log.info( "Image cache creation for Magic Pickups successful." );
		}
	}

	public static void buildEntityRegistry( ) {

		DubRes.build( );
		BomberRes.build( );
		RogueCannonRes.build( );
		MadBomberRes.build( );
		HealthPickupRes.build( );
		StaminaPickupRes.build( );
		MagicPickupRes.build( );

	}

}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

package com.rpscore.entities;

import java.util.ArrayList;
import java.util.List;

import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.entities.spawned.SpawnedEntity;
import com.rpscore.entities.spawned.pickups.Pickup;
import com.rpscore.entities.spawned.weapons.Bomb;
import com.rpscore.entities.spawned.weapons.Bullet;

public interface EntityContainer {

	public abstract void addSpawnedEntity( SpawnedEntity e );

	public abstract void addEventToKillfeed( Entity killer, Entity victim, Entity method );

	public abstract void addOtherPlayer( Player p );

	public abstract void setPlayer( Player p );

	public abstract void addEnemy( Enemy e );

	public abstract Player getPlayer( );

	public abstract List< Enemy > getCurrentEnemies( );

	public abstract List< Player > getCurrentOtherPlayers( );

	public abstract List< Bomb > getCurrentBombs( );

	public abstract List< Bullet > getCurrentBullets( );

	public abstract List< Pickup > getCurrentPickups( );

	public default List< SpawnedEntity > getAllSpawnedEntities( ) {

		List< Bomb > bombs = getCurrentBombs( );

		List< Bullet > bullets = getCurrentBullets( );

		List< Pickup > pickups = getCurrentPickups( );

		List< SpawnedEntity > spEnts = new ArrayList< >( bombs.size( ) + bullets.size( ) + pickups.size( ) );

		for ( Bomb bomb : bombs ) {
			spEnts.add( bomb );
		}

		for ( Bullet bullet : bullets ) {
			spEnts.add( bullet );
		}

		for ( Pickup pickup : pickups ) {
			spEnts.add( pickup );
		}

		return spEnts;

	}

	public default List< LivingEntity > getAllLivingEntities( ) {

		List< Enemy > enemies = getCurrentEnemies( );

		List< Player > otherPlayers = getCurrentOtherPlayers( );

		List< LivingEntity > spEnts = new ArrayList< >( 1 + enemies.size( ) + otherPlayers.size( ) );

		spEnts.add( getPlayer( ) );

		for ( Enemy enemy : enemies ) {
			spEnts.add( enemy );
		}

		for ( Player otherPlayer : otherPlayers ) {
			spEnts.add( otherPlayer );
		}

		return spEnts;
	}

	public default List< Entity > getAllEntities( ) {

		List< LivingEntity > lents = getAllLivingEntities( );
		List< SpawnedEntity > sents = getAllSpawnedEntities( );

		List< Entity > spEnts = new ArrayList< >( lents.size( ) + sents.size( ) );

		for ( LivingEntity lent : lents ) {
			spEnts.add( lent );
		}

		for ( SpawnedEntity sent : sents ) {
			spEnts.add( sent );
		}

		return spEnts;

	}

	public abstract void onWaveStart( );

	public abstract void onWaveEnd( );

}

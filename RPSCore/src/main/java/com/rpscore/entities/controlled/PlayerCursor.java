/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-08-30 21:23:28 -0400 (Thu, 30 Aug 2018) $
 * $Revision: 367 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.controlled;

import java.awt.Color;
import java.awt.Font;
import java.util.Iterator;
import java.util.List;

import com.rpscore.entities.Entity;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.entities.spawned.SpawnedEntity;
import com.rpscore.entities.spawned.pickups.Pickup;
import com.rpscore.gui.game.GraphicsAdapter;

public class PlayerCursor extends Entity {

	private boolean				enemyHighlighted, playerHighlighted;

	private List< Entity >		entities;

	private String				info;

	private static final Font	INFO_FONT	= new Font( "Consolas", Font.BOLD, 20 );

	public PlayerCursor( double x, double y, int width, int height ) {
		super( x, y, width, height );

		this.setName( "Player Cursor" );

		this.isCentered = true;

		this.enemyHighlighted = false;
		this.playerHighlighted = false;

		this.info = "";

	}

	public void setInfo( String info ) {
		this.info = info;
	}

	public void setEntities( List< Entity > e ) {
		this.entities = e;
	}

	public List< Entity > getEntities( ) {
		return this.entities;
	}

	@Override
	public void draw( GraphicsAdapter g ) {

		g.setFont( INFO_FONT );
		g.drawString( this.info, ( int ) ( x - ( width / 2 ) ), ( int ) y - 40 );

		if ( this.enemyHighlighted ) {
			g.setColor( new Color( 0x70AA0000, true ) );
		} else if ( this.playerHighlighted ) {
			g.setColor( new Color( 0x7000AA00, true ) );
		} else {
			g.setColor( new Color( 0x704292f4, true ) );
		}

		g.fillOval( ( ( int ) this.getX( ) ) - ( this.getWidth( ) / 2 ),
				( ( int ) this.getY( ) ) - ( this.getHeight( ) / 2 ), this.getWidth( ), this.getHeight( ) );

	}

	@Override
	public void update( ) {

		if ( this.entities != null ) {
			Iterator< Entity > ents = entities.iterator( );

			while ( ents.hasNext( ) ) {
				Entity e = ents.next( );
				if ( e instanceof Enemy && this.isInBounds( e ) ) {
					this.enemyHighlighted = true;
					this.playerHighlighted = false;
					return;
				}
				if ( e instanceof Player && this.isInBounds( e ) ) {
					this.enemyHighlighted = false;
					this.playerHighlighted = true;
					return;
				}
				if ( e instanceof SpawnedEntity && this.isInBounds( e ) && ( ( SpawnedEntity ) e ).isEnemyOrigin( ) ) {
					this.enemyHighlighted = true;
					this.playerHighlighted = false;
					return;
				}
				if ( e instanceof SpawnedEntity && this.isInBounds( e ) && ( ( SpawnedEntity ) e ).isPlayerOrigin( ) ) {
					this.enemyHighlighted = false;
					this.playerHighlighted = true;
					return;
				}
				if ( e instanceof Pickup && this.isInBounds( e ) ) {
					this.enemyHighlighted = false;
					this.playerHighlighted = true;
					return;
				}
			}

			this.enemyHighlighted = false;
			this.playerHighlighted = false;
		}

	}

	@Override
	public boolean isInBounds( Entity e ) {

		double ex1, ex2, tx1, tx2, ey1, ey2, ty1, ty2;

		if ( this.isCentered ) {
			tx1 = this.x - ( this.width / 2.0 );
			tx2 = this.x + ( this.width / 2.0 );
			ty1 = this.y - ( this.height / 2.0 );
			ty2 = this.y + ( this.height / 2.0 );
		} else {
			tx1 = this.x;
			tx2 = this.x + this.width;
			ty1 = this.y;
			ty2 = this.y + this.height;
		}
		if ( e.isCentered( ) ) {
			ex1 = e.getDX( ) - ( e.getWidth( ) / 2.0 );
			ex2 = e.getDX( ) + ( e.getWidth( ) / 2.0 );
			ey1 = e.getDY( ) - ( e.getHeight( ) / 2.0 );
			ey2 = e.getDY( ) + ( e.getHeight( ) / 2.0 );
		} else {
			ex1 = e.getDX( );
			ex2 = e.getDX( ) + e.getWidth( );
			ey1 = e.getDY( );
			ey2 = e.getDY( ) + e.getHeight( );
		}

		return ( ( ( tx1 >= ex1 && tx1 <= ex2 ) && ( ty1 >= ey1 && ty1 <= ey2 ) )
				|| ( ( tx2 >= ex1 && tx2 <= ex2 ) && ( ty1 >= ey1 && ty1 <= ey2 ) )
				|| ( ( tx1 >= ex1 && tx1 <= ex2 ) && ( ty2 >= ey1 && ty2 <= ey2 ) )
				|| ( ( tx2 >= ex1 && tx2 <= ex2 ) && ( ty2 >= ey1 && ty2 <= ey2 ) ) )
				|| ( ( ( ex1 >= tx1 && ex1 <= tx2 ) && ( ey1 >= ty1 && ey1 <= ty2 ) )
						|| ( ( ex2 >= tx1 && ex2 <= tx2 ) && ( ey1 >= ty1 && ey1 <= ty2 ) )
						|| ( ( ex1 >= tx1 && ex1 <= tx2 ) && ( ey2 >= ty1 && ey2 <= ty2 ) )
						|| ( ( ex2 >= tx1 && ex2 <= tx2 ) && ( ey2 >= ty1 && ey2 <= ty2 ) ) );
	}

	@Override
	protected void updateHealth( ) {}

	@Override
	public boolean isFinished( ) {
		return false;
	}

}

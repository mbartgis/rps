/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-11-30 18:15:44 -0500 (Fri, 30 Nov 2018) $
 * $Revision: 373 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.controlled;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;

import com.rpscore.C;
import com.rpscore.cfg.ConfigManager;
import com.rpscore.debug.DebugUtils;
import com.rpscore.debug.Log;
import com.rpscore.entities.Entity;
import com.rpscore.entities.LivingEntity;
import com.rpscore.entities.spawned.SpawnedEntity;
import com.rpscore.entities.spawned.weapons.Bomb;
import com.rpscore.entities.spawned.weapons.Bullet;
import com.rpscore.entities.spawned.weapons.FusionBomb;
import com.rpscore.entities.spawned.weapons.Mine;
import com.rpscore.entities.spawned.weapons.Mortar;
import com.rpscore.game.PlayerManager;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.gui.game.StatusBar;
import com.rpscore.gui.game.WeaponSelectMenu.WEAPON;
import com.rpscore.lib.FileUtils;
import com.rpscore.lib.GUIUtils;
import com.rpscore.server.Communicator;
import com.rpscore.server.MessageManager;
import com.rpscore.terrain.TerrainStitcher;

public class Player extends LivingEntity {

	public static final int			PLAYER_WIDTH	= 96;
	public static final int			PLAYER_HEIGHT	= 96;

	public static final int			DEFAULT_STAT	= 100;

	public static final double		EDGE_OFFSET		= 0.25;

	public static final double		BAR_SCALE		= 1.0;

	protected BufferedImage			img;

	protected double				mousex, mousey;

	protected int					secondaryWeapon;

	protected boolean				isRunning, hasMovedOnTick;

	protected int					score;

	protected int					magicRegenCD;

	protected int					staminaRegenCD;

	protected int					xpticker, xpsessioncnt;

	protected int					shootCD;

	protected boolean[ ]			directionalMovement;

	protected boolean				shootPrimary, shootSecondary;

	protected boolean				nameIsSet;

	protected int					screenBoundsW, screenBoundsH;

	protected PlayerManager			playerManager;

	protected static ConfigManager	cfgm			= ConfigManager.getConfigManager( );

	protected StatusBar				healthBar, magicBar, staminaBar, xpBar;

	protected boolean				clearHUDForChat;

	protected Communicator			session;

	protected boolean				isRemote;

	protected boolean				healthChanged;
	
	protected FusionBomb 			activeFusionBomb;

	/*
	 * 	@formatter:off
	public Player( byte[ ] b ) {
		super( 100, 100, PLAYER_WIDTH, PLAYER_HEIGHT );

		this.player_x_offset = 0;
		this.player_y_offset = 0;

		this.mousex = this.x;
		this.mousey = this.y;

		this.isCentered = true;

		this.isRunning = false;

		this.hasMovedOnTick = false;

		this.score = 0;

		this.magicRegenCD = 0;
		this.staminaRegenCD = 0;

		this.xpticker = 0;

		this.xpsessioncnt = 0;

		this.directionalMovement = new boolean[ ] { false, false, false, false };

		this.shootPrimary = false;
		this.shootSecondary = false;

		this.screenBoundsW = 0;
		this.screenBoundsH = 0;

		this.speedMod = 0;

		// All location data is a default data, and will be modified when the
		// parent panel calls these objects.
		this.healthBar = new StatusBar( 20, 0, 320, 10, this.healthMax, this.health );
		this.healthBar.setOutlineColor( Color.WHITE );
		this.healthBar.setFillColor( Color.RED );

		this.magicBar = new StatusBar( 20, 0, 320, 10, this.magicMax, this.magic );
		this.magicBar.setOutlineColor( Color.WHITE );
		this.magicBar.setFillColor( Color.BLUE );

		this.staminaBar = new StatusBar( 20, 0, 320, 10, staminaMax, stamina );
		this.staminaBar.setOutlineColor( Color.WHITE );
		this.staminaBar.setFillColor( Color.GREEN );

		this.xpBar = new StatusBar( 100, 30, 100, 30, playerManager.getXPRequiredForNextLevel( ),
				playerManager.getLevelProgress( ) );
		this.xpBar.setBackingColor( null );
		this.xpBar.setOutlineColor( Color.WHITE );
		this.xpBar.setFillColor( new Color( 0xA041f47d, true ) );

		int h = 0;
		h += b[ 0 ] * ( 256 * 256 * 256 );
		h += b[ 1 ] * ( 256 * 256 );
		h += b[ 2 ] * ( 256 );
		h += b[ 3 ];

		this.setHealth( h );
		this.setHealthMax( h );

		int m = 0;
		m += b[ 4 ] * ( 256 * 256 * 256 );
		m += b[ 5 ] * ( 256 * 256 );
		m += b[ 6 ] * ( 256 );
		m += b[ 7 ];

		this.setMagic( m );
		this.setMagicMax( m );

		int s = 0;
		s += b[ 8 ] * ( 256 * 256 * 256 );
		s += b[ 9 ] * ( 256 * 256 );
		s += b[ 10 ] * ( 256 );
		s += b[ 11 ];

		this.setStamina( s );
		this.setStaminaMax( s );

		int nLen = b[ 16 ];

		StringBuilder sb = new StringBuilder( );
		for ( int i = 0; i < nLen; i++ ) {
			sb.append( ( char ) b[ 17 + i ] );
		}

	}
	@formatter:on
	*/

	public Player( byte[ ] b ) {
		super( 100, 100, PLAYER_WIDTH, PLAYER_HEIGHT );

		this.baseRunSpeed = 10.0;
		this.baseWalkSpeed = 5.0;
		this.runSpeed = 10.0;
		this.walkSpeed = 5.0;

		this.nameIsSet = false;

		this.isRemote = true;

		this.name = MessageManager.decodeNameFromJoinMessage( b );
		this.id = MessageManager.getAddressFromMessage( b );
		this.nameIsSet = true;

		this.setHealth( 100 );
		this.setHealthMax( 100 );
		this.setMagic( 100 );
		this.setMagicMax( 100 );
		this.setStamina( 100 );
		this.setStaminaMax( 100 );

		this.mousex = x;
		this.mousey = y;

		this.isCentered = true;

		this.isRunning = false;

		this.hasMovedOnTick = false;

		this.score = 0;

		this.magicRegenCD = 0;
		this.staminaRegenCD = 0;

		this.xpticker = 0;

		this.xpsessioncnt = 0;

		this.directionalMovement = new boolean[ ] { false, false, false, false };

		this.shootPrimary = false;
		this.shootSecondary = false;

		this.screenBoundsW = 0;
		this.screenBoundsH = 0;

		this.healthChanged = false;

		// If this is on a client machine only...
		if ( PLAYER_ICON != null ) {
			this.img = TerrainStitcher.toBufferedImage( OTHER_PLAYER_ICON, true );
		}

		// All location data is a default data, and will be modified when the
		// parent panel calls these objects.
		this.healthBar = new StatusBar( 20, 0, ( int ) ( this.healthMax * BAR_SCALE ), 15, this.healthMax,
				this.health );
		this.healthBar.setOutlineColor( Color.WHITE.darker( ) );
		this.healthBar.setFillColor( new Color( 0x00820000 ) );

		this.magicBar = new StatusBar( 20, 0, ( int ) ( this.magicMax * BAR_SCALE ), 15, this.magicMax, this.magic );
		this.magicBar.setOutlineColor( Color.WHITE.darker( ) );
		this.magicBar.setFillColor( new Color( 0x000200a8 ) );

		this.staminaBar = new StatusBar( 20, 0, ( int ) ( this.staminaMax * BAR_SCALE ), 15, staminaMax, stamina );
		this.staminaBar.setOutlineColor( Color.WHITE.darker( ) );
		this.staminaBar.setFillColor( new Color( 0x00005b09 ) );

		this.xpBar = new StatusBar( 100, 20, 100, 30, PlayerManager.getTotalXPRequiredForLevel( 2 ), 0 );
		this.xpBar.setBackingColor( null );
		this.xpBar.setOutlineColor( Color.WHITE );
		this.xpBar.setFillColor( new Color( 0xA041f47d, true ) );

		this.clearHUDForChat = false;

	}

	public Player( int x, int y, PlayerManager pm ) {
		super( x, y, PLAYER_WIDTH, PLAYER_HEIGHT );

		this.baseWalkSpeed = pm.getMovementSpeed( );
		this.baseRunSpeed = baseWalkSpeed * 2.0;
		this.walkSpeed = baseWalkSpeed;
		this.runSpeed = walkSpeed * 2.0;

		this.armorRating = pm.getArmorRating( );

		this.nameIsSet = false;

		this.isRemote = false;

		this.playerManager = pm;
		this.name = playerManager.getName( );
		this.nameIsSet = true;

		this.setHealth( playerManager.getHealth( ) );
		this.setHealthMax( playerManager.getHealth( ) );
		this.setMagic( playerManager.getMagic( ) );
		this.setMagicMax( playerManager.getMagic( ) );
		this.setStamina( playerManager.getStamina( ) );
		this.setStaminaMax( playerManager.getStamina( ) );

		this.mousex = x;
		this.mousey = y;

		this.isCentered = true;

		this.isRunning = false;

		this.hasMovedOnTick = false;

		this.score = 0;

		this.magicRegenCD = 0;
		this.staminaRegenCD = 0;

		this.xpticker = 0;

		this.xpsessioncnt = 0;

		this.directionalMovement = new boolean[ ] { false, false, false, false };

		this.shootPrimary = false;
		this.shootSecondary = false;

		this.screenBoundsW = 0;
		this.screenBoundsH = 0;

		this.img = TerrainStitcher.toBufferedImage( PLAYER_ICON, true );

		// All location data is a default data, and will be modified when the
		// parent panel calls these objects.
		this.healthBar = new StatusBar( 20, 0, ( int ) ( this.healthMax * BAR_SCALE ), 15, this.healthMax,
				this.health );
		this.healthBar.setOutlineColor( Color.WHITE.darker( ) );
		this.healthBar.setFillColor( new Color( 0x00820000 ) );

		this.magicBar = new StatusBar( 20, 0, ( int ) ( this.magicMax * BAR_SCALE ), 15, this.magicMax, this.magic );
		this.magicBar.setOutlineColor( Color.WHITE.darker( ) );
		this.magicBar.setFillColor( new Color( 0x000200a8 ) );

		this.staminaBar = new StatusBar( 20, 0, ( int ) ( this.staminaMax * BAR_SCALE ), 15, staminaMax, stamina );
		this.staminaBar.setOutlineColor( Color.WHITE.darker( ) );
		this.staminaBar.setFillColor( new Color( 0x00005b09 ) );

		this.xpBar = new StatusBar( 100, 20, 100, 30, playerManager.getXPRequiredForNextLevel( ),
				playerManager.getLevelProgress( ) );
		this.xpBar.setBackingColor( null );
		this.xpBar.setOutlineColor( Color.WHITE.darker( ) );
		this.xpBar.setFillColor( new Color( /* 0xA041f47d */ 0x4FFFFFFF, true ) );

		this.clearHUDForChat = false;

	}

	public void adjustFreeSkills( ) {
		this.baseWalkSpeed = this.playerManager.getMovementSpeed( );
		this.baseRunSpeed = baseWalkSpeed * 2.0;
		this.walkSpeed = baseWalkSpeed;
		this.runSpeed = walkSpeed * 2.0;
		this.armorRating = this.playerManager.getArmorRating( );
	}

	public void setSession( Communicator c ) {
		this.session = c;
	}

	public void setRemoteLocation( int x, int y ) {
		this.x = x;
		this.y = y;
	}

	public void clearHUDForChat( boolean b ) {
		this.clearHUDForChat = b;
	}

	@Override
	@SuppressWarnings( "hiding" )
	public void teleportTo( int x, int y ) {
		super.teleportTo( x, y );
		this.setHealth( this.getHealthMax( ) );
		this.setMagic( this.getMagicMax( ) );
		this.setStamina( this.getStaminaMax( ) );
	}

	public PlayerManager getPlayerManager( ) {
		return this.playerManager;
	}

	public void setPlayerManager( PlayerManager p ) {
		this.playerManager = p;
	}

	public void setScreenBounds( int w, int h ) {
		this.screenBoundsW = w;
		this.screenBoundsH = h;
	}

	public double[ ] getPlayerCoordinates( ) {
		return new double[ ] { this.getX( ), this.getY( ) };
	}

	public double[ ] getPlayerMousePosition( ) {
		return new double[ ] { this.getMX( ), this.getMY( ) };
	}

	public boolean canShoot( ) {
		return this.shootCD <= 0;
	}

	public void setShootCooldown( int cooldown ) {
		this.shootCD = cooldown;
	}

	public void setMousePosition( int x, int y ) {
		this.mousex = x;
		this.mousey = y;
	}

	public void increaseScore( int n ) {
		this.score += n;

		this.xpsessioncnt += n;
		this.xpticker = 90;
	}

	public void resetScore( ) {
		this.score = 0;
		this.xpsessioncnt = 0;
		this.xpticker = 0;
	}

	public int getScore( ) {
		return this.score;
	}

	@Override
	public double getMX( ) {
		return this.mousex + this.player_x_offset;
	}

	@Override
	public double getMY( ) {
		return this.mousey + this.player_y_offset;
	}

	public double getRemoteMX( ) {
		return this.mousex;
	}

	public double getRemoteMY( ) {
		return this.mousey;
	}

	public boolean isRunning( ) {
		return this.isRunning && this.stamina > 0;
	}

	public boolean isHasMovedOnTick( ) {
		return hasMovedOnTick;
	}

	public void setHasMovedOnTick( boolean hasMovedOnTick ) {
		this.hasMovedOnTick = hasMovedOnTick;
	}

	public void setRunning( boolean running ) {

		this.isRunning = running;

	}

	public void shootPrimary( ) {
		this.shootPrimary = true;
	}

	public void shootSecondary( ) {
		this.shootSecondary = true;
	}

	@Override
	public void setHealth( double health ) {

		boolean equals = health == this.getHealth( );

		super.setHealth( health );

		if ( !this.isRemote && !equals ) {
			if ( this.session != null && !this.session.isClosed( ) ) {
				this.session.sendMessage( MessageManager.getPlayerStatedef( this ) );
			}
		}

	}

	public double getPlayerMouseHypotenuse( ) {
		double xdiff = this.getX( ) - this.getMX( );
		double ydiff = this.getY( ) - this.getMY( );

		return Math.sqrt( Math.pow( xdiff, 2 ) + Math.pow( ydiff, 2 ) );
	}

	public double getPlayerMouseAngleRelative( ) {
		double hypotenuse = getPlayerMouseHypotenuse( );

		// Opposite over hypotenuse.
		if ( hypotenuse == 0 ) {
			hypotenuse = 1;
		}

		double sine = ( this.getY( ) - this.getMY( ) ) / hypotenuse;

		return Math.asin( sine );
	}

	public double getPlayerMouseAngleAbsolute( ) {
		double radians = getPlayerMouseAngleRelative( );
		boolean qt = ( this.getY( ) - this.getMY( ) ) >= 0;
		boolean qr = ( this.getX( ) - this.getMX( ) ) >= 0;

		// Quadrant 2
		if ( qt && !qr ) {
			radians = ( Math.PI / 2 - radians ) + ( Math.PI / 2 );
		}
		// Quadrant 3
		else if ( !qt && !qr ) {
			radians = ( Math.abs( radians ) ) + ( Math.PI );
		}
		// Quadrant 4
		else if ( !qt && qr ) {
			radians = ( ( Math.PI ) - Math.abs( radians ) ) + ( ( 3 / 2 ) * Math.PI );
		}

		return radians;

	}

	public double getRemotePlayerMouseHypotenuse( ) {
		double xdiff = this.getX( ) - this.getRemoteMX( );
		double ydiff = this.getY( ) - this.getRemoteMY( );

		return Math.sqrt( Math.pow( xdiff, 2 ) + Math.pow( ydiff, 2 ) );
	}

	public double getRemotePlayerMouseAngleRelative( ) {
		double hypotenuse = getRemotePlayerMouseHypotenuse( );

		// Opposite over hypotenuse.
		if ( hypotenuse == 0 ) {
			hypotenuse = 1;
		}

		double sine = ( this.getY( ) - this.getRemoteMY( ) ) / hypotenuse;

		return Math.asin( sine );
	}

	public double getRemotePlayerMouseAngleAbsolute( ) {
		double radians = getRemotePlayerMouseAngleRelative( );
		boolean qt = ( this.getY( ) - this.getRemoteMY( ) ) >= 0;
		boolean qr = ( this.getX( ) - this.getRemoteMX( ) ) >= 0;

		// Quadrant 2
		if ( qt && !qr ) {
			radians = ( Math.PI / 2 - radians ) + ( Math.PI / 2 );
		}
		// Quadrant 3
		else if ( !qt && !qr ) {
			radians = ( Math.abs( radians ) ) + ( Math.PI );
		}
		// Quadrant 4
		else if ( !qt && qr ) {
			radians = ( ( Math.PI ) - Math.abs( radians ) ) + ( ( 3 / 2 ) * Math.PI );
		}

		return radians;

	}

	public double getPlayerMouseAngleAbsoluteAsDegree( ) {
		return 57.2958 * getPlayerMouseAngleAbsolute( );
	}

	private int[ ] getPlayerPointsAngle( ) {

		return null;

	}

	public void moveAwayFromMouse( ) {
		double xdiff = Math.abs( this.getX( ) - this.getMX( ) );
		double ydiff = Math.abs( this.getY( ) - this.getMY( ) );

		double ror = ydiff / xdiff;

		double ydist = 0;
		double xdist = 0;

		int speed = ( this.isRunning ) ? 10 : 5;

		if ( ror > 1 ) {
			ror = 1 / ror;
			xdist = speed * ror;
			ydist = speed - xdist;
		} else {
			ydist = speed * ror;
			xdist = speed - ydist;
		}

		if ( this.getMX( ) > this.getX( ) ) {
			this.setX( this.getX( ) - ( int ) xdist );
		} else {
			this.setX( this.getX( ) + ( int ) xdist );
		}

		if ( this.getMY( ) > this.getY( ) ) {
			this.setY( this.getY( ) - ( int ) ydist );
		} else {
			this.setY( this.getY( ) + ( int ) ydist );
		}
	}

	public void moveLeftRelative( ) {
		double xdiff = Math.abs( this.getX( ) - this.getMX( ) );
		double ydiff = Math.abs( this.getY( ) - this.getMY( ) );

		if ( xdiff < 20 && ydiff < 20 ) {
			return;
		}

		double ror = ydiff / xdiff;

		double ydist = 0;
		double xdist = 0;

		int speed = ( this.isRunning ) ? 10 : 5;

		if ( ror > 1 ) {
			ror = 1 / ror;
			xdist = speed * ror;
			ydist = speed - xdist;
		} else {
			ydist = speed * ror;
			xdist = speed - ydist;
		}

		if ( this.getMX( ) > this.getX( ) ) {
			this.setX( this.getX( ) - ( int ) ydist );
		} else {
			this.setX( this.getX( ) + ( int ) ydist );
		}

		if ( this.getMY( ) > this.getY( ) ) {
			this.setY( this.getY( ) - ( int ) xdist );
		} else {
			this.setY( this.getY( ) + ( int ) xdist );
		}
	}

	public void moveRightRelative( ) {
		double xdiff = Math.abs( this.getX( ) - this.getMX( ) );
		double ydiff = Math.abs( this.getY( ) - this.getMY( ) );

		if ( xdiff < 20 && ydiff < 20 ) {
			return;
		}

		double ror = ydiff / xdiff;

		double ydist = 0;
		double xdist = 0;

		int speed = ( this.isRunning ) ? 10 : 5;

		if ( ror > 1 ) {
			ror = 1 / ror;
			xdist = speed * ror;
			ydist = speed - xdist;
		} else {
			ydist = speed * ror;
			xdist = speed - ydist;
		}

		if ( this.getMX( ) > this.getX( ) ) {
			this.setX( this.getX( ) + ( int ) ydist );
		} else {
			this.setX( this.getX( ) - ( int ) ydist );
		}

		if ( this.getMY( ) > this.getY( ) ) {
			this.setY( this.getY( ) + ( int ) xdist );
		} else {
			this.setY( this.getY( ) - ( int ) xdist );
		}
	}

	// TODO Get this thing to work.
	public void moveTorwardMouse( ) {

		double xdiff = Math.abs( this.getX( ) - this.getMX( ) );
		double ydiff = Math.abs( this.getY( ) - this.getMY( ) );

		if ( xdiff < 20 && ydiff < 20 ) {
			return;
		}

		double ror = ydiff / xdiff;

		double ydist = 0;
		double xdist = 0;

		int speed = ( this.isRunning ) ? 10 : 5;

		if ( ror > 1 ) {
			ror = 1 / ror;
			xdist = speed * ror;
			ydist = speed - xdist;
		} else {
			ydist = speed * ror;
			xdist = speed - ydist;
		}

		if ( this.getMX( ) > this.getX( ) ) {
			this.setX( this.getX( ) + ( int ) xdist );
		} else {
			this.setX( this.getX( ) - ( int ) xdist );
		}

		if ( this.getMY( ) > this.getY( ) ) {
			this.setY( this.getY( ) + ( int ) ydist );
		} else {
			this.setY( this.getY( ) - ( int ) ydist );
		}
	}

	@Override
	public void drawBoundingBoxes( GraphicsAdapter g ) {
		super.drawBoundingBoxes( g );

		g.setColor( Color.WHITE );
		g.setFont( new Font( "Arial", Font.BOLD, 24 ) );

		g.drawString( "Health: " + this.getHealth( ), ( ( int ) this.getDX( ) ) + ( this.getWidth( ) / 2 ) + 10,
				( ( int ) this.getDY( ) ) - this.getHeight( ) / 2 + 20 );

		g.drawString( "Magic: " + this.getMagic( ), ( int ) this.getDX( ) + ( this.getWidth( ) / 2 ) + 10,
				( int ) this.getDY( ) - this.getHeight( ) / 2 + 50 );

		g.drawString( "Stamina: " + this.getStamina( ), ( int ) this.getDX( ) + ( this.getWidth( ) / 2 ) + 10,
				( int ) this.getDY( ) - this.getHeight( ) / 2 + 80 );

		g.drawString( "XOff: " + this.getXOffset( ), ( int ) this.getDX( ) + ( this.getWidth( ) / 2 ) + 10,
				( int ) this.getDY( ) - this.getHeight( ) / 2 + 110 );

		g.drawString( "YOff: " + this.getYOffset( ), ( int ) this.getDX( ) + ( this.getWidth( ) / 2 ) + 10,
				( int ) this.getDY( ) - this.getHeight( ) / 2 + 140 );

		g.drawString( "X: " + this.getX( ), ( int ) this.getDX( ) + ( this.getWidth( ) / 2 ) + 10,
				( int ) this.getDY( ) - this.getHeight( ) / 2 + 170 );

		g.drawString( "Y: " + this.getY( ), ( int ) this.getDX( ) + ( this.getWidth( ) / 2 ) + 10,
				( int ) this.getDY( ) - this.getHeight( ) / 2 + 200 );

		g.drawString( "DX: " + this.getDX( ), ( int ) this.getDX( ) + ( this.getWidth( ) / 2 ) + 10,
				( int ) this.getDY( ) - this.getHeight( ) / 2 + 230 );

		g.drawString( "DY: " + this.getDY( ), ( int ) this.getDX( ) + ( this.getWidth( ) / 2 ) + 10,
				( int ) this.getDY( ) - this.getHeight( ) / 2 + 260 );

		g.drawString( "DMX: " + this.getMX( ), ( int ) this.getDX( ) + ( this.getWidth( ) / 2 ) + 10,
				( int ) this.getDY( ) - this.getHeight( ) / 2 + 290 );

		g.drawString( "DMY: " + this.getMY( ), ( int ) this.getDX( ) + ( this.getWidth( ) / 2 ) + 10,
				( int ) this.getDY( ) - this.getHeight( ) / 2 + 320 );

	}

	public void drawRemote( GraphicsAdapter g ) {

		double locationX = this.img.getWidth( ) / 2.0;
		double locationY = this.img.getHeight( ) / 2.0;

		AffineTransform tx = AffineTransform.getRotateInstance( getRemotePlayerMouseAngleAbsolute( ), locationX,
				locationY );

		AffineTransformOp op = new AffineTransformOp( tx, AffineTransformOp.TYPE_BILINEAR );

		g.drawImage( op.filter( this.img, null ), ( ( int ) this.getDX( ) ) - ( this.getWidth( ) / 2 ),
				( ( int ) this.getDY( ) ) - ( this.getHeight( ) / 2 ) );

		this.healthBar.draw( g );

		g.setColor( Color.WHITE );

		g.setFont( new Font( "Serif", Font.BOLD, 20 ) );

		g.drawString( this.getName( ),
				( ( ( int ) this.getDX( ) ) - ( this.getWidth( ) / ( 18 - this.getName( ).length( ) ) ) ),
				( ( int ) this.getDY( ) ) - 80 );

	}

	@Override
	public void draw( GraphicsAdapter g ) {

		double locationX = this.img.getWidth( ) / 2.0;
		double locationY = img.getHeight( ) / 2.0;

		AffineTransform tx = AffineTransform.getRotateInstance( getPlayerMouseAngleAbsolute( ), locationX, locationY );
		AffineTransformOp op = new AffineTransformOp( tx, AffineTransformOp.TYPE_BILINEAR );

		g.drawImage( op.filter( this.img, null ), ( ( int ) this.getDX( ) ) - ( this.getWidth( ) / 2 ),
				( ( int ) this.getDY( ) ) - ( this.getHeight( ) / 2 ) );

		if ( !this.clearHUDForChat && cfgm.getSetting( "HUD", true ) && !isFinished( ) ) {

			if ( this.xpticker > 0 ) {
				g.setColor( Color.WHITE );
				g.setFont( new Font( "Arial", Font.BOLD, 20 ) );
				g.drawString( "+" + this.xpsessioncnt + " XP", ( ( int ) this.getDX( ) ),
						( ( int ) this.getDY( ) ) - 60 );
			}

			if ( this.healthBar != null ) {
				this.healthBar.draw( g );
			}

			if ( this.magicBar != null ) {
				this.magicBar.draw( g );
			}

			if ( this.staminaBar != null ) {
				this.staminaBar.draw( g );
			}

			if ( this.xpBar != null ) {
				this.xpBar.draw( g );
			}

			g.setColor( Color.WHITE );

			g.setFont( new Font( "Serif", Font.BOLD, 16 ) );

			if ( playerManager != null ) {
				g.drawString( "" + playerManager.getLevel( ), this.xpBar.getX( ) + this.xpBar.getWidth( ) / 2, 36 );
			}

		}

	}

	@Override
	public void updateStamina( ) {
		if ( this.isRunning( ) && this.hasMovedOnTick && this.staminaRegenCD <= 0 ) {
			this.stamina--;
			if ( this.stamina <= 0 ) {
				this.staminaRegenCD = 60;
			}
		} else {
			if ( this.stamina < this.staminaMax && this.staminaRegenCD <= 0 ) {
				this.stamina++;
			}
		}

		if ( this.staminaRegenCD > 0 ) {
			this.staminaRegenCD--;
		}

		if ( playerManager != null ) {
			this.staminaMax = playerManager.getStamina( );
		}

		this.staminaBar.setStatus( this.stamina );
		this.staminaBar.setMax( this.staminaMax );

		this.staminaBar.setWidth( ( int ) ( this.staminaMax * BAR_SCALE ) );

	}

	@Override
	public void updateMagic( ) {

		if ( this.magic < this.magicMax ) {
			if ( this.magicRegenCD <= 0 ) {
				this.magic++;
				this.magicRegenCD = 10;
			}
			this.magicRegenCD--;
		}

		if ( playerManager != null ) {
			this.magicMax = playerManager.getMagic( );
		}

		this.magicBar.setStatus( this.magic );
		this.magicBar.setMax( this.magicMax );

		this.magicBar.setWidth( ( int ) ( this.magicMax * BAR_SCALE ) );

	}

	@Override
	public void updateHealth( ) {

		// Edge case where a player's health upgrades in game.
		if ( playerManager != null ) {
			this.healthMax = playerManager.getHealth( );
		}

		this.healthBar.setStatus( this.health );

		this.healthBar.setMax( this.healthMax );

		this.healthBar.setWidth( ( int ) ( this.healthMax * BAR_SCALE ) );

		if ( this.isRemote ) {
			this.healthBar.setX( ( ( int ) this.getDX( ) ) - ( this.getWidth( ) / 2 ) );
			this.healthBar.setY( ( ( int ) this.getDY( ) ) - ( this.getHeight( ) / 2 ) );
		} else {

		}
	}

	public StatusBar getHealthBar( ) {
		return healthBar;
	}

	public void setHealthBar( StatusBar healthBar ) {
		this.healthBar = healthBar;
	}

	public StatusBar getMagicBar( ) {
		return magicBar;
	}

	public void setMagicBar( StatusBar magicBar ) {
		this.magicBar = magicBar;
	}

	public StatusBar getStaminaBar( ) {
		return staminaBar;
	}

	public void setStaminaBar( StatusBar staminaBar ) {
		this.staminaBar = staminaBar;
	}

	public StatusBar getXPBar( ) {
		return xpBar;
	}

	public void setXPBar( StatusBar xpBar ) {
		this.xpBar = xpBar;
	}

	public void setMoveUp( boolean m ) {
		this.directionalMovement[ 0 ] = m;
	}

	public void setMoveDown( boolean m ) {
		this.directionalMovement[ 1 ] = m;
	}

	public void setMoveLeft( boolean m ) {
		this.directionalMovement[ 2 ] = m;
	}

	public void setMoveRight( boolean m ) {
		this.directionalMovement[ 3 ] = m;
	}

	public void setSecondaryWeapon( int w ) {
		this.secondaryWeapon = w;
	}

	public void updateRemote( ) {
		updateHealth( );
	}

	@Override
	public void update( ) {

		if ( this.xpticker > 0 ) {
			this.xpticker--;
		}
		if ( this.xpticker == 0 ) {
			this.xpsessioncnt = 0;
		}

		updateHealth( );
		updateMagic( );
		updateStamina( );

		if ( this.playerManager == null ) {
			return;
		}

		int xpOnBar = this.playerManager.getLevelProgress( ) + this.getScore( );
		int xpRequired = this.playerManager.getXPRequiredForNextLevel( );
		// level up condition
		if ( xpOnBar >= xpRequired ) {
			xpOnBar = xpOnBar - xpRequired;
			playerManager.addXPToProfile( getScore( ) );
			resetScore( );
			this.xpBar.setMax( playerManager.getXPRequiredForNextLevel( ) );
		}

		this.xpBar.setStatus( xpOnBar );

		if ( Entity.hasEntityContainer( ) && !this.isFinished( ) ) {

			int[ ] sess_dims = GUIUtils.getSessionResolution( );

			int win_width = sess_dims[ 0 ];
			int win_height = sess_dims[ 1 ];

			setHasMovedOnTick( false );
			if ( directionalMovement[ 0 ] ) {
				setHasMovedOnTick( true );
				int upEdgeRange = ( int ) ( win_height * EDGE_OFFSET );
				if ( getDY( ) <= upEdgeRange ) {
					if ( isRunning( ) ) {
						this.player_y_offset -= this.runSpeed;
					} else {
						this.player_y_offset -= this.walkSpeed;
					}
				}
				if ( isRunning( ) ) {
					setY( getY( ) - this.runSpeed );
				} else {
					setY( getY( ) - this.walkSpeed );
				}

			}
			if ( directionalMovement[ 1 ] ) {
				setHasMovedOnTick( true );
				int downEdgeRange = win_height - ( int ) ( win_height * EDGE_OFFSET );
				if ( getDY( ) >= downEdgeRange ) {
					if ( isRunning( ) ) {
						this.player_y_offset += this.runSpeed;
					} else {
						this.player_y_offset += this.walkSpeed;
					}
				}

				if ( isRunning( ) ) {
					setY( getY( ) + this.runSpeed );
				} else {
					setY( getY( ) + this.walkSpeed );
				}

			}
			if ( directionalMovement[ 2 ] ) {
				setHasMovedOnTick( true );
				int leftEdgeRange = ( int ) ( win_width * EDGE_OFFSET );
				if ( getDX( ) <= leftEdgeRange ) {
					if ( isRunning( ) ) {
						this.player_x_offset -= this.runSpeed;
					} else {
						this.player_x_offset -= this.walkSpeed;
					}
				}
				if ( isRunning( ) ) {
					setX( getX( ) - this.runSpeed );
				} else {
					setX( getX( ) - this.walkSpeed );
				}

			}
			if ( directionalMovement[ 3 ] ) {
				setHasMovedOnTick( true );
				int rightEdgeRange = win_width - ( int ) ( win_width * EDGE_OFFSET );
				if ( getDX( ) >= rightEdgeRange ) {
					if ( isRunning( ) ) {
						this.player_x_offset += this.runSpeed;
					} else {
						this.player_x_offset += this.walkSpeed;
					}
				}
				if ( isRunning( ) ) {
					setX( getX( ) + this.runSpeed );
				} else {
					setX( getX( ) + this.walkSpeed );
				}

			}

			if ( this.shootCD > 0 ) {
				this.shootCD--;
			}

			if ( this.shootPrimary ) {

				Bullet bill = new Bullet( getX( ), getY( ), playerManager.getBulletDamage( ), getMX( ), getMY( ),
						playerManager.getBulletSpeed( ), playerManager.getBulletDuration( ), this );

				this.createSpawnedEntity( bill );

				setShootCooldown( playerManager.getBulletCooldown( ) );

				this.sendActionMessage( SpawnedEntity.TYPE.BULLET, ( ( int ) getX( ) ), ( ( int ) getY( ) ),
						playerManager.getBulletDamage( ), ( ( int ) getMX( ) ), ( ( int ) getMY( ) ),
						playerManager.getBulletSpeed( ), playerManager.getBulletDuration( ) );

				this.shootPrimary = false;
			}

			if ( this.shootSecondary ) {
				switch ( this.secondaryWeapon ) {
					case WEAPON.BOMB: {
						if ( getMagic( ) >= 20 ) {
							Bomb boom = new Bomb( ( ( int ) getX( ) ), ( ( int ) getY( ) ), ( ( int ) getMX( ) ),
									( ( int ) getMY( ) ), playerManager.getBombPower( ), 60,
									playerManager.getBombThrow( ), this );
							this.createSpawnedEntity( boom );
							setMagic( getMagic( ) - 20 );

							this.sendActionMessage( SpawnedEntity.TYPE.BOMB, ( ( int ) getX( ) ), ( ( int ) getY( ) ),
									( ( int ) getMX( ) ), ( ( int ) getMY( ) ), playerManager.getBombPower( ), 60,
									playerManager.getBombThrow( ) );
						}
						break;
					}
					case WEAPON.MORTAR: {
						if ( getMagic( ) >= 20 ) {
							Mortar boom = new Mortar( getX( ), getY( ), getMX( ), getMY( ),
									playerManager.getBombPower( ) / 4, this );
							this.createSpawnedEntity( boom );
							setMagic( getMagic( ) - 20 );

							this.sendActionMessage( SpawnedEntity.TYPE.MORTAR, ( ( int ) getX( ) ), ( ( int ) getY( ) ),
									( ( int ) getMX( ) ), ( ( int ) getMY( ) ), playerManager.getBombPower( ) / 4 );
						}
						break;
					}
					case WEAPON.LANDMINE: {
						if ( getMagic( ) >= 20 ) {
							Mine boom = new Mine( getMX( ), getMY( ), playerManager.getBombPower( ), this );
							this.createSpawnedEntity( boom );
							setMagic( getMagic( ) - 20 );

							this.sendActionMessage( SpawnedEntity.TYPE.LANDMINE, ( ( int ) getX( ) ),
									( ( int ) getY( ) ), ( ( int ) getMX( ) ), ( ( int ) getMY( ) ),
									playerManager.getBombPower( ) );

						}
						break;
					}
					case WEAPON.DEBUG_ENEMY_BOMB: {
						if ( getMagic( ) >= 20 ) {
							Bomb boom = new Bomb( getMX( ), getMY( ), playerManager.getBombPower( ), 60,
									playerManager.getBombThrow( ), DebugUtils.DEBUG_DUB );

							this.createSpawnedEntity( boom );
							// setMagic( getMagic( ) - 20 );
						}
						break;
					}
					case WEAPON.DEBUG_ENEMY_MORTAR: {
						if ( getMagic( ) >= 20 ) {
							Mortar boom = new Mortar( getMX( ), getMY( ), playerManager.getBombPower( ) / 4,
									DebugUtils.DEBUG_DUB );

							this.createSpawnedEntity( boom );
							// setMagic( getMagic( ) - 20 );
						}
						break;
					}
					case WEAPON.FUSION_BOMB: {
						if ( this.activeFusionBomb != null && this.activeFusionBomb.isFinished( ) ) {
							this.activeFusionBomb = null;
						}
						if ( this.activeFusionBomb == null  ) {
							if ( getMagic( ) >= 20) {
								FusionBomb boom = new FusionBomb( getMX( ), getMY( ), playerManager.getBombPower( ), playerManager.getBombThrow(), this );
								this.activeFusionBomb = boom;
								this.createSpawnedEntity( boom );
								setMagic( getMagic( ) - 20 );
	                            this.sendActionMessage( SpawnedEntity.TYPE.FUSION_BOMB, ( ( int ) getX( ) ), ( ( int ) getY( ) ),
	                                    ( ( int ) getMX( ) ), ( ( int ) getMY( ) ), playerManager.getBombPower( ),
	                                    playerManager.getBombThrow( ) );
							}
						}
						else {
							this.activeFusionBomb.detonate();
                            this.sendActionMessage( SpawnedEntity.TYPE.FUSION_BOMB_DETONATION );
						}
						break;
					}

					default: {
						break;
					}
				}
				this.shootSecondary = false;
			}
		}
	}

	private void sendActionMessage( int... properties ) {
		if ( this.session != null && !this.session.isClosed( ) ) {
			this.session.sendMessage( MessageManager.createActionMessage( this.id, properties ) );
		}
	}

	public static Image	PLAYER_ICON;
	public static Image	OTHER_PLAYER_ICON;
	public static Image	ENEMY_PLAYER_ICON;

	public static void buildPlayer( ) {
		try {
			Log.info( "Creating Image cache for Players..." );
			PLAYER_ICON = FileUtils.importImage( C.File.ICON_PLAYER_NORMAL );
			OTHER_PLAYER_ICON = FileUtils.importImage( C.File.ICON_PLAYER_ALLY );
			ENEMY_PLAYER_ICON = FileUtils.importImage( C.File.ICON_PLAYER_ENEMY );
			Log.info( "Image cache creation for Players successful." );
		}
		catch ( IOException e ) { Log.error( e.getMessage( ) ); }
	}
	
	public void removeActiveFusionBomb() {
	    this.activeFusionBomb = null;
	}

}

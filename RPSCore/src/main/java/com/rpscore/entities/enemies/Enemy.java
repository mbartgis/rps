/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-08-30 21:23:28 -0400 (Thu, 30 Aug 2018) $
 * $Revision: 367 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.enemies;

import java.awt.Color;
import java.awt.Font;
import java.util.List;
import java.util.Random;

import com.rpscore.cfg.ConfigManager;
import com.rpscore.entities.Entity;
import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.LivingEntity;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.gui.game.StatusBar;

public abstract class Enemy extends LivingEntity {

	private int						xpEarnedOnHit;

	public static final double		DIFFICULTY_PEACEFUL		= 0.0;
	public static final double		DIFFICULTY_EASY			= 0.5;
	public static final double		DIFFICULTY_NORMAL		= 1.0;
	public static final double		DIFFICULTY_HARD			= 1.5;
	public static final double		DIFFICULTY_INSANE		= 2.5;
	public static final double		DIFFICULTY_IMPOSSIBLE	= 5.0;

	protected static ConfigManager	cfgm					= ConfigManager.getConfigManager( );

	protected StatusBar				healthBar, magicBar, staminaBar;

	protected int					worth;

	protected int					nameOffset;

	protected boolean				isInvincible;

	protected double				difficulty;

	protected Random				actionGenerator;

	protected int					baseHealth, baseMagic, baseStamina;

	protected boolean				displayIdentifiers, displayBars;

	protected LivingEntity			target;

	public Enemy( double x, double y, int w, int h, String name ) {
		super( x, y, w, h );

		this.nameOffset = 50;

		this.name = name;
		this.isCentered = true;

		this.worth = 0;

		this.isInvincible = false;

		this.difficulty = Enemy.DIFFICULTY_NORMAL;

		this.actionGenerator = new Random( );

		this.displayBars = true;
		this.displayIdentifiers = true;

		this.xpEarnedOnHit = 0;

	}

	public void setTarget( LivingEntity p ) {
		this.target = p;
	}

	public LivingEntity getTarget( ) {
		return this.target;
	}

	public void setDifficulty( double diff ) {
		this.difficulty = diff;
		if ( diff == 0.0 ) {
			this.setHealthMax( 1 );
			this.setMagicMax( 1 );
			this.setStaminaMax( 1 );
			this.setHealth( 1 );
			this.setMagic( 1 );
			this.setStamina( 1 );

		} else {
			this.setHealthMax( ( int ) ( diff * this.baseHealth ) );
			this.setMagicMax( ( int ) ( diff * this.baseMagic ) );
			this.setStaminaMax( ( int ) ( diff * this.baseStamina ) );
			this.setHealth( ( int ) ( diff * this.baseHealth ) );
			this.setMagic( ( int ) ( diff * this.baseMagic ) );
			this.setStamina( ( int ) ( diff * this.baseStamina ) );
		}

		if ( this.difficulty >= DIFFICULTY_INSANE ) {
			this.displayBars = false;
			this.displayIdentifiers = false;
		} else {
			this.displayBars = true;
			this.displayIdentifiers = true;
		}

	}

	public double getDifficulty( ) {
		return this.difficulty;
	}

	public boolean isInvinvible( ) {
		return this.isInvincible;
	}

	public int getDifficultyScaledWorth( ) {
		return ( int ) ( this.worth * this.difficulty );
	}

	public int getWorth( ) {
		return this.worth;
	}

	@Override
	public void draw( GraphicsAdapter g ) {
		if ( cfgm.getSetting( "DisplayBoundingBoxes", false ) ) {
			super.drawBoundingBoxes( g );
		}
		if ( cfgm.getSetting( "HUD", true ) ) {
			if ( displayIdentifiers ) {
				g.setColor( Color.WHITE );
				g.setFont( new Font( "Arial", Font.BOLD, 20 ) );
				int xloc = ( ( int ) this.getDX( ) );
				if ( this.isCentered ) {
					xloc = ( ( int ) this.getDX( ) ) - ( this.width / 2 );
				}

				int level = 0;

				if ( this.difficulty < Enemy.DIFFICULTY_EASY ) {
					level = 0;
				} else if ( this.difficulty < Enemy.DIFFICULTY_NORMAL ) {
					level = 1;
				} else if ( this.difficulty < Enemy.DIFFICULTY_HARD ) {
					level = 2;
				} else if ( this.difficulty < Enemy.DIFFICULTY_INSANE ) {
					level = 3;
				} else if ( this.difficulty < Enemy.DIFFICULTY_IMPOSSIBLE ) {
					level = 4;
				} else if ( difficulty >= Enemy.DIFFICULTY_IMPOSSIBLE ) {
					level = 5;
				}

				g.drawString( "( " + level + " ) " + this.name, xloc, ( ( int ) this.getDY( ) ) - this.nameOffset );
			}

			if ( displayBars ) {
				if ( this.healthBar != null ) {
					this.healthBar.draw( g );
				}
				if ( this.magicBar != null ) {
					this.magicBar.draw( g );
				}
				if ( this.staminaBar != null ) {
					this.staminaBar.draw( g );
				}
			}
		}

	}

	@Override
	public void damage( double dmg, boolean ignoreArmor ) {
		int xp = 0;

		if ( health - dmg < 0 ) {
			xp = ( int ) Math.round( ( health / healthMax ) * this.getDifficultyScaledWorth( ) );
		} else {
			xp = ( int ) Math.round( ( dmg / healthMax ) * this.getDifficultyScaledWorth( ) );
		}

		this.health -= dmg;
		if ( this.health < 0 ) {
			this.health = 0;
		}
		this.xpEarnedOnHit = xp;
	}

	public int getEarnedXP( ) {
		int xp = this.xpEarnedOnHit;

		this.xpEarnedOnHit = 0;

		return xp;
	}

	private final void updateActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		if ( this.isFinished( ) ) {
			this.performActionsAfterDeath( player, otherEnemies, gi );
			return;
		}

		if ( this.difficulty < Enemy.DIFFICULTY_EASY ) {
			return;
		} else if ( this.difficulty < Enemy.DIFFICULTY_NORMAL ) {
			this.performEasyActions( player, otherEnemies, gi );
		}

		else if ( this.difficulty < Enemy.DIFFICULTY_HARD ) {
			this.performNormalActions( player, otherEnemies, gi );

		} else if ( this.difficulty < Enemy.DIFFICULTY_INSANE ) {
			this.performHardActions( player, otherEnemies, gi );

		} else if ( this.difficulty < Enemy.DIFFICULTY_IMPOSSIBLE ) {
			this.performInsaneActions( player, otherEnemies, gi );
		}

		else if ( this.difficulty >= Enemy.DIFFICULTY_IMPOSSIBLE ) {
			this.performImpossibleActions( player, otherEnemies, gi );
		}
	}

	protected abstract void performActionsAfterDeath( LivingEntity player, List< Enemy > otherEnemies,
			EntityContainer gi );

	protected abstract void performEasyActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi );

	protected abstract void performNormalActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi );

	protected abstract void performHardActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi );

	protected abstract void performInsaneActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi );

	protected abstract void performImpossibleActions( LivingEntity player, List< Enemy > otherEnemies,
			EntityContainer gi );

	@Override
	public void setHealthMax( double healthMax ) {
		super.setHealthMax( healthMax );
		if ( this.healthBar != null ) {
			this.healthBar.setMax( healthMax );
		}
	}

	@Override
	public void setMagicMax( double magicMax ) {
		super.setMagicMax( magicMax );
		if ( this.magicBar != null ) {
			this.magicBar.setMax( magicMax );
		}
	}

	@Override
	public void setStaminaMax( double staminaMax ) {
		super.setStaminaMax( staminaMax );
		if ( this.staminaBar != null ) {
			this.staminaBar.setMax( staminaMax );
		}
	}

	public StatusBar getHealthBar( ) {
		return healthBar;
	}

	public void setHealthBar( StatusBar healthBar ) {
		this.healthBar = healthBar;
	}

	public boolean canDisplayIdentifiers( ) {
		return displayIdentifiers;
	}

	public void setDisplayIdentifiers( boolean displayIdentifiers ) {
		this.displayIdentifiers = displayIdentifiers;
	}

	public boolean canDisplayBars( ) {
		return displayBars;
	}

	public void setDisplayBars( boolean displayBars ) {
		this.displayBars = displayBars;
	}

	@Override
	public void update( ) {
		super.update( );

		if ( Entity.hasEntityContainer( ) ) {
			EntityContainer ec = Entity.getEntityContainer( );

			this.updateActions( this.target, ec.getCurrentEnemies( ), ec );
		}

	}

	@Override
	public void setX( double x ) {
		super.setX( x );
		if ( this.healthBar != null ) {
			this.healthBar.setX( ( ( int ) this.getDX( ) ) - ( this.width / 2 ) );
		}
		if ( this.magicBar != null ) {
			this.magicBar.setX( ( ( int ) this.getDX( ) ) - ( this.width / 2 ) );
		}

		if ( this.staminaBar != null ) {
			this.staminaBar.setX( ( ( int ) this.getDX( ) ) - ( this.width / 2 ) );
		}
	}

	@Override
	public void setY( double y ) {
		super.setY( y );
		if ( this.healthBar != null ) {
			this.healthBar.setY( ( ( int ) this.getDY( ) ) - 70 );
		}
		if ( this.magicBar != null ) {
			this.magicBar.setY( ( ( int ) this.getDY( ) ) - 60 );
		}
		if ( this.staminaBar != null ) {
			this.staminaBar.setY( ( ( int ) this.getDY( ) ) - 50 );
		}
	}

}

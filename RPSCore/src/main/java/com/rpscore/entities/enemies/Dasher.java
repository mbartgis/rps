/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.enemies;

import java.awt.Color;
import java.util.List;

import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.LivingEntity;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.gui.game.StatusBar;

public class Dasher extends Enemy {

	private int	speed;

	private int	damagedSinceTick;

	public Dasher( Double x, Double y ) {
		super( x, y, 24, 24, "Dasher" );

		this.baseHealth = 50;
		this.baseStamina = 100;
		this.baseMagic = 50;

		this.nameOffset += 10;

		this.setDifficulty( Enemy.DIFFICULTY_NORMAL );

		this.healthBar = new StatusBar( ( ( int ) this.x ) - ( this.width / 2 ), ( ( int ) this.y ) - 70,
				this.width - 1, 10, this.getHealthMax( ), this.getHealth( ) );

		this.healthBar = new StatusBar( ( ( int ) this.x ) - ( this.width ), ( ( int ) this.y ) - 50, this.width * 2,
				10, this.getHealthMax( ), this.getHealth( ) );

		this.staminaBar = new StatusBar( ( ( int ) this.x ) - ( this.width ), ( ( int ) this.y ) - 40, this.width * 2,
				10, this.getStaminaMax( ), this.getStamina( ) );

		this.healthBar.setOutlineColor( Color.WHITE );
		this.healthBar.setFillColor( Color.RED.darker( ) );
		this.healthBar.setBackingColor( null );

		this.staminaBar.setOutlineColor( Color.WHITE );
		this.staminaBar.setFillColor( Color.GREEN.darker( ) );
		this.staminaBar.setBackingColor( null );

		this.damagedSinceTick = 0;

		this.worth = 50;

		this.isCentered = true;
	}

	@Override
	public void draw( GraphicsAdapter g ) {
		super.draw( g );

		g.setColor( Color.RED );
		g.fillOval( ( ( int ) this.getDX( ) ) - ( this.width / 2 ), ( ( ( int ) this.getDY( ) ) - this.height / 2 ),
				this.width, this.height );
		g.setColor( Color.YELLOW );
		g.fillOval( ( ( int ) this.getDX( ) ) - ( this.width / 4 ), ( ( ( int ) this.getDY( ) ) - this.height / 4 ),
				this.width / 2, this.height / 2 );
	}

	@Override
	public void updateStamina( ) {
		this.staminaBar.setStatus( this.getStamina( ) );

	}

	@Override
	public void updateMagic( ) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateHealth( ) {
		this.healthBar.setStatus( this.getHealth( ) );
	}

	@Override
	protected void performEasyActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		if ( this.damagedSinceTick <= 0 ) {

			int speed = 1;

			int xdiff = ( int ) Math.abs( player.getX( ) - this.x );
			int ydiff = ( int ) Math.abs( player.getY( ) - this.y );

			if ( ( xdiff < 500 ) && ( ydiff < 500 ) ) {
				if ( stamina > 0 ) {
					speed = 4;
					this.stamina--;
				}
			} else {
				if ( this.stamina <= this.staminaMax ) {
					this.stamina++;
				}
			}

			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + speed );
			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - speed );
			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + speed );
			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - speed );
			}

			if ( player.isInBounds( this ) ) {
				player.damage( 15, false );
				this.damagedSinceTick = 60;
			}
		} else {
			this.damagedSinceTick--;
		}

	}

	@Override
	protected void performNormalActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		if ( this.damagedSinceTick <= 0 ) {

			int speed = 1;

			int xdiff = ( int ) Math.abs( player.getX( ) - this.x );
			int ydiff = ( int ) Math.abs( player.getY( ) - this.y );

			if ( ( xdiff < 500 ) && ( ydiff < 500 ) ) {
				if ( stamina > 0 ) {
					speed = 4;
					this.stamina--;
				}
			} else {
				if ( this.stamina <= this.staminaMax ) {
					this.stamina++;
				}
			}

			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + speed );
			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - speed );
			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + speed );
			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - speed );
			}

			if ( player.isInBounds( this ) ) {
				player.damage( 18, false );
				this.damagedSinceTick = 60;
			}
		} else {
			this.damagedSinceTick--;
		}

	}

	@Override
	protected void performHardActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		if ( this.damagedSinceTick <= 0 ) {

			int speed = 1;

			int xdiff = ( int ) Math.abs( player.getX( ) - this.x );
			int ydiff = ( int ) Math.abs( player.getY( ) - this.y );

			if ( ( xdiff < 500 ) && ( ydiff < 500 ) ) {
				if ( stamina > 0 ) {
					speed = 4;
					this.stamina--;
				}
			} else {
				if ( this.stamina <= this.staminaMax ) {
					this.stamina++;
				}
			}

			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + speed );
			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - speed );
			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + speed );
			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - speed );
			}

			if ( player.isInBounds( this ) ) {
				player.damage( 24, false );
				this.damagedSinceTick = 60;
			}
		} else {
			this.damagedSinceTick--;
		}

	}

	@Override
	protected void performInsaneActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		if ( this.damagedSinceTick <= 0 ) {

			int speed = 1;

			int xdiff = ( int ) Math.abs( player.getX( ) - this.x );
			int ydiff = ( int ) Math.abs( player.getY( ) - this.y );

			if ( ( xdiff < 500 ) && ( ydiff < 500 ) ) {
				if ( stamina > 0 ) {
					speed = 4;
					this.stamina--;
				}
			} else {
				if ( this.stamina <= this.staminaMax ) {
					this.stamina++;
				}
			}

			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + speed );
			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - speed );
			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + speed );
			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - speed );
			}

			if ( player.isInBounds( this ) ) {
				player.damage( 30, false );
				this.damagedSinceTick = 60;
			}
		} else {
			this.damagedSinceTick--;
		}

	}

	@Override
	protected void performImpossibleActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		if ( this.damagedSinceTick <= 0 ) {

			int speed = 1;

			int xdiff = ( int ) Math.abs( player.getX( ) - this.x );
			int ydiff = ( int ) Math.abs( player.getY( ) - this.y );

			if ( ( xdiff < 500 ) && ( ydiff < 500 ) ) {
				if ( stamina > 0 ) {
					speed = 4;
					this.stamina--;
				}
			} else {
				if ( this.stamina <= this.staminaMax ) {
					this.stamina++;
				}
			}

			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + speed );
			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - speed );
			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + speed );
			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - speed );
			}

			if ( player.isInBounds( this ) ) {
				player.damage( 36, false );
				this.damagedSinceTick = 60;
			}
		} else {
			this.damagedSinceTick--;
		}

	}

	@Override
	protected void performActionsAfterDeath( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// TODO Auto-generated method stub

	}

}

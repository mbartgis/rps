/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.enemies;

public interface Hollowable {

	public abstract boolean isHollow( );

	public abstract void setHollow( boolean hollow );

}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.enemies;

import java.awt.Color;
import java.util.List;

import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.EntityRegistry;
import com.rpscore.entities.LivingEntity;
import com.rpscore.entities.spawned.weapons.Mortar;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.gui.game.StatusBar;
import com.rpscore.lib.MathUtils;

public class MadBomber extends Boss {

	protected boolean	stride;
	protected int		strideDelay;

	public MadBomber( Double x, Double y ) {
		super( x, y, 208, 256, "Mad Bomber" );

		this.isCentered = true;

		this.stride = false;
		this.strideDelay = 0;

		this.baseHealth = 1000;
		this.baseStamina = 100;
		this.baseMagic = 2500;

		this.setDifficulty( Enemy.DIFFICULTY_NORMAL );

		this.healthBar = new StatusBar( ( ( int ) this.x ) - ( this.width / 2 ), ( ( int ) this.y - 150 ),
				this.width - 1, 10, this.getHealthMax( ), this.getHealth( ) );

		this.magicBar = new StatusBar( ( ( int ) this.x ) - ( this.width / 2 ), ( ( int ) this.y ) - 140,
				this.width - 1, 10, this.getMagicMax( ), this.getMagic( ) );

		this.staminaBar = new StatusBar( ( ( int ) this.x ) - ( this.width / 2 ), ( ( int ) this.y ) - 130,
				this.width - 1, 10, this.getStaminaMax( ), this.getStamina( ) );

		this.nameOffset += 100;

		this.healthBar.setOutlineColor( Color.WHITE );
		this.healthBar.setFillColor( Color.RED.darker( ) );
		this.healthBar.setBackingColor( null );

		this.magicBar.setOutlineColor( Color.WHITE );
		this.magicBar.setFillColor( Color.BLUE.darker( ) );
		this.magicBar.setBackingColor( null );

		this.staminaBar.setOutlineColor( Color.WHITE );
		this.staminaBar.setFillColor( Color.GREEN.darker( ) );
		this.staminaBar.setBackingColor( null );

		this.worth = 100;

		setX( x );
		setY( y );

	}

	@Override
	public void update( ) {

		if ( this.strideDelay > 15 ) {
			this.stride = !this.stride;
			this.strideDelay = 0;
		}

		this.strideDelay++;

		super.update( );

	}

	@Override
	public void draw( GraphicsAdapter g ) {
		super.draw( g );

		int i = stride ? 0 : 1;

		g.drawImage( EntityRegistry.MadBomberRes.FRAMES_NORMAL[ i ],
				( ( int ) this.getDX( ) ) - ( this.getWidth( ) / 2 ),
				( ( int ) this.getDY( ) ) - ( this.getHeight( ) / 2 ) );

	}

	@Override
	protected void performEasyActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// statsRef = gi.getPlayer( ).getPlayerManager( );

		if ( player != null ) {

			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + 1 );

			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - 1 );

			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + 1 );

			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - 1 );

			}

			if ( player.isInBounds( this ) ) {
				player.damage( 5, false );
			}

			if ( MathUtils.getDiscreteChance( 0.10 ) ) {
				int xdiff = ( int ) ( player.getX( ) - this.x );
				int ydiff = ( int ) ( player.getY( ) - this.y );

				int xdir = 1;
				if ( xdiff >= 0 ) {
					xdir++;
				}

				xdiff = Math.abs( xdiff );
				ydiff = Math.abs( ydiff );

				if ( ( xdiff < 2000 ) && ( ydiff < 2000 ) && ( ( xdiff > 400 ) || ( ydiff > 400 ) ) ) {

					int xinaccurate = actionGenerator.nextInt( 100 );
					boolean xinaccuracydir = actionGenerator.nextBoolean( );
					int yinaccurate = actionGenerator.nextInt( 100 );
					boolean yinaccuracydir = actionGenerator.nextBoolean( );

					xinaccurate *= ( xinaccuracydir ) ? -1 : 1;
					yinaccurate *= ( yinaccuracydir ) ? -1 : 1;

					Mortar m = new Mortar( player.getX( ) + xinaccurate, player.getY( ) + yinaccurate, 50, this );
					this.createSpawnedEntity( m );

				}
			}
		}

		gi = null;

	}

	@Override
	protected void performNormalActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		// statsRef = gi.getPlayer( ).getPlayerManager( );

		if ( player != null ) {

			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + 1 );

			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - 1 );

			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + 1 );

			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - 1 );

			}

			if ( player.isInBounds( this ) ) {
				player.damage( 5, false );

			}

			if ( MathUtils.getDiscreteChance( 0.125 ) ) {
				int xdiff = ( int ) ( player.getX( ) - this.x );
				int ydiff = ( int ) ( player.getY( ) - this.y );

				int xdir = 1;
				if ( xdiff >= 0 ) {
					xdir++;
				}

				xdiff = Math.abs( xdiff );
				ydiff = Math.abs( ydiff );

				if ( ( xdiff < 2000 ) && ( ydiff < 2000 ) && ( ( xdiff > 400 ) || ( ydiff > 400 ) ) ) {

					int xinaccurate = actionGenerator.nextInt( 200 );
					boolean xinaccuracydir = actionGenerator.nextBoolean( );
					int yinaccurate = actionGenerator.nextInt( 200 );
					boolean yinaccuracydir = actionGenerator.nextBoolean( );

					xinaccurate *= ( xinaccuracydir ) ? -1 : 1;
					yinaccurate *= ( yinaccuracydir ) ? -1 : 1;

					Mortar m = new Mortar( player.getX( ) + xinaccurate, player.getY( ) + yinaccurate, 50, this );
					this.createSpawnedEntity( m );
					this.magic -= 20;

				}
			}
		}

		gi = null;

	}

	@Override
	protected void performHardActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		// statsRef = gi.getPlayer( ).getPlayerManager( );

		if ( player != null ) {

			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + 1 );

			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - 1 );

			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + 1 );

			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - 1 );

			}

			if ( player.isInBounds( this ) ) {
				player.damage( 5, false );

			}

			if ( MathUtils.getDiscreteChance( 0.25 ) ) {
				int xdiff = ( int ) ( player.getX( ) - this.x );
				int ydiff = ( int ) ( player.getY( ) - this.y );

				int xdir = 1;
				if ( xdiff >= 0 ) {
					xdir++;
				}

				xdiff = Math.abs( xdiff );
				ydiff = Math.abs( ydiff );

				if ( ( xdiff < 2000 ) && ( ydiff < 2000 ) && ( ( xdiff > 400 ) || ( ydiff > 400 ) ) ) {

					int xinaccurate = actionGenerator.nextInt( 300 );
					boolean xinaccuracydir = actionGenerator.nextBoolean( );
					int yinaccurate = actionGenerator.nextInt( 300 );
					boolean yinaccuracydir = actionGenerator.nextBoolean( );

					xinaccurate *= ( xinaccuracydir ) ? -1 : 1;
					yinaccurate *= ( yinaccuracydir ) ? -1 : 1;

					Mortar m = new Mortar( player.getX( ) + xinaccurate, player.getY( ) + yinaccurate, 50, this );
					this.createSpawnedEntity( m );
					this.magic -= 20;

				}
			}
		}

		gi = null;

	}

	@Override
	protected void performInsaneActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// statsRef = gi.getPlayer( ).getPlayerManager( );

		if ( player != null ) {

			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + 1 );

			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - 1 );

			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + 1 );

			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - 1 );

			}

			if ( player.isInBounds( this ) ) {
				player.damage( 5, false );

			}

			if ( MathUtils.getDiscreteChance( 0.5 ) ) {
				int xdiff = ( int ) ( player.getX( ) - this.x );
				int ydiff = ( int ) ( player.getY( ) - this.y );

				int xdir = 1;
				if ( xdiff >= 0 ) {
					xdir++;
				}

				xdiff = Math.abs( xdiff );
				ydiff = Math.abs( ydiff );

				if ( ( xdiff < 2000 ) && ( ydiff < 2000 ) && ( ( xdiff > 400 ) || ( ydiff > 400 ) ) ) {

					int xinaccurate = actionGenerator.nextInt( 400 );
					boolean xinaccuracydir = actionGenerator.nextBoolean( );
					int yinaccurate = actionGenerator.nextInt( 400 );
					boolean yinaccuracydir = actionGenerator.nextBoolean( );

					xinaccurate *= ( xinaccuracydir ) ? -1 : 1;
					yinaccurate *= ( yinaccuracydir ) ? -1 : 1;

					Mortar m = new Mortar( player.getX( ) + xinaccurate, player.getY( ) + yinaccurate, 50, this );
					this.createSpawnedEntity( m );
					this.magic -= 20;

				}
			}
		}

		gi = null;

	}

	@Override
	protected void performImpossibleActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// statsRef = gi.getPlayer( ).getPlayerManager( );

		if ( player != null ) {

			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + 1 );

			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - 1 );

			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + 1 );

			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - 1 );

			}

			if ( player.isInBounds( this ) ) {
				player.damage( 5, false );

			}

			int xdiff = ( int ) ( player.getX( ) - this.x );
			int ydiff = ( int ) ( player.getY( ) - this.y );

			int xdir = 1;
			if ( xdiff >= 0 ) {
				xdir++;
			}

			xdiff = Math.abs( xdiff );
			ydiff = Math.abs( ydiff );

			if ( ( xdiff < 2000 ) && ( ydiff < 2000 ) && ( ( xdiff > 400 ) || ( ydiff > 400 ) ) ) {

				int xinaccurate = actionGenerator.nextInt( 500 );
				boolean xinaccuracydir = actionGenerator.nextBoolean( );
				int yinaccurate = actionGenerator.nextInt( 500 );
				boolean yinaccuracydir = actionGenerator.nextBoolean( );

				xinaccurate *= ( xinaccuracydir ) ? -1 : 1;
				yinaccurate *= ( yinaccuracydir ) ? -1 : 1;

				Mortar m = new Mortar( player.getX( ) + xinaccurate, player.getY( ) + yinaccurate, 50, this );
				this.createSpawnedEntity( m );
				this.magic -= 20;

			}

		}

		gi = null;

	}

	@Override
	public void updateStamina( ) {
		this.staminaBar.setStatus( this.getStamina( ) );

	}

	@Override
	public void updateMagic( ) {
		this.magicBar.setStatus( this.getMagic( ) );

	}

	@Override
	public void updateHealth( ) {
		this.healthBar.setStatus( this.getHealth( ) );

	}

	@Override
	protected void performActionsAfterDeath( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// TODO Auto-generated method stub

	}

}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.enemies;

import java.util.List;

import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.LivingEntity;

public class Sharpshooter extends Enemy {

	public Sharpshooter( Double x, Double y, int w, int h, String name ) {
		super( x, y, w, h, name );
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void performEasyActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void performNormalActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void performHardActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void performInsaneActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void performImpossibleActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateStamina( ) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateMagic( ) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateHealth( ) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void performActionsAfterDeath( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// TODO Auto-generated method stub

	}

}

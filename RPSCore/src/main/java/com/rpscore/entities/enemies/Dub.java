/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.enemies;

import java.awt.Color;
import java.util.List;

import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.EntityRegistry;
import com.rpscore.entities.LivingEntity;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.spawned.weapons.Bomb;
import com.rpscore.entities.spawned.weapons.Bullet;
import com.rpscore.game.PlayerManager;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.gui.game.StatusBar;

public class Dub extends Enemy {

	private int				damagedSinceTick;

	protected int			direction;

	protected boolean		stride;

	protected int			strideCD;

	protected int			dodgeDX, dodgeDY;

	protected Bomb			activeBombThreat;

	protected int			dodgeCD;

	protected int			dodgeActive;

	private PlayerManager	statsRef;

	public Dub( Double x, Double y ) {
		super( x, y, 64, 64, "Dub" );

		this.baseHealth = 35;
		this.baseStamina = 60;
		this.baseMagic = 0;

		this.setDifficulty( Enemy.DIFFICULTY_NORMAL );

		this.healthBar = new StatusBar( ( ( int ) this.x ) - ( this.width / 2 ), ( ( int ) this.y ) - 40,
				this.width - 1, 10, this.getHealthMax( ), this.getHealth( ) );

		this.healthBar.setOutlineColor( Color.WHITE );
		this.healthBar.setFillColor( Color.RED.darker( ) );
		this.healthBar.setBackingColor( null );

		this.damagedSinceTick = 0;
		this.worth = 10;

		this.nameOffset += 5;

		this.direction = 0;

		this.stride = false;

		this.strideCD = 30;

		this.activeBombThreat = null;
		this.dodgeDX = 0;
		this.dodgeDY = 0;
		this.dodgeCD = 0;
		this.dodgeActive = 0;

	}

	/*
	 * @Override public void updateActions( Player player, ArrayList< Enemy >
	 * otherEnemies, Object... otherAruments ) {
	 *
	 * if ( this.damagedSinceTick <= 0 ) {
	 *
	 * if ( player.getX( ) > this.x ) { this.setX( this.getX( ) + 1 );
	 * this.direction = 3; } else if ( player.getX( ) < this.x ) { this.setX(
	 * this.getX( ) - 1 ); this.direction = 2; }
	 *
	 * if ( player.getY( ) > this.y ) { this.setY( this.getY( ) + 1 );
	 * this.direction = 1; } else if ( player.getY( ) < this.y ) { this.setY(
	 * this.getY( ) - 1 ); this.direction = 0; }
	 *
	 * if ( player.isInBounds( this ) ) { player.setHealth( player.getHealth( )
	 * - 5 ); this.damagedSinceTick = 60; } } else { this.damagedSinceTick--; }
	 *
	 * this.strideCD--; if ( strideCD <= 0 ) { this.stride = !this.stride;
	 * strideCD += 30; }
	 *
	 * }
	 */

	@Override
	public void updateStamina( ) {}

	@Override
	public void updateMagic( ) {}

	@Override
	public void updateHealth( ) {
		this.healthBar.setStatus( this.getHealth( ) );
	}

	@Override
	public void draw( GraphicsAdapter g ) {
		super.draw( g );

		if ( this.stride ) {
			if ( this.health <= this.healthMax / 2 ) {
				g.drawImage( EntityRegistry.DubRes.FRAMES_DAMAGED[ direction ],
						( ( int ) this.getDX( ) ) - ( this.width / 2 ),
						( ( int ) this.getDY( ) ) - ( this.height / 2 ) );
			} else {
				g.drawImage( EntityRegistry.DubRes.FRAMES_NORMAL[ direction ],
						( ( int ) this.getDX( ) ) - ( this.width / 2 ),
						( ( int ) this.getDY( ) ) - ( this.height / 2 ) );
			}
		} else {
			if ( this.health <= this.healthMax / 2 ) {
				g.drawImage( EntityRegistry.DubRes.FRAMES_DAMAGED[ direction + 4 ],
						( ( int ) this.getDX( ) ) - ( this.width / 2 ),
						( ( int ) this.getDY( ) ) - ( this.height / 2 ) );
			} else {
				g.drawImage( EntityRegistry.DubRes.FRAMES_NORMAL[ direction + 4 ],
						( ( int ) this.getDX( ) ) - ( this.width / 2 ),
						( ( int ) this.getDY( ) ) - ( this.height / 2 ) );
			}
		}
		// g.setColor( Color.RED );
		// g.fillRect( this.x - ( this.width / 2 ), ( this.y - this.height / 2
		// ), this.width, this.height );

	}

	/**
	 * Dubs on Easy will walk toward the player with a speed of 1 plus a speed
	 * of 1 on the bisecting axis if applicable. Upon contacting the player they
	 * will inflict 5 damage to health and then enter a cooldown period. During
	 * a cooldown period they will stand still and be unable to inflict damage.
	 * The default cooldown on easy is 60 ticks (1 second).
	 */
	@Override
	protected void performEasyActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		statsRef = gi.getPlayer( ).getPlayerManager( );

		if ( player != null ) {
			if ( this.damagedSinceTick <= 0 ) {

				if ( player.getX( ) > this.x ) {
					this.setX( this.getX( ) + 1 );
					this.direction = 3;
				} else if ( player.getX( ) < this.x ) {
					this.setX( this.getX( ) - 1 );
					this.direction = 2;
				}

				if ( player.getY( ) > this.y ) {
					this.setY( this.getY( ) + 1 );
					this.direction = 1;
				} else if ( player.getY( ) < this.y ) {
					this.setY( this.getY( ) - 1 );
					this.direction = 0;
				}

				if ( player.isInBounds( this ) ) {
					player.damage( 12, false );
					if ( statsRef != null && player.getHealth( ) <= 0 ) {
						statsRef.incrementDeathByEnemy( this );
					}
					this.damagedSinceTick = 60;
				}
			} else {
				this.damagedSinceTick--;
			}
		}

		this.strideCD--;
		if ( strideCD <= 0 ) {
			this.stride = !this.stride;
			strideCD += 30;
		}

		gi = null;

	}

	/**
	 * Dubs on Normal will walk toward the player with a speed of 1 plus a speed
	 * of 1 on the bisecting axis if applicable. Upon contacting the player they
	 * will inflict 8 damage to health and then enter a cooldown period. During
	 * a cooldown period they will be unable to inflict damage to the player but
	 * will still be able to move. The default cooldown on normal is 60 ticks (1
	 * second).
	 */
	@Override
	protected void performNormalActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		Player p = gi.getPlayer( );
		if ( p != null ) {
			statsRef = p.getPlayerManager( );
		}

		if ( player != null ) {

			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + 1 );
				this.direction = 3;
			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - 1 );
				this.direction = 2;
			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + 1 );
				this.direction = 1;
			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - 1 );
				this.direction = 0;
			}

			if ( this.damagedSinceTick <= 0 ) {

				if ( player.isInBounds( this ) ) {
					player.damage( 15, false );
					if ( statsRef != null && player.getHealth( ) <= 0 ) {
						statsRef.incrementDeathByEnemy( this );
					}
					this.damagedSinceTick = 60;
				}
			} else {
				this.damagedSinceTick--;
			}
		}

		this.strideCD--;
		if ( strideCD <= 0 ) {
			this.stride = !this.stride;
			strideCD += 30;
		}

		gi = null;

	}

	/**
	 * Dubs on Hard will walk toward the player with a speed of 1 plus a speed
	 * of 1 on the bisecting axis if applicable. Upon contacting the player they
	 * will inflict 10 damage to health and then enter a cooldown period. During
	 * a cooldown period they will be unable to inflict damage to the player but
	 * will still be able to move. The default cooldown on hard is 30 ticks (0.5
	 * seconds).
	 */
	@Override
	protected void performHardActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		Player p = gi.getPlayer( );
		if ( p != null ) {
			statsRef = p.getPlayerManager( );
		}

		if ( player != null ) {

			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + 1 );
				this.direction = 3;
			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - 1 );
				this.direction = 2;
			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + 1 );
				this.direction = 1;
			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - 1 );
				this.direction = 0;
			}

			if ( this.damagedSinceTick <= 0 ) {

				if ( player.isInBounds( this ) ) {
					player.damage( 18, false );
					if ( statsRef != null && player.getHealth( ) <= 0 ) {
						statsRef.incrementDeathByEnemy( this );
					}
					this.damagedSinceTick = 30;
				}
			} else {
				this.damagedSinceTick--;
			}
		}

		this.strideCD--;
		if ( strideCD <= 0 ) {
			this.stride = !this.stride;
			strideCD += 30;
		}

		gi = null;

	}

	/**
	 * Dubs on Insane will walk toward the player with a speed of 2 plus a speed
	 * of 2 on the bisecting axis if applicable. Upon contacting the player they
	 * will inflict 12 damage to health and then enter a cooldown period. During
	 * a cooldown period they will be unable to inflict damage to the player but
	 * will still be able to move. The default cooldown on insane is 15 ticks
	 * (0.25 seconds). In addition, Dubs will actively attempt to avoid bombs
	 * placed by the player.
	 */
	@Override
	protected void performInsaneActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		if ( player != null ) {
			Player p = gi.getPlayer( );
			if ( p != null ) {
				statsRef = p.getPlayerManager( );
			}
			List< Bomb > bombsOnField = gi.getCurrentBombs( );

			for ( int i = 0; i < bombsOnField.size( ); i++ ) {
				if ( bombsOnField.get( i ).isPlayerOrigin( )
						&& Math.abs( bombsOnField.get( i ).getX( ) - this.x ) <= 100
						&& Math.abs( bombsOnField.get( i ).getY( ) - this.y ) <= 100 ) {

					this.activeBombThreat = bombsOnField.get( i );

				}
			}

			if ( this.activeBombThreat != null ) {
				if ( this.activeBombThreat.getX( ) > this.x ) {
					this.setX( this.x - 1 );
				} else {
					this.setX( this.x + 1 );
				}
				if ( this.activeBombThreat.getY( ) > this.y ) {
					this.setY( this.y - 1 );
				} else {
					this.setY( this.y - 1 );
				}
			} else {
				if ( player.getX( ) > this.x ) {
					this.setX( this.getX( ) + 2 );
					this.direction = 3;
				} else if ( player.getX( ) < this.x ) {
					this.setX( this.getX( ) - 2 );
					this.direction = 2;
				}

				if ( player.getY( ) > this.y ) {
					this.setY( this.getY( ) + 2 );
					this.direction = 1;
				} else if ( player.getY( ) < this.y ) {
					this.setY( this.getY( ) - 2 );
					this.direction = 0;
				}
			}

		} else {

			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + 2 );
				this.direction = 3;
			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - 2 );
				this.direction = 2;
			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + 2 );
				this.direction = 1;
			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - 2 );
				this.direction = 0;
			}
		}

		if ( this.damagedSinceTick <= 0 ) {

			if ( player.isInBounds( this ) ) {
				player.damage( 21, false );
				if ( statsRef != null && player.getHealth( ) <= 0 ) {
					statsRef.incrementDeathByEnemy( this );
				}
				this.damagedSinceTick = 15;
			}
		} else {
			this.damagedSinceTick--;
		}

		this.strideCD--;
		if ( strideCD <= 0 ) {
			this.stride = !this.stride;
			strideCD += 30;
		}

		if ( this.activeBombThreat != null && this.activeBombThreat.isFinished( ) ) {
			this.activeBombThreat = null;
		}

		gi = null;

	}

	/**
	 * Dubs on Insane will walk toward the player with a speed of 2 plus a speed
	 * of 2 on the bisecting axis if applicable. Upon contacting the player they
	 * will inflict 12 damage to health and then enter a cooldown period. During
	 * a cooldown period they will be unable to inflict damage to the player but
	 * will still be able to move. The default cooldown on insane is 15 ticks
	 * (0.25 seconds). In addition, Dubs will actively attempt to avoid bombs
	 * placed by the player. Finally, when a player targets a bub with their
	 * cursor, the dub will actively attempt to get out of the "crosshairs".
	 */
	@Override
	protected void performImpossibleActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		if ( player != null ) {
			Player p = gi.getPlayer( );
			if ( p != null ) {
				statsRef = p.getPlayerManager( );
			}

			List< Bomb > bombsOnField = gi.getCurrentBombs( );

			for ( int i = 0; i < bombsOnField.size( ); i++ ) {
				if ( bombsOnField.get( i ).isPlayerOrigin( )
						&& Math.abs( bombsOnField.get( i ).getX( ) - this.x ) <= 100
						&& Math.abs( bombsOnField.get( i ).getY( ) - this.y ) <= 100 ) {

					this.activeBombThreat = bombsOnField.get( i );

				}
			}

			if ( this.dodgeCD <= 0 && this.dodgeActive <= 0 ) {

				List< Bullet > bulletsOnField = gi.getCurrentBullets( );

				for ( int i = 0; i < bulletsOnField.size( ); i++ ) {
					if ( bulletsOnField.get( i ).isPlayerOrigin( )
							&& Math.abs( bulletsOnField.get( i ).getX( ) - this.x ) <= 250
							&& Math.abs( bulletsOnField.get( i ).getY( ) - this.y ) <= 250 ) {

						int xdist = this.actionGenerator.nextInt( 5 );
						boolean reverseXD = this.actionGenerator.nextBoolean( );
						this.dodgeDX = ( reverseXD ? -1 * xdist : xdist );

						int ydist = this.actionGenerator.nextInt( 5 );
						boolean reverseYD = this.actionGenerator.nextBoolean( );
						this.dodgeDY = ( reverseYD ? -1 * ydist : ydist );

						this.dodgeActive = 15;
						break;
					}
				}
			} else if ( this.activeBombThreat != null && this.dodgeActive <= 0 ) {
				if ( this.activeBombThreat.getX( ) > this.x ) {
					this.setX( this.x - 1 );
				} else {
					this.setX( this.x + 1 );
				}
				if ( this.activeBombThreat.getY( ) > this.y ) {
					this.setY( this.y - 1 );
				} else {
					this.setY( this.y - 1 );
				}
			} else {
				if ( player.getX( ) > this.x ) {
					this.setX( this.getX( ) + 2 );
					this.direction = 3;
				} else if ( player.getX( ) < this.x ) {
					this.setX( this.getX( ) - 2 );
					this.direction = 2;
				}

				if ( player.getY( ) > this.y ) {
					this.setY( this.getY( ) + 2 );
					this.direction = 1;
				} else if ( player.getY( ) < this.y ) {
					this.setY( this.getY( ) - 2 );
					this.direction = 0;
				}
			}

		}
		if ( this.dodgeActive <= 0 && this.activeBombThreat == null ) {

			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + 2 );
				this.direction = 3;
			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - 2 );
				this.direction = 2;
			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + 2 );
				this.direction = 1;
			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - 2 );
				this.direction = 0;
			}
		}

		if ( this.damagedSinceTick <= 0 ) {

			if ( player.isInBounds( this ) ) {
				player.damage( 25, false );
				if ( statsRef != null && player.getHealth( ) <= 0 ) {
					statsRef.incrementDeathByEnemy( this );
				}
				this.damagedSinceTick = 15;
			}
		} else {
			this.damagedSinceTick--;
		}

		if ( this.dodgeActive > 0 ) {
			this.setX( this.x + this.dodgeDX );
			this.setY( this.y + this.dodgeDY );
			this.dodgeActive--;
			if ( this.dodgeActive <= 0 ) {
				this.dodgeCD = 30;
			}
		}

		if ( this.dodgeCD > 0 ) {
			this.dodgeCD--;
		}

		this.strideCD--;
		if ( strideCD <= 0 ) {
			this.stride = !this.stride;
			strideCD += 30;
		}

		if ( this.activeBombThreat != null && this.activeBombThreat.isFinished( ) ) {
			this.activeBombThreat = null;
		}

		gi = null;
	}

	@Override
	protected void performActionsAfterDeath( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// TODO Auto-generated method stub

	}

}

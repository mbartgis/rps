/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-07-26 22:16:38 -0400 (Thu, 26 Jul 2018) $
 * $Revision: 363 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.enemies;

import java.awt.Color;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;

import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.controlled.Player;
import com.rpscore.game.GameInstance;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.lib.GUIDGenerator;
import com.rpscore.lib.MathUtils;
import com.rpscore.lib.Updateable;
import com.rpscore.server.MessageManager;

public class SpawnManager implements Updateable {

	private int						unspawnedEnemiesRemaining;
	private int						wave;
	private Random					generator;
	private List< Player >			players;
	private int						waveBreakCD;
	private double					difficulty;
	private int						totalCDTime, totalEnemies;
	private boolean					isFinished;
	protected int					round_bonus;

	protected GUIDGenerator			idGenRef;

	protected EntityContainer		parent;

	public static final int			FIELD_OFFSET					= 2000;
	public static final double		PLAYER_SPAWN_PROTECTION_RADIUS	= 500.0;

	// 1 in 650 chance
	public static final double		DUB_SPAWN_CHANCE				= 0.01;

	// 1 in 650 chance
	public static final double		DASHER_SPAWN_CHANCE				= 0.0022222222222;

	// 1 in 650 chance
	public static final double		BOMBER_SPAWN_CHANCE				= 0.0016666666667;

	// 1 in 1000 chance
	public static final double		ROGUE_CANNON_SPAWN_CHANCE		= 0.001;

	// 1 in 650 chance
	public static final double		GHOST_SPAWN_CHANCE				= 0.0015384615384;

	private LinkedList< Enemy >		enemies;

	private boolean					isSlaveToServer, canSendMessages;

	private LinkedList< byte[ ] >	enemyTransmissions;

	private int						maxAllowableEnemies;

	public SpawnManager( EntityContainer instance, GUIDGenerator gen ) {

		this.idGenRef = gen;
		this.parent = instance;
		this.generator = new Random( );
		this.unspawnedEnemiesRemaining = 0;
		this.enemies = new LinkedList< >( );
		this.totalCDTime = 0;
		this.totalEnemies = 0;
		this.players = new LinkedList< >( );
		this.isFinished = false;
		this.isSlaveToServer = false;
		this.canSendMessages = false;

		this.enemyTransmissions = new LinkedList< >( );
		this.enemyTransmissionLock = new Semaphore( 1 );

		this.maxAllowableEnemies = 0;

		reset( );

	}

	public List< Player > getPlayers( ) {
		return this.players;
	}

	public void setTelegramEnemies( boolean set ) {
		this.canSendMessages = set;
	}

	public boolean canSendEmemyMessages( ) {
		return this.canSendMessages;
	}

	private Semaphore enemyTransmissionLock;

	public void addEnemyTransmissionToSpawn( Enemy e ) {

		if ( !this.canSendMessages ) {
			return;
		}

		byte[ ] enemy = MessageManager.encodeEnemy( e );

		try {
			this.enemyTransmissionLock.acquire( );
		} catch ( InterruptedException ex ) {
			this.enemyTransmissionLock.release( );
			Thread.currentThread( ).interrupt( );
		}

		this.enemyTransmissions.add( enemy );

		this.enemyTransmissionLock.release( );

	}

	public void triggerWaveStart( int wave ) {
		this.wave = wave;
		if ( this.parent != null ) {
			this.waveBreakCD = 0;
			this.parent.onWaveStart( );
		}
	}

	public void triggerWaveEnd( ) {
		if ( this.parent != null ) {
			this.waveBreakCD = 800 - ( ( ( int ) ( difficulty / 2 ) ) * 120 );
			this.parent.onWaveEnd( );
		}
	}

	public byte[ ][ ] getEnemyTransmissions( ) {

		try {
			this.enemyTransmissionLock.acquire( );
		} catch ( InterruptedException ex ) {
			this.enemyTransmissionLock.release( );
			Thread.currentThread( ).interrupt( );
		}

		byte[ ][ ] transmissions = new byte[ this.enemyTransmissions.size( ) ][ ];

		int i = 0;

		while ( !this.enemyTransmissions.isEmpty( ) ) {
			transmissions[ i ] = this.enemyTransmissions.removeFirst( );
			i++;
		}

		this.enemyTransmissionLock.release( );

		return transmissions;

	}

	public void onDestroy( ) {
		this.isFinished = true;
		if ( this.players != null ) {
			Iterator< Player > pi = this.players.iterator( );

			while ( pi.hasNext( ) ) {
				pi.next( );
				pi.remove( );
			}

			this.players = null;
		}

		this.parent = null;

	}

	public Player getRandomPlayer( ) {

		if ( this.players.isEmpty( ) ) {
			return null;
		}

		return this.players.get( MathUtils.getRandom( 0, this.players.size( ) ) );
	}

	public boolean isSlaveToServer( ) {
		return this.isSlaveToServer;
	}

	public void setSlaveToServer( boolean slave ) {
		this.isSlaveToServer = slave;
	}

	public Player getPlayerClosestTo( int x, int y ) {

		// If there is only 1 person.
		if ( this.players.size( ) == 1 ) {
			return this.players.get( 0 );
		}

		double closestPlayerDistance = Double.MAX_VALUE;

		Player closestPlayer = null;

		Iterator< Player > pi = this.players.iterator( );

		while ( pi.hasNext( ) ) {
			Player p = pi.next( );
			double pdist = MathUtils.getDistance( ( ( int ) p.getX( ) ), ( ( int ) p.getY( ) ), x, y );
			if ( pdist < closestPlayerDistance ) {
				closestPlayerDistance = pdist;
				closestPlayer = p;
			}
		}

		return closestPlayer;

	}

	public int getEnemiesRemaining( ) {
		return unspawnedEnemiesRemaining + this.enemies.size( );
	}

	public int getEnemiesLeftToSpawn( ) {
		return this.unspawnedEnemiesRemaining;
	}

	public int getWave( ) {
		return wave;
	}

	public void setWave( int wave ) {
		this.wave = wave;
		this.unspawnedEnemiesRemaining = ( int ) ( difficulty * 15 ) + ( int ) ( wave * difficulty );
		this.totalEnemies = this.unspawnedEnemiesRemaining;
	}

	public int getTotalCDTime( ) {
		return this.totalCDTime;
	}

	public int getTotalEnemies( ) {
		return this.totalEnemies;
	}

	public Random getGenerator( ) {
		return generator;
	}

	public void setGenerator( Random generator ) {
		this.generator = generator;
	}

	public void addPlayer( Player p ) throws IllegalArgumentException {
		if ( this.players != null ) {
			Iterator< Player > ps = this.players.iterator( );

			while ( ps.hasNext( ) ) {
				if ( ps.next( ).equals( p ) ) {
					throw new IllegalArgumentException( "Player already in list." );
				}
			}

			this.players.add( p );

		}
	}

	public void removePlayer( Player p ) throws IllegalArgumentException {
		if ( this.players != null ) {
			Iterator< Player > ps = this.players.iterator( );

			while ( ps.hasNext( ) ) {
				if ( ps.next( ).equals( p ) ) {
					ps.remove( );
					return;
				}
			}

			throw new IllegalArgumentException( "Player not found." );

		}

	}

	public int getWaveBreakCooldown( ) {
		return waveBreakCD;
	}

	public void setWaveBreakCooldown( int waveBreakCD ) {
		this.waveBreakCD = waveBreakCD;
	}

	public boolean isWaveBreak( ) {
		return this.waveBreakCD > 0;
	}

	public double getDifficulty( ) {
		return difficulty;
	}

	public void setDifficulty( double difficulty ) {
		this.difficulty = difficulty;
	}

	public List< Enemy > getEnemies( ) {
		return enemies;
	}

	public void setEnemies( LinkedList< Enemy > enemies ) {
		this.enemies = enemies;
	}

	public void reset( ) {
		synchronized ( this.enemies ) {
			while ( !this.enemies.isEmpty( ) ) {
				Enemy e = this.enemies.removeFirst( );
				// FIXME This duct tape is ugly
				if ( this.parent != null && this.parent instanceof GameInstance ) {
					( ( GameInstance ) this.parent ).getMiniMap( ).removeEntity( e );
				}
			}
		}
		this.round_bonus = 0;
		this.wave = 0;
		this.waveBreakCD = 600;
		this.unspawnedEnemiesRemaining = 0;

		this.totalCDTime = this.waveBreakCD;
		this.totalEnemies = this.unspawnedEnemiesRemaining;
	}

	public void renderEnemies( GraphicsAdapter g, boolean displayBoundingBoxes ) {
		// Render all enemies
		if ( this.enemies != null ) {
			g.setColor( Color.red.darker( ) );
			for ( Enemy e : enemies ) {
				/* @formatter:off
				Iterator< Player > ps = this.players.iterator( );


				while ( ps.hasNext( ) ) {
					Player py = ps.next( );
					g.drawLine( py.getDX( ), py.getDY( ), e.getDX( ), e.getDY( ) );
				}
				@formatter:on
				*/

				e.draw( g );
			}
			if ( displayBoundingBoxes ) {
				for ( int i = 0; i < this.enemies.size( ); i++ ) {
					this.enemies.get( i ).drawBoundingBoxes( g );
				}
			}
		}
	}

	private void updateEnemies( ) {
		Iterator< Enemy > enItr = this.enemies.iterator( );

		while ( enItr.hasNext( ) ) {
			Enemy e = enItr.next( );
			e.setXOffset( GameInstance.PXO );
			e.setYOffset( GameInstance.PYO );
			e.update( );
			if ( e.isFinished( ) ) {
				enItr.remove( );
				// FIXME This duct tape is ugly
				if ( this.parent != null && this.parent instanceof GameInstance ) {
					( ( GameInstance ) this.parent ).getMiniMap( ).removeEntity( e );
				}
			}
		}
	}

	@Override
	public void update( ) {

		if ( !this.isSlaveToServer ) {

			if ( unspawnedEnemiesRemaining > 0 ) {

				maxAllowableEnemies = ( ( int ) difficulty ) + ( ( wave > 5 ) ? 5 : wave );
				if ( maxAllowableEnemies > unspawnedEnemiesRemaining ) {
					maxAllowableEnemies = unspawnedEnemiesRemaining;
				}

				try {

					Player player = getRandomPlayer( );

					if ( player != null ) {

						// Trigger Boss Wave
						if ( wave % 5 == 0 && wave != 0 ) {
							spawnBoss( player );
						} else {
							spawnNormalWave( player );
						}

					}
				} catch ( IllegalArgumentException e ) {
					// Odd issue that crops up on start, ensure that it
					// doesn't crash the game.
				}

			} else {
				// Wave break
				if ( enemies.isEmpty( ) ) {
					// Triger the on wave end event.
					if ( this.round_bonus > 0 ) {
						// Award round bonus
						Iterator< Player > pi = this.players.iterator( );

						while ( pi.hasNext( ) ) {
							Player p = pi.next( );
							p.increaseScore( this.round_bonus );
						}

						if ( this.parent != null ) {
							this.parent.onWaveEnd( );
						}

						this.round_bonus = 0;
					}

					if ( waveBreakCD <= 0 ) {
						waveBreakCD = 800 - ( ( ( int ) ( difficulty / 2 ) ) * 120 );
						this.totalCDTime = this.waveBreakCD;
					} else {
						waveBreakCD--;
						if ( waveBreakCD <= 0 ) {
							wave++;
							if ( ( ( wave % 5 ) == 0 ) ) {
								unspawnedEnemiesRemaining = 1;
							} else {
								unspawnedEnemiesRemaining = ( int ) ( difficulty * 15 ) + ( int ) ( wave * difficulty );
							}
							this.totalEnemies = this.unspawnedEnemiesRemaining;
							if ( this.parent != null ) {
								this.parent.onWaveStart( );
							}
						}
					}
				}
			}
		}

		if ( this.isSlaveToServer && this.waveBreakCD > 0 ) {
			this.waveBreakCD--;
		}

		checkTargets( );
		updateEnemies( );

	}

	private void spawnBoss( Player player ) {
		// Hack for fixing -sw issue.
		if ( unspawnedEnemiesRemaining > 1 ) {
			unspawnedEnemiesRemaining = 1;
		}

		int nX = ( ( int ) player.getX( ) );
		int nY = ( ( int ) player.getY( ) );

		while ( MathUtils.getDistance( nX, nY, ( ( int ) player.getX( ) ),
				( ( int ) player.getY( ) ) ) < PLAYER_SPAWN_PROTECTION_RADIUS ) {
			nX = MathUtils.getRandom( 0, ( int ) Math.abs( player.getX( ) ) + FIELD_OFFSET );
			nY = MathUtils.getRandom( 0, ( int ) Math.abs( player.getY( ) ) + FIELD_OFFSET );
		}

		MadBomber d = new MadBomber( ( double ) nX, ( double ) nY );
		d.setID( this.idGenRef.generate( ) );
		// FIXME This duct tape is ugly
		if ( this.parent != null && this.parent instanceof GameInstance ) {
			( ( GameInstance ) this.parent ).getMiniMap( ).addEntity( d );
		}
		d.setTarget( player );
		d.setDifficulty( this.difficulty );

		enemies.add( d );
		this.addEnemyTransmissionToSpawn( d );

		round_bonus += d.getWorth( );

		unspawnedEnemiesRemaining--;
		maxAllowableEnemies--;
	}

	private void spawnNormalWave( Player player ) {
		if ( MathUtils.getDiscreteChance( DUB_SPAWN_CHANCE ) ) {

			int nX = ( ( int ) player.getX( ) );
			int nY = ( ( int ) player.getY( ) );

			while ( MathUtils.getDistance( nX, nY, ( ( int ) player.getX( ) ),
					( ( int ) player.getY( ) ) ) < PLAYER_SPAWN_PROTECTION_RADIUS ) {
				nX = MathUtils.getRandom( 0, ( int ) Math.abs( player.getX( ) ) + FIELD_OFFSET );
				nY = MathUtils.getRandom( 0, ( int ) Math.abs( player.getY( ) ) + FIELD_OFFSET );
			}

			Dub d = new Dub( ( double ) nX, ( double ) nY );
			d.setID( this.idGenRef.generate( ) );
			// FIXME This duct tape is ugly
			if ( this.parent != null && this.parent instanceof GameInstance ) {
				( ( GameInstance ) this.parent ).getMiniMap( ).addEntity( d );
			}
			d.setDifficulty( this.difficulty );
			d.setTarget( player );

			round_bonus += d.getWorth( );

			enemies.add( d );
			this.addEnemyTransmissionToSpawn( d );
			unspawnedEnemiesRemaining--;
			maxAllowableEnemies--;
		}

		if ( ( maxAllowableEnemies > 0 ) && MathUtils.getDiscreteChance( DASHER_SPAWN_CHANCE ) ) {

			int nX = ( ( int ) player.getX( ) );
			int nY = ( ( int ) player.getY( ) );

			while ( MathUtils.getDistance( nX, nY, ( ( int ) player.getX( ) ),
					( ( int ) player.getY( ) ) ) < PLAYER_SPAWN_PROTECTION_RADIUS ) {
				nX = MathUtils.getRandom( 0, ( int ) Math.abs( player.getX( ) ) + FIELD_OFFSET );
				nY = MathUtils.getRandom( 0, ( int ) Math.abs( player.getY( ) ) + FIELD_OFFSET );
			}

			Dasher d = new Dasher( ( double ) nX, ( double ) nY );
			d.setID( this.idGenRef.generate( ) );
			// FIXME This duct tape is ugly
			if ( this.parent != null && this.parent instanceof GameInstance ) {
				( ( GameInstance ) this.parent ).getMiniMap( ).addEntity( d );
			}
			d.setTarget( player );
			d.setDifficulty( this.difficulty );

			enemies.add( d );
			this.addEnemyTransmissionToSpawn( d );

			round_bonus += d.getWorth( );

			unspawnedEnemiesRemaining--;
			maxAllowableEnemies--;
		}

		if ( wave > 1 ) {

			if ( ( maxAllowableEnemies > 0 ) && MathUtils.getDiscreteChance( BOMBER_SPAWN_CHANCE ) ) {

				int nX = ( ( int ) player.getX( ) );
				int nY = ( ( int ) player.getY( ) );

				while ( MathUtils.getDistance( nX, nY, ( ( int ) player.getX( ) ),
						( ( int ) player.getY( ) ) ) < PLAYER_SPAWN_PROTECTION_RADIUS ) {
					nX = MathUtils.getRandom( 0, ( int ) Math.abs( player.getX( ) ) + FIELD_OFFSET );
					nY = MathUtils.getRandom( 0, ( int ) Math.abs( player.getY( ) ) + FIELD_OFFSET );
				}

				Bomber b = new Bomber( ( double ) nX, ( double ) nY );
				b.setID( this.idGenRef.generate( ) );
				// FIXME This duct tape is ugly
				if ( this.parent != null && this.parent instanceof GameInstance ) {
					( ( GameInstance ) this.parent ).getMiniMap( ).addEntity( b );
				}
				b.setDifficulty( this.difficulty );
				b.setTarget( player );

				round_bonus += b.getWorth( );

				enemies.add( b );
				this.addEnemyTransmissionToSpawn( b );
				unspawnedEnemiesRemaining--;
				maxAllowableEnemies--;

			}
		}

		if ( wave > 2 ) {

			if ( ( maxAllowableEnemies > 0 ) && MathUtils.getDiscreteChance( ROGUE_CANNON_SPAWN_CHANCE ) ) {

				int nX = ( ( int ) player.getX( ) );
				int nY = ( ( int ) player.getY( ) );

				while ( MathUtils.getDistance( nX, nY, ( ( int ) player.getX( ) ),
						( ( int ) player.getY( ) ) ) < PLAYER_SPAWN_PROTECTION_RADIUS ) {

					// FIXME Update this when dynamic map motion
					// is
					// available.
					// BANDAID - because the rogue cannon does
					// not
					// move towards the player is must spawn on
					// screen where the player can hit it.
					nX = MathUtils.getRandom( 0, 1000 );
					nY = MathUtils.getRandom( 0, 1000 );
				}
				RogueCannon rc = new RogueCannon( ( double ) nX, ( double ) nY );
				rc.setID( this.idGenRef.generate( ) );
				// FIXME This duct tape is ugly
				if ( this.parent != null && this.parent instanceof GameInstance ) {
					( ( GameInstance ) this.parent ).getMiniMap( ).addEntity( rc );
				}
				rc.setDifficulty( this.difficulty );
				rc.setTarget( player );

				round_bonus += rc.getWorth( );

				enemies.add( rc );
				this.addEnemyTransmissionToSpawn( rc );
				unspawnedEnemiesRemaining--;
				maxAllowableEnemies--;

			}

			if ( ( maxAllowableEnemies > 0 ) && MathUtils.getDiscreteChance( GHOST_SPAWN_CHANCE ) ) {

				int nX = ( ( int ) player.getX( ) );
				int nY = ( ( int ) player.getY( ) );

				while ( MathUtils.getDistance( nX, nY, ( ( int ) player.getX( ) ),
						( ( int ) player.getY( ) ) ) < PLAYER_SPAWN_PROTECTION_RADIUS ) {
					nX = MathUtils.getRandom( 0, ( int ) Math.abs( player.getX( ) ) + FIELD_OFFSET );
					nY = MathUtils.getRandom( 0, ( int ) Math.abs( player.getY( ) ) + FIELD_OFFSET );
				}

				Ghost g = new Ghost( ( double ) nX, ( double ) nY );
				g.setID( this.idGenRef.generate( ) );
				// FIXME This duct tape is ugly
				if ( this.parent != null && this.parent instanceof GameInstance ) {
					( ( GameInstance ) this.parent ).getMiniMap( ).addEntity( g );
				}
				g.setDifficulty( this.difficulty );
				g.setTarget( player );

				round_bonus += g.getWorth( );

				enemies.add( g );
				this.addEnemyTransmissionToSpawn( g );
				unspawnedEnemiesRemaining--;
				maxAllowableEnemies--;

			}
		}
	}

	private void checkTargets( ) {
		// TODO ensure that no target an enemy is focusing on has left.
	}

	@Override
	public boolean isFinished( ) {
		return this.isFinished;
	}

}

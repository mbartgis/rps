/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-08-30 21:23:28 -0400 (Thu, 30 Aug 2018) $
 * $Revision: 367 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.enemies;

import java.awt.Color;
import java.util.List;

import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.LivingEntity;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.gui.game.StatusBar;
import com.rpscore.lib.MathUtils;

public class Ghost extends Enemy implements Hollowable {

	private int				damagedSinceTick;

	private int				magicCD;

	private boolean			isHollow;

	public static final int	TELEPORT_MP_COST	= 200;

	public Ghost( Double x, Double y ) {
		super( x, y, 48, 48, "Ghost" );

		this.baseHealth = 65;
		this.baseStamina = 60;
		this.baseMagic = 600;

		this.setDifficulty( Enemy.DIFFICULTY_NORMAL );

		this.healthBar = new StatusBar( ( ( int ) this.x ) - ( this.width / 2 ), ( ( int ) this.y ) - 50,
				this.width - 1, 10, this.getHealthMax( ), this.getHealth( ) );

		this.magicBar = new StatusBar( ( ( int ) this.x ) - ( this.width / 2 ), ( ( int ) this.y ) - 40, this.width - 1,
				10, this.getMagicMax( ), this.getMagic( ) );

		this.nameOffset += 10;

		this.healthBar.setOutlineColor( Color.WHITE );
		this.healthBar.setFillColor( Color.RED.darker( ) );
		this.healthBar.setBackingColor( null );

		this.magicBar.setOutlineColor( Color.WHITE );
		this.magicBar.setFillColor( Color.BLUE.darker( ) );
		this.magicBar.setBackingColor( null );

		this.damagedSinceTick = 0;
		this.magicCD = 0;
		this.worth = 65;

		this.isCentered = true;
		this.isHollow = false;

	}

	@Override
	public void draw( GraphicsAdapter g ) {

		Color c = Color.LIGHT_GRAY.brighter( );
		if ( this.isHollow ) {
			c = new Color( c.getRed( ), c.getGreen( ), c.getBlue( ), 0x20 );
		} else {
			c = new Color( c.getRed( ), c.getGreen( ), c.getBlue( ), 0xB0 );
			super.draw( g );

			this.healthBar.draw( g );
			this.magicBar.draw( g );
		}
		g.setColor( c );

		g.fillOval( ( ( int ) this.getDX( ) ) - ( this.width / 2 ), ( ( int ) this.getDY( ) ) - ( this.height / 2 ),
				this.width, this.height );

	}

	/*
	 * @Override public void updateActions( Player player, ArrayList< Enemy >
	 * otherEnemies, Object... otherAruments ) {
	 *
	 * if ( this.damagedSinceTick <= 0 ) {
	 *
	 * if ( this.isHollow ) {
	 *
	 * if ( player.getX( ) > this.x ) { this.setX( this.getX( ) + 2 ); } else if
	 * ( player.getX( ) < this.x ) { this.setX( this.getX( ) - 2 ); }
	 *
	 * if ( player.getY( ) > this.y ) { this.setY( this.getY( ) + 2 ); } else if
	 * ( player.getY( ) < this.y ) { this.setY( this.getY( ) - 2 ); }
	 *
	 * if ( player.isInBounds( this ) ) { player.setHealth( player.getHealth( )
	 * - 5 ); this.damagedSinceTick = 60; } } else { if ( player.getX( ) >
	 * this.x ) { this.setX( this.getX( ) + 1 ); } else if ( player.getX( ) <
	 * this.x ) { this.setX( this.getX( ) - 1 ); }
	 *
	 * if ( player.getY( ) > this.y ) { this.setY( this.getY( ) + 1 ); } else if
	 * ( player.getY( ) < this.y ) { this.setY( this.getY( ) - 1 ); }
	 *
	 * if ( player.isInBounds( this ) ) { player.setHealth( player.getHealth( )
	 * - 5 ); this.damagedSinceTick = 60; } }
	 *
	 * if ( Math.abs( player.getX( ) - this.x ) < 600 && Math.abs( player.getY(
	 * ) - this.y ) < 600 ) {
	 *
	 * if ( this.magic > 0 ) { this.isHollow = true; this.magic--; } else {
	 * this.isHollow = false; this.magicCD += 120; } } else { this.isHollow =
	 * false; this.magicCD += 120; }
	 *
	 * } else { this.damagedSinceTick--; }
	 *
	 * if ( this.magicCD > 0 ) { this.magicCD--; }
	 *
	 * if ( this.magic < this.magicMax ) { if ( this.magicCD <= 0 ) {
	 * this.magic++; } }
	 *
	 * }
	 */

	@Override
	public void setHealth( double health ) {
		if ( !this.isHollow ) {
			super.setHealth( health );
		}
	}

	@Override
	public void updateStamina( ) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateMagic( ) {
		this.magicBar.setStatus( this.getMagic( ) );

	}

	@Override
	public void updateHealth( ) {
		this.healthBar.setStatus( this.getHealth( ) );

	}

	@Override
	protected void performEasyActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		if ( this.damagedSinceTick <= 0 ) {

			if ( this.isHollow ) {

				if ( player.getX( ) > this.x ) {
					this.setX( this.getX( ) + 1 );
				} else if ( player.getX( ) < this.x ) {
					this.setX( this.getX( ) - 1 );
				}

				if ( player.getY( ) > this.y ) {
					this.setY( this.getY( ) + 1 );
				} else if ( player.getY( ) < this.y ) {
					this.setY( this.getY( ) - 1 );
				}

				if ( player.isInBounds( this ) ) {
					player.damage( 5, false );
					this.damagedSinceTick = 60;
				}
			} else {
				if ( player.getX( ) > this.x ) {
					this.setX( this.getX( ) + 1 );
				} else if ( player.getX( ) < this.x ) {
					this.setX( this.getX( ) - 1 );
				}

				if ( player.getY( ) > this.y ) {
					this.setY( this.getY( ) + 1 );
				} else if ( player.getY( ) < this.y ) {
					this.setY( this.getY( ) - 1 );
				}

				if ( player.isInBounds( this ) ) {
					player.damage( 5, false );
					this.damagedSinceTick = 60;
				}
			}

			if ( Math.abs( player.getX( ) - this.x ) < 600 && Math.abs( player.getY( ) - this.y ) < 600 ) {

				if ( this.magic > 0 ) {
					this.isHollow = true;
					this.magic--;
				} else {
					this.isHollow = false;
					this.magicCD += 240;
				}
			} else {
				if ( this.isHollow ) {
					this.magicCD += 120;
				}

				this.isHollow = false;

			}

		} else {
			this.damagedSinceTick--;
		}

		if ( this.magicCD > 0 ) {
			this.magicCD--;
		}

		if ( this.magic < this.magicMax ) {
			if ( this.magicCD <= 0 ) {
				this.magic++;
			}
		}
	}

	@Override
	protected void performNormalActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		if ( this.damagedSinceTick <= 0 ) {

			if ( this.isHollow ) {

				if ( player.getX( ) > this.x ) {
					this.setX( this.getX( ) + 2 );
				} else if ( player.getX( ) < this.x ) {
					this.setX( this.getX( ) - 2 );
				}

				if ( player.getY( ) > this.y ) {
					this.setY( this.getY( ) + 2 );
				} else if ( player.getY( ) < this.y ) {
					this.setY( this.getY( ) - 2 );
				}

				if ( player.isInBounds( this ) ) {
					player.damage( 5, false );
					this.damagedSinceTick = 60;
				}
			} else {
				if ( player.getX( ) > this.x ) {
					this.setX( this.getX( ) + 1 );
				} else if ( player.getX( ) < this.x ) {
					this.setX( this.getX( ) - 1 );
				}

				if ( player.getY( ) > this.y ) {
					this.setY( this.getY( ) + 1 );
				} else if ( player.getY( ) < this.y ) {
					this.setY( this.getY( ) - 1 );
				}

				if ( player.isInBounds( this ) ) {
					player.damage( 5, false );
					this.damagedSinceTick = 60;
				}
			}

			if ( Math.abs( player.getX( ) - this.x ) < 600 && Math.abs( player.getY( ) - this.y ) < 600 ) {

				if ( this.magic > 0 ) {
					this.isHollow = true;
					this.magic--;
				} else {
					this.isHollow = false;
					this.magicCD += 120;
				}
			} else {

				if ( this.isHollow ) {
					this.magicCD += 90;
				}

				this.isHollow = false;

			}

		} else {
			this.damagedSinceTick--;
		}

		if ( this.magicCD > 0 ) {
			this.magicCD--;
		}

		if ( this.magic < this.magicMax ) {
			if ( this.magicCD <= 0 ) {
				this.magic++;
			}
		}

	}

	@Override
	protected void performHardActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		if ( this.damagedSinceTick <= 0 ) {

			if ( this.isHollow ) {

				if ( player.getX( ) > this.x ) {
					this.setX( this.getX( ) + 2 );
				} else if ( player.getX( ) < this.x ) {
					this.setX( this.getX( ) - 2 );
				}

				if ( player.getY( ) > this.y ) {
					this.setY( this.getY( ) + 2 );
				} else if ( player.getY( ) < this.y ) {
					this.setY( this.getY( ) - 2 );
				}

				if ( player.isInBounds( this ) ) {
					player.damage( 8, false );
					this.damagedSinceTick = 60;
				}
			} else {
				if ( player.getX( ) > this.x ) {
					this.setX( this.getX( ) + 1 );
				} else if ( player.getX( ) < this.x ) {
					this.setX( this.getX( ) - 1 );
				}

				if ( player.getY( ) > this.y ) {
					this.setY( this.getY( ) + 1 );
				} else if ( player.getY( ) < this.y ) {
					this.setY( this.getY( ) - 1 );
				}

				if ( player.isInBounds( this ) ) {
					player.damage( 8, false );
					this.damagedSinceTick = 60;
				}
			}

			if ( Math.abs( player.getX( ) - this.x ) < 800 && Math.abs( player.getY( ) - this.y ) < 800 ) {

				if ( this.magic >= TELEPORT_MP_COST ) {

					if ( MathUtils.getDiscreteChance( 0.01 ) ) {

						try {
							int nx = MathUtils.getRandom( ( ( int ) player.getX( ) ) - 400,
									( ( int ) player.getX( ) ) + 400 );

							int ny = MathUtils.getRandom( ( ( int ) player.getY( ) ) - 400,
									( ( int ) player.getY( ) ) + 400 );

							this.setX( Math.abs( nx ) );
							this.setY( Math.abs( ny ) );
							this.magic -= TELEPORT_MP_COST;

						} catch ( IllegalArgumentException e ) {}
					}
				}

				if ( this.magic > 0 ) {
					this.isHollow = true;
					this.magic--;
				} else {
					this.isHollow = false;
					this.magicCD += 120;
				}
			} else {

				if ( this.isHollow ) {
					this.magicCD += 90;
				}

				this.isHollow = false;

			}

		} else {
			this.damagedSinceTick--;
		}

		if ( this.magicCD > 0 ) {
			this.magicCD--;
		}

		if ( this.magic < this.magicMax ) {
			if ( this.magicCD <= 0 ) {
				this.magic++;
			}
		}

	}

	@Override
	protected void performInsaneActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void performImpossibleActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isHollow( ) {
		return this.isHollow;
	}

	@Override
	public void setHollow( boolean hollow ) {
		this.isHollow = hollow;
	}

	@Override
	protected void performActionsAfterDeath( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// TODO Auto-generated method stub

	}

}

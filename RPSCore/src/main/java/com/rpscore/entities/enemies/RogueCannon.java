/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-07-26 14:46:56 -0400 (Thu, 26 Jul 2018) $
 * $Revision: 362 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.enemies;

import java.awt.Color;
import java.util.List;

import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.EntityRegistry;
import com.rpscore.entities.LivingEntity;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.spawned.weapons.Mortar;
import com.rpscore.game.PlayerManager;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.gui.game.StatusBar;

public class RogueCannon extends Enemy {

	private static PlayerManager	statsRef;

	private int						magicCD;

	private int						direction;

	public RogueCannon( Double x, Double y ) {
		super( x, y, 64, 64, "Rogue Cannon" );

		this.baseHealth = 50;
		this.baseStamina = 0;
		this.baseMagic = 100;

		this.setDifficulty( Enemy.DIFFICULTY_NORMAL );

		this.healthBar = new StatusBar( ( ( int ) this.x ) - ( this.width / 2 ), ( ( int ) this.y ) - 70,
				this.width - 1, 10, this.getHealthMax( ), this.getHealth( ) );

		this.magicBar = new StatusBar( ( ( int ) this.x ) - ( this.width / 2 ), ( ( int ) this.y ) - 60, this.width - 1,
				10, this.getMagicMax( ), this.getMagic( ) );

		this.nameOffset += 30;

		this.healthBar.setOutlineColor( Color.WHITE );
		this.healthBar.setFillColor( Color.RED.darker( ) );
		this.healthBar.setBackingColor( null );

		this.magicBar.setOutlineColor( Color.WHITE );
		this.magicBar.setFillColor( Color.BLUE.darker( ) );
		this.magicBar.setBackingColor( null );

		this.magicCD = 0;
		this.worth = 100;

		this.isCentered = true;

		this.direction = 0;

	}

	public void setDirection( int dir ) {
		this.direction = dir;
	}

	@Override
	public void draw( GraphicsAdapter g ) {
		super.draw( g );

		g.drawImage( EntityRegistry.RogueCannonRes.FRAMES_NORMAL[ this.direction ],
				( ( int ) this.getDX( ) ) - ( this.width / 2 ), ( ( int ) this.getDY( ) ) - ( this.height / 2 ) );

		/*
		 * g.setColor( Color.BLACK ); g.fillRect( this.x - ( this.width / 2 ), (
		 * this.y - this.height / 2 ), this.width, this.height );
		 *
		 * g.setColor( Color.WHITE ); g.fillOval( this.x - ( this.width / 4 ), (
		 * this.y - this.height / 4 ), this.width / 2, this.height / 2 );
		 */

	}

	@Override
	protected void performEasyActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		Player p = gi.getPlayer( );
		if ( p != null ) {
			statsRef = p.getPlayerManager( );
		}

		if ( player != null ) {
			int xdiff = ( int ) ( player.getX( ) - this.x );
			int ydiff = ( int ) ( player.getY( ) - this.y );

			if ( ydiff >= 0 ) {
				this.direction = 0;
			} else {
				this.direction = 1;
			}

			int xdir = 1;
			if ( xdiff >= 0 ) {
				xdir++;
			}

			xdiff = Math.abs( xdiff );
			ydiff = Math.abs( ydiff );

			if ( xdiff > ydiff ) {
				this.direction += xdir;
			}

			if ( ( xdiff < 600 ) && ( ydiff < 600 ) && ( ( xdiff > 400 ) || ( ydiff > 400 ) ) ) {
				int chance = actionGenerator.nextInt( 200 );
				if ( magic >= 20 && chance == 100 ) {

					int xinaccurate = actionGenerator.nextInt( 150 );
					boolean xinaccuracydir = actionGenerator.nextBoolean( );
					int yinaccurate = actionGenerator.nextInt( 150 );
					boolean yinaccuracydir = actionGenerator.nextBoolean( );

					xinaccurate *= ( xinaccuracydir ) ? -1 : 1;
					yinaccurate *= ( yinaccuracydir ) ? -1 : 1;

					Mortar m = new Mortar( player.getX( ) + xinaccurate, player.getY( ) + yinaccurate, 50, this );
					this.createSpawnedEntity( m );
					this.magic -= 20;
				}
			}
			if ( this.magic < this.magicMax ) {
				if ( this.magicCD <= 0 ) {
					this.magic++;
					this.magicCD = 10;
				} else {
					magicCD--;
				}
			}
		}

		gi = null;

	}

	@Override
	protected void performNormalActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		Player p = gi.getPlayer( );
		if ( p != null ) {
			statsRef = p.getPlayerManager( );
		}

		if ( player != null ) {
			int xdiff = ( int ) ( player.getX( ) - this.x );
			int ydiff = ( int ) ( player.getY( ) - this.y );

			if ( ydiff >= 0 ) {
				this.direction = 0;
			} else {
				this.direction = 1;
			}

			int xdir = 1;
			if ( xdiff >= 0 ) {
				xdir++;
			}

			xdiff = Math.abs( xdiff );
			ydiff = Math.abs( ydiff );

			if ( xdiff > ydiff ) {
				this.direction += xdir;
			}

			if ( ( xdiff < 1000 ) && ( ydiff < 1000 ) && ( ( xdiff > 400 ) || ( ydiff > 400 ) ) ) {
				int chance = actionGenerator.nextInt( 200 );
				if ( magic >= 20 && chance == 100 ) {

					int xinaccurate = actionGenerator.nextInt( 100 );
					boolean xinaccuracydir = actionGenerator.nextBoolean( );
					int yinaccurate = actionGenerator.nextInt( 100 );
					boolean yinaccuracydir = actionGenerator.nextBoolean( );

					xinaccurate *= ( xinaccuracydir ) ? -1 : 1;
					yinaccurate *= ( yinaccuracydir ) ? -1 : 1;

					Mortar m = new Mortar( player.getX( ) + xinaccurate, player.getY( ) + yinaccurate, 100, this );
					this.createSpawnedEntity( m );
					this.magic -= 20;
				}
			}
			if ( this.magic < this.magicMax ) {
				if ( this.magicCD <= 0 ) {
					this.magic++;
					this.magicCD = 10;
				} else {
					magicCD--;
				}
			}
		}

		gi = null;

	}

	@Override
	protected void performHardActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		Player p = gi.getPlayer( );
		if ( p != null ) {
			statsRef = p.getPlayerManager( );
		}

		if ( player != null ) {
			int xdiff = ( int ) ( player.getX( ) - this.x );
			int ydiff = ( int ) ( player.getY( ) - this.y );

			if ( ydiff >= 0 ) {
				this.direction = 0;
			} else {
				this.direction = 1;
			}

			int xdir = 1;
			if ( xdiff >= 0 ) {
				xdir++;
			}

			xdiff = Math.abs( xdiff );
			ydiff = Math.abs( ydiff );

			if ( xdiff > ydiff ) {
				this.direction += xdir;
			}

			if ( ( xdiff < 1200 ) && ( ydiff < 1200 ) && ( ( xdiff > 400 ) || ( ydiff > 400 ) ) ) {
				int chance = actionGenerator.nextInt( 150 );
				if ( magic >= 20 && chance == 100 ) {

					int xinaccurate = actionGenerator.nextInt( 75 );
					boolean xinaccuracydir = actionGenerator.nextBoolean( );
					int yinaccurate = actionGenerator.nextInt( 75 );
					boolean yinaccuracydir = actionGenerator.nextBoolean( );

					xinaccurate *= ( xinaccuracydir ) ? -1 : 1;
					yinaccurate *= ( yinaccuracydir ) ? -1 : 1;

					Mortar m = new Mortar( player.getX( ) + xinaccurate, player.getY( ) + yinaccurate, 100, this );
					this.createSpawnedEntity( m );
					this.magic -= 20;
				} else {

					chance = actionGenerator.nextInt( 600 );

					if ( chance == 300 && this.magic >= 50 ) {

						boolean type = actionGenerator.nextBoolean( );

						if ( type ) {
							Mortar m1 = new Mortar( player.getX( ) + 75, player.getY( ) + 75, 100, this );

							Mortar m2 = new Mortar( player.getX( ) + 75, player.getY( ) - 75, 100, this );

							Mortar m3 = new Mortar( player.getX( ) - 75, player.getY( ) + 75, 100, this );

							Mortar m4 = new Mortar( player.getX( ) - 75, player.getY( ) - 75, 100, this );

							this.createSpawnedEntity( m1 );
							this.createSpawnedEntity( m2 );
							this.createSpawnedEntity( m3 );
							this.createSpawnedEntity( m4 );

						} else {
							Mortar m1 = new Mortar( player.getX( ), player.getY( ) + 75, 100, this );

							Mortar m2 = new Mortar( player.getX( ), player.getY( ) - 75, 100, this );

							Mortar m3 = new Mortar( player.getX( ) - 75, player.getY( ), 100, this );

							Mortar m4 = new Mortar( player.getX( ) + 75, player.getY( ), 100, this );

							this.createSpawnedEntity( m1 );
							this.createSpawnedEntity( m2 );
							this.createSpawnedEntity( m3 );
							this.createSpawnedEntity( m4 );
						}
						this.magic -= 50;
					}
				}
			}
			if ( this.magic < this.magicMax ) {
				if ( this.magicCD <= 0 ) {
					this.magic++;
					this.magicCD = 10;
				} else {
					magicCD--;
				}
			}

		}

		gi = null;

	}

	@Override
	protected void performInsaneActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		Player p = gi.getPlayer( );
		if ( p != null ) {
			statsRef = p.getPlayerManager( );
		}

		if ( player != null ) {
			int xdiff = ( int ) ( player.getX( ) - this.x );
			int ydiff = ( int ) ( player.getY( ) - this.y );

			if ( ydiff >= 0 ) {
				this.direction = 0;
			} else {
				this.direction = 1;
			}

			int xdir = 1;
			if ( xdiff >= 0 ) {
				xdir++;
			}

			xdiff = Math.abs( xdiff );
			ydiff = Math.abs( ydiff );

			if ( xdiff > ydiff ) {
				this.direction += xdir;
			}

			if ( ( xdiff < 1500 ) && ( ydiff < 1500 ) && ( ( xdiff > 400 ) || ( ydiff > 400 ) ) ) {
				int chance = actionGenerator.nextInt( 125 );
				if ( magic >= 20 && chance == 100 ) {

					int xinaccurate = actionGenerator.nextInt( 50 );
					boolean xinaccuracydir = actionGenerator.nextBoolean( );
					int yinaccurate = actionGenerator.nextInt( 50 );
					boolean yinaccuracydir = actionGenerator.nextBoolean( );

					xinaccurate *= ( xinaccuracydir ) ? -1 : 1;
					yinaccurate *= ( yinaccuracydir ) ? -1 : 1;

					Mortar m = new Mortar( player.getX( ) + xinaccurate, player.getY( ) + yinaccurate, 150, this );
					m.setSpeed( 20 );
					this.createSpawnedEntity( m );
					this.magic -= 20;
				} else {

					chance = actionGenerator.nextInt( 500 );

					if ( chance == 300 && this.magic >= 50 ) {

						boolean type = actionGenerator.nextBoolean( );

						if ( type ) {
							Mortar m1 = new Mortar( player.getX( ) + 125, player.getY( ) + 125, 150, this );

							Mortar m2 = new Mortar( player.getX( ) + 125, player.getY( ) - 125, 150, this );

							Mortar m3 = new Mortar( player.getX( ) - 125, player.getY( ) + 125, 150, this );

							Mortar m4 = new Mortar( player.getX( ) - 125, player.getY( ) - 125, 150, this );

							this.createSpawnedEntity( m1 );
							this.createSpawnedEntity( m2 );
							this.createSpawnedEntity( m3 );
							this.createSpawnedEntity( m4 );

						} else {
							Mortar m1 = new Mortar( player.getX( ), player.getY( ) + 125, 150, this );

							Mortar m2 = new Mortar( player.getX( ), player.getY( ) - 125, 150, this );

							Mortar m3 = new Mortar( player.getX( ) - 125, player.getY( ), 150, this );

							Mortar m4 = new Mortar( player.getX( ) + 125, player.getY( ), 150, this );

							this.createSpawnedEntity( m1 );
							this.createSpawnedEntity( m2 );
							this.createSpawnedEntity( m3 );
							this.createSpawnedEntity( m4 );
						}
						this.magic -= 50;
					}
				}
			}
			if ( this.magic < this.magicMax ) {
				if ( this.magicCD <= 0 ) {
					this.magic++;
					this.magicCD = 10;
				} else {
					magicCD--;
				}
			}
		}

		gi = null;
	}

	@Override
	protected void performImpossibleActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		Player p = gi.getPlayer( );
		if ( p != null ) {
			statsRef = p.getPlayerManager( );
		}

		if ( player != null ) {
			int xdiff = ( int ) ( player.getX( ) - this.x );
			int ydiff = ( int ) ( player.getY( ) - this.y );

			if ( ydiff >= 0 ) {
				this.direction = 0;
			} else {
				this.direction = 1;
			}

			int xdir = 1;
			if ( xdiff >= 0 ) {
				xdir++;
			}

			xdiff = Math.abs( xdiff );
			ydiff = Math.abs( ydiff );

			if ( xdiff > ydiff ) {
				this.direction += xdir;
			}

			if ( ( xdiff < 2000 ) && ( ydiff < 2000 ) ) {
				int chance = actionGenerator.nextInt( 100 );
				if ( magic >= 20 && chance == 50 ) {

					Mortar m = new Mortar( player.getX( ), player.getY( ), 250, this );
					m.setSpeed( 25 );
					this.createSpawnedEntity( m );
					this.magic -= 20;
				} else {

					chance = actionGenerator.nextInt( 400 );

					if ( chance == 300 && this.magic >= 50 ) {

						boolean type = actionGenerator.nextBoolean( );

						if ( type ) {
							Mortar m1 = new Mortar( player.getX( ) + 200, player.getY( ) + 200, 200, this );
							m1.setSpeed( 20 );

							Mortar m2 = new Mortar( player.getX( ) + 200, player.getY( ) - 200, 200, this );
							m2.setSpeed( 20 );

							Mortar m3 = new Mortar( player.getX( ) - 200, player.getY( ) + 200, 200, this );
							m3.setSpeed( 20 );

							Mortar m4 = new Mortar( player.getX( ) - 200, player.getY( ) - 200, 200, this );
							m4.setSpeed( 20 );

							this.createSpawnedEntity( m1 );
							this.createSpawnedEntity( m2 );
							this.createSpawnedEntity( m3 );
							this.createSpawnedEntity( m4 );

						} else {
							Mortar m1 = new Mortar( player.getX( ), player.getY( ) + 200, 200, this );
							m1.setSpeed( 20 );

							Mortar m2 = new Mortar( player.getX( ), player.getY( ) - 200, 200, this );
							m2.setSpeed( 20 );

							Mortar m3 = new Mortar( player.getX( ) - 200, player.getY( ), 200, this );
							m3.setSpeed( 20 );

							Mortar m4 = new Mortar( player.getX( ) + 200, player.getY( ), 200, this );
							m4.setSpeed( 20 );

							this.createSpawnedEntity( m1 );
							this.createSpawnedEntity( m2 );
							this.createSpawnedEntity( m3 );
							this.createSpawnedEntity( m4 );
						}
						this.magic -= 50;
					}
				}
			}
			if ( this.magic < this.magicMax ) {
				if ( this.magicCD <= 0 ) {
					this.magic++;
					this.magicCD = 10;
				} else {
					magicCD--;
				}

			}
		}

		gi = null;

	}

	@Override
	public void updateStamina( ) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateMagic( ) {
		this.magicBar.setStatus( this.getMagic( ) );

		// ensure that the health/magic update correctly.
		this.setX( this.getX( ) );
		this.setY( this.getY( ) );
	}

	@Override
	public void updateHealth( ) {
		this.healthBar.setStatus( this.getHealth( ) );
	}

	@Override
	protected void performActionsAfterDeath( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// TODO Auto-generated method stub

	}

}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-08-30 21:23:28 -0400 (Thu, 30 Aug 2018) $
 * $Revision: 367 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.enemies;

import java.awt.Color;
import java.util.List;

import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.EntityRegistry;
import com.rpscore.entities.LivingEntity;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.spawned.weapons.Bomb;
import com.rpscore.game.PlayerManager;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.gui.game.StatusBar;

public class Bomber extends Enemy {

	private int						damagedSinceTick;

	private int						magicCD;

	private int						frame, frameTrCD;

	protected static PlayerManager	statsRef;

	public Bomber( Double x, Double y ) {
		super( x, y, 96, 96, "Bomber" );

		this.baseHealth = 120;
		this.baseStamina = 60;
		this.baseMagic = 100;

		this.setDifficulty( Enemy.DIFFICULTY_NORMAL );

		this.healthBar = new StatusBar( ( ( int ) this.x ) - ( this.width / 2 ), ( ( int ) this.y ) - 70,
				( this.width ) - 1, 10, this.getHealthMax( ), this.getHealth( ) );

		this.magicBar = new StatusBar( ( ( int ) this.x ) - ( this.width / 2 ), ( ( int ) this.y ) - 60, this.width - 1,
				10, this.getMagicMax( ), this.getMagic( ) );

		this.nameOffset += 30;

		this.healthBar.setOutlineColor( Color.WHITE );
		this.healthBar.setFillColor( Color.RED.darker( ) );
		this.healthBar.setBackingColor( null );

		this.magicBar.setOutlineColor( Color.WHITE );
		this.magicBar.setFillColor( Color.BLUE.darker( ) );
		this.magicBar.setBackingColor( null );

		this.damagedSinceTick = 0;
		this.magicCD = 0;
		this.worth = 80;

		this.isCentered = true;

		this.frame = 0;
		this.frameTrCD = 15;
	}

	@Override
	public void draw( GraphicsAdapter g ) {
		super.draw( g );

		g.drawImage( EntityRegistry.BomberRes.FRAMES_NORMAL[ this.frame ],
				( ( int ) this.getDX( ) ) - ( this.width / 2 ), ( ( int ) this.getDY( ) ) - ( this.height / 2 ) );

		/*
		 * g.setColor( Color.BLACK ); g.fillRect( this.x - ( this.width / 2 ), (
		 * this.y - this.height / 2 ), this.width, this.height );
		 *
		 * g.setColor( Color.WHITE ); g.fillOval( this.x - ( this.width / 4 ), (
		 * this.y - this.height / 4 ), this.width / 2, this.height / 2 );
		 */

	}

	@Override
	protected void performEasyActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		this.frameTrCD--;
		if ( this.frameTrCD <= 0 ) {
			this.frame = ( this.frame + 1 ) > 5 ? 0 : this.frame + 1;
			if ( this.frame > 5 ) {
				this.frame = 0;
			}
			this.frameTrCD = 15;
		}

		Player p = gi.getPlayer( );
		if ( p != null ) {
			statsRef = p.getPlayerManager( );
		}

		if ( player != null ) {
			if ( this.damagedSinceTick <= 0 ) {

				if ( player.getX( ) > this.x ) {
					this.setX( this.getX( ) + 1 );
				} else if ( player.getX( ) < this.x ) {
					this.setX( this.getX( ) - 1 );
				}

				if ( player.getY( ) > this.y ) {
					this.setY( this.getY( ) + 1 );
				} else if ( player.getY( ) < this.y ) {
					this.setY( this.getY( ) - 1 );
				}

				if ( player.isInBounds( this ) ) {
					player.damage( 15, false );
					if ( statsRef != null && player.getHealth( ) <= 0 ) {
						statsRef.incrementDeathByEnemy( this );
					}
					this.damagedSinceTick = 60;
				}

			} else {
				this.damagedSinceTick--;
			}

			if ( this.magic >= 20 ) {
				if ( Math.abs( player.getX( ) - this.x ) < 400 && Math.abs( player.getY( ) - this.y ) < 400 ) {

					int chance = this.actionGenerator.nextInt( 200 );
					if ( chance == 25 ) {

						int power = this.actionGenerator
								.nextInt( ( this.magic * 10 ) >= 500 ? 500 : ( ( int ) this.magic * 10 ) );

						int fuse = 150;

						Bomb b = new Bomb( player.getX( ), player.getY( ), power, fuse, 300, this );

						this.createSpawnedEntity( b );
						this.magic -= power / 10.0;
					}
				}
			}

			if ( this.magicCD > 0 ) {
				this.magicCD--;
			}

			if ( this.magic < this.magicMax ) {
				if ( this.magicCD <= 0 ) {
					this.magic++;
					this.magicCD += 10;
				}
			}
		}
	}

	@Override
	protected void performNormalActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		this.frameTrCD--;
		if ( this.frameTrCD <= 0 ) {
			this.frame = ( this.frame + 1 ) > 5 ? 0 : this.frame + 1;
			if ( this.frame > 5 ) {
				this.frame = 0;
			}
			this.frameTrCD = 15;
		}

		Player p = gi.getPlayer( );
		if ( p != null ) {
			statsRef = p.getPlayerManager( );
		}

		if ( player != null ) {
			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + 1 );
			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - 1 );
			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + 1 );
			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - 1 );
			}

			if ( this.damagedSinceTick <= 0 ) {

				if ( player.isInBounds( this ) ) {
					player.damage( 18, false );
					if ( statsRef != null && player.getHealth( ) <= 0 ) {
						statsRef.incrementDeathByEnemy( this );
					}
					this.damagedSinceTick = 45;
				}

			} else {
				this.damagedSinceTick--;
			}

			if ( this.magic >= 20 ) {
				if ( Math.abs( player.getX( ) - this.x ) < 500 && Math.abs( player.getY( ) - this.y ) < 500 ) {

					int chance = this.actionGenerator.nextInt( 100 );
					if ( chance == 25 ) {
						int power = 0;
						if ( this.getHealth( ) < 30 ) {
							int e = this.actionGenerator.nextInt( ( int ) ( this.magic * 10 ) );
							power = ( ( e >= 700 ) ? 700 : e );
						} else {
							power = this.actionGenerator
									.nextInt( ( this.magic * 10 ) >= 500 ? 500 : ( int ) ( this.magic * 10 ) );
						}

						int fuse = 120;

						if ( power > 500 ) {
							fuse += 60;
						}

						Bomb b = new Bomb( player.getX( ), player.getY( ), power, fuse, 500, this );

						this.createSpawnedEntity( b );
						this.magic -= power / 10.0;
					}
				}
			}
		}

		if ( this.magicCD > 0 ) {
			this.magicCD--;
		}

		if ( this.magic < this.magicMax ) {
			if ( this.magicCD <= 0 ) {
				this.magic++;
				this.magicCD += 10;
			}
		}

		gi = null;
	}

	@Override
	protected void performHardActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		this.frameTrCD--;
		if ( this.frameTrCD <= 0 ) {
			this.frame = ( this.frame + 1 ) > 5 ? 0 : this.frame + 1;
			if ( this.frame > 5 ) {
				this.frame = 0;
			}
			this.frameTrCD = 15;
		}

		Player p = gi.getPlayer( );
		if ( p != null ) {
			statsRef = p.getPlayerManager( );
		}

		if ( player != null ) {
			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + 1 );
			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - 1 );
			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + 1 );
			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - 1 );
			}

			if ( this.damagedSinceTick <= 0 ) {

				if ( player.isInBounds( this ) ) {
					player.damage( 21, false );
					if ( statsRef != null && player.getHealth( ) <= 0 ) {
						statsRef.incrementDeathByEnemy( this );
					}
					this.damagedSinceTick = 30;
				}

			} else {
				this.damagedSinceTick--;
			}

			if ( this.magic >= 20 ) {
				if ( Math.abs( player.getX( ) - this.x ) < 600 && Math.abs( player.getY( ) - this.y ) < 600 ) {

					int chance = this.actionGenerator.nextInt( 75 );
					if ( chance == 25 ) {
						int power = 0;
						if ( this.getHealth( ) < 30 ) {
							int e = this.actionGenerator.nextInt( ( int ) ( this.magic * 20 ) );
							power = ( ( e > 500 ) ? 500 : e );
						} else {
							power = this.actionGenerator
									.nextInt( ( this.magic * 10 ) >= 500 ? 500 : ( int ) ( this.magic * 10 ) );
						}

						int fuse = 90;

						if ( power > 500 ) {
							fuse += 60;
						}
						if ( power < 100 ) {
							fuse -= 60;
						}

						Bomb b = new Bomb( player.getX( ), player.getY( ), power, fuse, 600, this );

						this.createSpawnedEntity( b );
						this.magic -= power / 10.0;
					}
				}
			}

			if ( this.magicCD > 0 ) {
				this.magicCD--;
			}

			if ( this.magic < this.magicMax ) {
				if ( this.magicCD <= 0 ) {
					this.magic++;
					this.magicCD += 10;
				}
			}
		}

	}

	@Override
	protected void performInsaneActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		this.frameTrCD--;
		if ( this.frameTrCD <= 0 ) {
			this.frame = ( this.frame + 1 ) > 5 ? 0 : this.frame + 1;
			if ( this.frame > 5 ) {
				this.frame = 0;
			}
			this.frameTrCD = 15;
		}

		Player p = gi.getPlayer( );
		if ( p != null ) {
			statsRef = p.getPlayerManager( );
		}

		if ( player != null ) {
			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + 2 );
			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - 2 );
			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + 2 );
			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - 2 );
			}

			if ( this.damagedSinceTick <= 0 ) {

				if ( player.isInBounds( this ) ) {
					player.damage( 25, false );
					if ( statsRef != null && player.getHealth( ) <= 0 ) {
						statsRef.incrementDeathByEnemy( this );
					}
					this.damagedSinceTick = 20;
				}

			} else {
				this.damagedSinceTick--;
			}

			if ( this.magic >= 10 ) {
				if ( Math.abs( player.getX( ) - this.x ) < 650 && Math.abs( player.getY( ) - this.y ) < 650 ) {

					int chance = this.actionGenerator.nextInt( 60 );
					if ( chance == 25 ) {
						int power = 0;
						if ( this.getHealth( ) < 50 ) {
							int e = this.actionGenerator.nextInt( ( int ) ( this.magic * 25 ) );
							power = ( ( e > 500 ) ? 500 : e );
						} else {
							power = this.actionGenerator
									.nextInt( ( this.magic * 10 ) >= 700 ? 700 : ( ( int ) ( this.magic * 10 ) ) );
						}

						int fuse = 120;

						if ( power > 500 ) {
							fuse += 60;
						}
						if ( power < 100 ) {
							fuse -= 45;
						}

						Bomb b = new Bomb( player.getX( ), player.getY( ), power, fuse, 650, this );

						this.createSpawnedEntity( b );
						this.magic -= power / 20.0;
					}
				}
			}

			if ( this.magicCD > 0 ) {
				this.magicCD--;
			}

			if ( this.magic < this.magicMax ) {
				if ( this.magicCD <= 0 ) {
					this.magic++;
					this.magicCD += 7;
				}
			}
		}
	}

	@Override
	protected void performImpossibleActions( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {

		this.frameTrCD--;
		if ( this.frameTrCD <= 0 ) {
			this.frame = ( this.frame + 1 ) > 5 ? 0 : this.frame + 1;
			if ( this.frame > 5 ) {
				this.frame = 0;
			}
			this.frameTrCD = 15;
		}

		Player p = gi.getPlayer( );
		if ( p != null ) {
			statsRef = p.getPlayerManager( );
		}

		if ( player != null ) {
			if ( player.getX( ) > this.x ) {
				this.setX( this.getX( ) + 2 );
			} else if ( player.getX( ) < this.x ) {
				this.setX( this.getX( ) - 2 );
			}

			if ( player.getY( ) > this.y ) {
				this.setY( this.getY( ) + 2 );
			} else if ( player.getY( ) < this.y ) {
				this.setY( this.getY( ) - 2 );
			}

			if ( this.damagedSinceTick <= 0 ) {

				if ( player.isInBounds( this ) ) {
					player.damage( 35, false );
					if ( statsRef != null && player.getHealth( ) <= 0 ) {
						statsRef.incrementDeathByEnemy( this );
					}
					this.damagedSinceTick = 15;
				}

			} else {
				this.damagedSinceTick--;
			}

			if ( this.magic >= 5 ) {
				if ( Math.abs( player.getX( ) - this.x ) < 750 && Math.abs( player.getY( ) - this.y ) < 750 ) {

					int chance = this.actionGenerator.nextInt( 60 );
					if ( chance == 25 ) {
						int power = 0;

						int e = this.actionGenerator.nextInt( ( int ) ( this.magic * 25 ) );
						power = ( ( e > 1000 ) ? 1000 : e );

						int fuse = 120;

						if ( power > 500 ) {
							fuse += 60;
						}
						if ( power < 100 ) {
							fuse -= 45;
						}

						Bomb b = new Bomb( player.getX( ), player.getY( ), power, fuse, 750, this );

						this.createSpawnedEntity( b );
						this.magic -= power / 40;
					}
				}
			}

			if ( this.magicCD > 0 ) {
				this.magicCD--;
			}

			if ( this.magic < this.magicMax ) {
				if ( this.magicCD <= 0 ) {
					this.magic++;
					this.magicCD += 5;
				}
			}
		}

		gi = null;

	}

	@Override
	public void updateStamina( ) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateMagic( ) {
		this.magicBar.setStatus( this.getMagic( ) );

	}

	@Override
	public void updateHealth( ) {
		this.healthBar.setStatus( this.getHealth( ) );

	}

	@Override
	protected void performActionsAfterDeath( LivingEntity player, List< Enemy > otherEnemies, EntityContainer gi ) {
		// TODO Auto-generated method stub

	}

}

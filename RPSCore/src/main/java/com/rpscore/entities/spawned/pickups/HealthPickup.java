/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-08-30 21:23:28 -0400 (Thu, 30 Aug 2018) $
 * $Revision: 367 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.spawned.pickups;

import com.rpscore.entities.Entity;
import com.rpscore.entities.EntityRegistry;
import com.rpscore.entities.LivingEntity;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.gui.game.GraphicsAdapter;

public class HealthPickup extends Pickup {

	private int			potency, potencyDegCD;

	private int			rotation_state, cdframes;

	public static int	HEALTH_PICKUP_POTENCY	= 40;

	public HealthPickup( double x, double y, int potency ) {
		super( x, y, 64, 64 );

		this.potency = potency;
		this.potencyDegCD = 0;

		this.isCentered = true;

		this.rotation_state = 0;
		this.cdframes = Pickup.ANIMATION_CD_FRAMES;

	}

	@Override
	public void draw( GraphicsAdapter g ) {

		g.drawImage( EntityRegistry.HealthPickupRes.FRAMES_NORMAL[ this.rotation_state ], ( ( int ) this.getDX( ) ),
				( ( int ) this.getDY( ) ) );

		// Old code to manually generate a health icon
		/* @formatter:off
		g.setColor( Color.RED );

		g.drawOval( this.getDX() - ( this.width / 2 ), this.getDY() - ( this.height / 2 ), this.width, this.height );
		g.drawOval( this.getDX() - ( this.width / 2 ) + 1, this.getDY() - ( this.height / 2 ) + 1, this.width - 2,
				this.height - 1 );
		g.drawOval( this.getDX() - ( this.width / 2 ) + 2, this.getDY() - ( this.height / 2 ) + 2, this.width - 4,
				this.height - 2 );

		int hy = this.getDY() + ( ( int ) ( this.height * 0.375 ) ) - ( this.height / 2 );
		int hw = ( ( int ) ( this.height * 0.75 ) );
		int hx = this.getDX() + ( ( int ) ( this.width * 0.125 ) ) - ( this.width / 2 );
		int hh = ( ( int ) ( this.width * 0.25 ) );

		int vy = this.getDY() + ( ( int ) ( this.height * 0.125 ) ) - ( this.height / 2 );
		int vw = ( ( int ) ( this.height * 0.25 ) );
		int vx = this.getDX() + ( ( int ) ( this.width * 0.375 ) ) - ( this.width / 2 );
		int vh = ( ( int ) ( this.width * 0.75 ) );

		g.fillRect( hx, hy, hw, hh );
		g.fillRect( vx, vy, vw, vh );
		@formatter:on
		*/

	}

	@Override
	public void apply( Entity e ) {

		if ( this.isFinished ) {
			return;
		}

		if ( e instanceof HealthPickup ) {
			( ( HealthPickup ) e ).setPotency( this.potency + ( ( HealthPickup ) e ).getPotency( ) );
		} else if ( e instanceof Player || e instanceof Enemy ) {
			( ( LivingEntity ) e ).addHealth( this.potency );
		} else {
			return;
		}

		this.isFinished = true;

	}

	@Override
	public void updateHealth( ) {
		// TODO Auto-generated method stub

	}

	public int getPotency( ) {
		return potency;
	}

	public void setPotency( int potency ) {
		this.potency = potency;
	}

	@Override
	public void update( ) {

		if ( this.potencyDegCD <= 0 ) {
			this.potency--;
			this.potencyDegCD = 20;
		} else {
			this.potencyDegCD--;
		}

		if ( this.potency <= 0 ) {
			this.isFinished = true;
		}

		if ( this.cdframes <= 0 ) {

			if ( this.rotation_state >= 15 ) {
				this.rotation_state = 0;
			} else {
				this.rotation_state++;
			}

			this.cdframes = Pickup.ANIMATION_CD_FRAMES;
		} else {
			this.cdframes--;
		}

	}

}

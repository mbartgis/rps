/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-11-30 18:15:44 -0500 (Fri, 30 Nov 2018) $
 * $Revision: 373 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.spawned;

import com.rpscore.entities.Entity;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.enemies.Boss;
import com.rpscore.entities.enemies.Enemy;

public abstract class SpawnedEntity extends Entity {

    public static class TYPE {
        public static final int BULLET                 = 0b00000000;
        public static final int BOMB                   = 0b00000001;
        public static final int MORTAR                 = 0b00000010;
        public static final int LANDMINE               = 0b00000011;
        public static final int FUSION_BOMB            = 0b00000100;
        public static final int FUSION_BOMB_DETONATION = 0b00000101;
    }

    protected boolean isFinished;

    protected Entity  source;

    public SpawnedEntity( double x, double y, int width, int height, Entity source ) {
        super( x, y, width, height );
        this.source = source;
    }

    public void setSource( Entity e ) {

        this.source = e;

        // TODO make sure this works for all use cases.
        if ( e instanceof Enemy ) {
            Enemy p = ( Enemy ) e;

            this.setX( p.getX( ) );
            this.setY( p.getY( ) );

            this.setMX( p.getTarget( ).getX( ) );
            this.setMY( p.getTarget( ).getY( ) );

        }

    }

    public boolean isPlayerOrigin( ) {
        return this.source instanceof Player;
    }

    public boolean isEnemyOrigin( ) {
        return this.source instanceof Enemy;
    }

    public boolean isBossOrigin( ) {
        return this.source instanceof Boss;
    }

    public boolean isNonBossEnemyOrigin( ) {
        return this.source instanceof Enemy && !( this.source instanceof Boss );
    }

    @Override
    public boolean isFinished( ) {
        return this.isFinished;
    }

    public Entity getSource( ) {
        return this.source;
    }

    public void onDestroy( ) {

    }

}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.spawned.pickups;

import com.rpscore.entities.Entity;
import com.rpscore.entities.EntityRegistry;
import com.rpscore.entities.LivingEntity;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.gui.game.GraphicsAdapter;

public class MagicPickup extends Pickup {

	private int			potency, potencyDegCD;

	private int			rotation_state, cdframes;

	public static int	MAGIC_PICKUP_POTENCY	= 70;

	public MagicPickup( double x, double y, int potency ) {
		super( x, y, 64, 64 );

		this.potency = potency;
		this.potencyDegCD = 0;

		this.isCentered = true;

		this.rotation_state = 0;
		this.cdframes = Pickup.ANIMATION_CD_FRAMES;

	}

	@Override
	public void draw( GraphicsAdapter g ) {

		g.drawImage( EntityRegistry.MagicPickupRes.FRAMES_NORMAL[ this.rotation_state ], ( ( int ) this.getDX( ) ),
				( ( int ) this.getDY( ) ) );

	}

	@Override
	public void apply( Entity e ) {

		if ( this.isFinished ) {
			return;
		}

		if ( e instanceof MagicPickup ) {
			( ( MagicPickup ) e ).setPotency( this.potency + ( ( MagicPickup ) e ).getPotency( ) );
		} else if ( e instanceof Player || e instanceof Enemy ) {
			( ( LivingEntity ) e ).addMagic( this.potency );
		} else {
			return;
		}

		this.isFinished = true;

	}

	@Override
	public void updateHealth( ) {
		// TODO Auto-generated method stub

	}

	public int getPotency( ) {
		return potency;
	}

	public void setPotency( int potency ) {
		this.potency = potency;
	}

	@Override
	public void update( ) {

		if ( this.potencyDegCD <= 0 ) {
			this.potency--;
			this.potencyDegCD = 20;
		} else {
			this.potencyDegCD--;
		}

		if ( this.potency <= 0 ) {
			this.isFinished = true;
		}

		if ( this.cdframes <= 0 ) {

			if ( this.rotation_state >= 15 ) {
				this.rotation_state = 0;
			} else {
				this.rotation_state++;
			}

			this.cdframes = Pickup.ANIMATION_CD_FRAMES;
		} else {
			this.cdframes--;
		}

	}

}

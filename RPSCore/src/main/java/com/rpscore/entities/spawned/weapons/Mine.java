/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.spawned.weapons;

import java.awt.Color;
import java.util.Iterator;
import java.util.List;

import com.rpscore.entities.Entity;
import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.entities.enemies.Hollowable;
import com.rpscore.entities.spawned.pickups.PickupSpawner;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.lib.MathUtils;

public class Mine extends Bomb {

	protected boolean	armed;
	protected int		armTimer;
	protected boolean	triggered;
	private boolean		flashOn;
	private int			flashTimer;

	public Mine( double mx, double my, int power, Entity source ) {
		super( mx, my, power, -1, -1, source );

		this.setName( "Mine" );

		this.armed = false;
		this.armTimer = 300;
		this.triggered = false;
		this.flashOn = false;
		this.flashTimer = 30;

		this.hasLanded = true;

	}

	public Mine( double x, double y, double mx, double my, int power, Entity source ) {
		super( x, y, mx, my, power, -1, -1, source );

		this.armed = false;
		this.armTimer = 300;
		this.triggered = false;
		this.flashOn = false;
		this.flashTimer = 30;

		this.hasLanded = true;

	}

	@Override
	public void update( ) {

		EntityContainer gi = Entity.getEntityContainer( );
		Player p = gi.getPlayer( );

		if ( p != null ) {
			statRef = p.getPlayerManager( );
		}

		if ( this.armed && !this.triggered ) {

			List< Enemy > otherEnemies = gi.getCurrentEnemies( );
			Iterator< Enemy > e_itr = otherEnemies.iterator( );

			while ( e_itr.hasNext( ) && !this.triggered ) {

				Enemy e = e_itr.next( );

				if ( e.isInBounds( this ) ) {
					this.triggered = true;
				}

			}

			if ( p != null ) {
				if ( p.isInBounds( this ) ) {
					this.triggered = true;
				}
			}

		} else {
			this.armTimer--;
			if ( armTimer <= 0 ) {
				armed = true;
			}
		}

		if ( this.triggered ) {

			List< Enemy > otherEnemies = gi.getCurrentEnemies( );
			for ( int i = 0; i < otherEnemies.size( ); i++ ) {
				if ( otherEnemies.get( i ).isInBounds( this ) && !this.hitEnemies.contains( otherEnemies.get( i ) ) ) {

					if ( ( ! ( otherEnemies.get( i ) instanceof Hollowable ) )
							|| ( otherEnemies.get( i ) instanceof Hollowable
									&& ( ! ( ( Hollowable ) otherEnemies.get( i ) ).isHollow( ) ) ) ) {

						// Mines have a 50% chance to ignore armor.
						if ( MathUtils.getDiscreteChance( 0.50 ) ) {
							otherEnemies.get( i ).damage( power / BOMB_NERF_VALUE, false );
						} else {
							otherEnemies.get( i ).damage( power / BOMB_NERF_VALUE, true );
						}
						int hitXP = otherEnemies.get( i ).getEarnedXP( );

						if ( p != null && ( this.isPlayerOrigin( ) || this.wasShotByPlayer ) ) {
							p.increaseScore( hitXP );
						}

						if ( otherEnemies.get( i ).getHealth( ) <= 0 ) {
							synchronized ( otherEnemies ) {
								gi.addEventToKillfeed( this.source, otherEnemies.get( i ), this );
								PickupSpawner.processEntity( otherEnemies.get( i ), this.source, gi );

							}
						} else {
							this.hitEnemies.add( otherEnemies.get( i ) );
						}
					}
				}
			}

			if ( p != null ) {
				if ( p.isInBounds( this ) && !this.hasHitPlayer ) {
					this.hasHitPlayer = true;

					if ( MathUtils.getDiscreteChance( 0.50 ) ) {
						p.damage( ( power / BOMB_NERF_VALUE ), false );
					} else {
						p.damage( ( power / BOMB_NERF_VALUE ), true );
					}

					if ( p.getHealth( ) <= 0 ) {
						if ( statRef != null ) {
							if ( this.isPlayerOrigin( ) ) {
								statRef.incrementBombSuicides( 1 );
							} else {
								statRef.incrementBombDeaths( 1 );
							}

							gi.addEventToKillfeed( this.getSource( ), p, this );

							statRef.incrementTotalDeaths( 1 );
						}
					}
				}
			}

			this.setWidth( 32 + ( this.expansion - this.power ) );
			this.setHeight( 32 + ( this.expansion - this.power ) );

			int reduction = ( this.expansion / 20 );
			if ( reduction <= 0 ) {
				reduction = 1;
			}
			this.power -= reduction;

			if ( this.power <= 0 ) {
				this.isFinished = true;
			}

		}

		if ( !this.isExploding( ) && !this.isFinished( ) ) {

			List< Bomb > bombsOnMap = gi.getCurrentBombs( );

			for ( int i = 0; i < bombsOnMap.size( ); i++ ) {
				if ( bombsOnMap.get( i ) != this && bombsOnMap.get( i ).isExploding( )
						&& bombsOnMap.get( i ).isInBounds( this ) ) {
					this.detonate( bombsOnMap.get( i ) );
					if ( !this.isPlayerOrigin( ) && bombsOnMap.get( i ).wasShotByPlayer( ) ) {
						if ( p != null ) {
							p.increaseScore( 25 );
						}
					}
				}
			}
		}

		if ( this.flashTimer <= 0 ) {
			this.flashOn = !this.flashOn;
			this.flashTimer = 30;
		} else {
			this.flashTimer--;
		}

		gi = null;

	}

	@Override
	public boolean isExploding( ) {
		return this.triggered && !this.isFinished;
	}

	@Override
	public void detonate( ) {
		this.triggered = true;
	}

	@Override
	public void detonate( Bomb b ) {
		this.triggered = true;
	}

	@Override
	public void detonate( Bullet b ) {
		this.triggered = true;
	}

	@Override
	public void draw( GraphicsAdapter g ) {

		if ( this.triggered ) {
			g.setColor( Color.RED );
			g.drawOval( ( ( int ) this.getDX( ) ) - ( this.width / 2 ), ( ( int ) this.getDY( ) ) - ( this.height / 2 ),
					this.width, this.height );
		} else {
			g.setColor( Color.DARK_GRAY );
			g.fillOval( ( ( int ) this.getDX( ) ) - ( this.width / 2 ), ( ( int ) this.getDY( ) ) - ( this.height / 2 ),
					this.width, this.height );

			if ( this.flashOn ) {

				if ( this.armed ) {
					g.setColor( Color.RED );
				} else {
					g.setColor( Color.GREEN );
				}

				g.fillOval( ( ( int ) this.getDX( ) ) - ( ( this.width / 3 ) / 2 ),
						( ( int ) this.getDY( ) ) - ( ( this.height / 3 ) / 2 ), this.width / 3, this.height / 3 );

			}
		}

	}

}

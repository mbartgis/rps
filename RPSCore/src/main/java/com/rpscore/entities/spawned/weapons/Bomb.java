/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.spawned.weapons;

import java.awt.Color;
import java.awt.Font;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;

import com.rpscore.cfg.ConfigManager;
import com.rpscore.entities.Entity;
import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.entities.enemies.Hollowable;
import com.rpscore.entities.spawned.SpawnedEntity;
import com.rpscore.entities.spawned.pickups.PickupSpawner;
import com.rpscore.game.PlayerManager;
import com.rpscore.gui.game.GraphicsAdapter;

public class Bomb extends SpawnedEntity {

	protected int					power, fuseTime, expansion, throwRange, speed;

	protected ArrayList< Enemy >	hitEnemies;

	protected boolean				hasHitPlayer;

	protected boolean				hasLanded;

	protected boolean				reachedmx, reachedmy;

	protected double				startx, starty;

	protected boolean				wasShotByPlayer;

	protected static PlayerManager	statRef;

	public static final int			BOMB_NERF_VALUE		= 4;

	public static final double		THROW_RANGE_NERF	= 0.6;

	protected static ConfigManager	cfgm				= ConfigManager.getConfigManager( );

	public Bomb( double mx, double my, int power, int fuseTime, int throwRange, Entity source ) {
		super( ( source == null ) ? 0 : source.getX( ), ( source == null ) ? 0 : source.getY( ), 32, 32, source );

		this.setName( "Bomb" );

		this.power = power;
		this.expansion = power;
		this.fuseTime = fuseTime;
		this.throwRange = ( int ) ( throwRange * 0.65 );

		this.isFinished = false;

		this.hitEnemies = new ArrayList< Enemy >( );

		this.hasHitPlayer = false;

		this.isCentered = true;

		this.hasLanded = false;

		this.mx = mx;
		this.my = my;

		this.reachedmx = false;
		this.reachedmy = false;

		this.startx = this.x;
		this.starty = this.y;

		this.wasShotByPlayer = false;

		this.speed = 15;

	}

	public Bomb( double x, double y, double mx, double my, int power, int fuseTime, int throwRange, Entity source ) {
		super( x, y, 32, 32, source );

		this.power = power;
		this.expansion = power;
		this.fuseTime = fuseTime;
		this.throwRange = ( int ) ( throwRange * 0.65 );

		this.isFinished = false;

		this.hitEnemies = new ArrayList< Enemy >( );

		this.hasHitPlayer = false;

		this.isCentered = true;

		this.hasLanded = false;

		this.mx = mx;
		this.my = my;

		this.reachedmx = false;
		this.reachedmy = false;

		this.startx = this.x;
		this.starty = this.y;

		this.wasShotByPlayer = false;

		this.speed = 15;

	}

	public void setSpeed( int s ) {
		this.speed = s;
	}

	public int getSpeed( ) {
		return this.speed;
	}

	@Override
	public void drawBoundingBoxes( GraphicsAdapter g ) {
		super.drawBoundingBoxes( g );

		g.setColor( Color.WHITE );
		g.setFont( new Font( "Arial", Font.BOLD, 24 ) );

		g.drawString( "Power: " + this.power, ( ( int ) this.getX( ) ) + ( this.getWidth( ) / 2 ) + 10,
				( ( int ) this.getY( ) ) - this.getHeight( ) / 2 + 20 );

		g.drawString( "Fuse: " + this.fuseTime, ( ( int ) this.getX( ) ) + ( this.getWidth( ) / 2 ) + 10,
				( ( int ) this.getY( ) ) - this.getHeight( ) / 2 + 50 );

		g.drawString( "Vel: " + this.speed, ( ( int ) this.getX( ) ) + ( this.getWidth( ) / 2 ) + 10,
				( ( int ) this.getY( ) ) - this.getHeight( ) / 2 + 80 );

		g.drawString( "TRange: " + this.throwRange, ( ( int ) this.getX( ) ) + ( this.getWidth( ) / 2 ) + 10,
				( ( int ) this.getY( ) ) - this.getHeight( ) / 2 + 110 );

		g.drawString( "XOff: " + this.getXOffset( ), ( ( int ) this.getX( ) ) + ( this.getWidth( ) / 2 ) + 10,
				( ( int ) this.getY( ) ) - this.getHeight( ) / 2 + 140 );

		g.drawString( "YOff: " + this.getYOffset( ), ( ( int ) this.getX( ) ) + ( this.getWidth( ) / 2 ) + 10,
				( ( int ) this.getY( ) ) - this.getHeight( ) / 2 + 170 );
	}

	@Override
	public void draw( GraphicsAdapter g ) {

		if ( ( this.power / 100 ) > 5 ) {
			g.setColor( Color.RED );
		} else {
			g.setColor( Color.BLACK );
		}

		if ( !hasLanded ) {

			g.fillOval( ( ( int ) this.getDX( ) ) - ( this.width / 2 ), ( ( int ) this.getDY( ) ) - ( this.height / 2 ),
					this.width, this.height );

		} else {
			if ( this.fuseTime > 0 ) {

				g.fillOval( ( ( int ) this.getDX( ) ) - ( this.width / 2 ),
						( ( int ) this.getDY( ) ) - ( this.height / 2 ), this.width, this.height );

				if ( cfgm.getSetting( "BombPowerIndicator", true ) ) {
					if ( ( this.power / 100 ) > 5 ) {
						g.setColor( Color.WHITE );
					} else {
						g.setColor( Color.YELLOW );
					}
					g.setFont( new Font( "Arial", Font.BOLD, 20 ) );
					if ( ( this.power / 100 ) >= 10 ) {
						g.drawString( "" + ( this.power / 100 ),
								( ( int ) this.getDX( ) ) - ( ( int ) ( this.width / 2.5 ) ),
								( ( int ) this.getDY( ) ) + 6 );
					} else {
						g.drawString( "" + ( this.power / 100 ), ( ( int ) this.getDX( ) ) - ( this.width / 5 ),
								( ( int ) this.getDY( ) ) + 6 );
					}
				}

			}

			if ( this.fuseTime <= 0 && this.power > 0 ) {
				g.setColor( Color.RED );
				g.drawOval( ( ( int ) this.getDX( ) ) - ( this.width / 2 ),
						( ( int ) this.getDY( ) ) - ( this.height / 2 ), this.width, this.height );
			}
		}

	}

	public boolean isExploding( ) {
		return ( this.fuseTime <= 0 ) && ( this.power > 0 );
	}

	@Override
	public void updateHealth( ) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update( ) {

		if ( Entity.hasEntityContainer( ) ) {

			EntityContainer gi = Entity.getEntityContainer( );

			Player p = gi.getPlayer( );
			if ( p != null ) {
				statRef = p.getPlayerManager( );
			}

			if ( this.hasLanded ) {

				// Detonate bomb
				if ( this.fuseTime <= 0 && this.power > 0 ) {

					Iterator< Enemy > otherEnemies = gi.getCurrentEnemies( ).iterator( );

					while ( otherEnemies.hasNext( ) ) {
						Enemy e = otherEnemies.next( );
						if ( e.isInBounds( this ) && !this.hitEnemies.contains( e ) ) {

							if ( ( ! ( e instanceof Hollowable ) )
									|| ( e instanceof Hollowable && ( ! ( ( Hollowable ) e ).isHollow( ) ) ) ) {

								e.damage( power / BOMB_NERF_VALUE, false );
								int hitXP = e.getEarnedXP( );

								if ( p != null && ( this.isPlayerOrigin( ) || this.wasShotByPlayer ) ) {
									p.increaseScore( hitXP );
								}

								if ( e.getHealth( ) <= 0 ) {
									synchronized ( otherEnemies ) {
										gi.addEventToKillfeed( this.source, e, this );
										PickupSpawner.processEntity( e, this.source, gi );
										continue;
									}
								} else {
									this.hitEnemies.add( e );
								}

							}

						}
					}

					if ( p != null ) {
						if ( p.isInBounds( this ) && !this.hasHitPlayer ) {
							this.hasHitPlayer = true;
							p.damage( ( power / BOMB_NERF_VALUE ), false );
							if ( p.getHealth( ) <= 0 ) {
								if ( statRef != null ) {
									if ( this.isPlayerOrigin( ) ) {
										statRef.incrementBombSuicides( 1 );
									} else {
										statRef.incrementBombDeaths( 1 );
									}

									gi.addEventToKillfeed( this.getSource( ), p, this );

									statRef.incrementTotalDeaths( 1 );
								}
							}
						}
					}

					this.setWidth( 32 + ( this.expansion - this.power ) );
					this.setHeight( 32 + ( this.expansion - this.power ) );

					int reduction = ( this.expansion / 20 );
					if ( reduction <= 0 ) {
						reduction = 1;
					}
					this.power -= reduction;

					if ( this.power <= 0 ) {
						this.isFinished = true;
					}

				} else if ( this.fuseTime > 0 ) {
					this.fuseTime--;
				}

				if ( this.isFinished ) {
					while ( this.hitEnemies.size( ) > 0 ) {
						this.hitEnemies.remove( 0 );
					}
				}
			} else {
				this.move( );
			}

			if ( !this.isExploding( ) && !this.isFinished( ) ) {
				Iterator< Bomb > bombsOnMap = gi.getCurrentBombs( ).iterator( );

				while ( bombsOnMap.hasNext( ) ) {
					Bomb bomb = bombsOnMap.next( );
					if ( ( bomb != this ) && bomb.isExploding( ) && bomb.isInBounds( this ) ) {
						this.detonate( bomb );
						if ( p != null && !this.isPlayerOrigin( ) && bomb.wasShotByPlayer( ) ) {
							p.increaseScore( 25 );
						}
					}
				}
			}

			gi = null;

		}

	}

	public void move( ) {
		if ( this.hasLanded ) {
			return;
		}

		double xdiff = Math.abs( this.x - this.mx );
		double ydiff = Math.abs( this.y - this.my );

		double ror = ydiff / xdiff;

		double ydist = 0;
		double xdist = 0;

		if ( ror > 1 ) {
			ror = 1 / ror;
			xdist = this.speed * ror;
			ydist = this.speed - xdist;
		} else {
			ydist = this.speed * ror;
			xdist = this.speed - ydist;
		}

		if ( this.mx > this.x ) {
			this.setX( this.x + ( int ) xdist );
		} else {
			this.setX( this.x - ( int ) xdist );
		}

		if ( my > this.y ) {
			this.setY( this.y + ( int ) ydist );
		} else {
			this.setY( this.y - ( int ) ydist );
		}

		int landingArea = this.getHypotenuse( ) - this.throwRange;

		if ( landingArea >= 0 ) {
			if ( this.mx >= ( this.x - landingArea ) && this.mx <= ( this.x + landingArea ) ) {
				this.reachedmx = true;
			}

			if ( this.my >= ( this.y - landingArea ) && this.my <= ( this.y + landingArea ) ) {
				this.reachedmy = true;
			}
		} else {
			if ( this.mx >= ( this.x - this.width ) && this.mx <= ( this.x + this.width ) ) {
				this.reachedmx = true;
			}

			if ( this.my >= ( this.y - this.height ) && this.my <= ( this.y + this.height ) ) {
				this.reachedmy = true;
			}
		}

		this.hasLanded = this.reachedmx && this.reachedmy;
	}

	protected int getHypotenuse( ) {
		double xdiff = this.mx - this.startx;
		double ydiff = this.my - this.starty;

		return ( int ) Math.sqrt( Math.pow( xdiff, 2 ) + Math.pow( ydiff, 2 ) );
	}

	public void drop( ) {
		this.hasLanded = true;
	}

	public void detonate( ) {
		drop( );
		this.fuseTime = 0;
	}

	public void detonate( Bullet b ) {
		// this.wasShotByPlayer = b.isPlayerOrigin( );
		drop( );
		this.fuseTime = 0;
	}

	public void detonate( Bomb b ) {
		// this.wasShotByPlayer = b.isPlayerOrigin( ) || b.wasShotByPlayer( );
		drop( );
		this.fuseTime = 0;
	}

	public void diffuse( ) {
		this.isFinished = true;
	}

	public boolean wasShotByPlayer( ) {
		return this.wasShotByPlayer || this.isPlayerOrigin( );
	}

	public boolean hasLanded( ) {
		return this.hasLanded;
	}

	// Added for legacy support, this is a very unstable method, and has missing
	// features.
	@Deprecated
	public void updateActions( AbstractList< Enemy > otherEnemies, Player p, Object... objects ) {

		if ( this.hasLanded ) {

			// Detonate bomb
			if ( this.fuseTime <= 0 && this.power > 0 ) {

				for ( int i = 0; i < otherEnemies.size( ); i++ ) {
					if ( otherEnemies.get( i ).isInBounds( this )
							&& !this.hitEnemies.contains( otherEnemies.get( i ) ) ) {

						otherEnemies.get( i ).damage( power / BOMB_NERF_VALUE, false );
						int hitXP = otherEnemies.get( i ).getEarnedXP( );

						if ( p != null ) {
							p.increaseScore( hitXP );
						}

						if ( otherEnemies.get( i ).getHealth( ) <= 0 ) {
							synchronized ( otherEnemies ) {
								if ( this.isPlayerOrigin( ) || this.wasShotByPlayer ) {
									if ( statRef != null ) {
										statRef.incrementKillOfEnemy( otherEnemies.get( i ) );
									}
								}
							}
						} else {
							this.hitEnemies.add( otherEnemies.get( i ) );
						}
					}
				}

				if ( p != null ) {
					if ( p.isInBounds( this ) && !this.hasHitPlayer ) {
						this.hasHitPlayer = true;
						p.damage( ( power / BOMB_NERF_VALUE ), false );
						if ( p.getHealth( ) <= 0 ) {
							if ( statRef != null ) {
								if ( this.isPlayerOrigin( ) ) {
									statRef.incrementBombSuicides( 1 );
								} else {
									statRef.incrementBombDeaths( 1 );
								}

								statRef.incrementTotalDeaths( 1 );
							}
						}
					}
				}

				this.setWidth( 32 + ( this.expansion - this.power ) );
				this.setHeight( 32 + ( this.expansion - this.power ) );

				int reduction = ( this.expansion / 20 );
				if ( reduction <= 0 ) {
					reduction = 1;
				}
				this.power -= reduction;

				if ( this.power <= 0 ) {
					this.isFinished = true;
				}

			} else if ( this.fuseTime > 0 ) {
				this.fuseTime--;
			}

			if ( this.isFinished ) {
				while ( this.hitEnemies.size( ) > 0 ) {
					this.hitEnemies.remove( 0 );
				}
			}
		} else {
			this.move( );
		}

	}

	public int getFuseTime( ) {
		return this.fuseTime;
	}

	public boolean isLit( ) {
		return this.hasLanded( ) && this.fuseTime > 0;
	}

}

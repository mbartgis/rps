/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.spawned.pickups;

import com.rpscore.entities.Entity;
import com.rpscore.entities.spawned.SpawnedEntity;

public abstract class Pickup extends SpawnedEntity {

	public static final int ANIMATION_CD_FRAMES = 4;

	public Pickup( double x, double y, int width, int height ) {
		super( x, y, width, height, null );
		// TODO Auto-generated constructor stub
	}

	public abstract void apply( Entity e );

}

package com.rpscore.entities.spawned.weapons;

import java.util.Iterator;

import com.rpscore.entities.Entity;
import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.controlled.Player;

public class FusionBomb extends Bomb {
	

	public FusionBomb(double mx, double my, int power, int throwRange, Entity source) {
		super(mx, my, power, Integer.MAX_VALUE, throwRange, source);
	}
	
   public FusionBomb(double x, double y, double mx, double my, int power, int throwRange, Entity source) {
        super(x, y, mx, my, power, Integer.MAX_VALUE, throwRange, source);
    }

	@Override
	public void update() {
		if ( Entity.hasEntityContainer( ) ) {

			EntityContainer gi = Entity.getEntityContainer( );

			Player p = gi.getPlayer( );
			if ( p != null ) {
				statRef = p.getPlayerManager( );
				
				Iterator< Entity > entities = gi.getAllEntities( ).iterator( );
				
				while (entities.hasNext()) {
					Entity e = entities.next();
					if (!this.isExploding() && e instanceof Bomb && e != this && !this.isFinished() && e.isInBounds(this)) {
						Bomb b = (Bomb) e;
						this.power += b.power;
						this.fuseTime += b.fuseTime;
						b.diffuse();
					}
				}
				
			}
			
		}
		
		super.update();
	}

}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.spawned.weapons;

import java.awt.Color;

import com.rpscore.cfg.ConfigManager;
import com.rpscore.entities.Entity;
import com.rpscore.gui.game.GraphicsAdapter;

public class Mortar extends Bomb {

	protected double				initDistance;

	protected boolean				allowLandingAssist;

	protected static ConfigManager	cfgm	= ConfigManager.getConfigManager( );

	public Mortar( double mx, double my, int power, Entity source ) {
		super( mx, my, power, 0, 10000, source );

		this.initDistance = getHypotenuse( );
		this.allowLandingAssist = true;
	}

	public Mortar( double x, double y, double mx, double my, int power, Entity source ) {
		super( x, y, mx, my, power, 0, 10000, source );

		this.setName( "Mortar" );

		this.initDistance = getHypotenuse( );

		this.allowLandingAssist = true;
	}

	public boolean allowIndividualLandingAssist( ) {
		return this.allowLandingAssist;
	}

	public void setIndividualLandingAssist( boolean b ) {
		this.allowLandingAssist = b;
	}

	@Override
	public double getMX( ) {
		return this.mx - this.player_x_offset;
	}

	@Override
	public double getMY( ) {
		return this.my - this.player_y_offset;
	}

	@Override
	public void draw( GraphicsAdapter g ) {
		super.draw( g );

		if ( !this.hasLanded( ) && cfgm.getSetting( "MortarLandingAssist", true ) ) {

			double remaining = this.getCurrentHypotenuse( ) / initDistance;

			int gcomp = ( int ) ( 255 * remaining );

			g.setColor( new Color( 255, gcomp, 0 ) );
			g.fillOval( ( ( int ) this.getMX( ) ) - 8, ( ( int ) this.getMY( ) ) - 8, 16, 16 );
			for ( int i = 0; i < 5; i++ ) {
				g.drawOval( ( ( int ) this.getMX( ) ) - ( i ) - 15, ( ( int ) this.getMY( ) ) - ( i ) - 15,
						30 + ( i * 2 ), 30 + ( i * 2 ) );
			}

			for ( int i = 0; i < 5; i++ ) {
				g.drawOval( ( ( int ) this.getMX( ) ) - ( i ) - 25, ( ( int ) this.getMY( ) ) - ( i ) - 25,
						50 + ( i * 2 ), 50 + ( i * 2 ) );
			}

		}
	}

	protected double getCurrentHypotenuse( ) {
		double xdiff = this.mx - this.x;
		double ydiff = this.my - this.y;

		return ( int ) Math.sqrt( Math.pow( xdiff, 2 ) + Math.pow( ydiff, 2 ) );
	}

}

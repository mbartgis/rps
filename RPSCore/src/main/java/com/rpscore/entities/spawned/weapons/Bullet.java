/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.spawned.weapons;

import java.awt.Color;
import java.util.Iterator;
import java.util.List;

import com.rpscore.entities.Entity;
import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.entities.enemies.Hollowable;
import com.rpscore.entities.spawned.SpawnedEntity;
import com.rpscore.entities.spawned.pickups.PickupSpawner;
import com.rpscore.game.PlayerManager;
import com.rpscore.gui.game.GraphicsAdapter;

public class Bullet extends SpawnedEntity {

	private int						damage, speed, duration;

	private double					dx, dy;

	private double					termxdiff, termydiff;

	private boolean					reachedmx, reachedmy;

	private static PlayerManager	statRef;

	public Bullet( double x, double y, int damage, double mx, double my, int speed, int duration, Entity source ) {
		super( x, y, 5, 5, source );

		this.setName( "Bullet" );

		this.isCentered = true;
		this.isFinished = false;
		this.damage = damage;

		this.mx = mx;
		this.my = my;

		this.reachedmx = false;
		this.reachedmy = false;

		this.speed = speed;
		this.duration = duration;

		this.dx = x;
		this.dy = y;

	}

	@Override
	public void draw( GraphicsAdapter g ) {

		g.setColor( Color.WHITE );
		g.fillRect( ( ( int ) this.getDX( ) ) - ( this.width / 2 ), ( ( int ) this.getDY( ) ) - ( this.height / 2 ),
				width, height );

	}

	@Override
	public void updateHealth( ) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setX( double x ) {
		super.setX( x );
		this.dx = x;
	}

	@Override
	public void setY( double y ) {
		super.setY( y );
		this.dy = y;
	}

	private void move( ) {

		if ( reachedmx && reachedmy ) {
			this.setX( ( int ) ( this.dx + this.termxdiff ) );
			this.setY( ( int ) ( this.dy + this.termydiff ) );
			return;
		}

		double xdiff = Math.abs( this.dx - this.mx );
		double ydiff = Math.abs( this.dy - this.my );

		double ror = ydiff / xdiff;

		double ydist = 0;
		double xdist = 0;

		if ( ror > 1 ) {
			ror = 1 / ror;
			xdist = this.speed * ror;
			ydist = this.speed - xdist;
		} else {
			ydist = this.speed * ror;
			xdist = this.speed - ydist;
		}

		boolean xneg = false;

		boolean yneg = false;

		if ( this.mx > this.dx ) {
			this.setX( ( int ) ( this.dx + xdist ) );
		} else {
			this.setX( ( int ) ( this.dx - xdist ) );
			xneg = true;
		}

		if ( this.my > this.dy ) {
			this.setY( ( int ) ( this.dy + ydist ) );
		} else {
			this.setY( ( int ) ( this.dy - ydist ) );
			yneg = true;
		}

		if ( this.mx >= ( this.x - speed ) && this.mx <= ( this.x + speed ) ) {
			this.reachedmx = true;
		}

		if ( this.my >= ( this.y - speed ) && this.my <= ( this.y + speed ) ) {
			this.reachedmy = true;
		}

		if ( this.reachedmx && this.reachedmy ) {
			this.termxdiff = ( int ) xdist * ( ( xneg ) ? -1 : 1 );
			this.termydiff = ( int ) ydist * ( ( yneg ) ? -1 : 1 );
		}

	}

	@Override
	public void update( ) {

		if ( Entity.hasEntityContainer( ) ) {

			EntityContainer gi = Entity.getEntityContainer( );

			Player p = gi.getPlayer( );
			if ( p != null ) {
				statRef = p.getPlayerManager( );
			}

			this.move( );
			if ( this.duration > 0 ) {
				this.duration--;
			}

			if ( this.duration <= 0 ) {
				this.isFinished = true;
			}

			List< Enemy > otherEnemies = gi.getCurrentEnemies( );

			if ( !this.isFinished ) {
				for ( Enemy e : otherEnemies ) {

					if ( e.isInBounds( this ) ) {
						if ( ! ( e instanceof Hollowable ) || ! ( ( Hollowable ) e ).isHollow( ) ) {
							if ( this.isPlayerOrigin( ) ) {

								e.damage( this.damage, false );
								int hitXP = e.getEarnedXP( );
								if ( p != null ) {
									p.increaseScore( hitXP );
								}

								if ( e.getHealth( ) <= 0 ) {
									if ( statRef != null ) {
										statRef.incrementKillOfEnemy( e );
									}

									gi.addEventToKillfeed( this.source, e, this );
									PickupSpawner.processEntity( e, this.source, gi );

								}
								this.isFinished = true;
								break;
							}
						}
					}
				}
			}

			if ( !this.isFinished ) {
				if ( p != null && p.getID( ) != this.getSource( ).getID( ) ) {
					if ( p.isInBounds( this ) ) {
						this.isFinished = true;
						p.damage( this.damage, false );
						if ( p.getHealth( ) <= 0 ) {
							if ( statRef != null ) {
								// TODO add bullet deaths
								// statRef.incrementBulletDeaths( 1 );
								statRef.incrementTotalDeaths( 1 );
							}
						}
					}
				}
			}

			if ( !this.isFinished ) {

				Iterator< Bomb > bombsOnMap = gi.getCurrentBombs( ).iterator( );

				while ( bombsOnMap.hasNext( ) ) {
					Bomb bomb = bombsOnMap.next( );
					if ( !bomb.isExploding( ) && bomb.isInBounds( this ) ) {
						if ( bomb.hasLanded( ) ) {
							bomb.detonate( this );
						} else {
							bomb.drop( );
						}

						if ( p != null && this.isPlayerOrigin( ) && !bomb.isPlayerOrigin( ) ) {
							p.increaseScore( 25 );
						}
						this.isFinished = true;
					}
				}

				gi = null;
			}
		}

	}

}

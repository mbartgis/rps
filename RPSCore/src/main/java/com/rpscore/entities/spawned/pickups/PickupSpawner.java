/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

package com.rpscore.entities.spawned.pickups;

import com.rpscore.entities.Entity;
import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.lib.MathUtils;

public class PickupSpawner {

	public static Double	HEALTH_PICKUP_CHANCE	= 0.10;
	public static Double	STAMINA_PICKUP_CHANCE	= 0.15;
	public static Double	MAGIC_PICKUP_CHANCE		= 0.10;

	public static void processEntity( Entity victim, Entity source, EntityContainer field ) {

		if ( field == null || victim == null || source == null || ! ( source instanceof Player )
				|| ! ( victim instanceof Enemy ) ) {
			return;
		}

		Enemy v = ( Enemy ) victim;

		if ( MathUtils.getDiscreteChance( HEALTH_PICKUP_CHANCE ) ) {

			int x_offset = MathUtils.getRandom( 0, 100 ) * ( MathUtils.getDiscreteChance( 0.50 ) ? -1 : 0 );
			int y_offset = MathUtils.getRandom( 0, 100 ) * ( MathUtils.getDiscreteChance( 0.50 ) ? -1 : 0 );

			HealthPickup hp = new HealthPickup( v.getX( ) + x_offset, v.getY( ) + y_offset,
					HealthPickup.HEALTH_PICKUP_POTENCY );

			field.addSpawnedEntity( hp );

		}
		if ( MathUtils.getDiscreteChance( STAMINA_PICKUP_CHANCE ) ) {

			int x_offset = MathUtils.getRandom( 0, 100 ) * ( MathUtils.getDiscreteChance( 0.50 ) ? -1 : 0 );
			int y_offset = MathUtils.getRandom( 0, 100 ) * ( MathUtils.getDiscreteChance( 0.50 ) ? -1 : 0 );

			StaminaPickup sp = new StaminaPickup( v.getX( ) + x_offset, v.getY( ) + y_offset,
					StaminaPickup.STAMINA_PICKUP_POTENCY );

			field.addSpawnedEntity( sp );

		}

		if ( MathUtils.getDiscreteChance( MAGIC_PICKUP_CHANCE ) ) {

			int x_offset = MathUtils.getRandom( 0, 100 ) * ( MathUtils.getDiscreteChance( 0.50 ) ? -1 : 0 );
			int y_offset = MathUtils.getRandom( 0, 100 ) * ( MathUtils.getDiscreteChance( 0.50 ) ? -1 : 0 );

			MagicPickup mp = new MagicPickup( v.getX( ) + x_offset, v.getY( ) + y_offset,
					MagicPickup.MAGIC_PICKUP_POTENCY );

			field.addSpawnedEntity( mp );

		}

	}

}

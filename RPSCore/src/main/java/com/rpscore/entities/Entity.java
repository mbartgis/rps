/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-08-30 21:23:28 -0400 (Thu, 30 Aug 2018) $
 * $Revision: 367 $
 * $Author: mbartgis $
 */

package com.rpscore.entities;

import java.awt.Color;

import com.rpscore.entities.spawned.SpawnedEntity;
import com.rpscore.game.GameInstance;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.lib.Drawable;
import com.rpscore.lib.GUIUtils;
import com.rpscore.lib.Updateable;

public abstract class Entity implements Drawable, Updateable {

	// Duct tape to fix a larger issue,
	private static EntityContainer	entContainer;

	protected double				x, y;

	protected double				mx, my;

	protected int					width, height;

	protected int					maxVelocity;

	protected double				player_x_offset, player_y_offset;

	// TODO this variable is cancer, it must be deleted...eventually.
	protected boolean				isCentered;

	protected String				name;

	protected int					id;

	protected int					teleportTimer;

	public static final int			NO_TELEPORT	= -1;

	public static void setEntityContainer( EntityContainer ec ) {
		Entity.entContainer = ec;
	}

	public static EntityContainer getEntityContainer( ) {
		return Entity.entContainer;
	}

	public static boolean hasEntityContainer( ) {
		return Entity.entContainer != null;
	}

	@SuppressWarnings( "hiding" )
	public Entity( double x, double y, int width, int height ) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.isCentered = false;
		this.teleportTimer = NO_TELEPORT;
	}

	public int getID( ) {
		return this.id;
	}

	public void setID( int nid ) {
		this.id = nid;
	}

	public void setName( String n ) {
		this.name = n;
	}

	public String getName( ) {
		return this.name;
	}

	public void setTeleportTimer( int x ) {
		this.teleportTimer = x;
	}

	public int getTeleportTImer( ) {
		return this.teleportTimer;
	}

	public boolean isTeleportTimerActive( ) {
		return this.teleportTimer >= 0;
	}

	public void cancelTeleport( ) {
		this.teleportTimer = NO_TELEPORT;
	}

	public void teleportTo( int x, int y ) {
		this.setX( x );
		this.setY( y );
		int[ ] screenDims = GUIUtils.getSessionResolution( );
		this.setXOffset( x - screenDims[ 0 ] / 2 );
		this.setYOffset( y - screenDims[ 1 ] / 2 );
	}

	public void tickTeleportTimer( ) {
		this.teleportTimer--;
	}

	public double getX( ) {
		return x;
	}

	public void setX( double x ) {
		this.x = x;
	}

	public double getY( ) {
		return y;
	}

	public void setY( double y ) {
		this.y = y;
	}

	public double getMX( ) {
		return mx;
	}

	public void setMX( double mx ) {
		this.mx = mx;
	}

	public double getMY( ) {
		return my;
	}

	public void setMY( double my ) {
		this.my = my;
	}

	public int getWidth( ) {
		return width;
	}

	public void setWidth( int width ) {
		this.width = width;
	}

	public int getHeight( ) {
		return height;
	}

	public void setHeight( int height ) {
		this.height = height;
	}

	public boolean isInBounds( Entity e ) {

		int ex1, ex2, tx1, tx2, ey1, ey2, ty1, ty2;

		if ( this.isCentered ) {
			tx1 = ( int ) ( this.x - ( this.width / 2 ) );
			tx2 = ( int ) ( this.x + ( this.width / 2 ) );
			ty1 = ( int ) ( this.y - ( this.height / 2 ) );
			ty2 = ( int ) ( this.y + ( this.height / 2 ) );
		} else {
			tx1 = ( int ) this.x;
			tx2 = ( int ) ( this.x + this.width );
			ty1 = ( int ) this.y;
			ty2 = ( int ) ( this.y + this.height );
		}
		if ( e.isCentered ) {
			ex1 = ( int ) ( e.x - ( e.width / 2 ) );
			ex2 = ( int ) ( e.x + ( e.width / 2 ) );
			ey1 = ( int ) ( e.y - ( e.height / 2 ) );
			ey2 = ( int ) ( e.y + ( e.height / 2 ) );
		} else {
			ex1 = ( int ) e.x;
			ex2 = ( int ) ( e.x + e.width );
			ey1 = ( int ) e.y;
			ey2 = ( int ) ( e.y + e.height );
		}

		// This spaghetti ruffled my meatballs dammit!
		return ( ( ( tx1 >= ex1 && tx1 <= ex2 ) && ( ty1 >= ey1 && ty1 <= ey2 ) )
				|| ( ( tx2 >= ex1 && tx2 <= ex2 ) && ( ty1 >= ey1 && ty1 <= ey2 ) )
				|| ( ( tx1 >= ex1 && tx1 <= ex2 ) && ( ty2 >= ey1 && ty2 <= ey2 ) )
				|| ( ( tx2 >= ex1 && tx2 <= ex2 ) && ( ty2 >= ey1 && ty2 <= ey2 ) ) )
				|| ( ( ( ex1 >= tx1 && ex1 <= tx2 ) && ( ey1 >= ty1 && ey1 <= ty2 ) )
						|| ( ( ex2 >= tx1 && ex2 <= tx2 ) && ( ey1 >= ty1 && ey1 <= ty2 ) )
						|| ( ( ex1 >= tx1 && ex1 <= tx2 ) && ( ey2 >= ty1 && ey2 <= ty2 ) )
						|| ( ( ex2 >= tx1 && ex2 <= tx2 ) && ( ey2 >= ty1 && ey2 <= ty2 ) ) );

	}

	public int getMaxVelocity( ) {
		return maxVelocity;
	}

	public void setMaxVelocity( int maxVelocity ) {
		this.maxVelocity = maxVelocity;
	}

	protected abstract void updateHealth( );

	public void drawBoundingBoxes( GraphicsAdapter g ) {

		g.setColor( Color.WHITE );

		if ( this.isCentered ) {
			g.drawRect( ( int ) ( this.getDX( ) - ( this.width / 2.0 ) ),
					( int ) ( this.getDY( ) - ( this.height / 2.0 ) ), this.width, this.height );
		} else {
			g.drawRect( ( int ) this.getDX( ), ( int ) this.getDY( ), this.width, this.height );
		}

	}

	public void setXOffset( double xo ) {
		this.player_x_offset = xo;
	}

	public void setYOffset( double yo ) {
		this.player_y_offset = yo;
	}

	public double getXOffset( ) {
		return this.player_x_offset;
	}

	public double getYOffset( ) {
		return this.player_y_offset;
	}

	public double getDX( ) {
		return this.x - this.player_x_offset;
	}

	public double getDY( ) {
		return this.y - this.player_y_offset;
	}

	public boolean isCentered( ) {
		return this.isCentered;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( obj instanceof Entity ) {
			return ( ( Entity ) obj ).id == this.id;
		}
		return false;
	}

	public void createSpawnedEntity( SpawnedEntity e ) {
		if ( Entity.hasEntityContainer( ) ) {

			EntityContainer ec = Entity.getEntityContainer( );

			if ( ec instanceof GameInstance && ! ( ( GameInstance ) ec ).getSpawner( ).isSlaveToServer( ) ) {
				ec.addSpawnedEntity( e );
			}
		}
	}

}

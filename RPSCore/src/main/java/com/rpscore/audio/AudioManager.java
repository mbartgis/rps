/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-02-04 15:57:36 -0500 (Sun, 04 Feb 2018) $
 * $Revision: 326 $
 * $Author: mbartgis $
 */

package com.rpscore.audio;

public class AudioManager {

	public static void onAudioTrackPlaybackEnd( AudioThread track ) {

	}

	public static void onNextTrackRequested( AudioThread track ) {

	}

}

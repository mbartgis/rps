/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-02-04 15:57:36 -0500 (Sun, 04 Feb 2018) $
 * $Revision: 326 $
 * $Author: mbartgis $
 */

package com.rpscore.audio;

import static javax.sound.sampled.AudioSystem.getAudioInputStream;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.rpscore.C;
import com.rpscore.debug.Log;
import com.rpscore.lib.AudioUtils;
import com.rpscore.lib.Unused;

public class AudioThread extends Thread {

	private String				filename;

	private volatile boolean	hasStopped;
	private volatile boolean	fadeOut;

	@Unused
	private volatile boolean	fadeIn;
	private volatile boolean	loop;
	private volatile int		fadeOutRate;

	private volatile boolean	manualStop;
	private volatile boolean	canContinue;

	private float				defGain;

	private String				key;
	private int					bufferSize;

	public AudioThread( String inFile, int id, boolean isSFX ) {

		setName( "Audio Controller - " + id );

		// Ensure the the JVM kills this thread if it exits (No sound
		// leaks).
		setDaemon( true );
		this.filename = inFile;
		this.hasStopped = false;
		this.fadeOut = false;
		this.loop = false;
		this.fadeOutRate = 3000;

		this.defGain = 0.0f;

		this.manualStop = false;
		this.canContinue = false;
		this.setSFX( isSFX );

	}

	public void setSFX( boolean isSFX ) {
		this.key = isSFX ? C.SettingKeys.SFX_AUDIO_VOLUME : C.SettingKeys.BG_AUDIO_VOLUME;
		this.bufferSize = isSFX ? C.Size.SFX_BUFFER_SIZE : C.Size.BGM_BUFFER_SIZE;
	}

	public void setTrackSkipMode( boolean tsm ) {
		this.canContinue = tsm;
	}

	public void terminate( ) {
		this.hasStopped = true;
		this.manualStop = true;
	}

	public void fadeOut( ) {

		this.fadeOut = true;
		this.terminate( );

	}

	public void fadeOut( int newRate ) {

		this.fadeOutRate = newRate;
		this.fadeOut = true;
		this.terminate( );

	}

	public void fadeIn( ) {
		// TODO implement fade in
	}

	@Override
	public boolean equals( Object obj ) {

		if ( obj instanceof String ) {
			String n = ( String ) obj;
			return n.equals( this.filename );
		}
		return false;
	}

	@Override
	public int hashCode( ) {
		return super.hashCode( );
	}

	@SuppressWarnings( "resource" )
	@Override
	public void run( ) {
		super.run( );

		do {

			File soundFile = new File( filename );

			Log.debug( "Playing track: " + soundFile.getAbsolutePath( ) );

			try ( final AudioInputStream soundIn = AudioSystem.getAudioInputStream( soundFile ) ) {

				AudioFormat inFormat = soundIn.getFormat( );

				final AudioFormat outFormat = AudioUtils.getOutFormat( inFormat );

				DataLine.Info info = new DataLine.Info( SourceDataLine.class, outFormat );

				try {
					SourceDataLine line = ( SourceDataLine ) AudioSystem.getLine( info );
					line.open( outFormat );

					if ( !line.isOpen( ) ) {
						throw new IOException( "Clip could not be opened." );
					}

					FloatControl gainControl = ( FloatControl ) line.getControl( FloatControl.Type.MASTER_GAIN );

					this.defGain = AudioUtils.getVolume( gainControl, this.key );

					gainControl.setValue( this.defGain );

					line.start( );

					AudioInputStream in = getAudioInputStream( outFormat, soundIn );

					final byte[ ] buffer = new byte[ this.bufferSize ];
					for ( int n = 0; n != -1 && !this.hasStopped; n = in.read( buffer, 0, buffer.length ) ) {
						line.write( buffer, 0, n );
						this.defGain = AudioUtils.getVolume( gainControl, this.key );
						gainControl.setValue( this.defGain );
					}

					if ( this.fadeOut ) {
						fadeOut( gainControl, this.fadeOutRate );
					}

					line.stop( );
					line.flush( );
					line.drain( );
				} catch ( LineUnavailableException e ) {

				}

			} catch ( IOException | UnsupportedAudioFileException e ) {
				Log.error( "Error with Audio Control Thread: " + e.getClass( ).getName( ) + " " + e.getMessage( ) );
				return;
			}

		} while ( loop && !hasStopped );

		Log.debug( "Audio thread stopped" );

		if ( !this.manualStop && this.canContinue ) {
			AudioManager.onNextTrackRequested( this );
		}

		this.filename = null;
		this.loop = false;
		this.hasStopped = true;

	}

	private void fadeOut( FloatControl gainControl, int rate ) {
		float delta = ( gainControl.getValue( ) - gainControl.getMinimum( ) ) / rate;
		for ( int i = 0; i < rate; i++ ) {
			try {
				gainControl.setValue( gainControl.getValue( ) - delta );
			}
			/*
			 * Because the value is decreasing, it can be inferred that the
			 * cause of an IllegalArgumentException roots from a value being
			 * entered which is below the threshold. If such a value is entered,
			 * kill the thread, because the audio will be silent.
			 */
			catch ( IllegalArgumentException e ) {
				return;
			}
			AudioUtils.audioDelay( C.Time.ONE_MILLISECOND );
		}
	}

}

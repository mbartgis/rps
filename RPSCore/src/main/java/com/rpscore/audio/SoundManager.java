/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-08-30 21:23:28 -0400 (Thu, 30 Aug 2018) $
 * $Revision: 367 $
 * $Author: mbartgis $
 */

package com.rpscore.audio;

import static javax.sound.sampled.AudioFormat.Encoding.PCM_SIGNED;
import static javax.sound.sampled.AudioSystem.getAudioInputStream;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Semaphore;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.rpscore.C;
import com.rpscore.cfg.ConfigManager;
import com.rpscore.debug.Log;
import com.rpscore.lib.MathUtils;
import com.rpscore.lib.Unused;

@Deprecated
public class SoundManager {

	public static final int				EMPTY		= 0;

	public static final int				UINT_16_MAX	= 65535;

	private static int					counter		= 0;

	private static ConfigManager		cfgm		= ConfigManager.getConfigManager( );

	private static SoundManager			sm;

	private static volatile boolean		audioEnabled;

	/* @formatter:off */
	protected static final String[ ][ ]	BG_LISTINGS		= {
			// Metal Tracks
			{ "Conflict (Placeholder)", "The Risen (Placeholder)", "Clutch (Placeholder)" },
			// Boss Tracks
			{},
			// Dungeon Tracks
			{ "Kerker Der Verdammten" },
			// Menu Tracks
			{ "Somber overture", "Dreamscape", "New Adventure (Placeholder)", "Title", "Title (in E-flat)" }
	};
	/* @formatter:on */

	protected boolean					customSong;

	public enum BG_LISTINGS {
		TOWN, PLAINS, FOREST, DUNGEON, BOSS, DESERT, SEA, METAL, MENU
	}

	public enum SFX_LISTINGS {
		BULLET, BOMB
	}

	private static int	bgGainControl		= 0;
	private static int	masterGainControl	= 0;
	private static int	sfxGainControl		= 0;

	public static int getBGGainControl( ) {
		return bgGainControl;
	}

	public static void enableAudio( ) {
		audioEnabled = true;
	}

	public static void disableAudio( ) {
		audioEnabled = false;
	}

	public static void setBGGainControl( int newGain ) {
		bgGainControl = newGain;
	}

	public static int getMasterGainControl( ) {
		return masterGainControl;
	}

	public static void setMasterGainControl( int newGain ) {
		masterGainControl = newGain;
	}

	public static int getSFXGainControl( ) {
		return sfxGainControl;
	}

	public static void setSFXGainControl( int newGain ) {
		sfxGainControl = newGain;
	}

	public static void updateAudioSettings( ) {

		ConfigManager inst = ConfigManager.getConfigManager( );

		bgGainControl = inst.getSetting( C.SettingKeys.BG_AUDIO_VOLUME, 0 );
		masterGainControl = inst.getSetting( C.SettingKeys.MASTER_AUDIO_VOLUME, 0 );
		sfxGainControl = inst.getSetting( C.SettingKeys.SFX_AUDIO_VOLUME, 0 );

	}

	public static SoundManager getSoundManager( ) {

		updateAudioSettings( );

		if ( SoundManager.sm == null ) {
			SoundManager.sm = new SoundManager( );
		}
		return SoundManager.sm;
	}

	private SoundManagerThread	bgm;

	@Unused
	private boolean				fadeInMode;

	@Unused
	private boolean				fadeOutMode;

	private boolean				hasNewSong;

	private Semaphore			songPushLock;

	private SoundManager( ) {

		this.fadeInMode = false;
		this.fadeOutMode = false;
		this.hasNewSong = false;
		this.customSong = false;

		enableAudio( );

		this.songPushLock = new Semaphore( 1 );
	}

	public boolean playSongFromGroup( String folder, int song ) {

		try {
			this.songPushLock.acquire( );
		} catch ( InterruptedException e ) {
			Thread.currentThread( ).interrupt( );
		}
		this.songPushLock.release( );

		File f = new File( folder );
		if ( !f.exists( ) ) {
			return false;
		}

		File[ ] songs = f.listFiles( );

		Log.info( "Playing Song: " + songs[ song ].getAbsolutePath( ) );

		fadeCurrentBGTrack( 1750 );

		// TODO This is messy, clean it up.
		this.bgm = new SoundManagerThread( songs[ song ].getAbsolutePath( ) );
		this.bgm.start( );
		this.customSong = true;
		this.hasNewSong = true;

		return true;
	}

	public static void setTrackSkipMode( boolean b ) {
		SoundManager smi = getSoundManager( );

		if ( smi.bgm != null ) {
			smi.bgm.setTrackSkipMode( b );
		}
	}

	private static void audioDelay( int time ) {
		try {
			Thread.sleep( time );
		} catch ( InterruptedException e ) {
			Thread.currentThread( ).interrupt( );
		}
	}

	private static AudioFormat getOutFormat( AudioFormat inFormat ) {
		final int ch = inFormat.getChannels( );
		final float rate = inFormat.getSampleRate( );
		return new AudioFormat( PCM_SIGNED, rate, 16, ch, ch * 2, rate, false );
	}

	public boolean playSongFromGroup( String folder ) {

		try {
			this.songPushLock.acquire( );
		} catch ( InterruptedException e ) {
			Thread.currentThread( ).interrupt( );
		}
		this.songPushLock.release( );

		File f = new File( folder );
		if ( !f.exists( ) ) {
			return false;
		}

		File[ ] songs = f.listFiles( );

		return playSongFromGroup( folder, MathUtils.getRandom( 0, songs.length ) );
	}

	public static float getVolume( FloatControl fc, String key ) {
		float gain = cfgm.getSetting( key, 0 );

		if ( gain >= 0.0 ) {
			gain = gain * ( fc.getMaximum( ) / 100 );
		} else {
			gain = gain * ( -1 * ( fc.getMinimum( ) / 100 ) );
		}

		return gain;
	}

	public boolean playSFXFromGroup( String folder ) {

		File f = new File( folder );
		if ( !f.exists( ) ) {
			return false;
		}

		File[ ] sfx = f.listFiles( );

		return playSFXFromGroup( folder, MathUtils.getRandom( 0, sfx.length ) );
	}

	public boolean playSFXFromGroup( String folder, int listing ) {
		File f = new File( folder );
		if ( !f.exists( ) ) {
			return false;
		}

		File[ ] songs = f.listFiles( );

		SoundManagerThread smt = new SoundManagerThread( songs[ listing ].getAbsolutePath( ) );
		smt.setSFX( true );
		smt.start( );

		// TODO find a way to determine thread creation success.
		return true;
	}

	public void setSongLoop( boolean b ) {
		this.bgm.loop = b;
	}

	public boolean hasNewSong( ) {
		return this.hasNewSong;
	}

	public String getSongInfo( ) {
		this.hasNewSong = false;

		if ( this.customSong && this.bgm != null ) {
			File f = new File( this.bgm.filename );
			return f.getName( ).split( C.Strings.FILE_EXTENSION_DELIMITER )[ 0 ];
		}

		return C.Strings.BLANK;
	}

	public void fadeCurrentBGTrack( ) {
		if ( this.bgm != null ) {
			this.bgm.fadeOut( );
			this.bgm = null;
		}
	}

	public void fadeCurrentBGTrack( int rate ) {
		if ( this.bgm != null ) {
			this.bgm.fadeOut( rate );
			this.bgm = null;
		}
	}

	public void endCurrentBGTrack( ) {
		if ( this.bgm != null ) {
			this.bgm.terminate( );
			this.bgm = null;
		}
	}

	private class SoundManagerThread extends Thread {

		private String				filename;

		private volatile boolean	hasStopped;
		private volatile boolean	fadeOut;

		@Unused
		private volatile boolean	fadeIn;
		private volatile boolean	loop;
		private volatile int		fadeOutRate;

		private volatile boolean	manualStop;
		private volatile boolean	canContinue;

		private float				defGain;

		private boolean				isSFX;

		private String				key;

		public SoundManagerThread( String filename ) {

			if ( !audioEnabled ) {
				return;
			}

			setName( "Audio Controller - " + counter );
			counter++;

			// Ensure the the JVM kills this thread if it exits (No sound
			// leaks).
			setDaemon( true );
			this.filename = filename;
			this.hasStopped = false;
			this.fadeOut = false;
			this.loop = false;
			this.fadeOutRate = 3000;

			this.defGain = 0.0f;

			this.manualStop = false;
			this.canContinue = false;
			this.setSFX( false );

		}

		public void setSFX( boolean isSFX ) {
			this.isSFX = isSFX;
			this.key = this.isSFX ? C.SettingKeys.SFX_AUDIO_VOLUME : C.SettingKeys.BG_AUDIO_VOLUME;
		}

		public void setTrackSkipMode( boolean tsm ) {
			this.canContinue = tsm;
		}

		public void terminate( ) {
			this.hasStopped = true;
			this.manualStop = true;
		}

		public void fadeOut( ) {

			this.fadeOut = true;
			this.terminate( );

		}

		public void fadeOut( int newRate ) {

			this.fadeOutRate = newRate;
			this.fadeOut = true;
			this.terminate( );

		}

		public void fadeIn( ) {
			// TODO implement fade in
		}

		@Override
		public boolean equals( Object obj ) {

			if ( obj instanceof String ) {
				String n = ( String ) obj;
				return n.equals( this.filename );
			}
			return false;
		}

		@Override
		public int hashCode( ) {
			return super.hashCode( );
		}

		@Override
		public synchronized void start( ) {
			super.start( );
		}

		@SuppressWarnings( "resource" )
		@Override
		public void run( ) {
			super.run( );

			do {

				File soundFile = new File( filename );

				Log.debug( "Playing track: " + soundFile.getAbsolutePath( ) );

				try ( final AudioInputStream soundIn = AudioSystem.getAudioInputStream( soundFile ) ) {

					AudioFormat inFormat = soundIn.getFormat( );

					final AudioFormat outFormat = getOutFormat( inFormat );

					DataLine.Info info = new DataLine.Info( SourceDataLine.class, outFormat );

					try {
						SourceDataLine line = ( SourceDataLine ) AudioSystem.getLine( info );
						line.open( outFormat );

						if ( !line.isOpen( ) ) {
							throw new IOException( "Clip could not be opened." );
						}

						FloatControl gainControl = ( FloatControl ) line.getControl( FloatControl.Type.MASTER_GAIN );

						this.defGain = getVolume( gainControl, this.key );

						gainControl.setValue( this.defGain );

						line.start( );

						AudioInputStream in = getAudioInputStream( outFormat, soundIn );

						int size = in.available( );

						// The file is full bg track
						if ( size == EMPTY || size > UINT_16_MAX ) {
							size = UINT_16_MAX + 1;
						} else {
							size = 4;
						}

						final byte[ ] buffer = new byte[ size ];
						for ( int n = 0; n != -1 && !this.hasStopped; n = in.read( buffer, 0, buffer.length ) ) {
							line.write( buffer, 0, n );
							this.defGain = getVolume( gainControl, this.key );
							gainControl.setValue( this.defGain );
						}

						if ( this.fadeOut ) {
							fadeOut( gainControl, this.fadeOutRate );
						}

						line.stop( );
						line.flush( );
						line.drain( );
					} catch ( LineUnavailableException e ) {}

				} catch ( IOException | UnsupportedAudioFileException e ) {
					Log.error( "Error with Audio Control Thread: " + e.getClass( ).getName( ) + " " + e.getMessage( ) );
					return;
				}

			} while ( loop && !hasStopped );

			Log.debug( "Audio thread stopped" );

			if ( !this.manualStop && this.canContinue ) {
				String path = new File( this.filename ).getParentFile( ).getAbsolutePath( );
				SoundManager.this.playSongFromGroup( path );
				SoundManager.setTrackSkipMode( true );
			}

			this.filename = null;
			this.loop = false;
			this.hasStopped = true;

		}

		private void fadeOut( FloatControl gainControl, int rate ) {
			float delta = ( gainControl.getValue( ) - gainControl.getMinimum( ) ) / rate;
			for ( int i = 0; i < rate; i++ ) {
				try {
					gainControl.setValue( gainControl.getValue( ) - delta );
				}
				/*
				 * Because the value is decreasing, it can be inferred that the
				 * cause of an IllegalArgumentException roots from a value being
				 * entered which is below the threshold. If such a value is
				 * entered, kill the thread, because the audio will be silent.
				 */
				catch ( IllegalArgumentException e ) {
					return;
				}
				audioDelay( C.Time.ONE_MILLISECOND );
			}
		}

	}

}

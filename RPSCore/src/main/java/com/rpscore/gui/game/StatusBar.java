/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-08-30 21:23:28 -0400 (Thu, 30 Aug 2018) $
 * $Revision: 367 $
 * $Author: mbartgis $
 */

package com.rpscore.gui.game;

import java.awt.Color;

public class StatusBar extends GUIElement {

	protected int		x, y, width, height;
	protected double	max, current;
	protected Color		outline, fill, secondaryFill, back;

	public StatusBar( int x, int y, int width, int height, double max, double initial ) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.max = max;
		this.current = initial;
		this.back = Color.BLACK;
	}

	public void setBackingColor( Color c ) {
		this.back = c;
	}

	// TODO See if this can be rearranged from (width-2)*(current/max) to
	// ((width-2)*current)/max.
	private int getRemainingPixels( ) {
		return ( int ) ( ( this.width - 2 ) * ( this.current / this.max ) );
	}

	@Override
	public void draw( GraphicsAdapter g ) {

		// Draw Outline
		g.setColor( this.outline );
		g.drawRect( this.x, this.y, this.width, this.height );

		// Draw remaining.
		int pixelsRemaining = this.getRemainingPixels( );

		if ( pixelsRemaining < 0 || this.current > this.max ) {
			pixelsRemaining = 0;
		}

		g.setColor( this.fill );
		g.fillRect( this.x + 1, this.y + 1, pixelsRemaining, ( this.height - 2 ) );

		if ( back != null ) {
			g.setColor( back );
			g.fillRect( ( this.x + 1 ) + pixelsRemaining, this.y + 1, ( this.width - 3 ) - pixelsRemaining,
					( this.height - 2 ) );
		}

	}

	public void setOutlineColor( Color c ) {
		this.outline = c;
	}

	public Color getOutlineColor( ) {
		return this.outline;
	}

	public void setFillColor( Color c ) {
		this.fill = c;
	}

	public Color getFillColor( ) {
		return this.fill;
	}

	public void setSecondaryFillColor( Color c ) {
		this.secondaryFill = c;
	}

	public Color getSecondaryFillColor( ) {
		return this.secondaryFill;
	}

	public boolean isGradientBar( ) {
		return this.secondaryFill != null;
	}

	@Override
	public int getX( ) {
		return x;
	}

	@Override
	public void setX( int x ) {
		this.x = x;
	}

	@Override
	public int getY( ) {
		return y;
	}

	@Override
	public void setY( int y ) {
		this.y = y;
	}

	@Override
	public int getWidth( ) {
		return width;
	}

	@Override
	public void setWidth( int width ) {
		this.width = width;
	}

	@Override
	public int getHeight( ) {
		return height;
	}

	@Override
	public void setHeight( int height ) {
		this.height = height;
	}

	public double getMax( ) {
		return max;
	}

	public void setMax( double max ) {
		this.max = max;
	}

	public double getStatus( ) {
		return current;
	}

	public void setStatus( double current ) {
		this.current = current;
	}

	public void setToMax( ) {
		this.current = this.max;
	}

	public void decrement( int n ) {
		this.current -= n;
		if ( this.current < 0 ) {
			this.current = 0;
		}
	}

	public void increment( int n ) {
		this.current += n;
		if ( this.current > this.max ) {
			this.current = this.max;
		}
	}

	@Override
	public void update( ) {
		// TODO Auto-generated method stub

	}

}

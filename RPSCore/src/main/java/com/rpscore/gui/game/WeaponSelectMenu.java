/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */

package com.rpscore.gui.game;

import java.awt.Color;
import java.awt.Image;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import com.rpscore.C;
import com.rpscore.debug.Log;
import com.rpscore.lib.FileUtils;

public class WeaponSelectMenu extends GUIElement {

	public static final int	BOX_SIZE	= 80;

	public static Image		LANDMINE_IMAGE;
	public static Image		BOMB_IMAGE;
	public static Image		MORTAR_IMAGE;
	public static Image		DEBUG_BOMB_IMAGE;
	public static Image		DEBUG_MORTAR_IMAGE;

	public static boolean loadWeaponIcons( ) {

		boolean success = true;

		Log.info( "Creating Image cache for the Weapon Select Menu..." );
		try {
			LANDMINE_IMAGE = FileUtils.importImage( C.File.WEAPON_ICONS_FOLDER + "landmine.png" );
			LANDMINE_IMAGE = LANDMINE_IMAGE.getScaledInstance( BOX_SIZE, BOX_SIZE, Image.SCALE_REPLICATE );
		} catch ( MalformedURLException e ) {
			Log.error( "A MalformedURLException occurred while creating the image cache for Dub." );
			success = false;
		} catch ( IOException e ) {
			Log.error( "An IOException occurred while creating the image cache for Dub." );
			success = false;
		}

		try {
			BOMB_IMAGE = FileUtils.importImage( C.File.WEAPON_ICONS_FOLDER + "bomb.png" );
			BOMB_IMAGE = BOMB_IMAGE.getScaledInstance( BOX_SIZE, BOX_SIZE, Image.SCALE_REPLICATE );
		} catch ( MalformedURLException e ) {
			Log.error( "A MalformedURLException occurred while creating the image cache for Dub." );
			success = false;
		} catch ( IOException e ) {
			Log.error( "An IOException occurred while creating the image cache for Dub." );
			success = false;
		}

		try {
			MORTAR_IMAGE = FileUtils.importImage( C.File.WEAPON_ICONS_FOLDER + "mortar.png" );
			MORTAR_IMAGE = MORTAR_IMAGE.getScaledInstance( BOX_SIZE, BOX_SIZE, Image.SCALE_REPLICATE );
		} catch ( MalformedURLException e ) {
			Log.error( "A MalformedURLException occurred while creating the image cache for Dub." );
			success = false;
		} catch ( IOException e ) {
			Log.error( "An IOException occurred while creating the image cache for Dub." );
			success = false;
		}

		try {
			DEBUG_BOMB_IMAGE = FileUtils.importImage( C.File.WEAPON_ICONS_FOLDER + "bomb_debug.png" );
			DEBUG_BOMB_IMAGE = DEBUG_BOMB_IMAGE.getScaledInstance( BOX_SIZE, BOX_SIZE, Image.SCALE_REPLICATE );
		} catch ( MalformedURLException e ) {
			Log.error( "A MalformedURLException occurred while creating the image cache for Dub." );
			success = false;
		} catch ( IOException e ) {
			Log.error( "An IOException occurred while creating the image cache for Dub." );
			success = false;
		}

		try {
			DEBUG_MORTAR_IMAGE = FileUtils.importImage( C.File.WEAPON_ICONS_FOLDER + "mortar_debug.png" );
			DEBUG_MORTAR_IMAGE = DEBUG_MORTAR_IMAGE.getScaledInstance( BOX_SIZE, BOX_SIZE, Image.SCALE_REPLICATE );
		} catch ( MalformedURLException e ) {
			Log.error( "A MalformedURLException occurred while creating the image cache for Dub." );
			success = false;
		} catch ( IOException e ) {
			Log.error( "An IOException occurred while creating the image cache for Dub." );
			success = false;
		}

		if ( success ) {
			Log.info( "Image cache creation for the Weapon Select Menu successful." );
		}

		return success;
	}

	public static class WEAPON {
		public static final int	MORTAR				= 0x00;
		public static final int	SNIPER				= 0x01;
		public static final int	SPECTRAL_RIFLE		= 0x02;
		public static final int	BOMB				= 0x03;
		public static final int	FUSION_BOMB			= 0x04;
		public static final int	LANDMINE			= 0x05;

		public static final int	DEBUG_ENEMY_BOMB	= 0x06;
		public static final int	DEBUG_ENEMY_MORTAR	= 0x07;

		private WEAPON( ) {}

	}

	// Class begin

	private ArrayList< Integer >	weapons;
	private int						selectedIndex;

	public WeaponSelectMenu( int x, int y ) {
		this.x = x;
		this.y = y;

		this.width = 0;
		this.height = BOX_SIZE;

		this.weapons = new ArrayList< Integer >( 8 );
		this.selectedIndex = 0;
	}

	/**
	 * @param direction
	 *            false = left, true = right
	 */
	public void scroll( boolean direction ) {
		// scroll right
		if ( direction ) {
			if ( this.selectedIndex >= ( this.weapons.size( ) - 1 ) ) {
				this.selectedIndex = 0;
			} else if ( this.selectedIndex < 0 ) {
				this.selectedIndex = 0;
			} else {
				this.selectedIndex++;
			}
		}
		// scroll left
		else {
			if ( this.selectedIndex <= 0 ) {
				this.selectedIndex = this.weapons.size( ) - 1;
			} else if ( this.selectedIndex > ( this.weapons.size( ) - 1 ) ) {
				this.selectedIndex = this.weapons.size( ) - 1;
			} else {
				this.selectedIndex--;
			}
		}
	}

	public void addWeapon( Integer weapon ) {
		this.weapons.add( weapon );
	}

	public int getCurrentWeapon( ) {
		return this.weapons.get( this.selectedIndex );
	}

	@Override
	public int getWidth( ) {
		if ( this.weapons == null ) {
			return super.getWidth( );
		}
		return this.weapons.size( ) * BOX_SIZE;
	}

	@Override
	public void draw( GraphicsAdapter g ) {

		g.setColor( Color.WHITE );
		g.drawRect( this.x, this.y, this.getWidth( ), this.height );

		for ( int i = 0; i < this.weapons.size( ); i++ ) {
			g.drawImage( getWeaponImage( this.weapons.get( i ) ), this.x + ( BOX_SIZE * i ), this.y );
		}

		Color c = new Color( 255, 255, 255, 80 );
		g.setColor( c );
		g.fillRect( this.x + ( this.selectedIndex * BOX_SIZE ) + 1, this.y + 1, BOX_SIZE - 1, this.height - 1 );

	}

	@Override
	public void update( ) {}

	public int getSelectedIndex( ) {
		return selectedIndex;
	}

	public void setSelectedIndex( int index ) {
		this.selectedIndex = index;
	}

	public Image getCutrrentWeaponIconImage( ) {
		return getWeaponImage( this.selectedIndex );
	}

	@Override
	public boolean isFinished( ) {
		return false;
	}

	public Image getWeaponImage( int image ) {
		switch ( image ) {
			case WEAPON.BOMB: {
				return BOMB_IMAGE;
			}
			case WEAPON.MORTAR: {
				return MORTAR_IMAGE;
			}
			case WEAPON.LANDMINE: {
				return LANDMINE_IMAGE;
			}
			case WEAPON.DEBUG_ENEMY_BOMB: {
				return DEBUG_BOMB_IMAGE;
			}
			case WEAPON.DEBUG_ENEMY_MORTAR: {
				return DEBUG_MORTAR_IMAGE;
			}
			default: {
				return BOMB_IMAGE;
			}
		}
	}

}

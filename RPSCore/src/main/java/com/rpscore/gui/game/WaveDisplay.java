/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */

package com.rpscore.gui.game;

import java.awt.Color;
import java.awt.Font;

import com.rpscore.entities.enemies.SpawnManager;

public class WaveDisplay extends GUIElement {

	private SpawnManager	spawner;

	private StatusBar		statsBar;

	private int				x, y, width, height;

	private int				cdTimeMax, enemiesMax;

	private boolean			lastCDState;

	private double			scale;

	private int				pwidth;

	public WaveDisplay( SpawnManager sm, int parent_width ) {
		if ( sm == null ) {
			throw new IllegalArgumentException( );
		}
		this.spawner = sm;

		this.pwidth = parent_width;

		this.scale = ( this.pwidth ) / 2560.0;

		if ( this.spawner.isWaveBreak( ) ) {
			this.statsBar = new StatusBar( this.x + 20, this.y + ( ( int ) ( this.height * 0.67 ) ), this.width / 2,
					this.height / 8, this.spawner.getTotalCDTime( ), this.spawner.getWaveBreakCooldown( ) );
		} else {
			this.statsBar = new StatusBar( this.x + 20, this.y + ( ( int ) ( this.height * 0.67 ) ), this.width / 2,
					this.height / 8, this.spawner.getTotalEnemies( ), this.spawner.getEnemiesRemaining( ) );
		}

		this.statsBar.setOutlineColor( Color.WHITE );
		this.statsBar.setBackingColor( Color.BLACK );

		this.statsBar.setFillColor( Color.WHITE );

		this.width = 300;
		this.height = 300;
	}

	public boolean stateChange( ) {
		if ( this.spawner != null ) {
			boolean stateChange = lastCDState != this.spawner.isWaveBreak( );
			this.lastCDState = this.spawner.isWaveBreak( );
			return stateChange;
		}

		return false;
	}

	@Override
	public void draw( GraphicsAdapter g ) {

		this.scale = ( this.pwidth ) / 2560.0;
		g.setColor( new Color( 0xAF000000, true ) );
		g.fillRect( this.x + 3, this.y + 3, this.width - 6, this.height - 6 );

		g.setFont( new Font( "Arial", Font.BOLD, ( int ) ( 36 * scale ) ) );

		if ( this.spawner.getWave( ) == 0 ) {
			g.setColor( Color.LIGHT_GRAY );
			this.statsBar.setBackingColor( Color.BLACK );
			g.drawString( "New Game Starting...", this.x + this.width / 10, this.y + ( ( int ) ( 40 * scale ) ) );
		} else if ( spawner.getWave( ) % 5 == 0 ) {
			g.setColor( Color.RED.brighter( ).brighter( ) );
			this.statsBar.setBackingColor( g.getColor( ) );
			g.drawString( "Wave " + this.spawner.getWave( ) + " (BOSS)", this.x + this.width / 5,
					this.y + ( ( int ) ( 40 * scale ) ) );
		} else {
			g.setColor( Color.WHITE );
			this.statsBar.setBackingColor( Color.BLACK );
			g.drawString( "Wave " + this.spawner.getWave( ), ( this.x + this.width / 3 ) + ( ( int ) ( 1 * scale ) ),
					this.y + ( ( int ) ( 40 * scale ) ) );
		}

		this.statsBar.setFillColor( g.getColor( ) );

		g.drawRect( this.x, this.y, this.width - 1, this.height - 1 );
		g.drawRect( this.x + 2, this.y + 2, this.width - 5, this.height - 5 );

		if ( stateChange( ) ) {
			if ( this.spawner.isWaveBreak( ) ) {
				this.cdTimeMax = this.spawner.getTotalCDTime( );
				this.statsBar.setMax( this.cdTimeMax );
			} else {
				this.enemiesMax = this.spawner.getTotalEnemies( );
				this.statsBar.setMax( this.enemiesMax );
			}
		}

		g.setFont( new Font( "Arial", Font.BOLD, ( ( int ) ( 24 * scale ) ) ) );
		if ( this.spawner.isWaveBreak( ) ) {
			this.statsBar.setStatus( this.spawner.getWaveBreakCooldown( ) );
			this.statsBar.setMax( this.spawner.getTotalCDTime( ) );

			g.drawString( "Time Remaining: ", this.x + ( ( int ) ( 20 * scale ) ),
					this.statsBar.getY( ) - ( ( int ) ( 15 * scale ) ) );

			int secs = this.spawner.getWaveBreakCooldown( ) / 60;

			if ( secs < 10 ) {
				g.setFont( new Font( "Arial", Font.BOLD, ( ( int ) ( 72 * scale ) ) ) );
			} else {
				g.setFont( new Font( "Arial", Font.BOLD, ( ( int ) ( 60 * scale ) ) ) );
			}

			int millis = ( int ) ( ( 100.0 / 60.0 ) * ( this.spawner.getWaveBreakCooldown( ) % 60 ) );
			String mstr = "" + millis;

			if ( mstr.length( ) < 2 ) {
				mstr = "0" + mstr;
			}

			g.drawString( secs + "." + mstr, ( this.x + this.width / 2 ) + ( ( int ) ( 45 * scale ) ),
					( this.y + this.height / 2 ) + ( ( int ) ( 55 * scale ) ) );

		} else {
			this.statsBar.setStatus( this.spawner.getEnemiesRemaining( ) );
			this.statsBar.setMax( this.spawner.getTotalEnemies( ) );

			g.drawString( "Troops Remaining: ", this.x + ( ( int ) ( 20 * scale ) ),
					this.statsBar.getY( ) - ( ( int ) ( 15 * scale ) ) );

			if ( this.spawner.getEnemiesRemaining( ) < 1000 ) {
				g.setFont( new Font( "Arial", Font.BOLD, ( ( int ) ( 72 * scale ) ) ) );
			} else if ( this.spawner.getEnemiesRemaining( ) < 10000 ) {
				g.setFont( new Font( "Arial", Font.BOLD, ( ( int ) ( 60 * scale ) ) ) );
			} else {
				g.setFont( new Font( "Arial", Font.BOLD, ( ( int ) ( 44 * scale ) ) ) );
			}

			String eStr = "" + this.spawner.getEnemiesRemaining( );

			int indent = 3 - eStr.length( );

			int cnt = 0;

			while ( cnt < indent ) {
				eStr = " " + eStr;
				cnt++;
			}

			g.drawString( eStr, ( this.x + this.width / 2 ) + ( ( int ) ( 60 * scale ) ),
					( this.y + this.height / 2 ) + ( ( int ) ( 55 * scale ) ) );
		}

		this.statsBar.draw( g );

	}

	public void onDestroy( ) {
		this.spawner = null;
	}

	@Override
	public int getX( ) {
		return x;
	}

	@Override
	public void setX( int x ) {
		this.x = x;
		this.statsBar.setX( this.x + 20 );
	}

	@Override
	public int getY( ) {
		return y;
	}

	@Override
	public void setY( int y ) {
		this.y = y;
		this.statsBar.setY( this.y + ( ( int ) ( this.height * 0.67 ) ) );
	}

	@Override
	public int getWidth( ) {
		return width;
	}

	@Override
	public void setWidth( int width ) {
		this.width = width;
		this.statsBar.setWidth( this.width / 2 );
	}

	@Override
	public int getHeight( ) {
		return height;
	}

	@Override
	public void setHeight( int height ) {
		this.height = height;
		this.statsBar.setHeight( this.height / 8 );
	}

	public void setParentWidth( int parent_width ) {
		this.pwidth = parent_width;
	}

	@Override
	public void update( ) {
		// TODO Auto-generated method stub

	}

}

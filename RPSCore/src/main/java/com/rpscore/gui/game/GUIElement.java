/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */

package com.rpscore.gui.game;

import com.rpscore.lib.Drawable;
import com.rpscore.lib.Updateable;

public abstract class GUIElement implements Drawable, Updateable {

	protected int x, y, width, height;

	@Override
	public boolean isFinished( ) {
		return false;
	}

	public int getX( ) {
		return x;
	}

	public void setX( int nx ) {
		this.x = nx;
	}

	public int getY( ) {
		return y;
	}

	public void setY( int ny ) {
		this.y = ny;
	}

	public int getWidth( ) {
		return width;
	}

	public void setWidth( int nWidth ) {
		this.width = nWidth;
	}

	public int getHeight( ) {
		return height;
	}

	public void setHeight( int nHeight ) {
		this.height = nHeight;
	}

}

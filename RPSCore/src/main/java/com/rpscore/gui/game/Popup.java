/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */

package com.rpscore.gui.game;

import java.awt.Color;
import java.awt.Font;

import com.rpscore.lib.TimedMessage;

public class Popup extends GUIElement {

	private String			text;

	private Font			font;

	private int				duration, durationOrig, fadeCtr;

	private int				x, y, width, height, xinit, yinit;

	private Color			foreground, background, fadeFG, fadeBG;

	private ANIMATION		animation;

	public static final int	FADE_TIME		= 60;

	public static final int	BIT_24			= 256 * 256 * 256;

	public static final int	ANIMATION_SPEED	= 2;

	public enum ANIMATION {
		NONE, FADE, VERTICAL_SLIDE_DESCENDING, VERTICAL_SLIDE_ASCENDING, HORIZONTAL_SLIDE_POSITIVE,
		HORIZONTAL_SLIDE_NEGATIVE
	}

	public Popup( String text, int duration ) {
		this.text = text;
		this.duration = duration;
		this.durationOrig = duration;
		this.font = new Font( "Arial", Font.ITALIC, 20 );
		this.animation = ANIMATION.NONE;
		this.foreground = new Color( 0xFF000000, true );
		this.background = new Color( 0x55FFFFFF, true );
		this.fadeFG = new Color( 0x00000000, true );
		this.fadeBG = new Color( 0x00000000, true );
		this.x = 0;
		this.y = 0;
		this.xinit = 0;
		this.yinit = 0;
		this.width = ( this.text.length( ) * 10 ) + 20;
		this.height = 40;
		this.fadeCtr = 0;
	}

	public Popup( TimedMessage m ) {
		this( m.getMessage( ), m.getDuration( ) );
	}

	@Deprecated
	public void display( int duration ) {
		this.duration = duration;
	}

	@Deprecated
	public void extend( int duration ) {
		this.duration += duration;
	}

	@Override
	public boolean isFinished( ) {
		return this.duration <= 0;
	}

	@Override
	public void update( ) {
		this.duration--;

		switch ( this.animation ) {
			case FADE:
				int mbg = Math.max( this.background.getAlpha( ), this.foreground.getAlpha( ) );
				// Fade In
				if ( ( durationOrig - duration ) < ( mbg / ANIMATION_SPEED ) ) {
					int fa = this.fadeCtr < this.foreground.getAlpha( ) ? this.fadeCtr : this.foreground.getAlpha( );
					int ba = this.fadeCtr < this.background.getAlpha( ) ? this.fadeCtr : this.background.getAlpha( );

					this.fadeFG = new Color(
							this.foreground.getRGB( ) - ( this.foreground.getAlpha( ) * BIT_24 ) + ( fa * BIT_24 ),
							true );
					this.fadeBG = new Color(
							this.background.getRGB( ) - ( this.background.getAlpha( ) * BIT_24 ) + ( ba * BIT_24 ),
							true );

					this.fadeCtr += ANIMATION_SPEED;

				}
				// Fade Out
				else if ( ( duration ) < ( mbg / ANIMATION_SPEED ) ) {

					int fa = this.fadeCtr < this.foreground.getAlpha( ) ? this.fadeCtr : this.foreground.getAlpha( );
					int ba = this.fadeCtr < this.background.getAlpha( ) ? this.fadeCtr : this.background.getAlpha( );

					this.fadeFG = new Color(
							this.foreground.getRGB( ) - ( this.foreground.getAlpha( ) * BIT_24 ) + ( fa * BIT_24 ),
							true );
					this.fadeBG = new Color(
							this.background.getRGB( ) - ( this.background.getAlpha( ) * BIT_24 ) + ( ba * BIT_24 ),
							true );

					this.fadeCtr -= ANIMATION_SPEED;
				}
				// Display Fully
				else {
					this.fadeFG = this.foreground;
					this.fadeBG = this.background;
				}

				break;
			case HORIZONTAL_SLIDE_NEGATIVE:
				break;
			case HORIZONTAL_SLIDE_POSITIVE:

				if ( this.duration > ( this.durationOrig * 0.90 ) ) {
					this.x = ( int ) ( xinit + ( this.width
							* ( ( this.durationOrig - this.duration ) / ( this.durationOrig * 0.10 ) ) ) );
				} else if ( this.duration < ( this.durationOrig * 0.40 ) ) {
					this.x -= ( this.width * ( 1 - ( this.duration / ( this.durationOrig * 0.40 ) ) ) );
				} else {
					this.x = this.xinit + this.width;
				}
				break;
			case NONE:
				break;
			case VERTICAL_SLIDE_ASCENDING:
				break;
			case VERTICAL_SLIDE_DESCENDING:
				break;
			default:
				break;

		}
	}

	@Override
	public void draw( GraphicsAdapter g ) {

		g.setFont( this.font );

		switch ( this.animation ) {
			case FADE:
				g.setColor( this.fadeBG );
				g.fillRect( this.x, this.y, this.width, this.height );

				g.setColor( this.fadeFG );
				g.drawString( this.text, this.x + 10, this.y + ( ( int ) ( ( this.height ) / 1.5 ) ) );
				break;
			case HORIZONTAL_SLIDE_NEGATIVE:
				break;
			case HORIZONTAL_SLIDE_POSITIVE:
				g.setColor( this.background );
				g.fillRect( this.x, this.y, this.width, this.height );

				g.setColor( this.foreground );
				g.drawString( this.text, this.x + 10, this.y + ( ( int ) ( ( this.height ) / 1.5 ) ) );
				break;
			case NONE:
				break;
			case VERTICAL_SLIDE_ASCENDING:
				break;
			case VERTICAL_SLIDE_DESCENDING:
				break;
			default:
				break;

		}

	}

	@Override
	public int getWidth( ) {
		return width;
	}

	@Override
	public void setWidth( int width ) {
		this.width = width;
	}

	@Override
	public int getHeight( ) {
		return height;
	}

	@Override
	public void setHeight( int height ) {
		this.height = height;
	}

	public String getText( ) {
		return text;
	}

	public void setText( String text ) {
		this.text = text;
	}

	public Font getFont( ) {
		return font;
	}

	public void setFont( Font font ) {
		this.font = font;
	}

	public int getDuration( ) {
		return duration;
	}

	public void setDuration( int duration ) {
		this.duration = duration;
	}

	@Override
	public int getX( ) {
		return x;
	}

	@Override
	public void setX( int x ) {
		this.x = x;
		this.xinit = x;
	}

	@Override
	public int getY( ) {
		return y;
	}

	@Override
	public void setY( int y ) {
		this.y = y;
		this.yinit = y;
	}

	public Color getForeground( ) {
		return foreground;
	}

	public void setForeground( Color foreground ) {
		this.foreground = foreground;
	}

	public Color getBackground( ) {
		return background;
	}

	public void setBackground( Color background ) {
		this.background = background;
	}

	public ANIMATION getAnimation( ) {
		return animation;
	}

	public void setAnimation( ANIMATION animation ) {
		this.animation = animation;

		switch ( this.animation ) {
			case FADE:
				break;
			case HORIZONTAL_SLIDE_NEGATIVE:
				break;
			case HORIZONTAL_SLIDE_POSITIVE:
				this.setX( this.getX( ) - this.getWidth( ) );
				break;
			case NONE:
				break;
			case VERTICAL_SLIDE_ASCENDING:
				break;
			case VERTICAL_SLIDE_DESCENDING:
				break;
			default:
				break;

		}

	}

}

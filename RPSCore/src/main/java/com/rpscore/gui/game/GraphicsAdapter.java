/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

package com.rpscore.gui.game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

public interface GraphicsAdapter {

	public abstract void setColor( final Color color );

	public abstract void setFont( final Font font );

	public abstract void drawImage( final Image image, final int x, final int y );

	public abstract void drawString( final String text, final int x, final int y );

	public abstract void drawRect( final int x, final int y, final int width, final int height );

	public abstract void fillRect( final int x, final int y, final int width, final int height );

	public abstract void drawOval( final int x, final int y, final int width, final int height );

	public abstract void fillOval( final int x, final int y, final int width, final int height );

	public abstract void drawLine( final int x1, final int y1, final int x2, final int y2 );

	public abstract Color getColor( );

	public abstract Font getFont( );

}

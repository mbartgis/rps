/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-11-30 18:15:44 -0500 (Fri, 30 Nov 2018) $
 * $Revision: 373 $
 * $Author: mbartgis $
 */

package com.rpscore.game;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.rpscore.C;
import com.rpscore.audio.SoundManager;
import com.rpscore.cfg.ConfigManager;
import com.rpscore.debug.Log;
import com.rpscore.entities.Entity;
import com.rpscore.entities.EntityContainer;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.controlled.PlayerCursor;
import com.rpscore.entities.enemies.Bomber;
import com.rpscore.entities.enemies.Dasher;
import com.rpscore.entities.enemies.Dub;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.entities.enemies.Ghost;
import com.rpscore.entities.enemies.SpawnManager;
import com.rpscore.entities.spawned.SpawnedEntity;
import com.rpscore.entities.spawned.pickups.Pickup;
import com.rpscore.entities.spawned.weapons.Bomb;
import com.rpscore.entities.spawned.weapons.Bullet;
import com.rpscore.entities.spawned.weapons.FusionBomb;
import com.rpscore.entities.spawned.weapons.Mine;
import com.rpscore.entities.spawned.weapons.Mortar;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.gui.game.WeaponSelectMenu;
import com.rpscore.gui.game.WeaponSelectMenu.WEAPON;
import com.rpscore.lib.Drawable;
import com.rpscore.lib.GUIDGenerator;
import com.rpscore.lib.MathUtils;
import com.rpscore.lib.TimedMessage;
import com.rpscore.lib.Updateable;
import com.rpscore.server.Communicator;
import com.rpscore.terrain.BlockRegistry;
import com.rpscore.terrain.Map;
import com.rpscore.terrain.MiniMap;
import com.rpscore.terrain.TerrainGenerator;

public class GameInstance implements Updateable, Drawable, EntityContainer {

    protected SpawnManager              spawner;

    protected ConfigManager             cfgm = ConfigManager.getConfigManager( );

    protected boolean                   gamePaused;

    protected Map                       map;

    protected Player                    player;

    protected ArrayList< Bullet >       bullets;
    protected ArrayList< Bomb >         bombs;
    protected ArrayList< Pickup >       pickups;

    protected ArrayList< TimedMessage > killfeed;

    protected WeaponSelectMenu          weaponSelect;

    protected PlayerCursor              playerCursor;

    protected boolean                   displayDebugRotationData, displayBoundingBoxes, textureSmoothingEnabled;

    protected LinkedList< Player >      otherPlayers;

    protected Communicator              connection;

    protected int                       lastx, lasty, lastmx, lastmy;

    public static double                PXO, PYO = 0.0;

    protected boolean                   isOnline;

    protected boolean                   clearHUDForChat;

    protected MiniMap                   minimap;

    protected boolean                   saveAfterLoss;

    protected GUIDGenerator             idGenerator;

    public GameInstance( Communicator c, long seed, int mapSize ) {

        this.connection = c;

        // TODO Fix this hot garbage. This needs to be outside of the Game Instance.
        String saveFolder = C.File.PLAYER_SAVEDATA_FOLDER
                + cfgm.getSetting( "LastProfile", C.GameplayConstants.DEFAULT_PLAYER_NAME ) + C.File.FS;

        PlayerManager playerManager = PlayerManager.getPlayerManager( saveFolder, true, 0L );

        this.idGenerator = new GUIDGenerator( );
        this.idGenerator.reserve( 0 );

        this.player = new Player( 100, 100, playerManager );
        this.player.setID( 0 );

        this.map = TerrainGenerator.generateMap( mapSize, seed );

        this.minimap = new MiniMap( this.map );
        this.minimap.setY( 20 );
        this.minimap.addEntity( player );

        this.bullets = new ArrayList< >( 10 );
        this.bombs = new ArrayList< >( 10 );
        this.pickups = new ArrayList< >( 10 );

        this.spawner = new SpawnManager( this, this.idGenerator );

        this.spawner.setDifficulty( Enemy.DIFFICULTY_NORMAL );
        this.spawner.setWaveBreakCooldown( 0 );
        this.spawner.addPlayer( this.player );

        this.gamePaused = false;

        this.killfeed = new ArrayList< >( 6 );

        loadWeaponSelectMenu( );

        this.playerCursor = new PlayerCursor( 150, 150, 32, 32 );

        this.displayDebugRotationData = cfgm.getSetting( "DisplayRotationData", false );
        this.displayBoundingBoxes = cfgm.getSetting( "DisplayBoundingBoxes", false );
        this.textureSmoothingEnabled = cfgm.getSetting( "SmoothBlockEdges", false );

        this.otherPlayers = new LinkedList< >( );

        this.clearHUDForChat = false;

    }

    @Override
    public List< Player > getCurrentOtherPlayers( ) {
        return this.otherPlayers;
    }

    @Override
    public void addOtherPlayer( Player p ) {
        if ( this.otherPlayers != null && !this.otherPlayers.contains( p ) ) {
            this.otherPlayers.add( p );
        }
    }

    public void removeOtherPlayer( Player p ) {
        if ( this.otherPlayers != null ) {
            while ( this.otherPlayers.contains( p ) ) {
                this.otherPlayers.remove( p );
            }
        }
    }

    public void setMap( int id ) {
        this.map = TerrainGenerator.generateMap( 257, id );
        this.minimap = new MiniMap( this.map );

        try {
            Iterator< Entity > ents = this.getAllEntities( ).iterator( );

            while ( ents.hasNext( ) ) {
                this.minimap.addEntity( ents.next( ) );
            }
        } catch ( Exception e ) {
        }

    }

    /*
     * public boolean isOnline( ) { return this.connection != null; }
     */

    public void clearHUDForChat( boolean b ) {
        this.clearHUDForChat = b;
        if ( this.player != null ) {
            this.player.clearHUDForChat( b );
        }
    }

    public Entity getEntityByID( int id ) {
        Iterator< Entity > ents = this.getAllEntities( ).iterator( );

        while ( ents.hasNext( ) ) {
            Entity e = ents.next( );
            if ( e.getID( ) == id ) { return e; }
        }

        // TODO Find a better way to handle this.
        return null;

    }

    public boolean clearHUDForChat( ) {
        return this.clearHUDForChat;
    }

    public boolean isOnline( ) {
        return this.isOnline;
    }

    public void setOnline( boolean b ) {
        this.isOnline = b;
    }

    public void loadWeaponSelectMenu( ) {

        PlayerManager playerManager = this.player.getPlayerManager( );

        this.weaponSelect = new WeaponSelectMenu( 20, 0 );

        this.weaponSelect.addWeapon( WEAPON.BOMB );
        if ( playerManager.hasMortars( ) ) {
            this.weaponSelect.addWeapon( WEAPON.MORTAR );
        }
        if ( playerManager.hasSniperRifle( ) ) {
            this.weaponSelect.addWeapon( WEAPON.SNIPER );
        }
        if ( playerManager.hasSpectreRifle( ) ) {
            this.weaponSelect.addWeapon( WEAPON.SPECTRAL_RIFLE );
        }
        if ( playerManager.hasLandmines( ) ) {
            this.weaponSelect.addWeapon( WEAPON.LANDMINE );
        }

        this.weaponSelect.addWeapon( WEAPON.FUSION_BOMB );

        this.player.setSecondaryWeapon( this.weaponSelect.getCurrentWeapon( ) );

        this.onResize( cfgm.getSetting( "WindowWidth", 800 ), cfgm.getSetting( "WindowHeight", 600 ),
                cfgm.getSetting( "Fullscreen", true ) );

        System.out.println( );

    }

    public void updateGraphicsSettings( boolean restart ) {
        this.displayDebugRotationData = cfgm.getSetting( "DisplayRotationData", this.displayDebugRotationData );
        this.displayBoundingBoxes = cfgm.getSetting( "DisplayBoundingBoxes", this.displayBoundingBoxes );
        this.textureSmoothingEnabled = cfgm.getSetting( "SmoothBlockEdges", this.textureSmoothingEnabled );
        if ( this.map != null ) {
            this.map.setTextureSmoothingEnabled( this.textureSmoothingEnabled );
        }
    }

    @Override
    public boolean isFinished( ) {
        return false;
    }

    public List< TimedMessage > getKillfeed( ) {
        return this.killfeed;
    }

    @Override
    public void onWaveStart( ) {

        if ( this.spawner != null ) {
            if ( this.spawner.getWaveBreakCooldown( ) > 0 || this.spawner.getWave( ) == 0 ) {
                if ( SoundManager.getSoundManager( ).playSongFromGroup( C.File.MUSIC_BREAK ) ) {
                    SoundManager.getSoundManager( ).setSongLoop( false );
                    SoundManager.setTrackSkipMode( true );
                }
            } else if ( ( this.spawner.getWave( ) % 5 ) == 0 ) {
                if ( SoundManager.getSoundManager( ).playSongFromGroup( C.File.MUSIC_BOSS ) ) {
                    SoundManager.getSoundManager( ).setSongLoop( false );
                    SoundManager.setTrackSkipMode( true );
                }
            } else {
                if ( SoundManager.getSoundManager( ).playSongFromGroup( C.File.MUSIC_NORMAL ) ) {
                    SoundManager.getSoundManager( ).setSongLoop( false );
                    SoundManager.setTrackSkipMode( true );
                }
            }

            Log.info( "New Wave Starting" );
            this.player.getPlayerManager( ).incrementRoundsStarted( );

        }

    }

    public void onWaveDefeat( ) {
        if ( SoundManager.getSoundManager( ).playSongFromGroup( C.File.MUSIC_BREAK ) ) {
            SoundManager.getSoundManager( ).setSongLoop( false );
            SoundManager.setTrackSkipMode( true );
        }

        Log.info( "Wave Failed" );

        this.player.getPlayerManager( ).incrementRoundsPlayed( );
        this.player.getPlayerManager( ).concatenateDifficultyPlayedAt( this.spawner.getDifficulty( ) );
        this.player.adjustFreeSkills( );
    }

    @Override
    public void onWaveEnd( ) {

        if ( SoundManager.getSoundManager( ).playSongFromGroup( C.File.MUSIC_BREAK ) ) {
            SoundManager.getSoundManager( ).setSongLoop( false );
            SoundManager.setTrackSkipMode( true );
        }

        Log.info( "Wave Complete" );

        this.player.getPlayerManager( ).incrementRoundsPlayed( );
        this.player.getPlayerManager( ).concatenateDifficultyPlayedAt( this.spawner.getDifficulty( ) );
        this.player.adjustFreeSkills( );

    }

    private void performTeleportProcess( ) {
        if ( !player.isTeleportTimerActive( ) ) {
            player.setTeleportTimer( C.Time.Game.FIVE_SECONDS );
        } else {
            player.tickTeleportTimer( );

        }

        if ( !player.isTeleportTimerActive( ) ) {
            Point p = map.getLocationOf( BlockRegistry.BLOCK_TYPE.BLOCK_GRASS );

            player.teleportTo( p.x, p.y );
        }
    }

    @Override
    public void update( ) {

        player.update( );

        if ( this.map != null ) {

            if ( ConfigManager.getConfigManager( ).getSetting( "DisplayBoundingBoxes", false )
                    && this.playerCursor != null ) {
                this.playerCursor.setInfo( map.getBlockAtEntityCoords( this.playerCursor ).getBlockName( ) );
            } else {
                this.playerCursor.setInfo( "" );
            }

            // Apply effects to the player
            player.applyBlockEffects( map.getBlockAtEntityCoords( player ) );

            // Apply effects to all local instances of remote players.
            Iterator< Player > opi = this.otherPlayers.iterator( );
            while ( opi.hasNext( ) ) {
                Player p = opi.next( );
                p.applyBlockEffects( map.getBlockAtEntityCoords( p ) );
            }

            if ( this.spawner != null ) {
                Iterator< Enemy > enemies = this.spawner.getEnemies( ).iterator( );

                while ( enemies.hasNext( ) ) {
                    Enemy enemy = enemies.next( );
                    enemy.applyBlockEffects( map.getBlockAtEntityCoords( enemy ) );
                }
            }

            if ( map.getBlockAtEntityCoords( player ).equals( BlockRegistry.BLOCK_TYPE.BLOCK_NULL ) ) {

                performTeleportProcess( );

            } else if ( player.isFinished( ) ) {

                performTeleportProcess( );

            } else {
                player.cancelTeleport( );
            }

            this.map.setRenderEpicenter( ( int ) player.getX( ), ( int ) player.getY( ) );
            this.map.update( );

        }

        PXO = player.getXOffset( );
        PYO = player.getYOffset( );

        // Update all spawned entities.
        Iterator< Bullet > bul_iter = bullets.iterator( );
        while ( bul_iter.hasNext( ) ) {
            Bullet b = bul_iter.next( );

            b.setXOffset( PXO );
            b.setYOffset( PYO );

            b.update( );
            if ( b.isFinished( ) ) {
                bul_iter.remove( );
            }
        }

        Iterator< Bomb > bomb_iter = bombs.iterator( );
        boolean hasFusionBomb = false;
        while ( bomb_iter.hasNext( ) ) {
            Bomb b = bomb_iter.next( );

            b.setXOffset( PXO );
            b.setYOffset( PYO );

            if ( map != null ) {
                // Diffuse submerged bombs
                if ( b.hasLanded( )
                        && map.getBlockAtEntityCoords( b ).equals( BlockRegistry.BLOCK_TYPE.BLOCK_WATER ) ) {
                    b.diffuse( );
                }

                // Detonate bombs in lava
                if ( b.hasLanded( ) && map.getBlockAtEntityCoords( b ).equals( BlockRegistry.BLOCK_TYPE.BLOCK_LAVA ) ) {
                    b.detonate( );
                }

                // Remove active fusionbomb instances
                hasFusionBomb |= b instanceof FusionBomb && b.getSource( ).equals( this.player );

            }

            b.update( );
            if ( b.isFinished( ) ) {
                bomb_iter.remove( );
            }
        }

        if ( !hasFusionBomb ) {
            this.player.removeActiveFusionBomb( );
        }

        Iterator< Pickup > pick_iter = pickups.iterator( );
        while ( pick_iter.hasNext( ) ) {
            Pickup p = pick_iter.next( );

            p.setXOffset( PXO );
            p.setYOffset( PYO );

            p.update( );
            if ( p.isFinished( ) ) {
                pick_iter.remove( );
            }
        }

        Iterator< TimedMessage > killfeedItr = killfeed.iterator( );

        while ( killfeedItr.hasNext( ) ) {
            TimedMessage kfEntry = killfeedItr.next( );
            kfEntry.decrementTimer( );
            if ( kfEntry.isFinished( ) ) {
                killfeedItr.remove( );
            }
        }

        if ( spawner != null ) {
            spawner.update( );
        }

        if ( playerCursor != null ) {
            playerCursor.setEntities( getAllEntities( ) );
            playerCursor.update( );
        }

        if ( player != null ) {
            Iterator< Pickup > pi = getCurrentPickups( ).iterator( );
            while ( pi.hasNext( ) ) {
                Pickup p = pi.next( );
                if ( player.isInBounds( p ) ) {
                    p.apply( player );
                }

                Iterator< Enemy > enemies = getCurrentEnemies( ).iterator( );

                while ( enemies.hasNext( ) ) {
                    Enemy e = enemies.next( );
                    if ( e.isInBounds( p ) ) {
                        p.apply( e );
                    }
                }
            }
        }

        if ( this.spawner != null && ( this.spawner.getWaveBreakCooldown( ) > 0 ) ) {
            // Allow a user to tweak their player during a wave break.
            player.adjustFreeSkills( );
        }

        // Set Game Over Condition
        if ( player.isFinished( ) ) {

            synchronized ( bullets ) {
                while ( !bullets.isEmpty( ) ) {
                    bullets.remove( 0 );
                }
            }

            synchronized ( bombs ) {
                while ( !bombs.isEmpty( ) ) {
                    bombs.remove( 0 );
                }
            }

            if ( this.saveAfterLoss ) {
                if ( spawner.getWave( ) > player.getPlayerManager( ).getHighestWaveReached( ) ) {
                    player.getPlayerManager( ).setHighestWaveReached( spawner.getWave( ) );
                    player.getPlayerManager( ).setHighestWaveReachedDifficulty( spawner.getDifficulty( ) );
                }

                if ( !this.isOnline ) {
                    spawner.reset( );
                }

                player.getPlayerManager( ).addXPToProfile( player.getScore( ) );
                player.getPlayerManager( ).incrementRoundsLost( );
                player.getPlayerManager( ).save( );

                player.resetScore( );

                if ( !this.isOnline ) {
                    onWaveDefeat( );
                }

                this.saveAfterLoss = false;
            }

        } else {
            this.saveAfterLoss = true;
        }

    }

    @Override
    public void draw( GraphicsAdapter g ) {

        // Render the map
        if ( this.map != null ) {
            this.map.draw( g );
        }

        if ( this.bullets != null ) {
            for ( Bullet bill : this.bullets ) {
                bill.draw( g );
            }
        }

        if ( this.bombs != null ) {
            for ( Bomb boom : this.bombs ) {
                boom.draw( g );
            }
        }

        if ( this.pickups != null ) {
            for ( Pickup pick : this.pickups ) {
                pick.draw( g );
            }
        }

        if ( this.spawner != null ) {
            spawner.renderEnemies( g, false );
        }

        if ( this.displayBoundingBoxes ) {
            g.setColor( Color.WHITE );
            this.player.drawBoundingBoxes( g );
            this.playerCursor.drawBoundingBoxes( g );

            for ( int i = 0; i < getCurrentBullets( ).size( ); i++ ) {
                getCurrentBullets( ).get( i ).drawBoundingBoxes( g );
            }

            for ( int i = 0; i < getCurrentBombs( ).size( ); i++ ) {
                getCurrentBombs( ).get( i ).drawBoundingBoxes( g );
            }

            if ( map != null ) {
                g.drawString( map.getBlockAtEntityCoords( this.player ).getBlockName( ), 20, 20 );
            }

        }

        if ( !this.clearHUDForChat && cfgm.getSetting( "HUD", true ) && !player.isFinished( ) ) {
            this.weaponSelect.draw( g );
        }

        this.player.draw( g );

        if ( this.minimap != null ) {
            this.minimap.draw( g );
        }

        if ( this.player.isTeleportTimerActive( ) ) {

            double d = ( this.player.getTeleportTImer( ) ) / ( ( double ) C.Time.Game.FIVE_SECONDS );

            int gamma = 255 - ( ( int ) ( d * 255 ) );

            g.setColor( new Color( gamma * ( 256 * 256 * 256 ), true ) );
            g.fillRect( 0, 0, 10000, 10000 );
        }

        this.playerCursor.draw( g );

    }

    @Override
    public Player getPlayer( ) {
        return this.player;
    }

    @Override
    public List< Bullet > getCurrentBullets( ) {
        return this.bullets;
    }

    @Override
    public List< Pickup > getCurrentPickups( ) {
        return this.pickups;
    }

    @Override
    public List< Entity > getAllEntities( ) {
        List< Entity > ents = new LinkedList< >( );

        Iterator< Enemy > enemies = this.spawner.getEnemies( ).iterator( );

        while ( enemies.hasNext( ) ) {
            ents.add( enemies.next( ) );
        }

        ents.add( this.player );
        ents.add( this.playerCursor );

        Iterator< Bomb > bombs = this.getCurrentBombs( ).iterator( );

        while ( bombs.hasNext( ) ) {
            ents.add( bombs.next( ) );
        }

        Iterator< Bullet > bullets = this.getCurrentBullets( ).iterator( );
        while ( bullets.hasNext( ) ) {
            ents.add( bullets.next( ) );
        }

        Iterator< Pickup > pickups = this.getCurrentPickups( ).iterator( );

        while ( pickups.hasNext( ) ) {
            ents.add( pickups.next( ) );
        }

        Iterator< Player > players = this.otherPlayers.iterator( );

        while ( players.hasNext( ) ) {
            ents.add( players.next( ) );
        }

        return ents;

    }

    @Override
    public void addSpawnedEntity( SpawnedEntity e ) {

        if ( e instanceof Bomb ) {
            System.out.println( e.getClass( ).getName( ) );
            this.bombs.add( ( Bomb ) e );
        } else if ( e instanceof Bullet ) {
            SoundManager.getSoundManager( ).playSFXFromGroup( C.File.BULLET_SFX_FOLDER );
            this.bullets.add( ( Bullet ) e );
        } else if ( e instanceof Pickup ) {
            this.pickups.add( ( Pickup ) e );
        } else {
            throw new IllegalArgumentException( "Unsupported SpawnedEntity: " + e.getClass( ) );
        }

    }

    @Override
    public List< Bomb > getCurrentBombs( ) {
        return this.bombs;
    }

    public MiniMap getMiniMap( ) {
        return this.minimap;
    }

    @Override
    public void addEventToKillfeed( Entity killer, Entity victim, Entity source ) {
        String s = "";
        String s2 = "";

        boolean victimFirst = false;

        if ( source instanceof Mine ) {
            if ( killer == victim ) {
                if ( MathUtils.getDiscreteChance( 0.33 ) ) {
                    this.killfeed.add( new TimedMessage( killer.getName( ) + " failed at Minesweeper!", 90 ) );
                    // Ignore lint warnings for this second conditional,
                    // pseudo-random number generation is used in the function.
                } else if ( MathUtils.getDiscreteChance( 0.33 ) ) {
                    this.killfeed.add( new TimedMessage( killer.getName( ) + " forgot about that!", 90 ) );
                } else {
                    this.killfeed
                            .add( new TimedMessage( killer.getName( ) + " didn't see the red flashing light!", 90 ) );
                }
                return;
            }
            victimFirst = true;
            if ( MathUtils.getDiscreteChance( 0.50 ) ) {
                s = "stepped on";
                s2 = "'s mine!";
            } else {
                s = "found";
                s2 = "'s little present!";
            }
        } else if ( source instanceof Mortar ) {
            if ( killer == victim ) {
                this.killfeed.add( new TimedMessage( killer.getName( ) + " attempted a 90 degree trajectory!", 90 ) );
                return;
            }
            s = "shelled";
        } else if ( source instanceof Bomb ) {
            if ( killer == victim ) {
                this.killfeed.add(
                        new TimedMessage( killer.getName( ) + " learned a valuable lesson about explosives!", 90 ) );
                return;
            }
            s = "bombed";
        }

        if ( source instanceof Bullet ) {
            if ( killer == victim ) {
                this.killfeed.add( new TimedMessage( killer.getName( ) + " pointed a gun the wrong way!", 90 ) );
                return;
            }
            s = "shot";
        }
        if ( source instanceof Dub ) {
            s = "slapped";
        }
        if ( source instanceof Bomber ) {
            s = "smacked";
        }
        if ( source instanceof Dasher ) {
            s = "clawwed";
        }
        if ( source instanceof Ghost ) {
            s = "haunted";
        }

        if ( victimFirst ) {
            this.killfeed.add( new TimedMessage( victim.getName( ) + " " + s + " " + killer.getName( ) + s2, 150 ) );
        } else {
            this.killfeed.add( new TimedMessage( killer.getName( ) + " " + s + " " + victim.getName( ) + s2, 150 ) );
        }
    }

    public SpawnManager getSpawner( ) {
        return this.spawner;
    }

    public void setDifficulty( double diff ) {
        this.spawner.setDifficulty( diff );
    }

    @Override
    public List< Enemy > getCurrentEnemies( ) {
        if ( this.spawner != null ) { return this.spawner.getEnemies( ); }

        // Return an empty list.
        return new ArrayList< >( );
    }

    public WeaponSelectMenu getPlayerWeaponSelect( ) {
        return this.weaponSelect;
    }

    public PlayerManager getPlayerManager( ) {
        return this.player.getPlayerManager( );
    }

    public PlayerCursor getPlayerCursor( ) {
        return this.playerCursor;
    }

    public boolean isGamePaused( ) {
        return gamePaused;
    }

    public void setGamePaused( boolean gamePaused ) {
        if ( gamePaused ) {
            this.player.getPlayerManager( ).stopPlayerTimeClock( );
        } else {
            this.player.getPlayerManager( ).startPlayerTimeClock( );
        }
        this.gamePaused = gamePaused;
    }

    public boolean canDisplayDebugRotationData( ) {
        return this.displayDebugRotationData;
    }

    public boolean isTextureSmoothingEnabled( ) {
        return this.textureSmoothingEnabled;
    }

    public boolean canDisplayBoundingBoxes( ) {
        return this.displayBoundingBoxes;
    }

    public void onResize( int nWidth, int nHeight, boolean fullscreen ) {
        if ( !fullscreen ) {

            this.player.getHealthBar( ).setY( nHeight - 100 );
            this.player.getMagicBar( ).setY( nHeight - 80 );
            this.player.getStaminaBar( ).setY( nHeight - 60 );

            this.player.getXPBar( ).setWidth( nWidth / 3 );
            this.player.getXPBar( ).setHeight( 20 );
            this.player.getXPBar( ).setX( nWidth / 2 - ( this.player.getXPBar( ).getWidth( ) / 2 ) );
            this.player.getXPBar( ).setY( 20 );

            this.player.setScreenBounds( nWidth, nHeight );

            this.weaponSelect.setY( nHeight - 200 );
            this.minimap.setX( nWidth - ( this.minimap.getWidth( ) + 40 ) );

        } else {

            this.player.getHealthBar( ).setY( nHeight - 70 );
            this.player.getMagicBar( ).setY( nHeight - 50 );
            this.player.getStaminaBar( ).setY( nHeight - 30 );

            this.player.getXPBar( ).setWidth( nWidth / 3 );
            this.player.getXPBar( ).setHeight( 20 );
            this.player.getXPBar( ).setX( nWidth / 2 - ( this.player.getXPBar( ).getWidth( ) / 2 ) );
            this.player.getXPBar( ).setY( 20 );

            this.player.setScreenBounds( nWidth, nHeight );

            this.weaponSelect.setY( nHeight - 160 );
            this.minimap.setX( nWidth - ( this.minimap.getWidth( ) + 20 ) );

        }
    }

    public void onDestroy( ) {
        this.spawner.onDestroy( );
        this.player.getPlayerManager( ).addXPToProfile( this.player.getScore( ) );
        this.player.resetScore( );
        this.player.getPlayerManager( ).save( );

        this.player.getPlayerManager( ).setFinished( );
    }

    @Override
    public void setPlayer( Player p ) {
        this.player = p;
    }

    @Override
    public void addEnemy( Enemy e ) {
        if ( this.spawner != null ) {
            this.spawner.getEnemies( ).add( e );
        }
    }

}

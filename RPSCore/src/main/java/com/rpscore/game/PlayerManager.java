/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

package com.rpscore.game;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import com.rpscore.C;
import com.rpscore.cfg.ConfigManager;
import com.rpscore.debug.Log;
import com.rpscore.entities.enemies.Bomber;
import com.rpscore.entities.enemies.Dasher;
import com.rpscore.entities.enemies.Diffuser;
import com.rpscore.entities.enemies.Dub;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.entities.enemies.Ghost;
import com.rpscore.entities.enemies.MadBomber;
import com.rpscore.entities.enemies.Sharpshooter;
import com.rpscore.lib.FileUtils;
import com.rpscore.lib.MathUtils;
import com.rpscore.lib.Updateable;

public class PlayerManager implements Updateable {

	public static final int			BASE_LEVEL_XP				= 500;
	public static final double		LEVEL_INCEASE_PERCENTAGE	= 0.10;

	private static final int		USERNAME_LINE				= 0;
	private static final int		PASSWORD_LINE				= 1;

	/* @formatter:off */
	private static final String[] DEFAULT_FILE_CONTENT = {
		"Name=dummy",
		"PasswordHash=0",
		"XP=0",
		"HighestWaveReached=0",
		"HighestWaveReachedDifficulty=0",
		"TotalKills=0",
		"TotalDeaths=0",
		"RoundsStarted=0",
		"RoundsCompleted=0",
		"RoundsLost=0",
		"DifficultyPlayedAt=0.0",
		"TimePlayed=0",
		"DubsKilled=0",
		"DashersKilled=0",
		"BombersKilled=0",
		"GhostsKilled=0",
		"SharpshootersKilled=0",
		"DiffusersKilled=0",
		"MadBombersKilled=0",
		"BombsShot=0",
		"BombsShotInAir=0",
		"BombsExploded=0",
		"DubDeaths=0",
		"DasherDeaths=0",
		"BomberDeaths=0",
		"GhostDeaths=0",
		"SharpshooterDeaths=0",
		"DiffuserDeaths=0",
		"MadBomberDeaths=0",
		"BombDeaths=0",
		"BombSuicides=0",
		"Health=100",
		"Magic=100",
		"Stamina=100",
		"ArmorRating=25",
		"BombPower=100",
		"BombThrow=200",
		"BombCost=20",
		"BulletDuration=30",
		"BulletDamage=10",
		"BulletSpeed=25",
		"BulletCooldown=21",
		"RemoteBombs=false",
		"Landmines=false",
		"FusionBombs=false",
		"SniperRifle=false",
		"SpectreRifle=false",
		"BigBombs=false",
		"Mortars=false",
		"SpectralAnalyzer=false"
	};
	/* @formatter:on */

	private static PlayerManager	pgm;

	public static PlayerManager getPlayerManager( String saveFolder, boolean refresh, long key ) {

		if ( pgm == null || refresh ) {
			if ( pgm != null ) {
				pgm.setFinished( );
			}
			pgm = new PlayerManager( saveFolder, refresh, key );
			return pgm;
		} else {}

		pgm.reset( );

		return pgm;

	}

	/**
	 * Returns the current instance of a player manager, this instance can be
	 * null or incorrect. Use this only after calling the getPlayerManager
	 * method.
	 *
	 * @return
	 */
	public static PlayerManager getPlayerManagerInstance( ) {
		return pgm;
	}

	public static void createNewFile( File f ) {
		if ( !f.exists( ) ) {
			try {
				f.createNewFile( );
			} catch ( IOException e ) {
				e.printStackTrace( );
			}
		}

	}

	public static void generateNewProfile( String name, String password ) {

		Log.info( "Creating new user: " + name );

		// TODO add a utility for adding passwords
		long pw = password.isEmpty( ) ? 0L : MathUtils.hash64( password );

		String[ ] content = Arrays.copyOf( DEFAULT_FILE_CONTENT, DEFAULT_FILE_CONTENT.length );

		content[ USERNAME_LINE ] = "Name=" + name;
		content[ PASSWORD_LINE ] = "PasswordHash=" + pw;

		String folderName = deriveFolderName( name );

		Log.info( "Creating new save folder:  ../" + folderName );

		String gameSaveFolderURL = C.File.PLAYER_SAVEDATA_FOLDER;

		gameSaveFolderURL += ( folderName + C.File.FS );

		File f = new File( gameSaveFolderURL );

		if ( f.exists( ) ) {
			Log.error( "Save folder already exists: " + gameSaveFolderURL );
			return;
		}

		f.mkdirs( );

		File playerData = new File( gameSaveFolderURL + "player.dat" );
		try {
			if ( !playerData.createNewFile( ) ) {
				Log.error( "Failed to create new save file: " + playerData.getAbsolutePath( ) );
				return;
			}
		} catch ( IOException e ) {

		}

		byte[ ] encryptedContents = MathUtils.encryptFile( content, pw );
		FileUtils.writeRawBytesToFile( playerData, encryptedContents );

		/**
		 * if ( pw != 0L ) { byte[ ] encryptedContents = MathUtils.encryptFile(
		 * content, pw ); FileUtils.writeRawBytesToFile( playerData,
		 * encryptedContents ); } else { FileUtils.writeLinesToFile( playerData,
		 * content ); }
		 *
		 **/
		Log.info( "Creation of player complete: " + name );

	}

	/**
	 *
	 * Returns a string that can be used as a folder. Any returned values will
	 * only contain characters a-z, A-Z, and 0-9. Underscores will be returned,
	 * however they will not be leading or trailing the string (i.e. "Test_123"
	 * is valid, but "_Test_123" would not be returned).
	 *
	 * @param name
	 *            String to set as a folder name.
	 * @return A string suitable to make into a folder.
	 */
	public static String deriveFolderName( String name ) {

		String foldername = name.replaceAll( " ", "_" );

		StringBuilder sb = new StringBuilder( );
		for ( int i = 0; i < foldername.length( ); i++ ) {

			char c = foldername.charAt( i );

			if ( ( c >= '0' && c <= '9' ) || ( c >= 'a' && c <= 'z' ) || ( c >= 'A' && c <= 'Z' ) || ( c == '_' ) ) {
				sb.append( c );
			}
		}

		while ( sb.charAt( 0 ) == '_' ) {
			sb.deleteCharAt( 0 );
		}

		while ( sb.charAt( sb.length( ) - 1 ) == '_' ) {
			sb.deleteCharAt( sb.length( ) - 1 );
		}

		return sb.toString( );

	}

	public int getRemainingTokens( ) {

		int usedTokens = 0;

		usedTokens += ( this.Health - 100 ) / 20;
		usedTokens += ( this.Magic - 100 ) / 20;
		usedTokens += ( this.Stamina - 100 ) / 20;

		usedTokens += ( this.BombPower - 100 ) / 100;
		usedTokens += ( this.BombThrow - 200 ) / 100;

		// usedTokens += ( this.BulletSpeed - 25 ) / 5;
		usedTokens += ( this.BulletDamage - 10 ) / 5;
		usedTokens += ( this.BulletDuration - 30 ) / 10;
		usedTokens += ( 21 - this.BulletCooldown ) / 3;

		usedTokens += ( this.Mortars ) ? 3 : 0;

		usedTokens += RemoteBombs ? 3 : 0;
		usedTokens += Landmines ? 4 : 0;
		usedTokens += FusionBombs ? 3 : 0;
		usedTokens += SniperRifle ? 4 : 0;
		usedTokens += SpectreRifle ? 4 : 0;
		usedTokens += SpectralAnalyzer ? 2 : 0;

		return ( this.getLevel( ) + 2 ) - usedTokens;

	}

	public static PlayerManager getSessionPlayerManager( ) {
		return pgm;
	}

	// Object begin
	private int				xp;

	private long			timePlayed, startTime;
	private boolean			clockRunning;

	// [General Stats]
	private int				HighestWaveReached;
	private double			HighestWaveReachedDifficulty, difficultyPlayedAt;

	private int				TotalKills, DubsKilled, DashersKilled, BombersKilled, GhostsKilled, SharpshootersKilled,
			DiffusersKilled, MadBombersKilled, BombsShotInAir, BombsShot, BombsExploded, roundsPlayed, roundsStarted,
			roundsLost;

	private int				TotalDeaths, DubDeaths, DasherDeaths, BomberDeaths, GhostDeaths, SharpshooterDeaths,
			DiffuserDeaths, MadBomberDeaths, BombDeaths, BombSuicides;

	// [Perks - Stats]
	private int				Health, Magic, Stamina, armorRating;

	// [Perks - General]
	private int				BombPower, BombThrow, BombCost, BulletDuration, BulletSpeed, BulletCooldown, BulletDamage;

	// [Perks - Weapon]
	private boolean			RemoteBombs, Landmines, FusionBombs, SniperRifle, SpectreRifle, Mortars;

	// [Perks-Accessories]
	boolean					SpectralAnalyzer;

	private String			name;

	private boolean			isFinished;

	private ConfigManager	sm;

	public byte[ ] createStatsTransmission( ) {
		byte[ ] ret = new byte[ 16 + 1 + getName( ).length( ) ];

		byte[ ] stats = MathUtils.convertIntsToByteArray( this.getHealth( ), this.getMagic( ), this.getStamina( ),
				this.getLevel( ) );

		for ( int i = 0; i < stats.length; i++ ) {
			ret[ i ] = stats[ i ];
		}

		ret[ 16 ] = ( byte ) getName( ).length( );

		for ( int i = 17; i < ret.length; i++ ) {
			char c = this.getName( ).charAt( i - 17 );

			ret[ i ] = ( byte ) ( ( ( short ) c ) & 0x00FF );

		}

		return ret;
	}

	public void incrementRoundsLost( ) {
		this.roundsLost++;
	}

	public void setRoundsLost( int s ) {
		this.roundsLost = s;
	}

	public int getRoundsLost( ) {
		return this.roundsLost;
	}

	public void startPlayerTimeClock( ) {

		if ( !this.clockRunning ) {
			this.startTime = System.currentTimeMillis( );
			this.clockRunning = true;
		}

	}

	public void stopPlayerTimeClock( ) {
		if ( !this.clockRunning ) {
			return;
		}
		this.timePlayed += System.currentTimeMillis( ) - startTime;
		this.clockRunning = false;
	}

	public long getTImePlayed( ) {
		return this.timePlayed;
	}

	public double getDifficultyPlayedAt( ) {
		return this.difficultyPlayedAt;
	}

	public void concatenateDifficultyPlayedAt( double difficulty ) {

		double delta = difficulty - getDifficultyPlayedAt( );

		System.out.println( delta );

		// reduce the significance of each game based on how many games have
		// been played.

		int rp = getRoundsPlayed( );

		if ( rp < 1 ) {
			rp = 1;
		}

		delta /= rp;

		this.difficultyPlayedAt += delta;

	}

	public void incrementRoundsStarted( ) {
		this.roundsStarted++;
	}

	public void setRoundsStarted( int s ) {
		this.roundsStarted = s;
	}

	public int getRoundsStarted( ) {
		return this.roundsStarted;
	}

	public void incrementRoundsPlayed( ) {
		this.roundsPlayed++;
	}

	public void setRoundsPlayed( int s ) {
		this.roundsPlayed = s;
	}

	public int getRoundsPlayed( ) {
		return this.roundsPlayed;
	}

	public double getWLR( ) {
		int rs = getRoundsLost( );
		int rp = getRoundsPlayed( );

		if ( rs == 0 ) {
			rs++;
		}

		return ( ( double ) rp / ( double ) rs );
	}

	public int getDiscreteArmorRating( ) {
		return this.armorRating;
	}

	public double getArmorRating( ) {
		double d = this.armorRating;
		return d / 100.0;
	}

	public void setArmorRating( int newAR ) {
		this.armorRating = newAR;
	}

	public double getMovementSpeed( ) {
		return ( ( 1.0 - getArmorRating( ) ) * 7.0 ) + 1;
	}

	public int getRank( ) {

		double kdr = this.getKDR( ) / 2.0;

		double wlr = getWLR( );

		int wins = getRoundsPlayed( ) * 12;
		int kills = getTotalKills( ) / 8;

		double dpa = getDifficultyPlayedAt( );

		if ( dpa <= 0.0 ) {
			dpa = 1.0;
		}

		int skillLevel = ( int ) ( ( ( wlr * wins ) + ( kills * kdr ) ) * dpa );

		if ( skillLevel < 0 ) {
			skillLevel = 0;
		} else if ( skillLevel > 10000 ) {
			skillLevel = 10000;
		}

		return skillLevel;

	}

	public double getSkillLevel( ) {

		int skillLevel = getRank( );

		// Copy the DL stat system
		if ( skillLevel >= 0 && skillLevel < 200 ) {
			return 1.0 + ( ( double ) skillLevel / 200 );
		} else if ( skillLevel >= 200 && skillLevel < 800 ) {
			return 2.0 + ( ( double ) ( skillLevel - 200 ) / 600 );
		} else if ( skillLevel >= 800 && skillLevel < 1600 ) {
			return 3.0 + ( ( double ) ( skillLevel - 800 ) / 800 );
		} else if ( skillLevel >= 1600 && skillLevel < 2500 ) {
			return 4.0 + ( ( double ) ( skillLevel - 1600 ) / 900 );
		} else if ( skillLevel >= 2500 && skillLevel < 3500 ) {
			return 5.0 + ( ( double ) ( skillLevel - 2500 ) / 1000 );
		} else if ( skillLevel >= 3500 && skillLevel < 5000 ) {
			return 6.0 + ( ( double ) ( skillLevel - 3500 ) / 1500 );
		} else if ( skillLevel >= 5000 && skillLevel < 6500 ) {
			return 7.0 + ( ( double ) ( skillLevel - 5000 ) / 1500 );
		} else if ( skillLevel >= 6500 && skillLevel < 8000 ) {
			return 8.0 + ( ( double ) ( skillLevel - 6500 ) / 1500 );
		} else if ( skillLevel >= 8000 && skillLevel < 9500 ) {
			return 9.0 + ( ( double ) ( skillLevel - 8000 ) / 1500 );
		} else if ( skillLevel >= 9500 ) {
			return 10.00;
		}

		return 1.00;

	}

	public double getKDR( ) {
		// If the player has no deaths, the KD cannot be infinite.
		if ( this.TotalDeaths < 1 ) {
			return this.TotalKills;
		}
		return ( double ) this.TotalKills / ( double ) this.TotalDeaths;
	}

	private String hostFile;

	private PlayerManager( String file, boolean refresh, long key ) {

		this.hostFile = file;

		this.isFinished = false;

		this.clockRunning = false;

		this.importFromFile( new File( file ), refresh, key );

	}

	@Deprecated
	public void reset( ) {

	}

	public int getXP( ) {
		return this.xp;
	}

	public void addXPToProfile( int sessionXP ) {
		this.xp += sessionXP;
	}

	public void incrementKillOfEnemy( Enemy e ) {

		incrementTotalKills( 1 );

		if ( e instanceof MadBomber ) {
			incrementMadBombersKilled( 1 );
		} else if ( e instanceof Bomber ) {
			incrementBombersKilled( 1 );
		} else if ( e instanceof Dasher ) {
			incrementDashersKilled( 1 );
		} else if ( e instanceof Diffuser ) {
			incrementDiffusersKilled( 1 );
		} else if ( e instanceof Dub ) {
			incrementDubsKilled( 1 );
		} else if ( e instanceof Ghost ) {
			incrementGhostsKilled( 1 );
		} else if ( e instanceof Sharpshooter ) {
			incrementSharpshootersKilled( 1 );
		}
	}

	public void incrementDeathByEnemy( Enemy e ) {
		incrementTotalDeaths( 1 );

		if ( e instanceof MadBomber ) {
			incrementMadBomberDeaths( 1 );
		} else if ( e instanceof Bomber ) {
			incrementBomberDeaths( 1 );
		} else if ( e instanceof Dasher ) {
			incrementDasherDeaths( 1 );
		} else if ( e instanceof Diffuser ) {
			incrementDiffuserDeaths( 1 );
		} else if ( e instanceof Dub ) {
			incrementDubDeaths( 1 );
		} else if ( e instanceof Ghost ) {
			incrementGhostDeaths( 1 );
		} else if ( e instanceof Sharpshooter ) {
			incrementSharpshooterDeaths( 1 );
		}
	}

	public static int getXPRequiredForLevel( int level ) {
		if ( level < 0 ) {
			throw new IllegalArgumentException( "Level canot be negative or 0" );
		}
		if ( level == 0 ) {
			return 0;
		}
		if ( level == 1 ) {
			return BASE_LEVEL_XP;
		}
		int lastlvl = getXPRequiredForLevel( level - 1 );
		return lastlvl + ( ( int ) ( lastlvl * LEVEL_INCEASE_PERCENTAGE ) );
	}

	public static int getTotalXPRequiredForLevel( int level ) {
		int cnt = 0;
		while ( level > 0 ) {
			cnt += getXPRequiredForLevel( level );
			level--;
		}
		return cnt;
	}

	public int getLevel( ) {
		int cnt = 0;
		int temp = 0;
		while ( ( temp += getXPRequiredForLevel( cnt ) ) <= this.xp ) {
			cnt++;
		}
		return cnt - 1;
	}

	public int getXPRequiredForNextLevel( ) {
		return getXPRequiredForLevel( this.getLevel( ) + 1 );
	}

	public int getRemainingLevelXP( ) {
		int lvl = this.getLevel( );
		int nlvlxp = getXPRequiredForLevel( lvl + 1 );

		return nlvlxp - this.getLevelProgress( );
	}

	public int getLevelProgress( ) {
		int lvl = this.getLevel( );
		int lvlxp = getTotalXPRequiredForLevel( lvl );

		return this.xp - lvlxp;
	}

	public String getName( ) {
		return this.name;
	}

	public void setName( String n ) {
		this.name = n;
	}

	private void importFromFile( File f, boolean refresh, long key ) {
		this.sm = ConfigManager.getPlayerDataHolder( f, refresh, key );
		update( );
	}

	@Override
	public void update( ) {
		if ( sm != null ) {

			this.xp = this.sm.getSetting( "XP", 0 );

			this.HighestWaveReached = this.sm.getSetting( "HighestWaveReached", 0 );
			this.HighestWaveReachedDifficulty = this.sm.getSetting( "HighestWaveReachedDifficulty", 0.0 );
			this.roundsStarted = this.sm.getSetting( "RoundsStarted", 0 );
			this.roundsPlayed = this.sm.getSetting( "RoundsCompleted", 0 );
			this.difficultyPlayedAt = this.sm.getSetting( "DifficultyPlayedAt", 0.0 );
			this.timePlayed = this.sm.getSetting( "TimePlayed", 0L );
			this.roundsLost = this.sm.getSetting( "RoundsLost", 0 );

			this.TotalKills = this.sm.getSetting( "TotalKills", 0 );
			this.DubsKilled = this.sm.getSetting( "DubsKilled", 0 );
			this.DashersKilled = this.sm.getSetting( "DashersKilled", 0 );
			this.BombersKilled = this.sm.getSetting( "BombersKilled", 0 );
			this.GhostsKilled = this.sm.getSetting( "GhostsKilled", 0 );
			this.SharpshootersKilled = this.sm.getSetting( "SharpshootersKilled", 0 );
			this.DiffusersKilled = this.sm.getSetting( "DiffusersKilled", 0 );
			this.MadBombersKilled = this.sm.getSetting( "MadBombersKilled", 0 );
			this.BombsShotInAir = this.sm.getSetting( "BombsShotInAir", 0 );
			this.BombsShot = this.sm.getSetting( "BombsShot", 0 );
			this.BombsExploded = this.sm.getSetting( "BombsExploded", 0 );

			this.TotalDeaths = this.sm.getSetting( "TotalDeaths", 0 );
			this.DubDeaths = this.sm.getSetting( "DubDeaths", 0 );
			this.DasherDeaths = this.sm.getSetting( "DasherDeaths", 0 );
			this.BomberDeaths = this.sm.getSetting( "BomberDeaths", 0 );
			this.GhostDeaths = this.sm.getSetting( "GhostDeaths", 0 );
			this.SharpshooterDeaths = this.sm.getSetting( "SharpshooterDeaths", 0 );
			this.DiffuserDeaths = this.sm.getSetting( "DiffuserDeaths", 0 );
			this.MadBomberDeaths = this.sm.getSetting( "MadBomberDeaths", 0 );
			this.BombDeaths = this.sm.getSetting( "BombDeaths", 0 );
			this.BombSuicides = this.sm.getSetting( "BombSuicides", 0 );

			this.Health = this.sm.getSetting( "Health", 100 );
			this.Magic = this.sm.getSetting( "Magic", 100 );
			this.Stamina = this.sm.getSetting( "Stamina", 100 );
			this.armorRating = this.sm.getSetting( "ArmorRating", 50 );

			this.BombPower = this.sm.getSetting( "BombPower", 100 );
			this.BombThrow = this.sm.getSetting( "BombThrow", 200 );
			this.BombCost = this.sm.getSetting( "BombCost", 20 );
			this.BulletDuration = this.sm.getSetting( "BulletDuration", 40 );
			this.BulletSpeed = this.sm.getSetting( "BulletSpeed", 25 );
			this.BulletCooldown = this.sm.getSetting( "BulletCooldown", 15 );
			this.BulletDamage = this.sm.getSetting( "BulletDamage", 10 );

			this.RemoteBombs = this.sm.getSetting( "RemoteBombs", false );
			this.Landmines = this.sm.getSetting( "Landmines", false );
			this.FusionBombs = this.sm.getSetting( "FusionBombs", false );
			this.SniperRifle = this.sm.getSetting( "SniperRifle", false );
			this.SpectreRifle = this.sm.getSetting( "SpectreRifle", false );
			this.SpectralAnalyzer = this.sm.getSetting( "SpectralAnalyzer", false );
			this.Mortars = this.sm.getSetting( "Mortars", false );

			this.name = this.sm.getSetting( "Name", "" );

			if ( this.name.isEmpty( ) ) {
				throw new IllegalArgumentException( "Bad User File" );
			}

		}
	}

	@Override
	public boolean isFinished( ) {
		return this.isFinished;
	}

	public void setFinished( ) {
		this.isFinished = true;
	}

	public void save( ) {

		if ( this.sm != null ) {
			this.sm.modifySetting( "XP", "" + this.xp );

			this.sm.modifySetting( "HighestWaveReached", "" + this.HighestWaveReached );
			this.sm.modifySetting( "HighestWaveReachedDifficulty", "" + this.HighestWaveReachedDifficulty );
			this.sm.modifySetting( "RoundsStarted", "" + this.roundsStarted );
			this.sm.modifySetting( "RoundsCompleted", "" + this.roundsPlayed );
			this.sm.modifySetting( "DifficultyPlayedAt", "" + this.difficultyPlayedAt );
			this.sm.modifySetting( "TimePlayed", "" + this.timePlayed );
			this.sm.modifySetting( "RoundsLost", "" + this.roundsLost );

			this.sm.modifySetting( "TotalKills", "" + this.TotalKills );
			this.sm.modifySetting( "DubsKilled", "" + this.DubsKilled );
			this.sm.modifySetting( "DashersKilled", "" + this.DashersKilled );
			this.sm.modifySetting( "BombersKilled", "" + this.BombersKilled );
			this.sm.modifySetting( "GhostsKilled", "" + this.GhostsKilled );
			this.sm.modifySetting( "SharpshootersKilled", "" + this.SharpshootersKilled );
			this.sm.modifySetting( "DiffusersKilled", "" + this.DiffusersKilled );
			this.sm.modifySetting( "MadBombersKilled", "" + this.MadBombersKilled );
			this.sm.modifySetting( "BombsShotInAir", "" + this.BombsShotInAir );
			this.sm.modifySetting( "BombsShot", "" + this.BombsShot );
			this.sm.modifySetting( "BombsExploded", "" + this.BombsExploded );

			this.sm.modifySetting( "TotalDeaths", "" + this.TotalDeaths );
			this.sm.modifySetting( "DubDeaths", "" + this.DubDeaths );
			this.sm.modifySetting( "DasherDeaths", "" + this.DasherDeaths );
			this.sm.modifySetting( "BomberDeaths", "" + this.BomberDeaths );
			this.sm.modifySetting( "GhostDeaths", "" + this.GhostDeaths );
			this.sm.modifySetting( "SharpshooterDeaths", "" + this.SharpshooterDeaths );
			this.sm.modifySetting( "DiffuserDeaths", "" + this.DiffuserDeaths );
			this.sm.modifySetting( "MadBomberDeaths", "" + this.MadBomberDeaths );
			this.sm.modifySetting( "BombDeaths", "" + this.BombDeaths );
			this.sm.modifySetting( "BombSuicides", "" + this.BombSuicides );

			this.sm.modifySetting( "Health", "" + this.Health );
			this.sm.modifySetting( "Magic", "" + this.Magic );
			this.sm.modifySetting( "Stamina", "" + this.Stamina );
			this.sm.modifySetting( "ArmorRating", "" + this.armorRating );

			this.sm.modifySetting( "BombPower", "" + this.BombPower );
			this.sm.modifySetting( "BombThrow", "" + this.BombThrow );
			this.sm.modifySetting( "BombCost", "" + this.BombCost );
			this.sm.modifySetting( "BulletDuration", "" + this.BulletDuration );
			this.sm.modifySetting( "BulletSpeed", "" + this.BulletSpeed );
			this.sm.modifySetting( "BulletCooldown", "" + this.BulletCooldown );
			this.sm.modifySetting( "BulletDamage", "" + this.BulletDamage );

			this.sm.modifySetting( "RemoteBombs", "" + this.RemoteBombs );
			this.sm.modifySetting( "Landmines", "" + this.Landmines );
			this.sm.modifySetting( "FusionBombs", "" + this.FusionBombs );
			this.sm.modifySetting( "SniperRifle", "" + this.SniperRifle );
			this.sm.modifySetting( "SpectreRifle", "" + this.SpectreRifle );
			this.sm.modifySetting( "SpectralAnalyzer", "" + this.SpectralAnalyzer );
			this.sm.modifySetting( "Mortars", "" + this.Mortars );

			this.sm.modifySetting( "Name", this.name );

			String[ ] fileLines = this.sm.getFullConfigState( );

			byte[ ] raw_data = MathUtils.encryptFile( fileLines, this.sm.getSetting( "PasswordHash", 0L ) );

			FileUtils.writeRawBytesToFile( new File( this.hostFile + C.File.FS + C.File.PLAYER_SAVE_FILE_NAME ),
					raw_data );

		}
	}

	@Deprecated
	public ConfigManager getUnderlying( ) {
		return this.sm;
	}

	public void getPlayerAccuracy( ) {

	}

	public long getPasswordHash( ) {
		return this.sm.getSetting( "PasswordHash", 0L );
	}

	public boolean hasMortars( ) {
		return this.Mortars;
	}

	public void unlockMortars( boolean m ) {
		this.Mortars = m;
	}

	public int getHighestWaveReached( ) {
		return HighestWaveReached;
	}

	public void setHighestWaveReached( int highestWaveReached ) {
		HighestWaveReached = highestWaveReached;
	}

	public double getHighestWaveReachedDifficulty( ) {
		return HighestWaveReachedDifficulty;
	}

	public void setHighestWaveReachedDifficulty( double highestWaveReachedDifficulty ) {
		HighestWaveReachedDifficulty = highestWaveReachedDifficulty;
	}

	public int getTotalKills( ) {
		return TotalKills;
	}

	public void incrementTotalKills( int totalKills ) {
		TotalKills += totalKills;
	}

	public int getDubsKilled( ) {
		return DubsKilled;
	}

	public void incrementDubsKilled( int dubsKilled ) {
		DubsKilled += dubsKilled;
	}

	public int getDashersKilled( ) {
		return DashersKilled;
	}

	public void incrementDashersKilled( int dashersKilled ) {
		DashersKilled += dashersKilled;
	}

	public int getBombersKilled( ) {
		return BombersKilled;
	}

	public void incrementBombersKilled( int bombersKilled ) {
		BombersKilled += bombersKilled;
	}

	public int getGhostsKilled( ) {
		return GhostsKilled;
	}

	public void incrementGhostsKilled( int ghostsKilled ) {
		GhostsKilled += ghostsKilled;
	}

	public int getSharpshootersKilled( ) {
		return SharpshootersKilled;
	}

	public void incrementSharpshootersKilled( int sharpshootersKilled ) {
		SharpshootersKilled += sharpshootersKilled;
	}

	public int getDiffusersKilled( ) {
		return DiffusersKilled;
	}

	public void incrementDiffusersKilled( int diffusersKilled ) {
		DiffusersKilled += diffusersKilled;
	}

	public int getMadBombersKilled( ) {
		return MadBombersKilled;
	}

	public void incrementMadBombersKilled( int madBombersKilled ) {
		MadBombersKilled += madBombersKilled;
	}

	public int getBombsShotInAir( ) {
		return BombsShotInAir;
	}

	public void incrementBombsShotInAir( int bombsShotInAir ) {
		BombsShotInAir += bombsShotInAir;
	}

	public int getBombsShot( ) {
		return BombsShot;
	}

	public void incrementBombsShot( int bombsShot ) {
		BombsShot += bombsShot;
	}

	public int getBombsExploded( ) {
		return BombsExploded;
	}

	public void incrementBombsExploded( int bombsExploded ) {
		BombsExploded += bombsExploded;
	}

	public int getTotalDeaths( ) {
		return TotalDeaths;
	}

	public void incrementTotalDeaths( int totalDeaths ) {
		TotalDeaths += totalDeaths;
	}

	public int getDubDeaths( ) {
		return DubDeaths;
	}

	public void incrementDubDeaths( int dubDeaths ) {
		DubDeaths += dubDeaths;
	}

	public int getDasherDeaths( ) {
		return DasherDeaths;
	}

	public void incrementDasherDeaths( int dasherDeaths ) {
		DasherDeaths += dasherDeaths;
	}

	public int getBomberDeaths( ) {
		return BomberDeaths;
	}

	public void incrementBomberDeaths( int bomberDeaths ) {
		BomberDeaths += bomberDeaths;
	}

	public int getGhostDeaths( ) {
		return GhostDeaths;
	}

	public void incrementGhostDeaths( int ghostDeaths ) {
		GhostDeaths += ghostDeaths;
	}

	public int getSharpshooterDeaths( ) {
		return SharpshooterDeaths;
	}

	public void incrementSharpshooterDeaths( int sharpshooterDeaths ) {
		SharpshooterDeaths += sharpshooterDeaths;
	}

	public int getDiffuserDeaths( ) {
		return DiffuserDeaths;
	}

	public void incrementDiffuserDeaths( int diffuserDeaths ) {
		DiffuserDeaths += diffuserDeaths;
	}

	public int getMadBomberDeaths( ) {
		return MadBomberDeaths;
	}

	public void incrementMadBomberDeaths( int madBomberDeaths ) {
		MadBomberDeaths += madBomberDeaths;
	}

	public int getBombDeaths( ) {
		return BombDeaths;
	}

	public void incrementBombDeaths( int bombDeaths ) {
		BombDeaths += bombDeaths;
	}

	public int getBombSuicides( ) {
		return BombSuicides;
	}

	public void incrementBombSuicides( int bombSuicides ) {
		BombSuicides += bombSuicides;
	}

	public int getHealth( ) {
		return Health;
	}

	public void setHealth( int health ) {
		Health = health;
	}

	public int getMagic( ) {
		return Magic;
	}

	public void setMagic( int magic ) {
		Magic = magic;
	}

	public int getStamina( ) {
		return Stamina;
	}

	public void setStamina( int stamina ) {
		Stamina = stamina;
	}

	public int getBombPower( ) {
		return BombPower;
	}

	public void setBombPower( int bombPower ) {
		BombPower = bombPower;
	}

	public int getBulletDamage( ) {
		return BulletDamage;
	}

	public void setBulletDamage( int bulletDamage ) {
		BulletDamage = bulletDamage;
	}

	public int getBombThrow( ) {
		return BombThrow;
	}

	public void setBombThrow( int bombThrow ) {
		BombThrow = bombThrow;
	}

	public int getBombCost( ) {
		return BombCost;
	}

	public void setBombCost( int bombCost ) {
		BombCost = bombCost;
	}

	public int getBulletDuration( ) {
		return BulletDuration;
	}

	public void setBulletDuration( int bulletDuration ) {
		BulletDuration = bulletDuration;
	}

	public int getBulletSpeed( ) {
		return BulletSpeed;
	}

	public void setBulletSpeed( int bulletSpeed ) {
		BulletSpeed = bulletSpeed;
	}

	public int getBulletCooldown( ) {
		return BulletCooldown;
	}

	public void setBulletCooldown( int bulletCooldown ) {
		BulletCooldown = bulletCooldown;
	}

	public boolean hasRemoteBombs( ) {
		return RemoteBombs;
	}

	public void unlockRemoteBombs( boolean remoteBombs ) {
		RemoteBombs = remoteBombs;
	}

	public boolean hasLandmines( ) {
		return Landmines;
	}

	public void unlockLandmines( boolean landmines ) {
		Landmines = landmines;
	}

	public boolean hasFusionBombs( ) {
		return FusionBombs;
	}

	public void unlockFusionBombs( boolean fusionBombs ) {
		FusionBombs = fusionBombs;
	}

	public boolean hasSniperRifle( ) {
		return SniperRifle;
	}

	public void unlockSniperRifle( boolean sniperRifle ) {
		SniperRifle = sniperRifle;
	}

	public boolean hasSpectreRifle( ) {
		return SpectreRifle;
	}

	public void unlockSpectreRifle( boolean spectreRifle ) {
		SpectreRifle = spectreRifle;
	}

	public boolean hasSpectralAnalyzer( ) {
		return SpectralAnalyzer;
	}

	public void unlockSpectralAnalyzer( boolean spectralAnalyzer ) {
		SpectralAnalyzer = spectralAnalyzer;
	}

	public ConfigManager getSm( ) {
		return sm;
	}

	public void setSm( ConfigManager smgr ) {
		this.sm = smgr;
	}

}
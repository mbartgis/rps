/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date$
 * $Revision$
 * $Author$
 */

package com.rpscore.debug;

import com.rpscore.entities.enemies.Dub;

public class DebugUtils {

	public static final Dub DEBUG_DUB = new Dub( 100.0, 100.0 );

}
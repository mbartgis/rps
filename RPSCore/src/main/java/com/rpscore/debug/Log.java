/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date$
 * $Revision$
 * $Author$
 */

package com.rpscore.debug;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.rpscore.C;
import com.rpscore.lib.FileUtils;

public class Log {

	private static Log main_logger;

	public static Log getLogger( ) {
		if ( main_logger == null ) {
			main_logger = new Log( );
		}
		return main_logger;
	}

	public static void setLoggerPrintOut( boolean canPrint ) {
		if ( main_logger != null ) {
			main_logger.canPrint = canPrint;
		}
	}

	private Log( ) {
		messages = new ArrayList< Message >( );
		canPrint = false;
	}

	private static final Calendar			calendar						= Calendar.getInstance( );
	private static final Date				INIT_TIME						= calendar.getTime( );
	private static final SimpleDateFormat	LOG_MESSAGE_TIMESTAMP_FORMAT	= new SimpleDateFormat( "HH:mm:ss.SSS" );
	private static final SimpleDateFormat	LOG_FILE_TIMESTAMP_FORMAT		= new SimpleDateFormat(
			"YYYY-MM-dd-HH-mm-ss-SSS" );

	private enum TYPE {
		INFO, WARNING, ERROR, DEBUG;
	}

	private ArrayList< Message >	messages;
	private boolean					canPrint;

	private void addMessage( String message, TYPE message_type ) {
		Message m = new Message( message, message_type );
		messages.add( m );
		if ( this.canPrint ) {
			System.out.println( m );
		}
	}

	/**
	 * Used to log an expected event during program execution.
	 *
	 * @param s
	 *            message to log.
	 */
	public static void info( String s ) {
		Log.getLogger( ).addMessage( s, TYPE.INFO );
	}

	/**
	 * Used to log an event which is unexpected that may lead to program
	 * instability or an error.
	 *
	 * @param s
	 *            message to log.
	 */
	public static void warn( String s ) {
		Log.getLogger( ).addMessage( s, TYPE.WARNING );
	}

	/**
	 * Used to log a issue during runtime. An issue that is considered an error
	 * is one that causes the program to either crash or not perform as
	 * expected.
	 *
	 * @param s
	 *            message to log.
	 */
	public static void error( String s ) {
		Log.getLogger( ).addMessage( s, TYPE.ERROR );
	}

	/**
	 * Used to log any debug information. These will be printed to the console,
	 * but not exported to a log file. Messages added as debug text will only be
	 * added if the program is in debug mode.
	 *
	 * @param s
	 *            message to log.
	 */
	public static void debug( String s ) {
		Log.getLogger( ).addMessage( s, TYPE.DEBUG );
	}

	public void printLog( PrintStream printable_stream ) {

		if ( printable_stream == null ) {
			printable_stream = System.out;
		}

		for ( Message log_message : messages ) {
			printable_stream.println( log_message.toString( ) );
		}

	}

	public void printLogToFile( ) {

		// Remove all debug information.
		for ( int i = 0; i < messages.size( ); i++ ) {
			if ( messages.get( i ).type == TYPE.DEBUG ) {
				messages.remove( i );
				i--;
			}
		}

		String[ ] static_messages = new String[ messages.size( ) ];

		for ( int i = 0; i < static_messages.length; i++ ) {
			static_messages[ i ] = messages.get( i ).toString( );
		}

		// The release software will not contain a log folder, this block
		// ensures that a log folder is created in the event debug mode is
		// active.
		File log_dir = new File( C.File.LOGS_FOLDER );
		if ( !log_dir.exists( ) ) {
			log_dir.mkdirs( );
		}

		File fileToWrite = new File( C.File.LOGS_FOLDER + LOG_FILE_TIMESTAMP_FORMAT.format( Log.INIT_TIME ) + ".log" );
		try {
			fileToWrite.createNewFile( );
		} catch ( IOException e ) {
			e.printStackTrace( );
		}

		FileUtils.writeLinesToFile( fileToWrite, static_messages );

	}

	private class Message {

		private static final String	PREFIX_INFO		= "[INFO]  ";
		private static final String	PREFIX_WARNING	= "[WARN]  ";
		private static final String	PREFIX_ERROR	= "[ERROR] ";
		private static final String	PREFIX_DEBUG	= "[DEBUG] ";

		// The following class is all contained within the single class, to
		// preserve memory and CPU cycles, these variables will be accessed in
		// an inline fashion.
		TYPE						type;
		String						text;
		Date						message_time;

		@SuppressWarnings( "hiding" )
		public Message( String text, TYPE type ) {
			Message.this.type = type;
			Message.this.text = text;
			Message.this.message_time = Calendar.getInstance( ).getTime( );
		}

		@Override
		public String toString( ) {
			String built_text = "";
			switch ( Message.this.type ) {
				case INFO:
					built_text = PREFIX_INFO + Message.this.text;
					break;
				case WARNING:
					built_text = PREFIX_WARNING + Message.this.text;
					break;
				case ERROR:
					built_text = PREFIX_ERROR + Message.this.text;
					break;
				case DEBUG:
					built_text = PREFIX_DEBUG + Message.this.text;
					break;
				default:
					break;
			}
			String timestamp = "[" + LOG_MESSAGE_TIMESTAMP_FORMAT.format( Message.this.message_time ) + "] ";

			return timestamp + built_text;
		}

	}

}
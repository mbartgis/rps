/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-11-20 20:28:45 -0500 (Tue, 20 Nov 2018) $
 * $Revision: 370 $
 * $Author: mbartgis $
 */

package com.rpscore.terrain;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.rpscore.lib.FileUtils;
import com.rpscore.terrain.mutators.DirtCornersMutator;
import com.rpscore.terrain.mutators.DriedVolcanicRockFormationMutator;
import com.rpscore.terrain.mutators.LavaDamageMutator;
import com.rpscore.terrain.mutators.TerrainMutator;
import com.rpscore.terrain.mutators.VolcanicRockFormationMutator;

public class TerrainGenerator {

	@Deprecated
	public static Block[ ][ ] parseStaticFile( File f ) {

		int index = 0;
		String[ ] contents = FileUtils.getFileLines( f );

		Block[ ][ ] map = null;

		// get x size
		int x_size = Integer.parseInt( contents[ index ] );
		index++;

		map = new Block[ x_size ][ ];

		// get y size
		int y_size = Integer.parseInt( contents[ index ] );
		index++;

		for ( int i = 0; i < x_size; i++ ) {

			map[ i ] = new Block[ y_size ];
			for ( int j = 0; j < y_size; j++ ) {
				int blockID = Integer.parseInt( contents[ index ] );
				map[ i ][ j ] = BlockRegistry.getBlockFromID( blockID ).clone( );
				index++;
			}
		}

		return map;

	}

	@Deprecated
	@SuppressWarnings( "incomplete-switch" )
	public static Block[ ][ ] createChunk( long seed, int sizeX, int sizeY ) {

		if ( ( sizeX == 0 ) || ( sizeY == 0 ) ) {
			return null;
		}

		Block[ ][ ] chunk = new Block[ sizeX ][ ];

		for ( int i = 0; i < sizeY; i++ ) {
			chunk[ i ] = new Block[ sizeY ];
		}

		// Create a grass background
		for ( int i = 0; i < sizeX; i++ ) {
			for ( int j = 0; j < sizeY; j++ ) {
				chunk[ i ][ j ] = BlockRegistry.BLOCK_TYPE.BLOCK_GRASS;
			}
		}

		// Generate a dirt path based on seed
		// The probability is 1/32
		if ( ( seed & 0b11111 ) == 31 ) {
			// use the next 32-bits to determine the location of the path start.

			int locationx = Math.abs( ( ( int ) ( seed >> 5 ) | 0b0000000000000000 ) & 0x0000FFFF );
			locationx %= sizeX;

			int locationy = Math.abs( ( ( int ) ( seed >> 21 ) | 0b0000000000000000 ) & 0x0000FFFF );
			locationy %= sizeY;

			chunk[ locationx ][ locationy ] = BlockRegistry.BLOCK_TYPE.BLOCK_DIRT;

			int connectx = locationx;
			int connecty = locationy;

			boolean hasFinished = false;
			for ( int i = 38; !hasFinished; i++ ) {
				if ( i > 64 ) {
					i = 1;
				}

				int direction = ( int ) ( ( seed >> i ) | 0x00 );
				direction &= 0b00000000000000000000000000000011;

				switch ( direction ) {
					// UP
					case 0b00:
						connecty++;
						break;
					// DOWN
					case 0b01:
						connecty--;
						break;
					// RIGHT
					case 0b10:
						connectx++;
						break;
					// LEFT
					case 0b11:
						connectx--;
						break;
					default:
						break;
				}

				// Ensure that no AIOoB Exceptions can be thrown.
				if ( connectx < 0 ) {
					connectx = 0;
				}

				if ( connectx >= chunk.length ) {
					connectx = chunk.length - 1;
				}

				if ( connecty < 0 ) {
					connecty = 0;
				}

				if ( connecty >= chunk[ 0 ].length ) {
					connecty = chunk[ 0 ].length - 1;
				}

				chunk[ connectx ][ connecty ] = BlockRegistry.BLOCK_TYPE.BLOCK_DIRT;

				if ( ( connectx == 0 ) || ( connecty == 0 ) || ( connectx == ( sizeX - 1 ) )
						|| ( connecty == ( sizeY - 1 ) ) ) {
					hasFinished = true;
				}
			}

		}

		return chunk;

	}

	private static List< TerrainMutator > mutators;

	static {
		mutators = new LinkedList< >( );

		mutators.add( new LavaDamageMutator( ) );
		mutators.add( new VolcanicRockFormationMutator( ) );
		mutators.add( new DriedVolcanicRockFormationMutator( ) );
		mutators.add( new DirtCornersMutator( ) );
	}

	public static void resetTerrainMutators( TerrainMutator[ ] tm ) {
		mutators.clear( );
		mutators = Arrays.stream( tm ).collect( Collectors.toList( ) );
	}

	public static Map generateMap( int size, long seed ) {

		double[ ][ ] map = FogGenerator.diamondSquare( size, seed );

		Map m = Map.createMapFromDoubleMatrix( map, size );

		Block[ ][ ] blocks = m.getUnderlying( );

		mutateMap( blocks );

		return m;

	}

	public static void mutateMap( Block[ ][ ] blocks ) {
		for ( int i = 0; i < blocks.length; i++ ) {
			for ( int j = 0; j < blocks[ i ].length; j++ ) {

				List< Block > adj = new ArrayList< >( 4 );
				List< Block > diags = new ArrayList< >( 4 );

				// Note: this logic is optimized on the assumption that there
				// will always be at least a 3x3 square grid of blocks.

				int start = 0;
				int end = blocks.length - 1;

				if ( i > start ) {
					adj.add( blocks[ i - 1 ][ j ] );
					if ( j > start ) {
						adj.add( blocks[ i ][ j - 1 ] );
						diags.add( blocks[ i - 1 ][ j - 1 ] );
					}
					if ( j < end ) {
						diags.add( blocks[ i - 1 ][ j + 1 ] );
						adj.add( blocks[ i ][ j + 1 ] );
					}
				} else {
					if ( j > start ) {
						adj.add( blocks[ i ][ j - 1 ] );
					}
					if ( j < end ) {
						adj.add( blocks[ i ][ j + 1 ] );
					}
				}

				if ( i < end ) {
					adj.add( blocks[ i + 1 ][ j ] );
					if ( j < end ) {
						diags.add( blocks[ i + 1 ][ j + 1 ] );
					}
					if ( j > start ) {
						diags.add( blocks[ i + 1 ][ j - 1 ] );
					}
				}

				for ( TerrainMutator mut : mutators ) {
					mut.apply( blocks, i, j, adj.stream( ).toArray( Block[ ]::new ),
							diags.stream( ).toArray( Block[ ]::new ) );
				}
			}
		}
	}

	private static void applyLavaDamage( Block[ ][ ] blocks, int i, int j ) {
		// TOP LEFT
		if ( !blocks[ i ][ j ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_LAVA ) ) {
			return;
		}

		if ( ( i > 0 && j > 0 ) && ( i < ( blocks.length - 1 ) && j < ( blocks[ i ].length - 1 ) ) ) {
			if ( blocks[ i - 1 ][ j ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_LAVA )
					&& blocks[ i + 1 ][ j ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_LAVA )
					&& blocks[ i ][ j - 1 ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_LAVA )
					&& blocks[ i ][ j + 1 ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_LAVA ) ) {

				blocks[ i ][ j ].setTemperature( Block.PROPERTIES.TEMPERATURE.MAGMA );

			}
		}

		// TOP RIGHT

		// BOTTOM LEFT

		// BOTTOM RIGHT

		// TOP

		// LEFT

		// RIGHT

		// BOTTOM

		// EVERYTHING ELSE
	}

	// If there is water next to lava, make some volcanic rock.
	private static void applyVolcanicRock( Block[ ][ ] blocks, int i, int j ) {
		if ( blocks[ i ][ j ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_LAVA ) ) {
			if ( ( i > 0 && j > 0 ) && ( i < ( blocks.length - 1 ) && j < ( blocks[ i ].length - 1 ) ) ) {
				if ( blocks[ i - 1 ][ j ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_WATER )
						|| blocks[ i + 1 ][ j ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_WATER )
						|| blocks[ i ][ j - 1 ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_WATER )
						|| blocks[ i ][ j + 1 ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_WATER )
						|| blocks[ i - 1 ][ j - 1 ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_WATER )
						|| blocks[ i + 1 ][ j - 1 ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_WATER )
						|| blocks[ i + 1 ][ j + 1 ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_WATER )
						|| blocks[ i - 1 ][ j + 1 ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_WATER ) ) {

					blocks[ i ][ j ] = BlockRegistry.BLOCK_TYPE.BLOCK_VOLCANIC_ROCK.clone( );

				}
			}
		}
	}

}

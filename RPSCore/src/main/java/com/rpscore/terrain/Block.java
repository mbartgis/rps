/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-11-20 20:28:45 -0500 (Tue, 20 Nov 2018) $
 * $Revision: 370 $
 * $Author: mbartgis $
 */

package com.rpscore.terrain;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Random;

import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.lib.Drawable;
import com.rpscore.lib.FileUtils;
import com.rpscore.lib.Unused;

public class Block implements Drawable {

	protected static Random	generator	= new Random( );

	public static final int	BLOCK_SIZE	= 256;

	public static final class PROPERTIES {
		public static final int	NONE		= 0;
		public static final int	LOW			= 1;
		public static final int	MED			= 2;
		public static final int	HIGH		= 3;
		public static final int	CRITICAL	= 5;

		public static final class TEMPERATURE {
			public static final int	TEMPERATE		= 0;
			public static final int	HOT				= 1;
			public static final int	FIRE			= 2;
			public static final int	LAVA			= 3;
			public static final int	MAGMA			= 4;
			public static final int	PLASMA			= 5;
			public static final int	COLD			= -1;
			public static final int	FREEZING		= -2;
			public static final int	ABSOLUTE_ZERO	= -3;

			private TEMPERATURE( ) {}
		}

		public static final class SPEED_MODIFIER {
			private SPEED_MODIFIER( ) {}
		}

		private PROPERTIES( ) {}

	}

	public static final int	ANIMATION_SPEED_REAL_TIME	= 1;
	public static final int	ANIMATION_SPEED_FAST		= 5;
	public static final int	ANIMATION_SPEED_MEDIUM		= 12;
	public static final int	ANIMATION_SPEED_SLOW		= 20;
	public static final int	ANIMATION_SPEED_VERY_SLOW	= 30;

	/**
	 * Plays through each image frame and resets to the first frame after all
	 * frames have been iterated over. (i.e. 1 -> 2 -> 3 -> 1 -> 2 -> 3)
	 */
	public static final int	ANIMATION_TYPE_SEQUENTIAL	= 0;

	/**
	 * Selects a frame at random to present. (i.e. 1 -> 2 -> 2 -> 1 -> 3 -> 2)
	 */
	public static final int	ANIMATION_TYPE_RANDOM		= 1;

	/**
	 * Plays through each image frame and then reverses the order of direction
	 * of the animation. (i.e. 1 -> 2 -> 3 -> 2 -> 1 -> 2)
	 */
	public static final int	ANIMATION_TYPE_SINUSOIDAL	= 2;

	protected String			blockName;
	protected boolean			isBlockSolid;
	protected boolean			isBlockMinable;
	protected int				blockLuminance;
	protected int				healthDamage;
	protected int				magicDamage;
	protected int				staminaDamage;
	protected int				temperature;
	protected int				pH;
	protected int				radiation;
	protected Image[ ]			graphics;

	protected Color				minimapColor;

	protected int				x, y;

	protected int				id;

	protected int				imgctr;
	protected int				speedctr;
	protected int				animationspeed;
	protected int				animationType;
	protected int				lastImageFrameDelivered;

	protected double			movementModifier;

	// true = ascending, false = descending
	protected boolean			sinusoidDirection;

	public Block( int ID ) {

		this.id = ID;
		this.blockName = "";
		this.isBlockSolid = false;
		this.isBlockMinable = false;
		this.blockLuminance = 0;
		this.graphics = null;

		// Block effects
		this.healthDamage = PROPERTIES.NONE;
		this.magicDamage = PROPERTIES.NONE;
		this.staminaDamage = PROPERTIES.NONE;
		this.temperature = 0;
		this.pH = 0;
		this.radiation = 0;

		this.imgctr = 0;
		this.speedctr = 0;

		this.animationspeed = Block.ANIMATION_SPEED_REAL_TIME;
		this.animationType = Block.ANIMATION_TYPE_SEQUENTIAL;
		this.sinusoidDirection = true;
		this.lastImageFrameDelivered = 0;

		this.movementModifier = 1.0;

	}

	public int getID( ) {
		return this.id;
	}

	public void setID( int ID ) {
		this.id = ID;
	}

	public Color getMinimapColor( ) {
		return this.minimapColor;
	}

	public void setMinimapColor( Color mc ) {
		this.minimapColor = mc;
	}

	public void setGraphicsFromImageURLs( String[ ] images_list ) throws MalformedURLException, IOException {
		this.graphics = new Image[ images_list.length ];
		for ( int i = 0; i < images_list.length; i++ ) {
			this.graphics[ i ] = FileUtils.importImage( images_list[ i ] );
		}
	}

	public static void animateWater( ) {
		Block waterBlock = BlockRegistry.BLOCK_TYPE.BLOCK_WATER;

		Image w_image = waterBlock.getBlockImage( );

		BufferedImage bi = TerrainStitcher.toBufferedImage( w_image, false );

		for ( int i = 0; i < bi.getWidth( ); i++ ) {

			// Store the last row for each column
			int temp = bi.getRGB( i, bi.getHeight( ) - 1 );

			// Swap the last row with the first.
			bi.setRGB( i, bi.getHeight( ) - 1, bi.getRGB( i, 0 ) );

			for ( int j = 0; j < bi.getHeight( ) - 2; j++ ) {
				bi.setRGB( i, j, bi.getRGB( i, j + 1 ) );
			}

			bi.setRGB( i, bi.getHeight( ) - 2, temp );

		}

		for ( int i = 0; i < bi.getHeight( ); i++ ) {

			// Store the last column for each row
			int temp = bi.getRGB( bi.getWidth( ) - 1, i );

			// Swap the last row with the first.
			bi.setRGB( bi.getWidth( ) - 1, i, bi.getRGB( 0, i ) );

			for ( int j = 0; j < bi.getWidth( ) - 2; j++ ) {
				bi.setRGB( j, i, bi.getRGB( j + 1, i ) );
			}

			bi.setRGB( bi.getWidth( ) - 2, i, temp );

		}

	}

	@Override
	public boolean equals( Object obj ) {

		if ( obj == null ) {
			return false;
		}

		// If the memory pointers are the same, the block is identical.
		if ( obj == this ) {
			return true;
		}

		if ( obj instanceof Block ) {
			Block blockToCompare = ( Block ) obj;

			/*
			 * // Basic primitive compares boolean luminanceMatches =
			 * blockToCompare.blockLuminance == this.blockLuminance; boolean
			 * minableMatches = blockToCompare.isBlockMinable ==
			 * this.isBlockMinable; boolean solidityMatches =
			 * blockToCompare.isBlockSolid == this.isBlockSolid; boolean
			 * healthDamageMatches = blockToCompare.healthDamage ==
			 * this.healthDamage; boolean magicDamageMatches =
			 * blockToCompare.magicDamage == this.magicDamage; boolean
			 * staminaDamageMatches = blockToCompare.staminaDamage ==
			 * this.staminaDamage; boolean temperatureMatches =
			 * blockToCompare.temperature == this.temperature; boolean pHMatches
			 * = blockToCompare.pH == this.pH; boolean radiationMatches =
			 * blockToCompare.radiation == this.radiation; boolean
			 * toxicityMatches = blockToCompare.toxicity == this.toxicity;
			 *
			 * // Short the comparison if any basic property does not match //
			 * (ensures that a more complex compares need not take place). if (
			 * ! ( luminanceMatches && minableMatches && solidityMatches &&
			 * healthDamageMatches && magicDamageMatches && staminaDamageMatches
			 * && temperatureMatches && pHMatches && radiationMatches &&
			 * toxicityMatches ) ) { return false; }
			 */

			// Medium complexity string compare
			return blockToCompare.blockName.equals( this.blockName );

			/*
			 * // If the names do not match, end the comparison before //
			 * comparing // graphics. if ( !namesMatch ) { return false; }
			 *
			 * // If the array sizes are different, the object does not have //
			 * the // same Images, therefore it is not the same. if (
			 * blockToCompare.graphics.length == this.graphics.length ) { for (
			 * int i = 0; i < this.graphics.length; i++ ) { if (
			 * !blockToCompare.graphics[ i ].equals( this.graphics[ i ] ) ) {
			 * return false; } } } else { return false; }
			 */
		}

		return false;
	}

	public double getMovementModifier( ) {
		return movementModifier;
	}

	public void setMovementModifier( double movementModifier ) {
		this.movementModifier = movementModifier;
	}

	public void setXPos( int i ) {
		this.x = i;
	}

	public void setYPos( int j ) {
		this.y = j;
	}

	public int getX( ) {
		return this.x * BLOCK_SIZE;
	}

	public int getY( ) {
		return this.y * BLOCK_SIZE;
	}

	public String getBlockName( ) {
		return blockName;
	}

	@SuppressWarnings( "hiding" )
	public void setBlockName( String blockName ) {
		this.blockName = blockName;
	}

	public boolean isBlockSolid( ) {
		return isBlockSolid;
	}

	@SuppressWarnings( "hiding" )
	public void setBlockSolid( boolean isBlockSolid ) {
		this.isBlockSolid = isBlockSolid;
	}

	public boolean isBlockMinable( ) {
		return isBlockMinable;
	}

	@SuppressWarnings( "hiding" )
	public void setBlockMinable( boolean isBlockMinable ) {
		this.isBlockMinable = isBlockMinable;
	}

	public int getBlockLuminance( ) {
		return blockLuminance;
	}

	@SuppressWarnings( "hiding" )
	public void setBlockLuminance( int blockLuminance ) {
		this.blockLuminance = blockLuminance;
	}

	public int getHealthDamage( ) {
		return healthDamage;
	}

	@SuppressWarnings( "hiding" )
	public void setHealthDamage( int healthDamage ) {
		this.healthDamage = healthDamage;
	}

	public int getMagicDamage( ) {
		return magicDamage;
	}

	@SuppressWarnings( "hiding" )
	public void setMagicDamage( int magicDamage ) {
		this.magicDamage = magicDamage;
	}

	public int getStaminaDamage( ) {
		return staminaDamage;
	}

	@SuppressWarnings( "hiding" )
	public void setStaminaDamage( int staminaDamage ) {
		this.staminaDamage = staminaDamage;
	}

	public int getTemperature( ) {
		return temperature;
	}

	@SuppressWarnings( "hiding" )
	public void setTemperature( int temperature ) {
		this.temperature = temperature;
	}

	public int getpH( ) {
		return pH;
	}

	@SuppressWarnings( "hiding" )
	public void setpH( int pH ) {
		this.pH = pH;
	}

	public int getRadiation( ) {
		return radiation;
	}

	@SuppressWarnings( "hiding" )
	public void setRadiation( int radiation ) {
		this.radiation = radiation;
	}

	public void setAnimationSpeed( int animspeed ) {
		this.animationspeed = animspeed;
	}

	public Image[ ] getGraphics( ) {
		return graphics;
	}

	public void setAnimationType( int type ) {
		this.animationType = type;
	}

	public Image getBlockImage( ) {

		if ( this.graphics == null ) {
			return null;
		}

		if ( !this.isAnimated( ) ) {
			return this.graphics[ 0 ];
		}
		switch ( this.animationType ) {
			case Block.ANIMATION_TYPE_SEQUENTIAL:
				return this.getNextBlockFrame( );
			case Block.ANIMATION_TYPE_RANDOM:
				return this.getRandomBlockFrame( );
			case Block.ANIMATION_TYPE_SINUSOIDAL:
				return this.getSinusoidalBlockFrame( );
			default:
				return this.graphics[ 0 ];
		}
	}

	protected Image getNextBlockFrame( ) {

		this.speedctr++;

		if ( ( this.speedctr % this.animationspeed ) != 0 ) {
			return this.graphics[ this.imgctr ];
		}

		if ( this.speedctr == 120 ) {
			this.speedctr = 0;
		}

		int frame = this.imgctr;
		if ( this.imgctr >= ( this.graphics.length - 1 ) ) {
			this.imgctr = 0;
		} else {
			this.imgctr++;
		}
		this.lastImageFrameDelivered = this.imgctr;
		return this.graphics[ frame ];
	}

	protected Image getSinusoidalBlockFrame( ) {

		this.speedctr++;

		if ( ( this.speedctr % this.animationspeed ) != 0 ) {
			return this.graphics[ this.imgctr ];
		}

		if ( this.speedctr == 120 ) {
			this.speedctr = 0;
		}

		if ( this.sinusoidDirection ) {
			this.imgctr++;
			if ( this.imgctr == ( this.graphics.length - 1 ) ) {
				this.sinusoidDirection = !this.sinusoidDirection;
			}
		} else {
			this.imgctr--;
			if ( this.imgctr == 0 ) {
				this.sinusoidDirection = !this.sinusoidDirection;
			}
		}
		this.lastImageFrameDelivered = this.imgctr;
		return this.graphics[ this.imgctr ];
	}

	protected Image getRandomBlockFrame( ) {

		this.speedctr++;

		if ( ( this.speedctr % this.animationspeed ) != 0 ) {
			return this.graphics[ this.lastImageFrameDelivered ];
		}

		if ( this.speedctr == 120 ) {
			this.speedctr = 0;
		}

		this.lastImageFrameDelivered = generator.nextInt( this.graphics.length - 1 );

		return this.graphics[ this.lastImageFrameDelivered ];
	}

	public boolean isAnimated( ) {
		if ( this.graphics == null ) {
			return false;
		}
		return this.graphics.length > 1;
	}

	public Image getLastBlockFrame( ) {
		if ( this.graphics == null ) {
			return null;
		}
		return this.graphics[ this.lastImageFrameDelivered ];
	}

	@Override
	public Block clone( ) {
		Block b = new Block( this.id );
		b.blockName = this.blockName;
		b.isBlockSolid = this.isBlockSolid;
		b.isBlockMinable = this.isBlockMinable;
		b.blockLuminance = this.blockLuminance;
		b.healthDamage = this.healthDamage;
		b.magicDamage = this.magicDamage;
		b.staminaDamage = this.staminaDamage;
		b.temperature = this.temperature;
		b.pH = this.pH;
		b.radiation = this.radiation;
		b.minimapColor = this.minimapColor;

		if ( this.graphics != null ) {
			b.graphics = new Image[ this.graphics.length ];

			for ( int i = 0; i < b.graphics.length; i++ ) {
				b.graphics[ i ] = this.graphics[ i ];
			}
		}

		b.animationspeed = this.animationspeed;
		b.imgctr = this.imgctr;
		b.speedctr = this.speedctr;

		b.animationType = this.animationType;
		b.sinusoidDirection = this.sinusoidDirection;
		b.lastImageFrameDelivered = this.lastImageFrameDelivered;

		b.movementModifier = this.movementModifier;

		return b;
	}

	@Override
	public int hashCode( ) {
		return super.hashCode( );
	}

	@Unused
	@Override
	public void draw( GraphicsAdapter g ) {}

}
/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

package com.rpscore.terrain;

import java.io.File;

import com.rpscore.entities.Entity;
import com.rpscore.server.Communicator;

public class MapManager {

	private File			mapFile;
	private Communicator	serverComm;

	public MapManager( File f ) {
		if ( f == null ) {
			throw new IllegalArgumentException( );
		}
		this.mapFile = f;
	}

	public MapManager( Communicator c ) {
		if ( c == null || c.isClosed( ) ) {
			throw new IllegalArgumentException( );
		}
		this.serverComm = c;
	}

	public boolean isOnline( ) {
		return ( this.serverComm != null && this.mapFile == null );
	}

	public double getRelativeX( Entity e ) {
		return e.getX( );
	}

	public double getRelativeY( Entity e ) {
		return e.getY( );
	}

	public double getRelativeX( Block b ) {
		return b.getX( );
	}

	public double getRelativeY( Block b ) {
		return b.getY( );
	}

}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-07-28 21:30:49 -0400 (Sat, 28 Jul 2018) $
 * $Revision: 365 $
 * $Author: mbartgis $
 */

package com.rpscore.terrain;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class TerrainStitcher {
	public static final int	ARGB_COMP		= 0x000000FF;

	public static int		STITCH_FACTOR	= 1;
	public static int		STITCH_LENGTH	= 10;

	private static class ImageGroup {
		private Image[ ][ ]	group;
		private Image		i;

		@SuppressWarnings( "hiding" )
		private ImageGroup( Image[ ][ ] group, Image i ) {
			this.group = group;
			this.i = i;
		}

		private Image getImage( ) {
			return this.i;
		}

		@SuppressWarnings( "hiding" )
		private boolean checkEquality( Image[ ][ ] group ) {
			for ( int i = 0; i < group.length; i++ ) {
				for ( int j = 0; j < group[ i ].length; j++ ) {
					// Test each image by heap location (because only 1 instance
					// of the image should exists and image object comparisons
					// are costly).
					if ( group[ i ][ j ] != this.group[ i ][ j ] ) {
						return false;
					}
				}
			}
			return true;
		}

	}

	public static void flushTerrainCache( ) {

		while ( !stitchedCache.isEmpty( ) ) {
			stitchedCache.remove( 0 );
		}

		// Signal that it may be a good time for the GC to run.
		System.gc( );
	}

	private static ArrayList< ImageGroup > stitchedCache = new ArrayList< >( 1000 );

	public static BufferedImage toBufferedImage( Image img, boolean deepCopy ) {

		if ( img instanceof BufferedImage && !deepCopy ) {
			return ( BufferedImage ) img;
		}

		// Create a buffered image with transparency
		BufferedImage bimage = new BufferedImage( img.getWidth( null ), img.getHeight( null ),
				BufferedImage.TYPE_INT_ARGB );

		// Draw the image on to the buffered image
		Graphics2D bGr = bimage.createGraphics( );
		bGr.drawImage( img, 0, 0, null );
		bGr.dispose( );

		// Return the buffered image
		return bimage;
	}

	// TODO make this more efficient.
	private static Image stitchBlockCacheLookup( Image[ ][ ] group ) {
		for ( ImageGroup i : stitchedCache ) {
			if ( i.checkEquality( group ) ) {
				return i.getImage( );
			}
		}
		return null;
	}

	private static void stitchBlockCacheAdd( Image[ ][ ] group, Image i ) {
		stitchedCache.add( new ImageGroup( group, i ) );
	}

	public static Image stitchBlock( Block[ ][ ] map, int blockx, int blocky ) {

		Block[ ][ ] group = getGroup( map, blockx, blocky );

		Block init = group[ 1 ][ 1 ];

		// Check to see if all blocks are the same, this method is designed for
		// efficiency as this method will be called continuously.
		if ( init.equals( map[ 0 ][ 0 ] ) && init.equals( map[ 0 ][ 1 ] ) && init.equals( map[ 0 ][ 2 ] )
				&& init.equals( map[ 1 ][ 0 ] ) && init.equals( map[ 1 ][ 2 ] ) && init.equals( map[ 2 ][ 0 ] )
				&& init.equals( map[ 2 ][ 1 ] ) && init.equals( map[ 2 ][ 2 ] ) ) {
			return init.getLastBlockFrame( );
		}

		Image[ ][ ] images = new Image[ 3 ][ ];
		for ( int i = 0; i < images.length; i++ ) {
			images[ i ] = new Image[ 3 ];
		}

		images[ 1 ][ 1 ] = group[ 1 ][ 1 ].getLastBlockFrame( );
		images[ 1 ][ 0 ] = ( group[ 1 ][ 0 ] != null ) ? group[ 1 ][ 0 ].getLastBlockFrame( ) : null;
		images[ 1 ][ 2 ] = ( group[ 1 ][ 2 ] != null ) ? group[ 1 ][ 2 ].getLastBlockFrame( ) : null;
		images[ 0 ][ 1 ] = ( group[ 0 ][ 1 ] != null ) ? group[ 0 ][ 1 ].getLastBlockFrame( ) : null;
		images[ 2 ][ 1 ] = ( group[ 2 ][ 1 ] != null ) ? group[ 2 ][ 1 ].getLastBlockFrame( ) : null;

		images[ 0 ][ 0 ] = null;
		images[ 0 ][ 2 ] = null;
		images[ 2 ][ 0 ] = null;
		images[ 2 ][ 2 ] = null;

		Image getCache = stitchBlockCacheLookup( images );

		if ( getCache != null ) {
			return getCache;
		}

		BufferedImage base = toBufferedImage( images[ 1 ][ 1 ], true );

		// Top
		if ( group[ 1 ][ 0 ] != null && !group[ 1 ][ 0 ].equals( init ) ) {
			BufferedImage top = toBufferedImage( images[ 1 ][ 0 ], true );
			int tlen = ( Block.BLOCK_SIZE / STITCH_LENGTH );
			for ( int y = 0; y < tlen; y++ ) {
				for ( int x = 0; x < Block.BLOCK_SIZE; x++ ) {
					int top_pixel = top.getRGB( x, top.getHeight( ) - tlen + y );
					int base_pixel = base.getRGB( x, y );
					// BASE
					int a1 = ( base_pixel >> 24 ) & ARGB_COMP;
					int r1 = ( base_pixel >> 16 ) & ARGB_COMP;
					int g1 = ( base_pixel >> 8 ) & ARGB_COMP;
					int b1 = base_pixel & ARGB_COMP;

					// TOP
					int a2 = ( top_pixel >> 24 ) & ARGB_COMP;
					int r2 = ( top_pixel >> 16 ) & ARGB_COMP;
					int g2 = ( top_pixel >> 8 ) & ARGB_COMP;
					int b2 = top_pixel & ARGB_COMP;

					int div = ( ( tlen - ( tlen - y ) ) / STITCH_FACTOR );
					if ( div < 2 ) {
						div = 2;
					}
					int adiff = ( ( a2 - a1 ) / div );
					int rdiff = ( ( r2 - r1 ) / div );
					int gdiff = ( ( g2 - g1 ) / div );
					int bdiff = ( b2 - b1 ) / div;

					a1 += adiff;
					r1 += rdiff;
					g1 += gdiff;
					b1 += bdiff;

					a1 <<= 24;
					r1 <<= 16;
					g1 <<= 8;

					base.setRGB( x, y, a1 + r1 + g1 + b1 );

				}
			}
		}

		// Bottom
		if ( group[ 1 ][ 2 ] != null && !group[ 1 ][ 2 ].equals( init ) ) {
			BufferedImage bottom = toBufferedImage( images[ 1 ][ 2 ], true );
			int tlen = ( Block.BLOCK_SIZE / STITCH_LENGTH );
			for ( int y = 0; y < tlen; y++ ) {
				for ( int x = 0; x < Block.BLOCK_SIZE; x++ ) {
					int bottom_pixel = bottom.getRGB( x, y );
					int base_pixel = base.getRGB( x, base.getHeight( ) - tlen + y );
					// BASE
					int a1 = ( base_pixel >> 24 ) & ARGB_COMP;
					int r1 = ( base_pixel >> 16 ) & ARGB_COMP;
					int g1 = ( base_pixel >> 8 ) & ARGB_COMP;
					int b1 = base_pixel & ARGB_COMP;

					// TOP
					int a2 = ( bottom_pixel >> 24 ) & ARGB_COMP;
					int r2 = ( bottom_pixel >> 16 ) & ARGB_COMP;
					int g2 = ( bottom_pixel >> 8 ) & ARGB_COMP;
					int b2 = bottom_pixel & ARGB_COMP;

					int div = ( ( ( tlen - y ) ) / STITCH_FACTOR );
					if ( div < 2 ) {
						div = 2;
					}
					int adiff = ( ( a2 - a1 ) / div );
					int rdiff = ( ( r2 - r1 ) / div );
					int gdiff = ( ( g2 - g1 ) / div );
					int bdiff = ( b2 - b1 ) / div;

					a1 += adiff;
					r1 += rdiff;
					g1 += gdiff;
					b1 += bdiff;

					a1 <<= 24;
					r1 <<= 16;
					g1 <<= 8;

					base.setRGB( x, bottom.getHeight( ) - tlen + y, a1 + r1 + g1 + b1 );

				}
			}
		}

		// Left
		if ( group[ 0 ][ 1 ] != null && !group[ 0 ][ 1 ].equals( init ) ) {
			BufferedImage right = toBufferedImage( images[ 0 ][ 1 ], true );
			int tlen = ( Block.BLOCK_SIZE / STITCH_LENGTH );
			for ( int y = 0; y < Block.BLOCK_SIZE; y++ ) {
				for ( int x = 0; x < tlen; x++ ) {
					int left_pixel = right.getRGB( base.getWidth( ) - tlen + x, y );
					int base_pixel = base.getRGB( x, y );
					// BASE
					int a1 = ( base_pixel >> 24 ) & ARGB_COMP;
					int r1 = ( base_pixel >> 16 ) & ARGB_COMP;
					int g1 = ( base_pixel >> 8 ) & ARGB_COMP;
					int b1 = base_pixel & ARGB_COMP;

					// TOP
					int a2 = ( left_pixel >> 24 ) & ARGB_COMP;
					int r2 = ( left_pixel >> 16 ) & ARGB_COMP;
					int g2 = ( left_pixel >> 8 ) & ARGB_COMP;
					int b2 = left_pixel & ARGB_COMP;

					int div = ( ( tlen - ( tlen - x ) ) / STITCH_FACTOR );
					if ( div < 2 ) {
						div = 2;
					}
					int adiff = ( ( a2 - a1 ) / div );
					int rdiff = ( ( r2 - r1 ) / div );
					int gdiff = ( ( g2 - g1 ) / div );
					int bdiff = ( b2 - b1 ) / div;

					a1 += adiff;
					r1 += rdiff;
					g1 += gdiff;
					b1 += bdiff;

					a1 <<= 24;
					r1 <<= 16;
					g1 <<= 8;

					base.setRGB( x, y, a1 + r1 + g1 + b1 );

				}
			}
		}

		// Right
		if ( group[ 2 ][ 1 ] != null && !group[ 2 ][ 1 ].equals( init ) ) {
			BufferedImage right = toBufferedImage( images[ 2 ][ 1 ], true );
			int tlen = ( Block.BLOCK_SIZE / STITCH_LENGTH );
			for ( int y = 0; y < Block.BLOCK_SIZE; y++ ) {
				for ( int x = 0; x < tlen; x++ ) {
					int right_pixel = right.getRGB( x, y );
					int base_pixel = base.getRGB( base.getWidth( ) - tlen + x, y );
					// BASE
					int a1 = ( base_pixel >> 24 ) & ARGB_COMP;
					int r1 = ( base_pixel >> 16 ) & ARGB_COMP;
					int g1 = ( base_pixel >> 8 ) & ARGB_COMP;
					int b1 = base_pixel & ARGB_COMP;

					// TOP
					int a2 = ( right_pixel >> 24 ) & ARGB_COMP;
					int r2 = ( right_pixel >> 16 ) & ARGB_COMP;
					int g2 = ( right_pixel >> 8 ) & ARGB_COMP;
					int b2 = right_pixel & ARGB_COMP;

					int div = ( ( ( tlen - x ) ) / STITCH_FACTOR );
					if ( div < 2 ) {
						div = 2;
					}
					int adiff = ( ( a2 - a1 ) / div );
					int rdiff = ( ( r2 - r1 ) / div );
					int gdiff = ( ( g2 - g1 ) / div );
					int bdiff = ( b2 - b1 ) / div;

					a1 += adiff;
					r1 += rdiff;
					g1 += gdiff;
					b1 += bdiff;

					a1 <<= 24;
					r1 <<= 16;
					g1 <<= 8;

					base.setRGB( right.getWidth( ) - tlen + x, y, a1 + r1 + g1 + b1 );

				}
			}
		}

		// TODO ADD TOP LEFT, TOP RIGHT, BOTTOM LEFT, and BOTTOM RIGHT
		// Note the x and y variables will make either a diagonal pattern on the
		// base block or an ellipse can be drawn.

		stitchBlockCacheAdd( images, base );

		return base;

	}

	public static Block[ ][ ] getGroup( Block[ ][ ] map, int x, int y ) {
		Block[ ][ ] group = new Block[ 3 ][ ];
		for ( int i = 0; i < group.length; i++ ) {
			group[ i ] = new Block[ 3 ];
		}

		group[ 1 ][ 1 ] = map[ x ][ y ];

		try {
			group[ 0 ][ 0 ] = map[ x - 1 ][ y - 1 ];
		} catch ( ArrayIndexOutOfBoundsException e ) {
			group[ 0 ][ 0 ] = null;
		}
		try {
			group[ 0 ][ 1 ] = map[ x - 1 ][ y ];
		} catch ( ArrayIndexOutOfBoundsException e ) {
			group[ 0 ][ 1 ] = null;
		}
		try {
			group[ 0 ][ 2 ] = map[ x - 1 ][ y + 1 ];
		} catch ( ArrayIndexOutOfBoundsException e ) {
			group[ 0 ][ 2 ] = null;
		}
		try {
			group[ 1 ][ 0 ] = map[ x ][ y - 1 ];
		} catch ( ArrayIndexOutOfBoundsException e ) {
			group[ 1 ][ 0 ] = null;
		}
		try {
			group[ 1 ][ 2 ] = map[ x ][ y + 1 ];
		} catch ( ArrayIndexOutOfBoundsException e ) {
			group[ 1 ][ 2 ] = null;
		}
		try {
			group[ 2 ][ 0 ] = map[ x + 1 ][ y - 1 ];
		} catch ( ArrayIndexOutOfBoundsException e ) {
			group[ 2 ][ 0 ] = null;
		}
		try {
			group[ 2 ][ 1 ] = map[ x + 1 ][ y ];
		} catch ( ArrayIndexOutOfBoundsException e ) {
			group[ 2 ][ 1 ] = null;
		}
		try {
			group[ 2 ][ 2 ] = map[ x + 1 ][ y + 1 ];
		} catch ( ArrayIndexOutOfBoundsException e ) {
			group[ 2 ][ 2 ] = null;
		}

		return group;
	}

}

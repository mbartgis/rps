/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-11-20 20:28:45 -0500 (Tue, 20 Nov 2018) $
 * $Revision: 370 $
 * $Author: mbartgis $
 */

package com.rpscore.terrain;

import java.awt.Color;
import java.io.IOException;
import java.lang.reflect.Field;

import com.rpscore.C;
import com.rpscore.debug.Log;

public class BlockRegistry {

	public static Block[ ] allBlocks;

	public static final class BLOCK_TYPE {

		private BLOCK_TYPE( ) {}

		// Allocate memory at runtime for each block
		public static final Block	BLOCK_NULL					 = new Block( 0 );
		public static final Block	BLOCK_ANIMATION_TEST		 = new Block( 6 );
		public static final Block	BLOCK_DIRT					 = new Block( 1 );
		public static final Block	BLOCK_DRIED_VOLCANIC_ROCK	 = new Block( 31 );
		public static final Block	BLOCK_GRASS					 = new Block( 2 );
		public static final Block	BLOCK_IRRADIATED_DIRT		 = new Block( 13 );
		public static final Block	BLOCK_LAVA					 = new Block( 5 );
		public static final Block	BLOCK_VOLCANIC_ROCK			 = new Block( 30 );
		public static final Block	BLOCK_WATER					 = new Block( 3 );
		
		public static final Block	BLOCK_DIRT_REC_TOP_LEFT      = new Block( 101 );
		public static final Block	BLOCK_DIRT_REC_TOP_RIGHT     = new Block( 201 );
		public static final Block	BLOCK_DIRT_REC_BOTTOM_LEFT   = new Block( 301 );
		public static final Block	BLOCK_DIRT_REC_BOTTOM_RIGHT  = new Block( 401 );
	}
	
	public static Block getBlockSeries(Block b) {
		int id = b.getID();
		if (id == 1 || id == 101 || id == 201 || id == 301 || id == 401) {
			return BLOCK_TYPE.BLOCK_DIRT;
		}
		
		return b;
	}

	public static Block getBlockFromID( int id ) {

		for ( Block b : allBlocks ) {
			if ( b.getID( ) == id ) {
				return b;
			}
		}

		return BLOCK_TYPE.BLOCK_NULL;

	}

	@Deprecated
	public static int getIDFromBlock( Block b ) {
		return b.getID( );
	}

	public static void buildBlockRegistry( ) {

		String[ ] img_urls = null;

		Log.info( "Building Block Registry..." );

		// Debug blocks

		// NULL BLOCK
		{
			BLOCK_TYPE.BLOCK_NULL.setBlockName( "NULL" );
			BLOCK_TYPE.BLOCK_NULL.setBlockSolid( false );
			BLOCK_TYPE.BLOCK_NULL.setBlockMinable( false );
			BLOCK_TYPE.BLOCK_NULL.setMinimapColor( Color.BLACK );

			Log.info( "Creating Block: BLOCK_NULL" );
		}
		// ANIMATION TEST Block
		{
			Log.info( "Creating Block: BLOCK_ANIMATION_TEST" );
			BLOCK_TYPE.BLOCK_ANIMATION_TEST.setBlockName( "Animation Test Block" );
			BLOCK_TYPE.BLOCK_ANIMATION_TEST.setBlockSolid( false );
			BLOCK_TYPE.BLOCK_ANIMATION_TEST.setBlockMinable( false );
			BLOCK_TYPE.BLOCK_ANIMATION_TEST.setAnimationSpeed( Block.ANIMATION_SPEED_REAL_TIME );
			BLOCK_TYPE.BLOCK_ANIMATION_TEST.setAnimationType( Block.ANIMATION_TYPE_RANDOM );
			BLOCK_TYPE.BLOCK_ANIMATION_TEST.setMinimapColor( Color.YELLOW.brighter( ).brighter( ) );

			String asset_folder = C.File.BLOCK_GFX_FOLDER + "block_animation_test" + C.File.FS;
			img_urls = new String[ ] { asset_folder + "at_1.png", asset_folder + "at_2.png", asset_folder + "at_3.png",
					asset_folder + "at_4.png", asset_folder + "at_5.png", asset_folder + "at_6.png",
					asset_folder + "at_7.png", asset_folder + "at_8.png" };
			try {
				BLOCK_TYPE.BLOCK_ANIMATION_TEST.setGraphicsFromImageURLs( img_urls );
			} catch ( IOException e ) {
				Log.error( "Could not fetch image for block: BLOCK_ANIMATION_TEST" );
				e.printStackTrace( );
			}
		}

		// Default BG terrain blocks
		{
			// DIRT Block
			{
				Log.info( "Creating Block: BLOCK_DIRT" );
				BLOCK_TYPE.BLOCK_DIRT.setBlockName( "Dirt" );
				BLOCK_TYPE.BLOCK_DIRT.setBlockSolid( false );
				BLOCK_TYPE.BLOCK_DIRT.setBlockMinable( false );
				BLOCK_TYPE.BLOCK_DIRT.setTemperature( Block.PROPERTIES.TEMPERATURE.TEMPERATE );
				BLOCK_TYPE.BLOCK_DIRT.setRadiation( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_DIRT.setpH( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_DIRT.setMovementModifier( 1.1 );
				BLOCK_TYPE.BLOCK_DIRT.setMinimapColor( new Color( 0xFF7E4F1D ) );

				String asset_folder = C.File.BLOCK_GFX_FOLDER + "block_dirt" + C.File.FS;
				img_urls = new String[ ] { asset_folder + "dirt_large.png" };
				try {
					BLOCK_TYPE.BLOCK_DIRT.setGraphicsFromImageURLs( img_urls );
				} catch ( IOException e ) {
					Log.error( "Could not fetch image for block: BLOCK_DIRT" );
				}

			}
			// GRASS Block
			{
				Log.info( "Creating Block: BLOCK_GRASS" );
				BLOCK_TYPE.BLOCK_GRASS.setBlockName( "Grass" );
				BLOCK_TYPE.BLOCK_GRASS.setBlockSolid( false );
				BLOCK_TYPE.BLOCK_GRASS.setBlockMinable( false );
				BLOCK_TYPE.BLOCK_GRASS.setTemperature( Block.PROPERTIES.TEMPERATURE.TEMPERATE );
				BLOCK_TYPE.BLOCK_GRASS.setRadiation( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_GRASS.setpH( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_GRASS.setMovementModifier( 0.9 );
				BLOCK_TYPE.BLOCK_GRASS.setMinimapColor( new Color( 0xFFA3E339 ) );

				String asset_folder = C.File.BLOCK_GFX_FOLDER + "block_grass" + C.File.FS;
				img_urls = new String[ ] { asset_folder + "grass_large.png" };
				try {
					BLOCK_TYPE.BLOCK_GRASS.setGraphicsFromImageURLs( img_urls );
				} catch ( IOException e ) {
					Log.error( "Could not fetch image for block: BLOCK_GRASS" );
				}
			}

			// WATER Block
			{
				Log.info( "Creating Block: BLOCK_WATER" );
				BLOCK_TYPE.BLOCK_WATER.setBlockName( "Water" );
				BLOCK_TYPE.BLOCK_WATER.setBlockSolid( false );
				BLOCK_TYPE.BLOCK_WATER.setBlockMinable( false );
				BLOCK_TYPE.BLOCK_WATER.setTemperature( Block.PROPERTIES.TEMPERATURE.TEMPERATE );
				BLOCK_TYPE.BLOCK_WATER.setRadiation( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_WATER.setpH( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_WATER.setMovementModifier( 0.5 );
				BLOCK_TYPE.BLOCK_WATER.setMinimapColor( new Color( 0xFF4391BB ) );

				String asset_folder = C.File.BLOCK_GFX_FOLDER + "block_water" + C.File.FS;
				img_urls = new String[ ] { asset_folder + "water_moving_1.png"// ,
						// BLOCKS_FOLDER + asset_folder + "water_moving_2.png",
						// BLOCKS_FOLDER + asset_folder + "water_moving_3.png",
						// BLOCKS_FOLDER + asset_folder + "water_moving_4.png"
				};
				try {
					BLOCK_TYPE.BLOCK_WATER.setGraphicsFromImageURLs( img_urls );
				} catch ( IOException e ) {
					Log.error( "Could not fetch image for block: BLOCK_WATER" );
				}
			}

			// LAVA Block
			{
				Log.info( "Creating Block: BLOCK_LAVA" );
				BLOCK_TYPE.BLOCK_LAVA.setBlockName( "Lava" );
				BLOCK_TYPE.BLOCK_LAVA.setBlockSolid( false );
				BLOCK_TYPE.BLOCK_LAVA.setBlockMinable( false );
				BLOCK_TYPE.BLOCK_LAVA.setTemperature( Block.PROPERTIES.TEMPERATURE.LAVA );
				BLOCK_TYPE.BLOCK_LAVA.setRadiation( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_LAVA.setpH( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_LAVA.setMovementModifier( 0.25 );

				BLOCK_TYPE.BLOCK_LAVA.setAnimationSpeed( Block.ANIMATION_SPEED_MEDIUM );
				BLOCK_TYPE.BLOCK_LAVA.setAnimationType( Block.ANIMATION_TYPE_SINUSOIDAL );
				BLOCK_TYPE.BLOCK_LAVA.setMinimapColor( new Color( 0xFFFF7724 ) );

				String asset_folder = C.File.BLOCK_GFX_FOLDER + "block_lava" + C.File.FS;
				img_urls = new String[ ] { asset_folder + "lava_heat_1.png", asset_folder + "lava_heat_2.png",
						asset_folder + "lava_heat_3.png", asset_folder + "lava_heat_4.png",
						asset_folder + "lava_heat_5.png" };
				try {
					BLOCK_TYPE.BLOCK_LAVA.setGraphicsFromImageURLs( img_urls );
				} catch ( IOException e ) {
					Log.error( "Could not fetch image for block: BLOCK_LAVA" );
					e.printStackTrace( );
				}
			}

			// IRRADIATED DIRT Block
			{
				Log.info( "Creating Block: BLOCK_IRRADIATED_DIRT" );
				BLOCK_TYPE.BLOCK_IRRADIATED_DIRT.setBlockName( "Irradiated Dirt" );
				BLOCK_TYPE.BLOCK_IRRADIATED_DIRT.setBlockSolid( false );
				BLOCK_TYPE.BLOCK_IRRADIATED_DIRT.setBlockMinable( false );
				BLOCK_TYPE.BLOCK_IRRADIATED_DIRT.setTemperature( Block.PROPERTIES.TEMPERATURE.TEMPERATE );
				BLOCK_TYPE.BLOCK_IRRADIATED_DIRT.setRadiation( Block.PROPERTIES.LOW );
				BLOCK_TYPE.BLOCK_IRRADIATED_DIRT.setpH( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_IRRADIATED_DIRT.setMovementModifier( 1.1 );
				BLOCK_TYPE.BLOCK_IRRADIATED_DIRT.setMinimapColor( new Color( 0xFFD4FF25 ) );

				String asset_folder = C.File.BLOCK_GFX_FOLDER + "block_irradiated_dirt" + C.File.FS;
				img_urls = new String[ ] { asset_folder + "irradiated_dirt_main.png" };
				try {
					BLOCK_TYPE.BLOCK_IRRADIATED_DIRT.setGraphicsFromImageURLs( img_urls );
				} catch ( IOException e ) {
					Log.error( "Could not fetch image for block: BLOCK_IRRADIATED_DIRT" );
				}

			}

			// VOLCANIC ROCK
			{
				Log.info( "Creating Block: BLOCK_VOLCANIC_ROCK" );
				BLOCK_TYPE.BLOCK_VOLCANIC_ROCK.setBlockName( "Volcanic Rock" );
				BLOCK_TYPE.BLOCK_VOLCANIC_ROCK.setBlockSolid( false );
				BLOCK_TYPE.BLOCK_VOLCANIC_ROCK.setBlockMinable( false );
				BLOCK_TYPE.BLOCK_VOLCANIC_ROCK.setTemperature( Block.PROPERTIES.TEMPERATURE.HOT );
				BLOCK_TYPE.BLOCK_VOLCANIC_ROCK.setRadiation( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_VOLCANIC_ROCK.setpH( Block.PROPERTIES.LOW );
				BLOCK_TYPE.BLOCK_VOLCANIC_ROCK.setMovementModifier( 0.85 );

				BLOCK_TYPE.BLOCK_VOLCANIC_ROCK.setAnimationSpeed( Block.ANIMATION_SPEED_MEDIUM );
				BLOCK_TYPE.BLOCK_VOLCANIC_ROCK.setAnimationType( Block.ANIMATION_TYPE_SINUSOIDAL );
				BLOCK_TYPE.BLOCK_VOLCANIC_ROCK.setMinimapColor( new Color( 0xFF04001E ) );

				String asset_folder = C.File.BLOCK_GFX_FOLDER + "block_volcanic_rock" + C.File.FS;
				img_urls = new String[ ] { asset_folder + "volcanic_rock_0.png", asset_folder + "volcanic_rock_1.png",
						asset_folder + "volcanic_rock_2.png", asset_folder + "volcanic_rock_3.png",
						asset_folder + "volcanic_rock_4.png" };
				try {
					BLOCK_TYPE.BLOCK_VOLCANIC_ROCK.setGraphicsFromImageURLs( img_urls );
				} catch ( IOException e ) {
					Log.error( "Could not fetch image for block: BLOCK_VOLCANIC_ROCK" );
				}

			}

			// DRIED VOLCANIC ROCK
			{
				Log.info( "Creating Block: BLOCK_VOLCANIC_ROCK" );
				BLOCK_TYPE.BLOCK_DRIED_VOLCANIC_ROCK.setBlockName( "Dried Volcanic Rock" );
				BLOCK_TYPE.BLOCK_DRIED_VOLCANIC_ROCK.setBlockSolid( false );
				BLOCK_TYPE.BLOCK_DRIED_VOLCANIC_ROCK.setBlockMinable( false );
				BLOCK_TYPE.BLOCK_DRIED_VOLCANIC_ROCK.setTemperature( Block.PROPERTIES.TEMPERATURE.TEMPERATE );
				BLOCK_TYPE.BLOCK_DRIED_VOLCANIC_ROCK.setRadiation( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_DRIED_VOLCANIC_ROCK.setpH( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_DRIED_VOLCANIC_ROCK.setMovementModifier( 0.95 );
				BLOCK_TYPE.BLOCK_DRIED_VOLCANIC_ROCK.setMinimapColor( new Color( 0xFF03001D ) );

				String asset_folder = C.File.BLOCK_GFX_FOLDER + "block_dried_volcanic_rock" + C.File.FS;
				img_urls = new String[ ] { asset_folder + "dried_volcanic_rock.png" };
				try {
					BLOCK_TYPE.BLOCK_DRIED_VOLCANIC_ROCK.setGraphicsFromImageURLs( img_urls );
				} catch ( IOException e ) {
					Log.error( "Could not fetch image for block: BLOCK_DRIED_VOLCANIC_ROCK" );
				}

			}
			
			// DIRT CORNER Blocks
			{
				Log.info( "Creating Block: BLOCK_DIRT_RECESSED_TOP_LEFT" );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_LEFT.setBlockName( "Dirt Corner Top Left" );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_LEFT.setBlockSolid( false );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_LEFT.setBlockMinable( false );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_LEFT.setTemperature( Block.PROPERTIES.TEMPERATURE.TEMPERATE );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_LEFT.setRadiation( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_LEFT.setpH( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_LEFT.setMovementModifier( 1.1 );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_LEFT.setMinimapColor( new Color( 0xFF7E4F1D ) );

				String asset_folder = C.File.BLOCK_GFX_FOLDER + "block_dirt_corner" + C.File.FS;
				img_urls = new String[ ] { asset_folder + "recessed_top_left.png" };
				try {
					BLOCK_TYPE.BLOCK_DIRT_REC_TOP_LEFT.setGraphicsFromImageURLs( img_urls );
				} catch ( IOException e ) {
					Log.error( "Could not fetch image for block: BLOCK_DIRT_TOP_LEFT" );
				}
				
				Log.info( "Creating Block: BLOCK_DIRT_RECESSED_TOP_RIGHT" );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_RIGHT.setBlockName( "Dirt Corner Top Right" );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_RIGHT.setBlockSolid( false );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_RIGHT.setBlockMinable( false );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_RIGHT.setTemperature( Block.PROPERTIES.TEMPERATURE.TEMPERATE );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_RIGHT.setRadiation( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_RIGHT.setpH( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_RIGHT.setMovementModifier( 1.1 );
				BLOCK_TYPE.BLOCK_DIRT_REC_TOP_RIGHT.setMinimapColor( new Color( 0xFF7E4F1D ) );

				img_urls = new String[ ] { asset_folder + "recessed_top_right.png" };
				try {
					BLOCK_TYPE.BLOCK_DIRT_REC_TOP_RIGHT.setGraphicsFromImageURLs( img_urls );
				} catch ( IOException e ) {
					Log.error( "Could not fetch image for block: BLOCK_DIRT_TOP_RIGHT" );
				}
				
				Log.info( "Creating Block: BLOCK_DIRT_RECESSED_BOTTOM_LEFT" );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_LEFT.setBlockName( "Dirt Corner Bottom Left" );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_LEFT.setBlockSolid( false );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_LEFT.setBlockMinable( false );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_LEFT.setTemperature( Block.PROPERTIES.TEMPERATURE.TEMPERATE );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_LEFT.setRadiation( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_LEFT.setpH( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_LEFT.setMovementModifier( 1.1 );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_LEFT.setMinimapColor( new Color( 0xFF7E4F1D ) );

				img_urls = new String[ ] { asset_folder + "recessed_bottom_left.png" };
				try {
					BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_LEFT.setGraphicsFromImageURLs( img_urls );
				} catch ( IOException e ) {
					Log.error( "Could not fetch image for block: BLOCK_DIRT_BOTTOM_LEFT" );
				}
				
				Log.info( "Creating Block: BLOCK_DIRT_RECESSED_BOTTOM_RIGHT" );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_RIGHT.setBlockName( "Dirt Corner Bottom Right" );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_RIGHT.setBlockSolid( false );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_RIGHT.setBlockMinable( false );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_RIGHT.setTemperature( Block.PROPERTIES.TEMPERATURE.TEMPERATE );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_RIGHT.setRadiation( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_RIGHT.setpH( Block.PROPERTIES.NONE );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_RIGHT.setMovementModifier( 1.1 );
				BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_RIGHT.setMinimapColor( new Color( 0xFF7E4F1D ) );

				img_urls = new String[ ] { asset_folder + "recessed_bottom_right.png" };
				try {
					BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_RIGHT.setGraphicsFromImageURLs( img_urls );
				} catch ( IOException e ) {
					Log.error( "Could not fetch image for block: BLOCK_DIRT_BOTTOM_RIGHT" );
				}

			}

			// Accumulate all blocks
			Class< ? > cls;
			try {

				Field[ ] fields = BLOCK_TYPE.class.getFields( );
				allBlocks = new Block[ fields.length ];

				for ( int i = 0; i < fields.length; ++i ) {
					allBlocks[ i ] = ( Block ) fields[ i ].get( Block.class );
				}

			} catch ( IllegalArgumentException | IllegalAccessException e ) {
				// TODO Auto-generated catch block
				e.printStackTrace( );
			}

		}
	}
}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2018, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-07-28 21:30:49 -0400 (Sat, 28 Jul 2018) $
 * $Revision: 365 $
 * $Author: mbartgis $
 */

package com.rpscore.terrain;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Arrays;

public class CompositeBlock extends Block {

	protected Image[] cache;
	
	public CompositeBlock(int id, Block... layers) {
		super(id);
		
		Image[][] allImages = new Image[layers.length][];
		int cnt = 0;
		for (Block layer : layers) {
			allImages[cnt] = layer.graphics;
			cnt++;
		}
		

		
		int minLen = Integer.MAX_VALUE;
		
		for (int i = 0; i < allImages.length; i++) {
			if (allImages[i] != null) {
				if (allImages[i].length < minLen) {
					minLen = allImages[i].length;
				}
			}
		}
		
		this.graphics = new Image[minLen];
		for (int i = 0; i < this.graphics.length; i++) {
			Image[] temp = new Image[allImages.length];
			for (int j = 0; j < allImages.length; j++) {
				if (allImages[j] != null) {
					temp[j] = allImages[j][i];
				}
			}
			this.graphics[i] = mergeLayers(temp);
		}
		
	}
	
	// Figure out some way to expedite this horribly inefficient code.
	protected static Image mergeLayers(Image[] layers) {
		
		BufferedImage[] images = new BufferedImage[layers.length];
		for (int i = 0; i < images.length; i++) {
			if (layers[i] != null) {
				images[i] = TerrainStitcher.toBufferedImage(layers[i], true);
			}
		}
		
		images = Arrays.stream(images).filter(t -> t != null).toArray(BufferedImage[]::new);
		
		if (images.length == 0) {
			return null;
		}
		
		BufferedImage ret = images[0];
		
		for(int i = 1; i < images.length; i++) {
			for (int x = 0; x < ret.getWidth(); x++) {
				for (int y = 0; y < ret.getWidth(); y++) {
					int alpha = images[i].getRGB(x, y) >> 24 & 0x000000FF;
					int nr = images[i].getRGB(x, y) >> 16 & 0x000000FF;
					int ng = images[i].getRGB(x, y) >> 8 & 0x000000FF;
					int nb = images[i].getRGB(x, y) & 0x000000FF;
					
					//nr *= (alpha / 255.0);
					//ng *= (alpha / 255.0);
					//nb *= (alpha / 255.0);
					
					int rr = ret.getRGB(x, y) >> 16 & 0x000000FF;
					int rg = ret.getRGB(x, y) >> 8 & 0x000000FF;
					int rb = ret.getRGB(x, y) & 0x000000FF;
					
					//rr *= ((255-alpha) / 255.0);
					//rg *= ((255-alpha) / 255.0);
					//rb *= ((255-alpha) / 255.0);
					
				    int color = (((nr + rr) / 2) * 65536) + (((ng + rg) / 2) * 256) + ((nb + rb)/2) | 0xFF000000;
				    

					
					if (alpha < 255 && alpha > 0) {
						ret.setRGB(x, y, color);
					}
					else if (alpha == 0) { }
					else {
						ret.setRGB(x, y, images[i].getRGB(x, y));
					}
				}
			}
		}
		
		return ret;
		
	}

}

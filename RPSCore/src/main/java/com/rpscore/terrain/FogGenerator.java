/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

package com.rpscore.terrain;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Random;

public class FogGenerator {

	public static double[ ][ ] diamondSquare( final int DATA_SIZE, long seed ) {

		final double SCALE = 1000.0;
		double[ ][ ] data = new double[ DATA_SIZE ][ DATA_SIZE ];

		data[ 0 ][ 0 ] = data[ 0 ][ DATA_SIZE
				- 1 ] = data[ DATA_SIZE - 1 ][ 0 ] = data[ DATA_SIZE - 1 ][ DATA_SIZE - 1 ] = SCALE;

		double h = 500.0;
		Random r = new Random( seed );

		for ( int sideLength = DATA_SIZE - 1; sideLength >= 2; sideLength /= 2, h /= 2.0 ) {

			int halfSide = sideLength / 2;

			for ( int x = 0; x < DATA_SIZE - 1; x += sideLength ) {
				for ( int y = 0; y < DATA_SIZE - 1; y += sideLength ) {

					double avg = data[ x ][ y ] + data[ x + sideLength ][ y ] + data[ x ][ y + sideLength ]
							+ data[ x + sideLength ][ y + sideLength ];

					avg /= 4.0;

					data[ x + halfSide ][ y + halfSide ] =

					avg + ( r.nextDouble( ) * 2 * h ) - h;
				}
			}

			for ( int x = 0; x < DATA_SIZE - 1; x += halfSide ) {

				for ( int y = ( x + halfSide ) % sideLength; y < DATA_SIZE - 1; y += sideLength ) {

					double avg = data[ ( x - halfSide + DATA_SIZE ) % DATA_SIZE ][ y ] +

					data[ ( x + halfSide ) % DATA_SIZE ][ y ] +

					data[ x ][ ( y + halfSide ) % DATA_SIZE ] +

					data[ x ][ ( y - halfSide + DATA_SIZE ) % DATA_SIZE ];

					avg /= 4.0;

					avg = avg + ( r.nextDouble( ) * 2 * h ) - h;

					data[ x ][ y ] = avg;

					if ( x == 0 ) {
						data[ DATA_SIZE - 1 ][ y ] = avg;
					}
					if ( y == 0 ) {
						data[ x ][ DATA_SIZE - 1 ] = avg;
					}
				}
			}
		}

		return data;

	}

	public static BufferedImage img;

	public static void update( int width, int height ) {

		Random r = new Random( );

		long l = r.nextLong( );

		img = new BufferedImage( width, height, BufferedImage.TYPE_INT_ARGB );

		double[ ][ ] data = diamondSquare( 4097, l );

		for ( int i = 0; i < img.getWidth( ); i++ ) {
			for ( int j = 0; j < img.getHeight( ); j++ ) {
				int c = 0x00FFFFFF;
				img.setRGB( i, j, ( int ) ( ( ( data[ i ][ j ] / 4000.0 ) * 255 ) * ( 0xFFFFFF ) ) + c );
			}
		}

	}

	public static void draw( Graphics g ) {

		if ( img == null ) {
			update( 4097, 4097 );
		}

		g.drawImage( img, 0, 0, null );

	}

}

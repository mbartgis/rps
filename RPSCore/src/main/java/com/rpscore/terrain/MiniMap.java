/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-11-20 20:28:45 -0500 (Tue, 20 Nov 2018) $
 * $Revision: 370 $
 * $Author: mbartgis $
 */

package com.rpscore.terrain;

import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.LinkedList;

import com.rpscore.entities.Entity;
import com.rpscore.entities.controlled.Player;
import com.rpscore.entities.enemies.Boss;
import com.rpscore.entities.enemies.Enemy;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.lib.Drawable;
import com.rpscore.lib.Updateable;

public class MiniMap implements Updateable, Drawable {

	private Map						map;

	private int						x, y, width, height;

	private LinkedList< Entity >	entities;

	public MiniMap( Map underlying ) {
		this.map = underlying;
		this.entities = new LinkedList< >( );
		this.width = 256;
		this.height = 256;
		this.x = 0;
		this.y = 0;
	}

	@Override
	public void draw( GraphicsAdapter g ) {
		Block[ ][ ] blocks = this.map.getUnderlying( );

		double scalar = 256.0 / blocks.length;
		int size = ( int ) Math.round( scalar );

		BufferedImage img = new BufferedImage( 256, 256, BufferedImage.TYPE_INT_ARGB );

		for ( int i = 0; i < img.getWidth( ); i++ ) {
			for ( int j = 0; j < img.getHeight( ); j++ ) {

				img.setRGB( i, j, 0x00000000 );

				if ( blocks[ i / ( ( size == 0 ) ? 1 : size ) ][ j / ( ( size == 0 ) ? 1 : size ) ]
						.getMinimapColor( ) != null ) {

					img.setRGB( i, j, blocks[ i / ( ( size == 0 ) ? 1 : size ) ][ j / ( ( size == 0 ) ? 1 : size ) ]
							.getMinimapColor( ).getRGB( ) );

				} else {

					// System.out.println( blocks[ i / ( ( size == 0 ) ? 1 : size ) ][ j / ( ( size == 0 ) ? 1 : size ) ].getBlockName( ) );

				}
			}
		}

		Iterator< Entity > entItr = this.entities.iterator( );

		while ( entItr.hasNext( ) ) {

			Entity e = entItr.next( );

			int px = ( int ) ( ( e.getX( ) ) / ( ( Block.BLOCK_SIZE )
					* ( 1 / ( ( double ) Block.BLOCK_SIZE / ( double ) this.map.getUnderlying( ).length ) ) ) );

			int py = ( int ) ( ( e.getY( ) ) / ( ( Block.BLOCK_SIZE )
					* ( 1 / ( ( double ) Block.BLOCK_SIZE / ( double ) this.map.getUnderlying( )[ 0 ].length ) ) ) );

			int color;

			if ( e instanceof Player ) {
				color = 0xFF00FF00;
			} else if ( e instanceof Boss ) {
				color = 0xFFFF2222;
			} else if ( e instanceof Enemy ) {
				color = 0xFFFF0000;
			} else {
				color = 0xFF0000FF;
			}

			for ( int i = -5; i < 5; i++ ) {
				for ( int j = -5; j < 5; j++ ) {
					try {
						img.setRGB( px + i, py + j, color );
					} catch ( ArrayIndexOutOfBoundsException ex ) {}
				}
			}
		}

		g.drawImage( img, this.x, this.y );
	}

	@Override
	public boolean isFinished( ) {
		return false;
	}

	@Override
	public void update( ) {}

	public void addEntity( Entity e ) {

		this.entities.add( e );

	}

	public void removeEntity( Entity e ) {
		Iterator< Entity > entItr = this.entities.iterator( );

		while ( entItr.hasNext( ) ) {
			if ( entItr.next( ).equals( e ) ) {
				entItr.remove( );
			}
		}
	}

	public int getX( ) {
		return x;
	}

	public void setX( int x ) {
		this.x = x;
	}

	public int getY( ) {
		return y;
	}

	public void setY( int y ) {
		this.y = y;
	}

	public int getWidth( ) {
		return width;
	}

	public void setWidth( int width ) {
		this.width = width;
	}

	public int getHeight( ) {
		return height;
	}

	public void setHeight( int height ) {
		this.height = height;
	}

}

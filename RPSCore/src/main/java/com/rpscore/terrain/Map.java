/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-07-27 04:03:54 -0400 (Fri, 27 Jul 2018) $
 * $Revision: 364 $
 * $Author: mbartgis $
 */

package com.rpscore.terrain;

import java.awt.Point;
import java.io.File;

import com.rpscore.C;
import com.rpscore.C.SettingKeys;
import com.rpscore.cfg.ConfigManager;
import com.rpscore.debug.Log;
import com.rpscore.entities.Entity;
import com.rpscore.game.GameInstance;
import com.rpscore.gui.game.GraphicsAdapter;
import com.rpscore.lib.Drawable;
import com.rpscore.lib.FileUtils;
import com.rpscore.lib.MathUtils;
import com.rpscore.lib.Updateable;

public class Map implements Drawable, Updateable {

	private Block[ ][ ]			blocks;

	private boolean				textureSmoothingEnabled;

	private int					animationCD;

	private Point				renderEpicenter;

	public static final String	BLOCK_DELIMITER	= ",";

	public static final int		ANIMATION_CD	= 15;

	private Map( Block[ ][ ] b ) {

		this.blocks = b;

		this.textureSmoothingEnabled = ConfigManager.getConfigManager( ).getSetting( SettingKeys.SMOOTH_BLOCK_EDGES,
				false );

		this.animationCD = ANIMATION_CD;

		this.renderEpicenter = new Point( 100, 100 );

	}

	private Point getLocationOf_MonteCarlo( Block b ) {
		for ( int i = 0; i < this.blocks.length; i++ ) {
			for ( int j = 0; j < this.blocks[ i ].length; j++ ) {
				if ( this.blocks[ i ][ j ].equals( b ) ) {
					return new Point( ( i * C.Dimensions.BLOCK_SIZE ) + C.Dimensions.BLOCK_SIZE / 2,
							( j * C.Dimensions.BLOCK_SIZE ) + C.Dimensions.BLOCK_SIZE / 2 );
				}
			}
		}

		Log.warn( "No grass block found to spawn on." );

		return new Point( 0, 0 );
	}

	public Block[ ][ ] getUnderlying( ) {
		return this.blocks;
	}

	private Point getLocationOf_LasVegas( Block b, int retryAttempts ) {
		Point p = new Point( 0, 0 );

		int cnt = 0;
		do {
			p.x = MathUtils.getRandom( 0, this.getPhysicalWidth( ) );
			p.y = MathUtils.getRandom( 0, this.getPhysicalHeight( ) );
			cnt++;

			if ( cnt == retryAttempts ) {
				throw new IllegalArgumentException(
						"After " + retryAttempts + ", could not reasonably find a block of type: " + b );
			}

		} while ( !this.getBlockAtLocation( p ).equals( b ) );

		return p;
	}

	/**
	 *
	 * Returns the location of a block with the specified type. An
	 * IllegalArgumentException is thrown when no blocks of a specific type are
	 * found. This method uses a L:as Vegas algorithm at first to determine a
	 * good block, however, if a block is not reasonably found, a subsequent
	 * Monte Carlo algorithm is invoked. If this algorithm fails, the method
	 * will throw an IllegalArguimentException.
	 *
	 * @param b
	 *            Type of block to find the location of.
	 * @return Location of a suitable block.
	 */
	public Point getLocationOf( Block b ) {

		try {
			return getLocationOf_LasVegas( b, 25 );
		} catch ( IllegalArgumentException e ) {
			return getLocationOf_MonteCarlo( b );
		}

	}

	public void setRenderEpicenter( int nx, int ny ) {
		this.renderEpicenter.x = nx;
		this.renderEpicenter.y = ny;
	}

	public Block getBlockAtLocation( int x, int y ) {
		try {
			return this.blocks[ x / C.Dimensions.BLOCK_SIZE ][ y / C.Dimensions.BLOCK_SIZE ].clone( );
		} catch ( ArrayIndexOutOfBoundsException e ) {
			return BlockRegistry.BLOCK_TYPE.BLOCK_NULL.clone( );
		}
	}

	public Block getBlockAtLocation( Point p ) {
		return this.getBlockAtLocation( p.x, p.y );
	}

	public void setTextureSmoothingEnabled( boolean b ) {
		this.textureSmoothingEnabled = b;
	}

	public boolean isTextureSmoothingEnabled( ) {
		return this.textureSmoothingEnabled;
	}

	public static Map createMapFromDoubleMatrix( double[ ][ ] array, int scale ) {

		Block[ ][ ] map = new Block[ array.length ][ ];

		for ( int i = 0; i < map.length; i++ ) {
			map[ i ] = new Block[ array[ i ].length ];
			for ( int j = 0; j < map[ i ].length; j++ ) {
				map[ i ][ j ] = getBlockFromValue( array[ i ][ j ], scale );
			}
		}

		return new Map( map );

	}

	private static Block getBlockFromValue( double val, int scale ) {
		if ( val < 250.0 ) {
			return BlockRegistry.BLOCK_TYPE.BLOCK_LAVA.clone( );
		} else if ( val < 600.0 ) {
			return BlockRegistry.BLOCK_TYPE.BLOCK_WATER.clone( );
		} else if ( val < 800.0 ) {
			return BlockRegistry.BLOCK_TYPE.BLOCK_DIRT.clone( );
		} else if ( val < 1000.0 ) {
			return BlockRegistry.BLOCK_TYPE.BLOCK_GRASS.clone( );
		} else if ( val < 1200.0 ) {
			return BlockRegistry.BLOCK_TYPE.BLOCK_IRRADIATED_DIRT.clone( );
		} else {
			return BlockRegistry.BLOCK_TYPE.BLOCK_DIRT.clone( );
		}
	}

	public static Map createMapFromFile( File f ) {

		String[ ] contents = FileUtils.getFileLines( f );

		// Determine the shape of a map
		int max = 0;
		String[ ][ ] blocks = new String[ contents.length ][ ];

		{
			int i = 0;
			for ( String line : contents ) {
				blocks[ i ] = line.split( BLOCK_DELIMITER );
				int t_len = blocks[ i ].length;

				if ( t_len > max ) {
					max = t_len;
				}
				i++;
			}
		}

		Block[ ][ ] map = new Block[ max ][ ];

		for ( int i = 0; i < map.length; i++ ) {
			map[ i ] = new Block[ contents.length ];
			for ( int j = 0; j < map[ i ].length; j++ ) {
				map[ i ][ j ] = BlockRegistry.BLOCK_TYPE.BLOCK_NULL.clone( );
			}
		}

		for ( int i = 0; i < blocks.length; i++ ) {

			for ( int j = 0; j < blocks[ i ].length; j++ ) {
				try {
					int val = Integer.parseInt( blocks[ i ][ j ] );
					map[ j ][ i ] = BlockRegistry.getBlockFromID( val ).clone( );
				} catch ( NumberFormatException e ) {
					Log.error( "Map Error: " + i + ", " + j + ": " + blocks[ i ][ j ] );
				}

			}

		}

		return new Map( map );
	}

	public int getTiledWidth( ) {
		return this.blocks.length;
	}

	public int getTiledHeight( ) {
		int max = 0;
		for ( int i = 0; i < this.blocks.length; i++ ) {
			max = this.blocks[ i ].length > max ? this.blocks[ i ].length : max;
		}
		return max;
	}

	public int getPhysicalWidth( ) {
		return this.getTiledWidth( ) * C.Dimensions.BLOCK_SIZE;
	}

	public int getPhysicalHeight( ) {
		return this.getTiledHeight( ) * C.Dimensions.BLOCK_SIZE;
	}

	@SuppressWarnings( "unused" )
	public static Map generateMap( long seed ) {
		return null;
	}

	@Override
	public boolean isFinished( ) {
		return false;
	}

	@Override
	public void update( ) {

		if ( animationCD <= 0 ) {
			Block.animateWater( );
			animationCD = ANIMATION_CD;
		} else {
			animationCD--;
		}

		for ( int i = 0; i < this.blocks.length; i++ ) {
			for ( int j = 0; j < this.blocks[ i ].length; j++ ) {
				this.blocks[ i ][ j ].getBlockImage( );
			}
		}

	}

	public int[ ] getBlockTileCoords( Entity e ) {
		return new int[ ] { ( ( int ) e.getX( ) ) / Block.BLOCK_SIZE, ( ( int ) e.getY( ) ) / Block.BLOCK_SIZE };
	}

	public int[ ] getBlockTileCoords( Point p ) {
		return new int[ ] { p.x / Block.BLOCK_SIZE, ( p.y / Block.BLOCK_SIZE ) };
	}

	public Block getBlockAtEntityCoords( Entity e ) {
		try {
			return blocks[ ( ( int ) ( e.getX( ) ) ) / Block.BLOCK_SIZE ][ ( ( int ) ( e.getY( ) ) )
					/ Block.BLOCK_SIZE ];
		} catch ( ArrayIndexOutOfBoundsException ex ) {
			return BlockRegistry.BLOCK_TYPE.BLOCK_NULL.clone( );
		}
	}

	@Override
	public void draw( GraphicsAdapter g ) {

		int render_distance = ConfigManager.getConfigManager( ).getSetting( "RenderDistance", 16 );

		int[ ] player_btile = getBlockTileCoords( this.renderEpicenter );

		int min_rx = player_btile[ 0 ] - ( render_distance / 2 );

		if ( min_rx < 0 ) {
			min_rx = 0;
		}
		if ( min_rx >= this.blocks.length ) {
			min_rx = this.blocks.length - 1;
		}

		int max_rx = min_rx + render_distance;

		if ( max_rx >= this.blocks.length ) {
			max_rx = this.blocks.length - 1;
		}

		int min_ry = player_btile[ 1 ] - ( render_distance / 2 );

		if ( min_ry < 0 ) {
			min_ry = 0;
		}
		if ( min_ry >= this.blocks[ 0 ].length ) {
			min_ry = this.blocks[ 0 ].length - 1;
		}

		int max_ry = min_ry + render_distance;

		if ( max_ry >= this.blocks[ 0 ].length ) {
			max_ry = this.blocks[ 0 ].length - 1;
		}

		for ( int i = min_rx; i < max_rx; i++ ) {
			for ( int j = min_ry; j < max_ry; j++ ) {
				if ( this.textureSmoothingEnabled ) {
					g.drawImage( TerrainStitcher.stitchBlock( this.blocks, i, j ),
							( i * Block.BLOCK_SIZE ) - ( ( int ) GameInstance.PXO ),
							( j * Block.BLOCK_SIZE ) - ( ( int ) GameInstance.PYO ) );
				} else {
					g.drawImage( blocks[ i ][ j ].getLastBlockFrame( ),
							( i * Block.BLOCK_SIZE ) - ( ( int ) GameInstance.PXO ),
							( j * Block.BLOCK_SIZE ) - ( ( int ) GameInstance.PYO ) );
				}
			}
		}

	}

}

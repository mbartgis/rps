package com.rpscore.terrain.mutators;

import com.rpscore.terrain.Block;
import com.rpscore.terrain.BlockRegistry;
import com.rpscore.terrain.CompositeBlock;

public class DirtCornersMutator extends TerrainMutator {

	// This is hideous, I will need to rewrite the TerrainMutator interface soon.
	// TODO Find a way to generalize this.
	@Override
	public void apply(Block[][] blocks, int i, int j, Block[] adjacent, Block[] diagonals) {

		Block dirt = BlockRegistry.BLOCK_TYPE.BLOCK_DIRT;
		if (blocks[i][j].equals(dirt)) {
			
			// Assume we are not on the edge.
			if (i > 0 && j > 0 && i < (blocks.length - 1) && j < (blocks[0].length - 1)) {
				Block north = BlockRegistry.getBlockSeries(blocks[i - 1][j]);
				Block south = BlockRegistry.getBlockSeries(blocks[i + 1][j]);
				Block east = BlockRegistry.getBlockSeries(blocks[i][j + 1]);
				Block west = BlockRegistry.getBlockSeries(blocks[i][j - 1]);
				
				Block northwest = BlockRegistry.getBlockSeries(blocks[i - 1][j - 1]);
				Block southwest = BlockRegistry.getBlockSeries(blocks[i + 1][j - 1]);
				Block northeast = BlockRegistry.getBlockSeries(blocks[i - 1][j + 1]);
				Block southeast = BlockRegistry.getBlockSeries(blocks[i + 1][j - 1]);
				
				Block mod = null;
				
				if ( !west.equals(dirt) && !north.equals(dirt) && !northwest.equals(dirt)) {
					blocks[i][j] = new CompositeBlock(101,  northwest, BlockRegistry.BLOCK_TYPE.BLOCK_DIRT_REC_TOP_LEFT);
					mod = blocks[i][j];
				}
				
				if (!east.equals(dirt) && !north.equals(dirt) && !northeast.equals(dirt)) {
					if (mod != null) {
						blocks[i][j] = new CompositeBlock(201,  northeast, BlockRegistry.BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_LEFT, mod);
					}
					else {
						blocks[i][j] = new CompositeBlock(201,  northeast, BlockRegistry.BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_LEFT);
					}
				}
				
				if (!west.equals(dirt) && !south.equals(dirt) && !southwest.equals(dirt)) {
					if (mod != null) {
						blocks[i][j] = new CompositeBlock(301,  southwest, BlockRegistry.BLOCK_TYPE.BLOCK_DIRT_REC_TOP_RIGHT, mod);
					}
					else {
						blocks[i][j] = new CompositeBlock(301,  southwest, BlockRegistry.BLOCK_TYPE.BLOCK_DIRT_REC_TOP_RIGHT);
					}
				}
				
				if (!east.equals(dirt) && !south.equals(dirt) && !southeast.equals(dirt)) {
					if (mod != null) {
						blocks[i][j] = new CompositeBlock(401,  southeast, BlockRegistry.BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_RIGHT, mod);
					}
					else {
						blocks[i][j] = new CompositeBlock(401,  southeast, BlockRegistry.BLOCK_TYPE.BLOCK_DIRT_REC_BOTTOM_RIGHT);
					}
				}
			}
			
		}
		
	}

}

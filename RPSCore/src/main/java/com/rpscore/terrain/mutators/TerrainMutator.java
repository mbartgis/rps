/*
 * COPYRIGHT NOTICE:
 * Copyright 2018, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */
package com.rpscore.terrain.mutators;

import com.rpscore.terrain.Block;

public abstract class TerrainMutator {

	public TerrainMutator( ) {}

	public abstract void apply( Block[ ][ ] blocks, int i, int j, Block[ ] adjacent, Block[ ] diagonals );

}

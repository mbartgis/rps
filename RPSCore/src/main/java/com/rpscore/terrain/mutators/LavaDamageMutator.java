/*
 * COPYRIGHT NOTICE:
 * Copyright 2018, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */
package com.rpscore.terrain.mutators;

import com.rpscore.terrain.Block;
import com.rpscore.terrain.BlockRegistry;

public class LavaDamageMutator extends TerrainMutator {

	@Override
	public void apply( Block[ ][ ] blocks, int i, int j, Block[ ] adjacent, Block[ ] diagonals ) {

		if ( blocks[ i ][ j ].equals( BlockRegistry.BLOCK_TYPE.BLOCK_LAVA ) ) {

			int cnt = 0;

			for ( Block b : adjacent ) {
				cnt += b.equals( BlockRegistry.BLOCK_TYPE.BLOCK_LAVA ) ? 1 : 0;
			}

			// Lava fully surrounds this lava block.
			if ( cnt >= 4 ) {
				blocks[ i ][ j ].setTemperature( Block.PROPERTIES.TEMPERATURE.MAGMA );
			}

		}

	}

}

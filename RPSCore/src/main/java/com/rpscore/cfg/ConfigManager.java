/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

package com.rpscore.cfg;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;

import com.rpscore.C;
import com.rpscore.debug.Log;
import com.rpscore.lib.FileUtils;
import com.rpscore.lib.MathUtils;

public class ConfigManager {

	/**
	 * Global constant definitions
	 */
	public static Log			LOGGER	= Log.getLogger( );

	/*
	 * Class variables
	 */
	private ArrayList< String >	keys;
	private ArrayList< String >	vars;

	private File				directory;

	private boolean				addOnModify;

	// Unlock Encrypted files
	public ConfigManager( File directory, long key ) {

		this.addOnModify = false;
		this.directory = directory;

		File[ ] files = this.directory.listFiles( );

		this.keys = new ArrayList< >( );
		this.vars = new ArrayList< >( );

		for ( File config_file : files ) {
			byte[ ] content = FileUtils.getRawFileContent( config_file );

			String[ ] raw_lines;
			try {
				raw_lines = MathUtils.decryptFile( content, key );
				interpretAndAddToConfig( raw_lines );
			} catch ( BadPaddingException e ) {

				e.printStackTrace( );
			}
		}
	}

	public ConfigManager( File directory ) {

		this.addOnModify = false;
		this.directory = directory;

		File[ ] files = this.directory.listFiles( );

		this.keys = new ArrayList< String >( );
		this.vars = new ArrayList< String >( );

		for ( File config_file : files ) {
			String[ ] raw_lines = FileUtils.getFileLines( config_file );
			interpretAndAddToConfig( raw_lines );
		}
	}

	public void modifySetting( String key, String value ) {
		for ( int i = 0; i < this.keys.size( ); i++ ) {
			if ( keys.get( i ).equals( key ) ) {
				this.vars.set( i, value );
				return;
			}
		}
		if ( this.addOnModify ) {
			this.keys.add( key );
			this.vars.add( value );
		} else {
			throw new IllegalArgumentException( "Key \"" + key + "\" not found." );
		}
	}

	// TODO fix this horrible redundant code
	public void flushSettings( ) {

		File[ ] config_files = this.directory.listFiles( );

		for ( File config_file : config_files ) {
			String[ ] raw_lines = FileUtils.getFileLines( config_file );

			for ( int i = 0; i < raw_lines.length; i++ ) {
				// Ignore the rest of the file
				if ( raw_lines[ i ].equals( "#ignore" ) ) {
					break;
				}

				// Ignore blank lines
				if ( raw_lines[ i ].equals( C.Strings.BLANK ) ) {
					continue;
				}

				// Ignore comment lines
				if ( raw_lines[ i ].startsWith( C.Strings.CONFIG_COMMENT ) ) {
					continue;
				}

				// Ignore section headers
				if ( raw_lines[ i ].startsWith( C.Strings.CONFIG_SECTION_IDENTIFIER ) ) {
					continue;
				}

				// If a value line contains fewer than 3 characters it cannot
				// have 3
				// lexical tokens, therefore it is invalid.
				if ( raw_lines[ i ].length( ) < 3 ) {
					LOGGER.error( "Line " + ( i + 1 ) + ": \"" + raw_lines[ i ]
							+ "\" does not follow the convention <Key>=<Value>." );
					continue;
				}

				if ( !raw_lines[ i ].contains( "=" ) ) {
					LOGGER.error( "Line " + ( i + 1 ) + ": \"" + raw_lines[ i ]
							+ "\" does not contain an assignment operator." );
					continue;
				}

				if ( raw_lines[ i ].startsWith( "=" ) ) {
					LOGGER.error(
							"Line " + ( i + 1 ) + ": \"" + raw_lines[ i ] + "\" starts with an assignment operator." );
					continue;
				}

				if ( raw_lines[ i ].endsWith( "=" ) ) {
					LOGGER.error(
							"Line " + ( i + 1 ) + ": \"" + raw_lines[ i ] + "\" ends with an assignment operator." );
					continue;
				}

				String[ ] definition = raw_lines[ i ].split( C.Strings.CONFIG_DEFINITION_DELIMITER );

				if ( definition.length > 2 ) {
					LOGGER.error( "Line " + ( i + 1 ) + ": \"" + raw_lines[ i ]
							+ "\" contains more than 1 assignment operator." );
					continue;
				}

				for ( int j = 0; j < this.keys.size( ); j++ ) {
					if ( definition[ 0 ].equals( this.keys.get( j ) ) ) {
						raw_lines[ i ] = this.keys.get( j ) + "=" + this.vars.get( j );
					}
				}
			}
			FileUtils.writeLinesToFile( config_file, raw_lines );

		}
	}

	private void interpretAndAddToConfig( String[ ] raw_lines ) {

		// If the input is null, the file was not read.
		if ( raw_lines == null ) {
			return;
		}

		for ( int i = 0; i < raw_lines.length; i++ ) {

			// Ignore the rest of the file
			if ( raw_lines[ i ].equals( "#ignore" ) ) {
				break;
			}

			// Ignore blank lines
			if ( raw_lines[ i ].equals( C.Strings.BLANK ) ) {
				continue;
			}

			// Ignore comment lines
			if ( raw_lines[ i ].startsWith( C.Strings.CONFIG_COMMENT ) ) {
				continue;
			}

			// Ignore section headers
			if ( raw_lines[ i ].startsWith( C.Strings.CONFIG_SECTION_IDENTIFIER ) ) {
				continue;
			}

			// If a value line contains fewer than 3 characters it cannot have 3
			// lexical tokens, therefore it is invalid.
			if ( raw_lines[ i ].length( ) < 3 ) {
				LOGGER.error( "Line " + ( i + 1 ) + ": \"" + raw_lines[ i ]
						+ "\" does not follow the convention <Key>=<Value>." );
				continue;
			}

			if ( !raw_lines[ i ].contains( "=" ) ) {
				LOGGER.error(
						"Line " + ( i + 1 ) + ": \"" + raw_lines[ i ] + "\" does not contain an assignment operator." );
				continue;
			}

			if ( raw_lines[ i ].startsWith( "=" ) ) {
				LOGGER.error(
						"Line " + ( i + 1 ) + ": \"" + raw_lines[ i ] + "\" starts with an assignment operator." );
				continue;
			}

			if ( raw_lines[ i ].endsWith( "=" ) ) {
				LOGGER.error( "Line " + ( i + 1 ) + ": \"" + raw_lines[ i ] + "\" ends with an assignment operator." );
				continue;
			}

			String[ ] definition = raw_lines[ i ].split( C.Strings.CONFIG_DEFINITION_DELIMITER );

			if ( definition.length > 2 ) {
				LOGGER.error( "Line " + ( i + 1 ) + ": \"" + raw_lines[ i ]
						+ "\" contains more than 1 assignment operator." );
				continue;
			}

			this.keys.add( definition[ 0 ] );
			this.vars.add( definition[ 1 ] );

		}

	}

	public String getSetting( String key, String default_value ) {

		for ( int i = 0; i < this.keys.size( ); i++ ) {
			if ( this.keys.get( i ).equals( key ) ) {
				return this.vars.get( i );
			}
		}

		return default_value;
	}

	public boolean getSetting( String key, boolean default_value ) {
		for ( int i = 0; i < this.keys.size( ); i++ ) {
			if ( this.keys.get( i ).equals( key ) ) {
				return Boolean.parseBoolean( this.vars.get( i ) );
			}
		}
		return default_value;
	}

	public int getSetting( String key, int default_value ) {
		for ( int i = 0; i < this.keys.size( ); i++ ) {
			if ( this.keys.get( i ).equals( key ) ) {
				try {

					boolean negative = false;

					String sval = this.vars.get( i );

					if ( sval.startsWith( "-" ) ) {
						sval = sval.replaceFirst( "-", "" );
						negative = true;
					}

					int value = Integer.parseInt( sval );
					return ( negative ) ? ( -1 * value ) : value;
				} catch ( NumberFormatException e ) {
					return default_value;
				}
			}
		}
		return default_value;
	}

	public double getSetting( String key, double default_value ) {
		for ( int i = 0; i < this.keys.size( ); i++ ) {
			if ( this.keys.get( i ).equals( key ) ) {
				try {
					return Double.parseDouble( this.vars.get( i ) );
				} catch ( NumberFormatException e ) {
					return default_value;
				}
			}
		}
		return default_value;
	}

	public long getSetting( String key, long default_value ) {
		for ( int i = 0; i < this.keys.size( ); i++ ) {
			if ( this.keys.get( i ).equals( key ) ) {
				try {
					return Long.parseLong( this.vars.get( i ) );
				} catch ( NumberFormatException e ) {
					return default_value;
				}
			}
		}
		return default_value;
	}

	public Color getSetting( String key, Color default_value ) {

		for ( int i = 0; i < this.keys.size( ); i++ ) {
			if ( this.keys.get( i ).equals( key ) ) {
				return new Color( ( int ) Long.parseLong( this.vars.get( i ), 16 ), true );
			}
		}
		return default_value;
	}

	public String[ ] listSettings( ) {

		String[ ] ret = new String[ this.keys.size( ) ];

		for ( int i = 0; i < ret.length; i++ ) {
			ret[ i ] = this.keys.get( i );
		}

		return ret;

	}

	public String[ ] getFullConfigState( ) {

		String[ ] ret = new String[ this.keys.size( ) ];

		for ( int i = 0; i < ret.length; i++ ) {
			ret[ i ] = this.keys.get( i ) + "=" + this.vars.get( i );
		}

		return ret;

	}

	// Singleton pattern for generating a config manager.
	// This reduces the likelihood of an error.
	private static ConfigManager cm;

	public static ConfigManager getConfigManager( ) {
		if ( cm == null ) {
			cm = new ConfigManager( new File( C.File.CONFIG_FOLDER ) );
		}
		return cm;
	}

	private static ConfigManager sm;

	public static ConfigManager getPlayerDataHolder( File dataFile, boolean refresh, long key ) {
		if ( sm == null || refresh ) {
			sm = new ConfigManager( dataFile, key );
		}
		return sm;
	}

}

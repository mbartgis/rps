/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-05-05 01:10:23 -0400 (Sat, 05 May 2018) $
 * $Revision: 356 $
 * $Author: mbartgis $
 */

package com.rpscore;

import java.awt.Font;

public class C {

	/**
	 * Serializable ID to represent the API versions used. This is used to
	 * determine client/server compatibility.
	 */
	public static final int BUILD_NUMBER = 10;

	public static final class Debug {
		public static final String MODIFIED_PARAMETER_DETECTED = "Modified Parameter Detected: ";

		private Debug( ) {}
	}

	public static final class Strings {

		public static final String	BLANK						= "";

		public static final String	CONFIG_COMMENT				= ";";

		public static final String	CONFIG_SECTION_IDENTIFIER	= "[";

		public static final String	CONFIG_DEFINITION_DELIMITER	= "=";

		public static final String	FILE_EXTENSION_DELIMITER	= "\\.";

		private Strings( ) {}

	}

	public static final class GameplayConstants {

		public static final String DEFAULT_PLAYER_NAME = "Player";

		private GameplayConstants( ) {}

	}

	public static final class File {

		/**
		 * Constant that is generated before runtime that determines the line
		 * separator used in the current OS's file system (i.e. "\n", "\r\n").
		 */
		public static final String	LS							= System.getProperty( "line.separator" );

		/**
		 * Constant that is generated before runtime that determines the folder
		 * and file separator used in the current OS's file system (i.e. "/",
		 * "\").
		 */
		public static final String	FS							= System.getProperty( "file.separator" );

		/**
		 * Constant that is generated before runtime that determines root folder
		 * of the program (i.e. The folder in which the contained executable was
		 * launched from).
		 */
		public static final String	CWD							= System.getProperty( "user.dir" ) + FS;

		// Config hierarchy
		public static final String	CONFIG_FOLDER				= CWD + FS + "cfg" + FS;

		// Savedata hierarchy
		public static final String	SAVEDATA_FOLDER				= CWD + "savedata" + FS;
		public static final String	PLAYER_SAVEDATA_FOLDER		= SAVEDATA_FOLDER + "game" + FS;
		public static final String	SERVER_SAVEDATA_FOLDER		= SAVEDATA_FOLDER + "servers" + FS;

		// Resources hierarchy
		public static final String	RESOURCE_FOLDER				= CWD + "res" + FS;

		// Log folder hierarchy
		public static final String	LOGS_FOLDER					= CWD + "logs" + FS;

		// Library folder hierarchy
		public static final String	LIBRARIES_FOLDER			= CWD + "lib" + FS;

		// Resources -- audio
		public static final String	SOUND_FOLDER				= RESOURCE_FOLDER + "sound" + FS;
		public static final String	SFX_FOLDER					= SOUND_FOLDER + "sfx" + FS;
		public static final String	BULLET_SFX_FOLDER			= SFX_FOLDER + "bullet" + FS;
		public static final String	BGM_FOLDER					= SOUND_FOLDER + "background" + FS;
		public static final String	MUSIC_NORMAL				= BGM_FOLDER + FS + "Normal" + FS;
		public static final String	MUSIC_BOSS					= BGM_FOLDER + FS + "Boss" + FS;
		public static final String	MUSIC_BREAK					= BGM_FOLDER + FS + "Break" + FS;
		public static final String	MUSIC_MENU					= BGM_FOLDER + FS + "Menu" + FS;

		// Resources -- Images -- Player
		public static final String	PLAYER_GFX_FOLDER			= RESOURCE_FOLDER + "player" + FS;
		public static final String	ICON_PLAYER_NORMAL			= PLAYER_GFX_FOLDER + "player_beta.png";
		public static final String	ICON_PLAYER_ALLY			= PLAYER_GFX_FOLDER + "other_player_beta.png";
		public static final String	ICON_PLAYER_ENEMY			= PLAYER_GFX_FOLDER + "enemy_player_beta.png";
		public static final String	ICON_PLAYER_CURSOR			= PLAYER_GFX_FOLDER + "null_cursor.png";

		// Resources -- Images -- Enemies
		public static final String	ENEMY_GFX_FOLDER			= RESOURCE_FOLDER + "enemies" + FS;
		public static final String	DUB_GFX_FOLDER				= ENEMY_GFX_FOLDER + "dub" + FS;
		public static final String	BOMBER_GFX_FOLDER			= ENEMY_GFX_FOLDER + "bomber" + FS;
		public static final String	MAD_BOMBER_GFX_FOLDER		= ENEMY_GFX_FOLDER + "mad_bomber" + FS;
		public static final String	ROGUE_CANNON_GFX_FOLDER		= ENEMY_GFX_FOLDER + "rogue_cannon" + FS;

		// Resources -- Images -- Pickups
		public static final String	PICKUP_GFX_FOLDER			= RESOURCE_FOLDER + "pickups" + FS;
		public static final String	HEALTH_PICKUP_GFX_FOLDER	= PICKUP_GFX_FOLDER + "health" + FS;
		public static final String	MAGIC_PICKUP_GFX_FOLDER		= PICKUP_GFX_FOLDER + "magic" + FS;
		public static final String	STAMINA_PICKUP_GFX_FOLDER	= PICKUP_GFX_FOLDER + "stamina" + FS;

		// Resources -- Images -- Blocks
		public static final String	BLOCK_GFX_FOLDER			= RESOURCE_FOLDER + "blocks" + FS;

		// Resources -- Images -- Icons
		public static final String	ICONS_FOLDER				= RESOURCE_FOLDER + "icons" + FS;
		public static final String	WEAPON_ICONS_FOLDER			= ICONS_FOLDER + "weapons" + FS;
		public static final String	RPS_LOGO					= ICONS_FOLDER + "rps_icon.png";
		public static final String	SETTINGS_ICON				= ICONS_FOLDER + "settings_icon.png";

		// Manual hierarchy
		public static final String	MANUAL_FOLDER				= CWD + "manual" + FS;
		public static final String	MANUAL_IMDEX_PAGE			= MANUAL_FOLDER + "index.html";

		// Misc. File Strings
		public static final String	WAVE_EXTENSION				= ".wav";
		public static final String	PLAYER_SAVE_FILE_NAME		= "player.dat";

		private File( ) {}
	}

	public static final class Size {

		public static final int	EMPTY			= 0;
		public static final int	UINT_16_MAX		= 65535;

		public static final int	BGM_BUFFER_SIZE	= 65536;
		public static final int	SFX_BUFFER_SIZE	= 4;

		private Size( ) {}
	}

	public static final class Dimensions {
		public static final int BLOCK_SIZE = 256;

		private Dimensions( ) {}
	}

	public static final class Time {

		public static final int ONE_MILLISECOND = 1;

		public static final class Game {

			public static final int	QUARTER_SECOND			= 15;
			public static final int	HALF_SECOND				= 30;
			public static final int	ONE_SECOND				= 60;
			public static final int	SECOND_AND_A_HALF		= 90;
			public static final int	TWO_SECONDS				= 120;
			public static final int	TWO_AND_A_HALF_SECONDS	= 150;
			public static final int	THREE_SECONDS			= 190;
			public static final int	FOUR_SECONDS			= 240;
			public static final int	FIVE_SECONDS			= 300;
			public static final int	TEN_SECONDS				= 600;

			private Game( ) {}

		}

		private Time( ) {}

	}

	public static final class Err {
		public static final IllegalArgumentException	EMPTY_COMMAND			= new IllegalArgumentException(
				"Empty command." );

		public static final IllegalArgumentException	NULL_COMMAND			= new IllegalArgumentException(
				"Null command." );

		public static final NumberFormatException		OCTET_OUT_OF_BNOUNDS	= new NumberFormatException(
				"An IP Address octet must be between 0-255." );

		private Err( ) {}

	}

	public static final class SettingKeys {

		public static final String	SMOOTH_BLOCK_EDGES		= "SmoothBlockEdges";
		public static final String	FULLSCREEN				= "Fullscreen";

		public static final String	DISPLAY_FPS				= "DisplayFPS";
		public static final String	FPS_DISPLAY_LOCATION	= "FPSDisplayLocation";
		public static final String	FPS_DISPLAY_COLOR		= "FPSDisplayColor";

		public static final String	DISPLAY_ROTATION_DATA	= "DisplayRotationData";
		public static final String	DISPLAY_BOUNDING_BOXES	= "DisplayBoundingBoxes";

		public static final String	CAP_FPS					= "CapFPS";
		public static final String	FPS_LIMIT				= "FPSLimit";

		public static final String	HUD						= "HUD";

		public static final String	MORTAR_LANDING_ASSIST	= "MortarLandingAssist";
		public static final String	BOMB_POWER_INDICATOR	= "BombPowerIndicator";

		public static final String	RENDER_DISTANCE			= "RenderDistance";

		public static final String	MASTER_AUDIO_VOLUME		= "MasterAudioVolume";
		public static final String	BG_AUDIO_VOLUME			= "BGAudioVolume";
		public static final String	SFX_AUDIO_VOLUME		= "SFXAudioVolume";

		public static final String	GENERATE_FOG			= "GenerateFog";

		private SettingKeys( ) {}

	}

	public static class Fonts {

		public static final Font	MENU_FONT		= new Font( "Arial", Font.BOLD, 48 );
		public static final Font	MENU_MOTD_FONT	= new Font( "Arial", Font.BOLD, 30 );

		private Fonts( ) {}
	}

	private C( ) {}

}

package com.rpscore;
/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.rpscore.cfg.ConfigManager;
import com.rpscore.lib.FileUtils;

public class TestSimpleConfigManager {

	private static final boolean	ALLOW_SKIP			= true;

	private static final int		NUMBER_OF_KEYS		= 9;

	private static final int		EXPECTED_PORT		= 22000;

	private static final int		EXPECTED_WIDTH		= 800;

	private static final int		EXPECTED_HEIGHT		= 600;

	private static final int		EXPECTED_GFX		= 5;

	private static final double		EXPECTED_VERSION	= 0.65;

	private static final String		BAD_KEY				= "askjlasdkjlkalsjdkjlas8ioaio";

	private static final String		BAD_VALUE			= "asdasdasdiuhaJSKD8I90OWWsadls";

	private static final int		BAD_INT_VALUE		= 12233001;

	private static final double		BAD_DOUBLE_VALUE	= 0.99899;

	/* @formatter:off */
	public static String[] MAIN_CONTENT = {
			"; Comment Header",
			"[Dimension Test]",
			"Width=800",
			"Height=600",
			"Graphics=5",
			"HUD=true",

	};

	public static String[] NET_CONTENT = {
			"; This files defines the network settings...",
			"[Socket Info]",
			"IP=127.0.0.1",
			"Port=22000",
			"",
			"[Credentials]",
			"Username=John Doe",
			"Password=welcome1"
	};

	public static String[] VERSION_CONTENT = {
			"; This files defines the network settings...",
			"[Version Info]",
			"VersionID=0.65"

	};

	/* @formatter:on */

	// Setup
	public static TemporaryFolder	CFG_ROOT			= new TemporaryFolder( );
	public static File				CFG_MAIN;
	public static File				CFG_NET;
	public static File				CFG_VERSION;

	public static ConfigManager		CFGM;

	@Before
	public void setup( ) {
		
		if ( ALLOW_SKIP ) {
			return;
		}
		
		try {
			CFG_ROOT.create( );
			CFG_MAIN = CFG_ROOT.newFile( "cfg_main.ini" );
			CFG_NET = CFG_ROOT.newFile( "cfg_net.ini" );
			CFG_VERSION = CFG_ROOT.newFile( "cfg_version.ini" );
			System.out.println( "Files set up correctly." );
		} catch ( IOException e ) {
			fail( "Failed to create temporary directory for test..." );
		}

		if ( CFG_MAIN == null ) {
			fail( "Null config file..." );
		}

		FileUtils.writeLinesToFile( CFG_MAIN, MAIN_CONTENT );
		FileUtils.writeLinesToFile( CFG_NET, NET_CONTENT );
		FileUtils.writeLinesToFile( CFG_VERSION, VERSION_CONTENT );

		CFGM = new ConfigManager( CFG_ROOT.getRoot( ) );
		
		System.out.println( "CFG set up correctly." );

	}

	@Test
	public void testListSettings( ) {

		if ( ALLOW_SKIP ) {
			return;
		}
		
		String[ ] keys = null;

		try {
			keys = CFGM.listSettings( );
		} catch ( ClassCastException e ) {
			// This is used to ensure that if the ArrayList<>.toArray() method
			// is called, the cast is successful.
			fail( "The settings could not be cast to a string array..." );
		}

		if ( keys == null ) {
			fail( "could not acquire keys..." );
		}

		assertEquals( NUMBER_OF_KEYS, keys.length );

		for (String key : keys) {
			System.out.println( key );
		}
		
		assertTrue( keys[ 0 ].equals( "Width" ) );
		assertTrue( keys[ 1 ].equals( "Height" ) );
		assertTrue( keys[ 2 ].equals( "Graphics" ) );
		assertTrue( keys[ 3 ].equals( "HUD" ) );
		assertTrue( keys[ 4 ].equals( "IP" ) );
		assertTrue( keys[ 5 ].equals( "Port" ) );
		assertTrue( keys[ 6 ].equals( "Username" ) );
		assertTrue( keys[ 7 ].equals( "Password" ) );
		assertTrue( keys[ 8 ].equals( "VersionID" ) );

	}

	@Test
	public void testKeyLookups( ) {
		
		if ( ALLOW_SKIP ) {
			return;
		}
		
		String[ ] keys = null;

		try {
			keys = CFGM.listSettings( );
		} catch ( ClassCastException e ) {
			// This is used to ensure that if the ArrayList<>.toArray() method
			// is called, the cast is successful.
			fail( "The settings could not be cast to a string array..." );
		}

		if ( keys == null ) {
			fail( "could not acquire keys..." );
		}

		assertEquals( NUMBER_OF_KEYS, keys.length );

		assertTrue( CFGM.getSetting( keys[ 0 ], "" ).equals( "800" ) );
		assertTrue( CFGM.getSetting( keys[ 1 ], "" ).equals( "600" ) );
		assertTrue( CFGM.getSetting( keys[ 2 ], "" ).equals( "5" ) );
		assertTrue( CFGM.getSetting( keys[ 3 ], "" ).equals( "true" ) );
		assertTrue( CFGM.getSetting( keys[ 4 ], "" ).equals( "127.0.0.1" ) );
		assertTrue( CFGM.getSetting( keys[ 5 ], "" ).equals( "22000" ) );
		assertTrue( CFGM.getSetting( keys[ 6 ], "" ).equals( "John Doe" ) );
		assertTrue( CFGM.getSetting( keys[ 7 ], "" ).equals( "welcome1" ) );
		assertTrue( CFGM.getSetting( keys[ 8 ], "" ).equals( "0.65" ) );

		// Ensure that a bad lookup returns a user defined key.
		assertTrue( CFGM.getSetting( BAD_KEY, BAD_VALUE ).equals( BAD_VALUE ) );

	}

	@Test
	public void testIntegerSetting( ) {
		
		if ( ALLOW_SKIP ) {
			return;
		}
		
		String[ ] keys = null;

		try {
			keys = CFGM.listSettings( );
		} catch ( ClassCastException e ) {
			// This is used to ensure that if the ArrayList<>.toArray() method
			// is called, the cast is successful.
			fail( "The settings could not be cast to a string array..." );
		}

		if ( keys == null ) {
			fail( "could not acquire keys..." );
		}

		assertEquals( NUMBER_OF_KEYS, keys.length );

		assertEquals( EXPECTED_WIDTH, CFGM.getSetting( keys[ 0 ], EXPECTED_WIDTH + 1 ) );
		assertEquals( EXPECTED_HEIGHT, CFGM.getSetting( keys[ 1 ], EXPECTED_HEIGHT + 1 ) );
		assertEquals( EXPECTED_GFX, CFGM.getSetting( keys[ 2 ], EXPECTED_GFX + 1 ) );
		assertEquals( EXPECTED_PORT, CFGM.getSetting( keys[ 5 ], EXPECTED_PORT + 1 ) );

		// Ensure that a bad lookup returns a user defined key.
		assertEquals( BAD_INT_VALUE, CFGM.getSetting( BAD_KEY, BAD_INT_VALUE ) );

	}

	@Test
	@SuppressWarnings( "deprecation" )
	public void testDoubleSetting( ) {
		
		if ( ALLOW_SKIP ) {
			return;
		}
		
		String[ ] keys = null;

		try {
			keys = CFGM.listSettings( );
		} catch ( ClassCastException e ) {
			// This is used to ensure that if the ArrayList<>.toArray() method
			// is called, the cast is successful.
			fail( "The settings could not be cast to a string array..." );
		}

		if ( keys == null ) {
			fail( "could not acquire keys..." );
		}

		assertEquals( NUMBER_OF_KEYS, keys.length );

		assertEquals( EXPECTED_VERSION, CFGM.getSetting( keys[ 8 ], EXPECTED_WIDTH + 0.35 ), 0.001 );

		// Ensure that a bad lookup returns a user defined key.
		assertEquals( BAD_DOUBLE_VALUE, CFGM.getSetting( BAD_KEY, BAD_DOUBLE_VALUE ), 0.001 );
	}

	@Test
	public void testColorSetting( ) {
		if ( ALLOW_SKIP ) {
			return;
		}
	}

	@Test
	public void testBooleanSetting( ) {
		if ( ALLOW_SKIP ) {
			return;
		}
	}

	@Test
	public void testLocalValueChanges( ) {
		if ( ALLOW_SKIP ) {
			return;
		}
	}

	@Test
	public void testNoPersistenceWithoutSettingsFlush( ) {
		if ( ALLOW_SKIP ) {
			return;
		}
	}

	@Test
	public void testPersistenceWithSettingsFlush( ) {
		if ( ALLOW_SKIP ) {
			return;
		}
	}

	@After
	public void teardown( ) {
		
		if ( ALLOW_SKIP ) {
			return;
		}
		
		CFG_ROOT.delete( );
		System.out.println( "Temp files removed." );
	}

}

/*
 * COPYRIGHT NOTICE:
 * Copyright 2018, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-03-31 16:45:02 -0400 (Sat, 31 Mar 2018) $
 * $Revision: 334 $
 * $Author: mbartgis $
 */
package com.rpscore;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.rpscore.terrain.Block;
import com.rpscore.terrain.BlockRegistry;
import com.rpscore.terrain.TerrainGenerator;
import com.rpscore.terrain.mutators.TerrainMutator;

public class TestTerrainMutatorBounds {

	@Test
	public void testMutatorBoundsMinCase( ) {
		Block NULL = BlockRegistry.BLOCK_TYPE.BLOCK_NULL;
		/**
		 * @formatter:off
		 */
		Block[ ][ ] map = new Block[ ][ ] {
			{ NULL, NULL, NULL },
			{ NULL, NULL, NULL },
			{ NULL, NULL, NULL }
		};
		/**
		 * @formatter:on
		 */

		// This should not thrown an exception
		TerrainGenerator.mutateMap( map );
	}

	@Test
	public void testMutatorGetsAllBlocks( ) {

		Block[ ][ ] map = new Block[ 3 ][ ];

		for ( int i = 0; i < map.length; i++ ) {
			map[ i ] = new Block[ 3 ];
			for ( int j = 0; j < map[ i ].length; j++ ) {
				map[ i ][ j ] = new Block( 0 );
			}
		}

		TerrainGenerator.resetTerrainMutators( new TerrainMutator[ ] { new TerrainMutator( ) {

			@Override
			public void apply( Block[ ][ ] blocks, int i, int j, Block[ ] adjacent, Block[ ] diagonals ) {
				int sum = adjacent.length + diagonals.length;
				blocks[ i ][ j ].setID( sum );
			}
		} } );
		// This should not thrown an exception
		TerrainGenerator.mutateMap( map );

		assertEquals( 3, map[ 0 ][ 0 ].getID( ) );
		assertEquals( 5, map[ 0 ][ 1 ].getID( ) );
		assertEquals( 3, map[ 0 ][ 2 ].getID( ) );
		assertEquals( 5, map[ 1 ][ 0 ].getID( ) );
		assertEquals( 8, map[ 1 ][ 1 ].getID( ) );
		assertEquals( 5, map[ 1 ][ 2 ].getID( ) );
		assertEquals( 3, map[ 2 ][ 0 ].getID( ) );
		assertEquals( 5, map[ 2 ][ 1 ].getID( ) );
		assertEquals( 3, map[ 2 ][ 2 ].getID( ) );

	}

	@Test
	public void testBigSquare( ) {

		Block[ ][ ] map = new Block[ 16 ][ ];

		for ( int i = 0; i < map.length; i++ ) {
			map[ i ] = new Block[ 16 ];
			for ( int j = 0; j < map[ i ].length; j++ ) {
				map[ i ][ j ] = new Block( 0 );
			}
		}

		TerrainGenerator.resetTerrainMutators( new TerrainMutator[ ] { new TerrainMutator( ) {

			@Override
			public void apply( Block[ ][ ] blocks, int i, int j, Block[ ] adjacent, Block[ ] diagonals ) {
				int sum = adjacent.length + diagonals.length;
				blocks[ i ][ j ].setID( sum );
			}
		} } );
		// This should not thrown an exception
		TerrainGenerator.mutateMap( map );

		for ( int i = 1; i < map.length - 1; i++ ) {
			for ( int j = 1; j < map[ i ].length - 1; j++ ) {
				assertEquals( 8, map[ i ][ j ].getID( ) );
			}
		}

		for ( int i = 0; i < 1; i++ ) {
			for ( int j = 1; j < map[ i ].length - 1; j++ ) {
				assertEquals( 5, map[ i ][ j ].getID( ) );
			}
		}

		for ( int i = 15; i < 16; i++ ) {
			for ( int j = 1; j < map[ i ].length - 1; j++ ) {
				assertEquals( 5, map[ i ][ j ].getID( ) );
			}
		}

		for ( int i = 1; i < map.length - 1; i++ ) {
			for ( int j = 0; j < 1; j++ ) {
				assertEquals( 5, map[ i ][ j ].getID( ) );
			}
		}

		for ( int i = 1; i < map.length - 1; i++ ) {
			for ( int j = 15; j < 16; j++ ) {
				assertEquals( 5, map[ i ][ j ].getID( ) );
			}
		}

		assertEquals( 3, map[ 0 ][ 0 ].getID( ) );
		assertEquals( 3, map[ 0 ][ 15 ].getID( ) );
		assertEquals( 3, map[ 15 ][ 0 ].getID( ) );
		assertEquals( 3, map[ 15 ][ 15 ].getID( ) );

	}

}

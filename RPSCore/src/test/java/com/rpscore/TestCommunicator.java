/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2019-01-21 10:32:39 -0500 (Mon, 21 Jan 2019) $
 * $Revision: 376 $
 * $Author: mbartgis $
 */

package com.rpscore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rpscore.lib.TestUtils;
import com.rpscore.server.BasicIIServer;
import com.rpscore.server.Communicator;

public class TestCommunicator {

	public static BasicIIServer	serv;

	public static boolean		ALLOW_SKIP	= false;

	@Before
	public void setup( ) {

		if ( ALLOW_SKIP ) {
			return;
		}

		// Create a basic information interchange server to test the
		// communicator class.
		serv = new BasicIIServer( );

		// Wait 0.5 seconds to ensure that the server is setup
		try {
			Thread.sleep( 500 );
		} catch ( InterruptedException e2 ) {}

	}

	@Test( expected = IllegalArgumentException.class )
	public void testNullClientWrap( ) {
		System.out.println( "Executing: " + new Throwable( ).getStackTrace( )[ 0 ].getMethodName( ) );
		if ( ALLOW_SKIP ) {
			throw new IllegalArgumentException( "Skip" );
		}

		Socket client = null;

		try {
			Communicator c1 = new Communicator( client );
		} catch ( IOException e ) {
			fail( "An I/O Exception should not be thrown if a communicator is null." );
		}

	}

	@Test( expected = IOException.class )
	public void testClosedSocketWrap( ) throws IOException {
		System.out.println( "Executing: " + new Throwable( ).getStackTrace( )[ 0 ].getMethodName( ) );
		if ( ALLOW_SKIP ) {
			throw new IOException( "Skip" );
		}

		Socket client = null;
		try {
			client = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client );

		try {
			client.close( );
		} catch ( IOException e1 ) {}

		// Ensure that the client is closed.
		assertTrue( client.isClosed( ) );

		Communicator c1 = new Communicator( client );
	}

	@Test
	public void testCreateAndShutDown( ) {
		System.out.println( "Executing: " + new Throwable( ).getStackTrace( )[ 0 ].getMethodName( ) );
		if ( ALLOW_SKIP ) {
			return;
		}

		Socket client = null;
		try {
			client = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client );

		Communicator c1 = null;
		try {
			c1 = new Communicator( client );
		} catch ( IOException e ) {}

		// Ensure that the Communicator was created successfully.
		assertNotNull( c1 );

		// Ensure that communications are not closed.
		assertFalse( c1.isClosed( ) );

		// Ensure that the client reference returned by the Communicator is
		// identical to the one provided.
		assertEquals( c1.getClient( ), client );

		// Ensure that the communicator is shut down successfully.
		assertTrue( c1.disconnect( ) );

		// Ensure that the client returns a status of being closed.
		assertTrue( c1.isClosed( ) );

		// Ensure that the contained socket is also closed.
		assertTrue( c1.getClient( ).isClosed( ) );

	}

	@Test
	public void testByteArrayTransfer( ) {
		System.out.println( "Executing: " + new Throwable( ).getStackTrace( )[ 0 ].getMethodName( ) );
		if ( !ALLOW_SKIP ) {
			return;
		}

		Socket client1 = null;
		try {
			client1 = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client1 );

		Communicator c1 = null;
		try {
			c1 = new Communicator( client1 );
		} catch ( IOException e ) {}

		// Ensure that the Communicator was created successfully.
		assertNotNull( c1 );

		// Ensure that communications are not closed.
		assertFalse( c1.isClosed( ) );

		Socket client2 = null;
		try {
			client2 = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client2 );

		Communicator c2 = null;
		try {
			c2 = new Communicator( client2 );
		} catch ( IOException e ) {}

		// Ensure that the Communicator was created successfully.
		assertNotNull( c2 );

		// Ensure that communications are not closed.
		assertFalse( c2.isClosed( ) );

		try {
			Thread.sleep( 5000 );
		} catch ( InterruptedException ex1 ) {}

		// Repeat this test 1000 times
		for ( int i = 0; i < 1000; i++ ) {
			byte[ ] c1message = TestUtils.createRandomTransmissionArray( TestUtils.SIZE_TRIVIAL );

			c1.sendMessage( c1message );

			long startTime = System.currentTimeMillis( );

			// Test the system latency and ensure that the communicator delivers
			// a
			// message is a reasonable amount of time.
			byte[ ][ ] messages;
			while ( ( messages = c2.getMessages( ) ).length == 0 ) {
				if ( ( System.currentTimeMillis( ) - startTime ) > 5000 ) {
					fail( "Communication has likely failed. Relevant string..." );
				}
			}

			assertEquals( 1, messages.length );
			assertEquals( c1message.length, messages[ 0 ].length );

			for ( int j = 0; j < messages.length; j++ ) {

				assertEquals( c1message[ j ], messages[ 0 ][ j ] );

			}
			// assertEquals( Communicator.toStringArray( messages[ 0 ] ),
			// c1message );

			byte[ ] c2message = TestUtils.createRandomTransmissionArray( TestUtils.SIZE_TRIVIAL );

			c2.sendMessage( c2message );

			startTime = System.currentTimeMillis( );

			// Test the system latency and ensure that the communicator delivers
			// a
			// message is a reasonable amount of time.
			while ( ( messages = c1.getMessages( ) ).length == 0 ) {
				if ( ( System.currentTimeMillis( ) - startTime ) > 5000 ) {
					fail( "Communication has likely failed. Relevant string: " + c2message );
				}
			}

			assertEquals( messages.length, 1 );

			assertEquals( c2message.length, messages[ 0 ].length );

			for ( int j = 0; j < messages.length; j++ ) {
				assertEquals( c2message[ j ], messages[ 0 ][ j ] );
			}

			// assertEquals( Communicator.toStringArray( messages[ 0 ] ),
			// c2message );

		}

		assertTrue( c1.disconnect( ) );
		assertTrue( c2.disconnect( ) );

		assertTrue( c1.isClosed( ) );
		assertTrue( c2.isClosed( ) );
	}

	@Test
	public void testLargeSerialTransfer( ) {
		System.out.println( "Executing: " + new Throwable( ).getStackTrace( )[ 0 ].getMethodName( ) );
		if ( ALLOW_SKIP ) {
			return;
		}

		Socket client1 = null;
		try {
			client1 = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client1 );

		Communicator c1 = null;
		try {
			c1 = new Communicator( client1 );
		} catch ( IOException e ) {}

		// Ensure that the Communicator was created successfully.
		assertNotNull( c1 );

		// Ensure that communications are not closed.
		assertFalse( c1.isClosed( ) );

		Socket client2 = null;
		try {
			client2 = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client2 );

		Communicator c2 = null;
		try {
			c2 = new Communicator( client2 );
		} catch ( IOException e ) {}

		// Ensure that the Communicator was created successfully.
		assertNotNull( c2 );

		// Ensure that communications are not closed.
		assertFalse( c2.isClosed( ) );

		try {
			Thread.sleep( 5000 );
		} catch ( InterruptedException ex1 ) {}

		// Create a 1GB byte array
		byte[ ] c1message = TestUtils.createRandomTransmissionArray( ( 1024 * 1024 ) );

		c1.sendMessage( c1message );

		long startTime = System.currentTimeMillis( );

		// Test the system latency and ensure that the communicator delivers
		// a
		// message is a reasonable amount of time.
		byte[ ][ ] messages;
		while ( ( messages = c2.getMessages( ) ).length == 0 ) {
			// Give the transfer a timeout of 5 minutes.
			if ( ( System.currentTimeMillis( ) - startTime ) > 300000 ) {
				fail( "Communication has likely failed. Relevant string..." );
			}
			try {
				Thread.sleep( 500 );
			} catch ( InterruptedException e ) {}
			c2.sendHeartbeat( );
		}

		long elapsed = System.currentTimeMillis( ) - startTime;

		// Get the speed in Kb/s
		double performance = ( ( double ) ( messages[ 0 ].length / elapsed ) * 1000 ) / 64.0;

		System.out.println( "Transfer speed of ~" + performance + " Kb/s." );

		assertEquals( 1, messages.length );
		assertEquals( c1message.length, messages[ 0 ].length );

		int cnt = 0;

		for ( int j = 0; j < messages[ 0 ].length; j++ ) {
			assertEquals( "Failed on Index: " + cnt, c1message[ j ], messages[ 0 ][ j ] );
			cnt++;
		}

		assertTrue( c1.disconnect( ) );
		assertTrue( c2.disconnect( ) );

		assertTrue( c1.isClosed( ) );
		assertTrue( c2.isClosed( ) );
	}

	@Test
	public void testSmallSegmentTransfer( ) {
		System.out.println( "Executing: " + new Throwable( ).getStackTrace( )[ 0 ].getMethodName( ) );
		if ( !ALLOW_SKIP ) {
			return;
		}

		Socket client1 = null;
		try {
			client1 = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client1 );

		Communicator c1 = null;
		try {
			c1 = new Communicator( client1 );
		} catch ( IOException e ) {}

		// Ensure that the Communicator was created successfully.
		assertNotNull( c1 );

		// Ensure that communications are not closed.
		assertFalse( c1.isClosed( ) );

		Socket client2 = null;
		try {
			client2 = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client2 );

		Communicator c2 = null;
		try {
			c2 = new Communicator( client2 );
		} catch ( IOException e ) {}

		// Ensure that the Communicator was created successfully.
		assertNotNull( c2 );

		// Ensure that communications are not closed.
		assertFalse( c2.isClosed( ) );

		try {
			Thread.sleep( 5000 );
		} catch ( InterruptedException ex1 ) {}

		byte[ ] c1message = new byte[ 16 ];

		for ( int i = 0; i < c1message.length; i++ ) {
			c1message[ i ] = ( byte ) i;
		}

		int MAX = 20000;

		long startTime = System.currentTimeMillis( );

		for ( int i = 0; i < MAX; i++ ) {

			long testTime = System.currentTimeMillis( );

			c1.sendMessage( c1message );

			byte[ ][ ] messages;
			while ( ( messages = c2.getMessages( ) ).length == 0 ) {
				if ( ( System.currentTimeMillis( ) - testTime ) > 5000 ) {
					fail( "Communication has likely failed. Relevant string..." );
				}
			}

			c2.sendHeartbeat( );

		}

		long elapsed = System.currentTimeMillis( ) - startTime;

		// Get the speed in Kb/s
		double performance = ( ( double ) ( ( c1message.length * MAX ) / elapsed ) * 1000 ) / 64.0;

		System.out.println( "Transfer speed of ~" + performance + " Kb/s." );

		assertTrue( c1.disconnect( ) );
		assertTrue( c2.disconnect( ) );

		assertTrue( c1.isClosed( ) );
		assertTrue( c2.isClosed( ) );
	}

	@Test
	public void testHeartbeatIsNotMessage( ) {
		System.out.println( "Executing: " + new Throwable( ).getStackTrace( )[ 0 ].getMethodName( ) );
		if ( !ALLOW_SKIP ) {
			return;
		}

		Socket client1 = null;
		try {
			client1 = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client1 );

		Communicator c1 = null;
		try {
			c1 = new Communicator( client1 );
		} catch ( IOException e ) {}

		// Ensure that the Communicator was created successfully.
		assertNotNull( c1 );

		// Ensure that communications are not closed.
		assertFalse( c1.isClosed( ) );

		Socket client2 = null;
		try {
			client2 = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client2 );

		Communicator c2 = null;
		try {
			c2 = new Communicator( client2 );
		} catch ( IOException e ) {}

		// Ensure that the Communicator was created successfully.
		assertNotNull( c2 );

		// Ensure that communications are not closed.
		assertFalse( c2.isClosed( ) );

		c1.sendHeartbeat( );

		long startTime = System.currentTimeMillis( );

		byte[ ][ ] messages;
		while ( ( messages = c2.getMessages( ) ).length == 0 ) {
			if ( ( System.currentTimeMillis( ) - startTime ) > 5000 ) {
				break;
			}
		}

		if ( messages.length > 0 ) {
			fail( "Heartbeat transmitted as a messaage..." );
		}

		assertTrue( c1.disconnect( ) );
		assertTrue( c2.disconnect( ) );

		assertTrue( c1.isClosed( ) );
		assertTrue( c2.isClosed( ) );
	}

	@Test
	public void testGetNext( ) {
		System.out.println( "Executing: " + new Throwable( ).getStackTrace( )[ 0 ].getMethodName( ) );

		if ( ALLOW_SKIP ) {
			return;
		}

		Socket client1 = null;
		try {
			client1 = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client1 );

		Communicator c1 = null;
		try {
			c1 = new Communicator( client1 );
		} catch ( IOException e ) {}

		// Ensure that the Communicator was created successfully.
		assertNotNull( c1 );

		// Ensure that communications are not closed.
		assertFalse( c1.isClosed( ) );

		Socket client2 = null;
		try {
			client2 = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client2 );

		Communicator c2 = null;
		try {
			c2 = new Communicator( client2 );
		} catch ( IOException e ) {}

		// Ensure that the Communicator was created successfully.
		assertNotNull( c2 );

		// Ensure that communications are not closed.
		assertFalse( c2.isClosed( ) );

		byte[ ] c1message = new byte[ 16 ];

		for ( int i = 0; i < c1message.length; i++ ) {
			c1message[ i ] = ( byte ) i;
		}

		try {
			Thread.sleep( 1000 );
		} catch ( InterruptedException ex1 ) {}

		for ( int j = 0; j < 100000; j++ ) {

			c1.sendMessage( c1message );

			try {
				byte[ ] messageRec = c2.getNext( 5000 );

				assertEquals( c1message.length, messageRec.length );

				for ( int i = 0; i < c1message.length; i++ ) {
					assertEquals( c1message[ i ], messageRec[ i ] );
				}

			} catch ( TimeoutException e ) {
				fail( "Timeout." );
			}

			if ( ( ( j + 1 ) % 1000 ) == 0 ) {
				System.out.println( ( j + 1 ) + "/100000 Runs Passed" );
			}

		}

		assertTrue( c1.disconnect( ) );
		assertTrue( c2.disconnect( ) );

		assertTrue( c1.isClosed( ) );
		assertTrue( c2.isClosed( ) );
	}

	@Test
	public void testGetNextTimeout( ) {
		System.out.println( "Executing: " + new Throwable( ).getStackTrace( )[ 0 ].getMethodName( ) );
		if ( ALLOW_SKIP ) {
			return;
		}

		Socket client1 = null;
		try {
			client1 = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client1 );

		Communicator c1 = null;
		try {
			c1 = new Communicator( client1 );
		} catch ( IOException e ) {}

		// Ensure that the Communicator was created successfully.
		assertNotNull( c1 );

		// Ensure that communications are not closed.
		assertFalse( c1.isClosed( ) );

		Socket client2 = null;
		try {
			client2 = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client2 );

		Communicator c2 = null;
		try {
			c2 = new Communicator( client2 );
		} catch ( IOException e ) {}

		// Ensure that the Communicator was created successfully.
		assertNotNull( c2 );

		// Ensure that communications are not closed.
		assertFalse( c2.isClosed( ) );

		byte[ ] c1message = new byte[ 16 ];

		for ( int i = 0; i < c1message.length; i++ ) {
			c1message[ i ] = ( byte ) i;
		}

		try {
			Thread.sleep( 1000 );
		} catch ( InterruptedException ex1 ) {}

		for ( int j = 0; j < 100; j++ ) {

			try {
				byte[ ] messageRec = c2.getNext( 1000 );
				fail( "Imaginary message magically found." );
			} catch ( TimeoutException e ) {
				System.out.println( "Run: " + j + "/100 - Pass" );
			}

		}

		assertTrue( c1.disconnect( ) );
		assertTrue( c2.disconnect( ) );

		assertTrue( c1.isClosed( ) );
		assertTrue( c2.isClosed( ) );
	}

	@Test
	public void testPerformance_SmallSegmentPipelined( ) {
		System.out.println( "Executing: " + new Throwable( ).getStackTrace( )[ 0 ].getMethodName( ) );
		if ( !ALLOW_SKIP ) {
			return;
		}

		Socket client1 = null;
		try {
			client1 = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client1 );

		Communicator c11 = null;
		try {
			c11 = new Communicator( client1 );
		} catch ( IOException e ) {}

		final Communicator c1 = c11;
		// Ensure that the Communicator was created successfully.
		assertNotNull( c1 );

		// Ensure that communications are not closed.
		assertFalse( c1.isClosed( ) );

		Socket client2 = null;
		try {
			client2 = new Socket( "localhost", 22000 );
		} catch ( UnknownHostException e1 ) {} catch ( IOException e1 ) {}

		// Ensure that the client is not null
		assertNotNull( client2 );

		Communicator c22 = null;
		try {
			c22 = new Communicator( client2 );
		} catch ( IOException e ) {}

		final Communicator c2 = c22;

		// Ensure that the Communicator was created successfully.
		assertNotNull( c2 );

		// Ensure that communications are not closed.
		assertFalse( c2.isClosed( ) );

		try {
			Thread.sleep( 5000 );
		} catch ( InterruptedException ex1 ) {}

		byte[ ] c1message = new byte[ 16 ];

		for ( int i = 0; i < c1message.length; i++ ) {
			c1message[ i ] = ( byte ) i;
		}

		int MAX = 20000;

		Thread sender = new Thread( ( ) -> {
			for ( int i = 0; i < MAX; i++ ) {
				long testTime = System.currentTimeMillis( );
				c1.sendMessage( c1message );
			}
		} );

		Thread receiver = new Thread( ( ) -> {

			long testTime = System.currentTimeMillis( );
			int msgCnt = 0;
			byte[ ][ ] messages;
			while ( msgCnt < MAX ) {
				while ( ( messages = c2.getMessages( ) ).length == 0 ) {
					if ( ( System.currentTimeMillis( ) - testTime ) > 5000 ) {
						fail( "Communication has likely failed. Relevant string..." );
					}
				}
				msgCnt += messages.length;
			}
		} );

		long startTime = System.currentTimeMillis( );
		receiver.start( );
		sender.start( );

		try {
			receiver.join( );
		} catch ( InterruptedException e ) {
			Thread.currentThread( ).interrupt( );
		}

		long elapsed = System.currentTimeMillis( ) - startTime;

		// Get the speed in Kb/s
		double performance = ( ( double ) ( ( c1message.length * MAX ) / elapsed ) * 1000 ) / 64.0;

		System.out.println( "Transfer speed of ~" + performance + " Kb/s." );

		assertTrue( c1.disconnect( ) );
		assertTrue( c2.disconnect( ) );

		assertTrue( c1.isClosed( ) );
		assertTrue( c2.isClosed( ) );
	}

	@After
	public void teardown( ) {
		if ( ALLOW_SKIP ) {
			return;
		}

		serv.shutDown( );
		try {
			Thread.sleep( 500 );
		} catch ( InterruptedException e ) {}
	}

}

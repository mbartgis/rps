package com.rpscore;
/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-01-29 03:17:08 -0500 (Mon, 29 Jan 2018) $
 * $Revision: 321 $
 * $Author: mbartgis $
 */

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.rpscore.lib.MathUtils;

public class TestMathUtils {

	@Test
	public void testDistanceFunction( ) {

		// Test Simple
		double actual = MathUtils.getDistance( 0, 0, 3, 4 );
		double expected = 5.0;

		assertEquals( expected, actual, 0.1 );

		// Test Negative
		actual = MathUtils.getDistance( 0, 0, -3, -4 );

		assertEquals( expected, actual, 0.1 );

		// Test Inverted
		actual = MathUtils.getDistance( 3, 4, 0, 0 );

		assertEquals( expected, actual, 0.1 );

		// Test Inverted Negative
		actual = MathUtils.getDistance( -3, -4, 0, 0 );

		assertEquals( expected, actual, 0.1 );

		// Test nonzero either
		actual = MathUtils.getDistance( -2, -3, 1, 1 );

		assertEquals( expected, actual, 0.1 );

	}

}

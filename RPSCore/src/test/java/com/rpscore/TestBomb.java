/*
 * COPYRIGHT NOTICE:
 * Copyright 2017, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date: 2018-04-19 20:51:37 -0400 (Thu, 19 Apr 2018) $
 * $Revision: 351 $
 * $Author: mbartgis $
 */

package com.rpscore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.rpscore.entities.enemies.Dub;
import com.rpscore.entities.spawned.weapons.Bomb;

public class TestBomb {

	public static final boolean IGNORE_TESTS = true;

	@Test
	public void testBombInitialization( ) {

		if ( IGNORE_TESTS ) {
			return;
		}

		Dub test_ent = new Dub( 0.0, 0.0 );

		Bomb b = new Bomb( 100, 100, 1, 10, 50, test_ent );

		assertFalse( b.isPlayerOrigin( ) );

		assertFalse( b.isBossOrigin( ) );

		assertTrue( b.isNonBossEnemyOrigin( ) );

		assertFalse( b.isExploding( ) );

		assertFalse( b.isLit( ) );

		assertFalse( b.isFinished( ) );

		assertEquals( 10, b.getFuseTime( ) );

	}

}

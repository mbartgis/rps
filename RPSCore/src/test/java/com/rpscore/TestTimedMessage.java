/*
 * COPYRIGHT NOTICE:
 * Copyright 2019, Mathew Bartgis, All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * VERSION INFORMATION:
 * $Date$
 * $Revision$
 * $Author$
 */

package com.rpscore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.rpscore.lib.TimedMessage;

public class TestTimedMessage {

	public static final String	MESSAGE	= "Hello World!";
	public static final int		TIME	= 10;

	@Test
	public void testTimedMessageCreation( ) {
		TimedMessage m = new TimedMessage( MESSAGE, TIME );
		assertEquals( TIME, m.getDuration( ) );
		assertEquals( MESSAGE, m.getMessage( ) );
	}

	@Test
	public void testTimedMessagePause( ) {
		TimedMessage m = new TimedMessage( MESSAGE, TIME );
		assertEquals( TIME, m.getDuration( ) );
		m.pause( );

		assertTrue( m.isPaused( ) );

		m.update( );
		assertEquals( TIME, m.getDuration( ) );
		m.resume( );
		m.update( );

		int expected = TIME - 1;
		assertEquals( expected, m.getDuration( ) );

		assertFalse( m.isPaused( ) );
	}

	@Test
	public void testTimedMessageFinish( ) {
		TimedMessage m = new TimedMessage( MESSAGE, TIME );

		while ( m.getDuration( ) > 0 ) {
			assertFalse( m.isFinished( ) );
			m.update( );
		}

		assertTrue( m.isFinished( ) );
		
		m.incrementTimer( );
		assertFalse( m.isFinished( ) );
		
		m.decrementTimer( );
		assertTrue( m.isFinished( ) );
		
	}
	
}
